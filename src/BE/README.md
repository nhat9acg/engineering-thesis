## Run Project
1. Tạo kết nối DB
    1. Đăng nhập vào với `authentication mode` là `Windows Authentication` va tạo database bình thường
    2. Đổi authentication mode cho phép login bằng username và password
    - [Hướng dẫn](https://learn.microsoft.com/en-us/sql/database-engine/configure-windows/change-server-authentication-mode?view=sql-server-ver16)
    3. Tạo tài khoản đăng nhập không dùng tài khoản `sa` mà dùng tài khoản riêng (xem trong `appsetting.json` để lấy `username` và `password`)
    - [Hướng dẫn](https://www.guru99.com/sql-server-create-user.html)
    4. Sau khi tạo tài khoản thì map tài khoản vừa tạo với db tạo bên trên
    - Vào `Security` -> `Logins` -> chọn tài khoản vừa tạo -> `Properties` -> `User Mapping` -> tích chọn vào db tạo bên trên
    - Login thử lại bằng username và password vừa tạo với `authentication mode` là `SQL Server Authentication`
2. Run project