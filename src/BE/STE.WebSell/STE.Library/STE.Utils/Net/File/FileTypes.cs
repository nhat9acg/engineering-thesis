﻿namespace STE.Utils.Net.File
{
    /// <summary>
    /// Extension file hình ảnh dự án
    /// </summary>
    public static class FileTypes
    {
        //Ảnh
        public const string JPG = ".jpg";
        public const string PNG = ".png";
        public const string JPEG = ".jpeg";
        public const string TIFF = ".tiff";
        public const string PDF = ".pdf";
        public const string AI = ".ai";
        public const string SVG = ".svg";

        //Video
        public const string MP4 = ".mp4";
        public const string MOV = ".mov";
    }
}
