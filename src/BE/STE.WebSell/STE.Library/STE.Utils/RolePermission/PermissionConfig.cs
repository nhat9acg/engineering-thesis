﻿using STE.Utils.RolePermission.Constant;

namespace STE.Utils.RolePermission
{
    public static class PermissionConfig
    {
        public static readonly Dictionary<string, PermissionContent> Configs = new()
        {
            { PermissionKeys.Dashboard, new(nameof(PermissionKeys.Dashboard), PermissionIcons.IconHome)},
            #region System Manager
            { PermissionKeys.SystemModule, new(nameof(PermissionKeys.SystemModule), PermissionIcons.IconUser)},
            //
            { PermissionKeys.RoleMenu, new(nameof(PermissionKeys.RoleMenu), PermissionIcons.IconDefault, PermissionKeys.SystemModule)},
            { PermissionKeys.RoleTable, new(nameof(PermissionKeys.RoleTable), PermissionIcons.IconDefault, PermissionKeys.RoleMenu) },
            { PermissionKeys.RoleCreate, new(nameof(PermissionKeys.RoleCreate), PermissionIcons.IconDefault, PermissionKeys.RoleMenu) },
            { PermissionKeys.RoleUpdate, new(nameof(PermissionKeys.RoleUpdate), PermissionIcons.IconDefault, PermissionKeys.RoleMenu) },
            { PermissionKeys.RoleDelete , new(nameof(PermissionKeys.RoleDelete), PermissionIcons.IconDefault, PermissionKeys.RoleMenu) },
            //
            { PermissionKeys.UserMenu, new(nameof(PermissionKeys.UserMenu), PermissionIcons.IconDefault, PermissionKeys.SystemModule)},
            { PermissionKeys.UserTable, new(nameof(PermissionKeys.UserTable), PermissionIcons.IconDefault, PermissionKeys.UserMenu) },
            { PermissionKeys.UserCreate, new(nameof(PermissionKeys.UserCreate), PermissionIcons.IconDefault, PermissionKeys.UserMenu) },
            { PermissionKeys.UserUpdate , new(nameof(PermissionKeys.UserUpdate), PermissionIcons.IconDefault, PermissionKeys.UserMenu) },
            { PermissionKeys.UserDelete , new(nameof(PermissionKeys.UserDelete), PermissionIcons.IconDefault, PermissionKeys.UserMenu) },
            { PermissionKeys.UserChangeStatus, new(nameof(PermissionKeys.UserChangeStatus), PermissionIcons.IconDefault, PermissionKeys.UserMenu) },

              //
            { PermissionKeys.CalendarMenu, new(nameof(PermissionKeys.CalendarMenu), PermissionIcons.IconDefault, PermissionKeys.SystemModule)},
            { PermissionKeys.CalendarTable, new(nameof(PermissionKeys.CalendarTable), PermissionIcons.IconDefault, PermissionKeys.CalendarMenu) },
            { PermissionKeys.CalendarUpdate , new(nameof(PermissionKeys.CalendarUpdate), PermissionIcons.IconDefault, PermissionKeys.CalendarMenu) },
           
            #endregion

            #region BusinessCustomerModule
            { PermissionKeys.BusinessCustomerModule, new(nameof(PermissionKeys.BusinessCustomerModule), PermissionIcons.IconUser)},
            //
            { PermissionKeys.BusinessCustomerMenu, new(nameof(PermissionKeys.BusinessCustomerMenu), PermissionIcons.IconDefault, PermissionKeys.BusinessCustomerModule)},
            { PermissionKeys.BusinessCustomerTable, new(nameof(PermissionKeys.BusinessCustomerTable), PermissionIcons.IconDefault, PermissionKeys.BusinessCustomerMenu) },
            { PermissionKeys.BusinessCustomerCreate, new(nameof(PermissionKeys.BusinessCustomerCreate), PermissionIcons.IconDefault, PermissionKeys.BusinessCustomerMenu) },
            { PermissionKeys.BusinessCustomerUpdate, new(nameof(PermissionKeys.BusinessCustomerUpdate), PermissionIcons.IconDefault, PermissionKeys.BusinessCustomerMenu) },
            { PermissionKeys.BusinessCustomerDelete , new(nameof(PermissionKeys.BusinessCustomerDelete), PermissionIcons.IconDefault, PermissionKeys.BusinessCustomerMenu) },
            //
            { PermissionKeys.ProductPriceMenu, new(nameof(PermissionKeys.ProductPriceMenu), PermissionIcons.IconDefault, PermissionKeys.BusinessCustomerModule)},
            { PermissionKeys.ProductPriceTable, new(nameof(PermissionKeys.ProductPriceTable), PermissionIcons.IconDefault, PermissionKeys.ProductPriceMenu) },
            { PermissionKeys.ProductPriceCreate, new(nameof(PermissionKeys.ProductPriceCreate), PermissionIcons.IconDefault, PermissionKeys.ProductPriceMenu) },
            { PermissionKeys.ProductPriceUpdate, new(nameof(PermissionKeys.ProductPriceUpdate), PermissionIcons.IconDefault, PermissionKeys.ProductPriceMenu) },
            { PermissionKeys.ProductPriceDelete , new(nameof(PermissionKeys.ProductPriceDelete), PermissionIcons.IconDefault, PermissionKeys.ProductPriceMenu) },
             //
            { PermissionKeys.BusinessCustomerAccountMenu, new(nameof(PermissionKeys.BusinessCustomerAccountMenu), PermissionIcons.IconDefault, PermissionKeys.BusinessCustomerModule)},
            { PermissionKeys.BusinessCustomerAccountTable, new(nameof(PermissionKeys.BusinessCustomerAccountTable), PermissionIcons.IconDefault, PermissionKeys.BusinessCustomerAccountMenu) },
            { PermissionKeys.BusinessCustomerAccountCreate, new(nameof(PermissionKeys.BusinessCustomerAccountCreate), PermissionIcons.IconDefault, PermissionKeys.BusinessCustomerAccountMenu) },
            { PermissionKeys.BusinessCustomerAccountUpdate, new(nameof(PermissionKeys.BusinessCustomerAccountUpdate), PermissionIcons.IconDefault, PermissionKeys.BusinessCustomerAccountMenu) },
            { PermissionKeys.BusinessCustomerAccountDelete , new(nameof(PermissionKeys.BusinessCustomerAccountDelete), PermissionIcons.IconDefault, PermissionKeys.BusinessCustomerAccountMenu) },
            //
            { PermissionKeys.RestaurantMenu, new(nameof(PermissionKeys.RestaurantMenu), PermissionIcons.IconDefault, PermissionKeys.BusinessCustomerModule)},
            { PermissionKeys.RestaurantTable, new(nameof(PermissionKeys.RestaurantTable), PermissionIcons.IconDefault, PermissionKeys.RestaurantMenu) },
            { PermissionKeys.RestaurantCreate, new(nameof(PermissionKeys.RestaurantCreate), PermissionIcons.IconDefault, PermissionKeys.RestaurantMenu) },
            { PermissionKeys.RestaurantUpdate , new(nameof(PermissionKeys.RestaurantUpdate), PermissionIcons.IconDefault, PermissionKeys.RestaurantMenu) },
            { PermissionKeys.RestaurantUpdateAddress , new(nameof(PermissionKeys.RestaurantUpdateAddress), PermissionIcons.IconDefault, PermissionKeys.RestaurantMenu) },
            { PermissionKeys.RestaurantDelete , new(nameof(PermissionKeys.RestaurantDelete), PermissionIcons.IconDefault, PermissionKeys.RestaurantMenu) },
              //
            { PermissionKeys.RestaurantAccountMenu, new(nameof(PermissionKeys.RestaurantAccountMenu), PermissionIcons.IconDefault, PermissionKeys.BusinessCustomerModule)},
            { PermissionKeys.RestaurantAccountTable, new(nameof(PermissionKeys.RestaurantAccountTable), PermissionIcons.IconDefault, PermissionKeys.RestaurantAccountMenu) },
            { PermissionKeys.RestaurantAccountCreate, new(nameof(PermissionKeys.RestaurantAccountCreate), PermissionIcons.IconDefault, PermissionKeys.RestaurantAccountMenu) },
            { PermissionKeys.RestaurantAccountUpdate, new(nameof(PermissionKeys.RestaurantAccountUpdate), PermissionIcons.IconDefault, PermissionKeys.RestaurantAccountMenu) },
            { PermissionKeys.RestaurantAccountDelete , new(nameof(PermissionKeys.RestaurantAccountDelete), PermissionIcons.IconDefault, PermissionKeys.RestaurantAccountMenu) },
            //
            { PermissionKeys.RestaurantDeliveryAddressMenu, new(nameof(PermissionKeys.RestaurantDeliveryAddressMenu), PermissionIcons.IconDefault, PermissionKeys.BusinessCustomerModule)},
            { PermissionKeys.RestaurantDeliveryAddressTable, new(nameof(PermissionKeys.RestaurantDeliveryAddressTable), PermissionIcons.IconDefault, PermissionKeys.RestaurantDeliveryAddressMenu) },
            { PermissionKeys.RestaurantDeliveryAddressCreate, new(nameof(PermissionKeys.RestaurantDeliveryAddressCreate), PermissionIcons.IconDefault, PermissionKeys.RestaurantDeliveryAddressMenu) },
            { PermissionKeys.RestaurantDeliveryAddressUpdate, new(nameof(PermissionKeys.RestaurantDeliveryAddressUpdate), PermissionIcons.IconDefault, PermissionKeys.RestaurantDeliveryAddressMenu) },
            { PermissionKeys.RestaurantDeliveryAddressDelete , new(nameof(PermissionKeys.RestaurantDeliveryAddressDelete), PermissionIcons.IconDefault, PermissionKeys.RestaurantDeliveryAddressMenu) },
            //
            { PermissionKeys.GroupCustomerMenu, new(nameof(PermissionKeys.GroupCustomerMenu), PermissionIcons.IconDefault, PermissionKeys.BusinessCustomerModule)},
            { PermissionKeys.GroupCustomerTable, new(nameof(PermissionKeys.GroupCustomerTable), PermissionIcons.IconDefault, PermissionKeys.GroupCustomerMenu) },
            { PermissionKeys.GroupCustomerCreate, new(nameof(PermissionKeys.GroupCustomerCreate), PermissionIcons.IconDefault, PermissionKeys.GroupCustomerMenu) },
            { PermissionKeys.GroupCustomerUpdate, new(nameof(PermissionKeys.GroupCustomerUpdate), PermissionIcons.IconDefault, PermissionKeys.GroupCustomerMenu) },
            { PermissionKeys.GroupCustomerDelete , new(nameof(PermissionKeys.GroupCustomerDelete), PermissionIcons.IconDefault, PermissionKeys.GroupCustomerMenu) },
            #endregion

            #region ProductManagementModule
            { PermissionKeys.ProductManagementModule, new(nameof(PermissionKeys.ProductManagementModule), PermissionIcons.IconUser)},
            //
            { PermissionKeys.ProductCategoryMenu, new(nameof(PermissionKeys.ProductCategoryMenu), PermissionIcons.IconDefault, PermissionKeys.ProductManagementModule)},
            { PermissionKeys.ProductCategoryTable, new(nameof(PermissionKeys.ProductCategoryTable), PermissionIcons.IconDefault, PermissionKeys.ProductCategoryMenu) },
            { PermissionKeys.ProductCategoryCreate, new(nameof(PermissionKeys.ProductCategoryCreate), PermissionIcons.IconDefault, PermissionKeys.ProductCategoryMenu) },
            { PermissionKeys.ProductCategoryUpdate, new(nameof(PermissionKeys.ProductCategoryUpdate), PermissionIcons.IconDefault, PermissionKeys.ProductCategoryMenu) },
            { PermissionKeys.ProductCategoryChangeStatus, new(nameof(PermissionKeys.ProductCategoryChangeStatus), PermissionIcons.IconDefault, PermissionKeys.ProductCategoryMenu) },
            { PermissionKeys.ProductCategoryDelete , new(nameof(PermissionKeys.ProductCategoryDelete), PermissionIcons.IconDefault, PermissionKeys.ProductCategoryMenu) },
            //
            { PermissionKeys.ProductMenu, new(nameof(PermissionKeys.ProductMenu), PermissionIcons.IconDefault, PermissionKeys.ProductManagementModule)},
            { PermissionKeys.ProductTable, new(nameof(PermissionKeys.ProductTable), PermissionIcons.IconDefault, PermissionKeys.ProductMenu) },
            { PermissionKeys.ProductCreate, new(nameof(PermissionKeys.ProductCreate), PermissionIcons.IconDefault, PermissionKeys.ProductMenu) },
            { PermissionKeys.ProductUpdate , new(nameof(PermissionKeys.ProductUpdate), PermissionIcons.IconDefault, PermissionKeys.ProductMenu) },
            { PermissionKeys.ProductChangeStatus , new(nameof(PermissionKeys.ProductChangeStatus), PermissionIcons.IconDefault, PermissionKeys.ProductMenu) },
            { PermissionKeys.ProductDelete , new(nameof(PermissionKeys.ProductDelete), PermissionIcons.IconDefault, PermissionKeys.ProductMenu) },
            //
             
            //calculation unit
            { PermissionKeys.CalculationUnitMenu, new(nameof(PermissionKeys.CalculationUnitMenu), PermissionIcons.IconDefault, PermissionKeys.ProductManagementModule)},
            { PermissionKeys.CalculationUnitTable, new(nameof(PermissionKeys.CalculationUnitTable), PermissionIcons.IconDefault, PermissionKeys.CalculationUnitMenu) },
            { PermissionKeys.CalculationUnitCreate, new(nameof(PermissionKeys.CalculationUnitCreate), PermissionIcons.IconDefault, PermissionKeys.CalculationUnitMenu) },
            { PermissionKeys.CalculationUnitUpdate, new(nameof(PermissionKeys.CalculationUnitUpdate), PermissionIcons.IconDefault, PermissionKeys.CalculationUnitMenu) },
            { PermissionKeys.CalculationUnitDelete , new(nameof(PermissionKeys.CalculationUnitDelete), PermissionIcons.IconDefault, PermissionKeys.CalculationUnitMenu) },
            #endregion

            #region Order management
            { PermissionKeys.OrderModule, new(nameof(PermissionKeys.OrderModule), PermissionIcons.IconUser)},
            //
            { PermissionKeys.OrderMenu, new(nameof(PermissionKeys.OrderMenu), PermissionIcons.IconDefault, PermissionKeys.OrderModule)},
            { PermissionKeys.OrderTable, new(nameof(PermissionKeys.OrderTable), PermissionIcons.IconDefault, PermissionKeys.OrderMenu) },
            { PermissionKeys.OrderCreate, new(nameof(PermissionKeys.OrderCreate), PermissionIcons.IconDefault, PermissionKeys.OrderMenu) },
            { PermissionKeys.OrderExportExcel, new(nameof(PermissionKeys.OrderExportExcel), PermissionIcons.IconDefault, PermissionKeys.OrderMenu) },
            { PermissionKeys.OrderDetail, new(nameof(PermissionKeys.OrderDetail), PermissionIcons.IconDefault, PermissionKeys.OrderMenu) },
            { PermissionKeys.OrderUpdate, new(nameof(PermissionKeys.OrderUpdate), PermissionIcons.IconDefault, PermissionKeys.OrderMenu) },
            { PermissionKeys.OrderShippingg, new(nameof(PermissionKeys.OrderShippingg), PermissionIcons.IconDefault, PermissionKeys.OrderMenu) },
            { PermissionKeys.OrderProcessing, new(nameof(PermissionKeys.OrderProcessing), PermissionIcons.IconDefault, PermissionKeys.OrderMenu) },
            { PermissionKeys.OrderCancel, new(nameof(PermissionKeys.OrderCancel), PermissionIcons.IconDefault, PermissionKeys.OrderMenu) },
            { PermissionKeys.OrderFeedback, new(nameof(PermissionKeys.OrderFeedback), PermissionIcons.IconDefault, PermissionKeys.OrderMenu) },
            { PermissionKeys.OrderDelete , new(nameof(PermissionKeys.OrderDelete), PermissionIcons.IconDefault, PermissionKeys.OrderMenu) },
            
           
            #endregion
        };
    }
}
