﻿namespace STE.Utils.RolePermission.Constant
{
    /// <summary>
    /// PermissionKey
    /// </summary>
    public static class PermissionKeys
    {
        #region các loại permission
        private const string Web = "web_";
        private const string Menu = "menu_";
        private const string Tab = "tab_";
        private const string Page = "page_";
        private const string Table = "table_";
        private const string Form = "form_";
        private const string ButtonTable = "btn_table_";
        private const string ButtonForm = "btn_form_";
        private const string ButtonAction = "btn_action_";
        #endregion

        public const string Dashboard = $"{Page}dashboad";

        #region System Manager
        public const string SystemModule = $"system_";
        //role
        public const string RoleMenu = $"{SystemModule}{Menu}role_menu";
        public const string RoleTable = $"{SystemModule}{Table}role_menu_danh_sach";
        public const string RoleCreate = $"{SystemModule}{ButtonAction}role_menu_them_moi";
        public const string RoleUpdate = $"{SystemModule}{ButtonAction}role_menu_cap_nhat";
        public const string RoleDelete = $"{SystemModule}{ButtonAction}role_menu_xoa";
        //account
        public const string UserMenu = $"{SystemModule}{Menu}use_menu";
        public const string UserTable = $"{SystemModule}{Table}use_menu_danh_sach";
        public const string UserCreate = $"{SystemModule}{ButtonAction}use_menu_them_moi";
        public const string UserUpdate = $"{SystemModule}{ButtonAction}use_menu_cap_nhat";
        public const string UserDelete = $"{SystemModule}{ButtonAction}use_menu_xoa";
        public const string UserChangeStatus = $"{SystemModule}{ButtonAction}kich_hoat_or_huy";
        //calendar
        public const string CalendarMenu = $"{SystemModule}{Menu}calendar_menu";
        public const string CalendarTable = $"{SystemModule}{Table}calendar_menu_danh_sach";
        public const string CalendarUpdate = $"{SystemModule}{ButtonAction}calendar_menu_cap_nhat";
        #endregion

        #region Business Customer Manager
        public const string BusinessCustomerModule = $"business_customer_module";
        //
        public const string BusinessCustomerMenu = $"{BusinessCustomerModule}{Menu}business_customer_menu";
        public const string BusinessCustomerTable = $"{BusinessCustomerModule}{Table}business_customer_menu_danh_sach";
        public const string BusinessCustomerCreate = $"{BusinessCustomerModule}{ButtonAction}business_customer_menu_them_moi";
        public const string BusinessCustomerUpdate = $"{BusinessCustomerModule}{ButtonAction}business_customer_menu_cap_nhat";
        public const string BusinessCustomerDelete = $"{BusinessCustomerModule}{ButtonAction}business_customer_menu_xoa";

        //price
        public const string ProductPriceMenu = $"{BusinessCustomerModule}{Menu}product_price_menu";
        public const string ProductPriceTable = $"{BusinessCustomerModule}{Table}product_price_menu_danh_sach";
        public const string ProductPriceCreate = $"{BusinessCustomerModule}{ButtonAction}product_price_menu_them_moi";
        public const string ProductPriceUpdate = $"{BusinessCustomerModule}{ButtonAction}product_price_menu_cap_nhat";
        public const string ProductPriceDelete = $"{BusinessCustomerModule}{ButtonAction}product_price_menu_xoa";
        //account BusinessCustomer
        public const string BusinessCustomerAccountMenu = $"{BusinessCustomerModule}{Menu}business_custome_account";
        public const string BusinessCustomerAccountTable = $"{BusinessCustomerModule}{Table}business_custome_account_danh_sach";
        public const string BusinessCustomerAccountCreate = $"{BusinessCustomerModule}{ButtonAction}business_custome_account_them_moi";
        public const string BusinessCustomerAccountUpdate = $"{BusinessCustomerModule}{ButtonAction}business_custome_account_cap_nhat";
        public const string BusinessCustomerAccountDelete = $"{BusinessCustomerModule}{ButtonAction}business_custome_account_xoa";
        public const string BusinessCustomerAccountChangeStatus = $"{BusinessCustomerModule}{ButtonAction}kich_hoat_or_huy";

        //
        public const string RestaurantMenu = $"{BusinessCustomerModule}{Menu}restaurant_menu";
        public const string RestaurantTable = $"{BusinessCustomerModule}{Table}restaurant_menu_menu_danh_sach";
        public const string RestaurantCreate = $"{BusinessCustomerModule}{ButtonAction}restaurant_menu_them_moi";
        public const string RestaurantUpdate = $"{BusinessCustomerModule}{ButtonAction}restaurant_menu_cap_nhat";
        public const string RestaurantUpdateAddress = $"{BusinessCustomerModule}{ButtonAction}restaurant_menu_cap_nhat_address";
        public const string RestaurantDelete = $"{BusinessCustomerModule}{ButtonAction}restaurant_menu_xoa";
        //account BusinessCustomer
        public const string RestaurantAccountMenu = $"{BusinessCustomerModule}{Menu}restaurant_account";
        public const string RestaurantAccountTable = $"{BusinessCustomerModule}{Table}restaurant_account_danh_sach";
        public const string RestaurantAccountCreate = $"{BusinessCustomerModule}{ButtonAction}restaurant_account_them_moi";
        public const string RestaurantAccountUpdate = $"{BusinessCustomerModule}{ButtonAction}restaurant_account_cap_nhat";
        public const string RestaurantAccountDelete = $"{BusinessCustomerModule}{ButtonAction}restaurant_account_xoa";
        public const string RestaurantAccountChangeStatus = $"{BusinessCustomerModule}{ButtonAction}kich_hoat_or_huy";
        //
        public const string RestaurantDeliveryAddressMenu = $"{BusinessCustomerModule}{Menu}restaurant_delivery_address";
        public const string RestaurantDeliveryAddressTable = $"{BusinessCustomerModule}{Table}restaurant_delivery_address_danh_sach";
        public const string RestaurantDeliveryAddressCreate = $"{BusinessCustomerModule}{ButtonAction}restaurant_delivery_address_them_moi";
        public const string RestaurantDeliveryAddressUpdate = $"{BusinessCustomerModule}{ButtonAction}restaurant_delivery_address_cap_nhat";
        public const string RestaurantDeliveryAddressDelete = $"{BusinessCustomerModule}{ButtonAction}restaurant_delivery_address_xoa";
        //
        public const string GroupCustomerMenu = $"{BusinessCustomerModule}{Menu}group_customer_menu";
        public const string GroupCustomerTable = $"{BusinessCustomerModule}{Table}group_customer_menu_danh_sach";
        public const string GroupCustomerCreate = $"{BusinessCustomerModule}{ButtonAction}group_customer_menu_them_moi";
        public const string GroupCustomerUpdate = $"{BusinessCustomerModule}{ButtonAction}group_customer_menu_cap_nhat";
        public const string GroupCustomerDelete = $"{BusinessCustomerModule}{ButtonAction}group_customer_menu_xoa";
        #endregion

        #region Product Management
        public const string ProductManagementModule = $"product_management_module";

        //
        public const string ProductCategoryMenu = $"{ProductManagementModule}{Menu}product_category_menu";
        public const string ProductCategoryTable = $"{ProductManagementModule}{Table}product_category_menu_danh_sach";
        public const string ProductCategoryCreate = $"{ProductManagementModule}{ButtonAction}product_category_menu_them_moi";
        public const string ProductCategoryUpdate = $"{ProductManagementModule}{ButtonAction}product_category_menu_cap_nhat";
        public const string ProductCategoryChangeStatus = $"{ProductManagementModule}{ButtonAction}product_category_menu_change_status";
        public const string ProductCategoryDelete = $"{ProductManagementModule}{ButtonAction}product_category_menu_xoa";
        //
        public const string ProductMenu = $"{ProductManagementModule}{Menu}product_menu";
        public const string ProductTable = $"{ProductManagementModule}{Table}product_menu_danh_sach";
        public const string ProductCreate = $"{ProductManagementModule}{ButtonAction}product_menu_them_moi";
        public const string ProductUpdate = $"{ProductManagementModule}{ButtonAction}product_menu_cap_nhat";
        public const string ProductChangeStatus = $"{ProductManagementModule}{ButtonAction}product_menu_change_status";
        public const string ProductDelete = $"{ProductManagementModule}{ButtonAction}product_menu_xoa";
        //
        public const string CalculationUnitMenu = $"{ProductManagementModule}{Menu}calculation_unit_menu";
        public const string CalculationUnitTable = $"{ProductManagementModule}{Table}calculation_unit_menu_danh_sach";
        public const string CalculationUnitCreate = $"{ProductManagementModule}{ButtonAction}calculation_unit_menu_them_moi";
        public const string CalculationUnitUpdate = $"{ProductManagementModule}{ButtonAction}calculation_unit_menu_cap_nhat";
        public const string CalculationUnitDelete = $"{ProductManagementModule}{ButtonAction}calculation_unit_menu_xoa";
        #endregion
        #region Order management
        public const string OrderModule = $"order_";
        //
        public const string OrderMenu = $"{SystemModule}{Menu}order_menu";
        public const string OrderTable = $"{SystemModule}{Table}order_menu_danh_sach";
        public const string OrderCreate = $"{SystemModule}{ButtonAction}order_menu_them_moi";
        public const string OrderExportExcel = $"{SystemModule}{ButtonAction}order_menu_export_excel";
        public const string OrderDetail = $"{SystemModule}{ButtonAction}order_menu_detail";
        public const string OrderUpdate = $"{SystemModule}{ButtonAction}order_menu_cap_nhat";
        public const string OrderShippingg = $"{SystemModule}{ButtonAction}order_menu_shipping";
        public const string OrderProcessing = $"{SystemModule}{ButtonAction}order_menu_processing";
        public const string OrderCancel = $"{SystemModule}{ButtonAction}order_menu_cancel";
        public const string OrderFeedback = $"{SystemModule}{ButtonAction}order_menu_feedback";
        public const string OrderDelete = $"{SystemModule}{ButtonAction}order_menu_xoa";

        #endregion
    }
}
