﻿namespace STE.Utils
{
    public class UserClaimTypes
    {
        public const string UserType = "user_type";
        public const string Username = "username";
        public const string UserId = "user_id";
        public const string BusinessCustomerId = "business_customer_id";
        public const string RestaurantId = "restaurent_id";
    }
}
