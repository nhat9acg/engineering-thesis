﻿namespace STE.Utils.ConstantVariables.Document
{
    /// <summary>
    /// Kiểu gửi tài liệu
    /// </summary>
    public static class DocumentSendTypes
    {
        /// <summary>
        /// Gửi cho tất cả khách hàng
        /// </summary>
        public const int ALL_CUSTOMER = 1;

        /// <summary>
        /// Gửi cho một vài khách hàng (nhóm hoặc 1 khách)
        /// </summary>
        public const int SOME_CUSTOMER = 2;
    }
}
