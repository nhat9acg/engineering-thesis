﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.Utils.ConstantVariables.Shared
{
    public static class ActiveStatus 
    {
        public const int ACTIVE = 1;
        public const int DEACTIVE = 2;
    }
}
