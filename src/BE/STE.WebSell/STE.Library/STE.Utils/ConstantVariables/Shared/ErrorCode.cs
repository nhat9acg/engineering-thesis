﻿namespace STE.Utils.ConstantVariables.Shared
{
    public enum ErrorCode
    {
        System = 1,
        BadRequest = 400,
        NotFound = 404,
        InternalServerError = 500,

        //user 1xxx
        UsernameOrPasswordIncorrect = 1000,
        UserNotFound = 1001,
        RoleNotFound = 1002,
        UserIsDeactive = 1003,
        InvalidUserType = 1004,
        UserOldPasswordIncorrect = 1005,

        //customer manager 2xxx
        BusinessCustomerNotFound = 2000,
        RestaurantNotFound = 2001,
        DeliveryAddressNotFound = 2002,
        GroupCustomerNotFound = 2003,
        BusinessCustomerIsBeingUsed = 2004,

        //Image && File 3xxx
        FileNotFound = 3000,

        //product manager 4xxx
        ProductCategoryNotFound = 4000,
        ParentProductCategoryNotFound = 4001,
        ProductNotFound = 4002,
        ProductCategoryIsBeingUsedByProduct = 4003,
        ProductCategoryIsBeingUsedAsParent = 4004,
        ProductPriceNotFound = 4005,
        CalculationUnitNotFound = 4006,

        #region Order 5xxx
        //Order
        OrderNotFound = 5000,
        OrderDetailNotFound = 5001,
        OrderDetailIsNotEmpty = 5002,
        OrderCannotCancel = 5003,
        OrderCannotOnDayOff = 5004,
        OrderCannotDelete = 5005,
        #endregion

        //ShippingProvider 6xxx
        ShippingProviderNotFound = 6000,
        
        //Document 7xxx
        DocumentNotFound = 7000,

        //cart 10000
        CartNotFound = 10000,
        //notification  11000
        SystemNotificationNotFound = 11000,
        NotificationNotFound = 11001,
    }
}
