﻿namespace STE.Utils.ConstantVariables.User
{
    public class UserTypes
    {
        public const int ADMIN = 1;
        public const int BUSINESS_CUSTOMER = 2;
        public const int RESTAURANT = 3;
    }
}
