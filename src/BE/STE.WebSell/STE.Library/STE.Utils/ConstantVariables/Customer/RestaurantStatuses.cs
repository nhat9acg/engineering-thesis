﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.Utils.ConstantVariables.Customer
{
    public static class RestaurantStatuses
    {
        public const int HOAT_DONG = 1;
        public const int DONG_CUA = 2;
    }
}
