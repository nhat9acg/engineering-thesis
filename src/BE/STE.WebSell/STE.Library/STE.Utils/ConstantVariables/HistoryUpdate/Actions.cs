﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.Utils.ConstantVariables.HistoryUpdate
{
    public static class Actions
    {
        /// <summary>
        /// Thêm
        /// </summary>
        public const int ADD = 1;
        /// <summary>
        /// Sửa
        /// </summary>
        public const int UPDATE = 2;
        /// <summary>
        /// Xoá
        /// </summary>
        public const int DELETE = 3; 
        /// <summary>
        /// Action Phản hồi
        /// </summary>
        public const int RESPONSE = 4; 
    }
}
