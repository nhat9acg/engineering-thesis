﻿namespace STE.Utils.ConstantVariables.Order
{
    /// <summary>
    /// Trạng thái đơn hàng
    /// </summary>
    public static class OrderStatus
    {
        /// <summary>
        /// Tạo mới
        /// </summary>
        public const int NEW = 1;
        /// <summary>
        /// Đang xử lý
        /// </summary>
        public const int PROCESSING = 2;
        /// <summary>
        /// Hoàn thành đơn hàng
        /// </summary>
        public const int COMPLETED = 3;
        /// <summary>
        /// Huỷ đơn hàng
        /// </summary>
        public const int CANCEL = 4;
    }
}
