﻿namespace STE.Utils.Localization
{
    public static class LocalizationNames
    {
        public const string English = "en";
        public const string Japan = "jp";

        public static readonly string[] All = new string[]
        {
            English, Japan,
        };
    }
}
