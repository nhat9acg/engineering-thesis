﻿using Microsoft.EntityFrameworkCore;

namespace STE.UnitTestsBase
{
    public static class TestHelper
    {
        public static TDbContext CreateDbContext<TDbContext>() where TDbContext : DbContext, new()
        {
            var builder = new DbContextOptionsBuilder<TDbContext>();
            builder.UseInMemoryDatabase(databaseName: "DbInMemory");
            //var dbContextOptions = builder.Options;
            var dbContext = new TDbContext();
            // Delete existing db before creating a new one
            dbContext.Database.EnsureDeleted();
            dbContext.Database.EnsureCreated();
            return dbContext;
        }
    }
}
