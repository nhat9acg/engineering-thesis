﻿using STE.EntitiesBase.Base;
using STE.EntitiesBase.Interfaces;

namespace STE.EntitiesBase.Entities
{
    public interface IRolePermission<TRoleId> : IEntity<int>, ICreatedBy
    {
        TRoleId RoleId { get; set; }
        string PermissionKey { get; set; }
    }
}
