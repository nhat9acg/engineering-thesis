﻿using STE.EntitiesBase.Base;
using STE.EntitiesBase.Interfaces;

namespace STE.EntitiesBase.Entities
{
    public interface IUserRole<TUserId, TRoleId> : IEntity<int>, IFullAudited
    {
        TUserId UserId { get; set; }
        TRoleId RoleId { get; set; }
    }
}
