﻿using STE.EntitiesBase.Base;
using STE.EntitiesBase.Interfaces;

namespace STE.EntitiesBase.Entities
{
    public interface IRole : IEntity<int>, IFullAudited
    {
        string Name { get; set; }
        string Description { get; set; }
    }
}
