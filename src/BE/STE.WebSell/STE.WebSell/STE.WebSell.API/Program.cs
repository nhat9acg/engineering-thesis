using EPIC.Utils.Security;
using STE.IdentityServerBase.StartUp;
using STE.Utils;
using STE.Utils.Localization;
using STE.WebAPIBase;
using STE.WebSell.Application.AuthenticationModule.Abstract;
using STE.WebSell.Application.AuthenticationModule.Implements;
using STE.WebSell.Application.CartModule.Abstract;
using STE.WebSell.Application.CartModule.Implements;
using STE.WebSell.Application.ChatModule.Abstract;
using STE.WebSell.Application.ChatModule.Implements;
using STE.WebSell.Application.Common.Localization;
using STE.WebSell.Application.ConfigurationModule.Abstract;
using STE.WebSell.Application.ConfigurationModule.Implements;
using STE.WebSell.Application.CustomerManagerModule.Abstract;
using STE.WebSell.Application.CustomerManagerModule.Implement;
using STE.WebSell.Application.DashboardModule.Abstract;
using STE.WebSell.Application.DashboardModule.Implement;
using STE.WebSell.Application.DemoModule.Abstract;
using STE.WebSell.Application.DemoModule.Implements;
using STE.WebSell.Application.DocumentModule.Abstract;
using STE.WebSell.Application.DocumentModule.Implements;
using STE.WebSell.Application.ExportExcelReportModule.Abstract;
using STE.WebSell.Application.ExportExcelReportModule.Implements;
using STE.WebSell.Application.FileModule.Abstract;
using STE.WebSell.Application.FileModule.Implements;
using STE.WebSell.Application.NotificationModule.Abstract;
using STE.WebSell.Application.NotificationModule.Implements;
using STE.WebSell.Application.OrderModule.Abstract;
using STE.WebSell.Application.OrderModule.Implements;
using STE.WebSell.Application.ProductModule.Abstract;
using STE.WebSell.Application.ProductModule.Implements;
using STE.WebSell.Application.ShippingProviderModule.Abstract;
using STE.WebSell.Application.ShippingProviderModule.Implements;
using STE.WebSell.Application.SignalR;
using STE.WebSell.Domain.Entities;
using STE.WebSell.Infrastructure.Persistence;
using STE.WebSellAPI.Middleware;
using System.Configuration;

namespace STE.WebSellAPI
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);
            builder.ConfigureCors();
            builder.ConfigureServices(isIdentityServer: true);
            builder.Services.AddCommonIdentityServer<WebSellDbContext>(builder.Configuration);

            builder.Services.AddSingleton<LocalizationBase, JVFLocalization>();
            builder.Services.AddSingleton<MapErrorCodeBase>();

            var emailConfig = builder.Configuration.GetSection("EmailConfiguration").Get<EmailConfiguration>();
            builder.Services.AddSingleton(emailConfig);

            var vnPayConfig = builder.Configuration.GetSection("VNPaySettings").Get<VNPaySettings>();
            builder.Services.AddSingleton(vnPayConfig);

            builder.Services.AddScoped<IConfigurationService, ConfigurationService>();
            builder.Services.AddScoped<IValueService, ValueService>();
            builder.Services.AddScoped<IUserService, UserService>();
            builder.Services.AddScoped<IUserBusinessCustomerService, UserBusinessCustomerService>();
            builder.Services.AddScoped<IUserRestaurantService, UserRestaurantService>();
            builder.Services.AddScoped<IUserCustomerService, UserCustomerService>();
            builder.Services.AddScoped<IFileService, FileService>();
            builder.Services.AddScoped<IBusinessCustomerService, BusinessCustomerService>();
            builder.Services.AddScoped<IRestaurantService, RestaurantService>();
            builder.Services.AddScoped<IDeliveryAddressService, DeliveryAddressService>();
            builder.Services.AddScoped<IDeliveryAddressCustomerService, DeliveryAddressCustomerService>();
            builder.Services.AddScoped<IPermissionService, PermissionService>();
            builder.Services.AddScoped<IGroupCustomerService, GroupCustomerService>();
            builder.Services.AddScoped<IProductCategoryService, ProductCategoryService>();
            builder.Services.AddScoped<IProductService, ProductService>();
            builder.Services.AddScoped<IRoleService, RoleService>();
            builder.Services.AddScoped<IOrderService, OrderService>();
            builder.Services.AddScoped<IBusinessCustomerOrderService, BusinessCustomerOrderService>();
            builder.Services.AddScoped<IRestaurantOrderService, RestaurantOrderService>();
            builder.Services.AddScoped<ICalculationUnitService, CalculationUnitService>();
            builder.Services.AddScoped<IProductPriceService, ProductPriceService>();
            builder.Services.AddScoped<IShippingProviderService, ShippingProviderService>();
            builder.Services.AddScoped<IDayOffService, DayOffService>();
            builder.Services.AddScoped<IDocumentService, DocumentService>();
            builder.Services.AddScoped<IDocumentBusinessCustomerService, DocumentBusinessCustomerService>();
            builder.Services.AddScoped<ICartService, CartService>();
            builder.Services.AddScoped<ICustomerOrderService, CustomerOrderService>();
            builder.Services.AddScoped<IEmailService, EmailService>();
            builder.Services.AddScoped<ISystemNotificationService, SystemNotificationService>();
            builder.Services.AddScoped<IChatService, ChatService>();
            builder.Services.AddScoped<ISignalRBroadcastService, SignalRBroadcastService>();
            builder.Services.AddScoped<IExportExcelReportService, ExportExcelReportService>();
            builder.Services.AddScoped<IDashboardService, DashboardService>();

            //builder.Services.AddSignalR();
            var app = builder.Build();

            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.Configure();
            app.UseCheckUser();
            app.ConfigureEndpoint();

            app.MapHub<WebSellHub>("/hub/webSell");
            app.Run();

            //app.TestResolveService(builder.Services);
        }
    }
}