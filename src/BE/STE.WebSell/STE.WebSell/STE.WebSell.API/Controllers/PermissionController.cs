﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;
using STE.Utils;
using STE.Utils.RolePermission;
using STE.WebAPIBase.Controllers;
using STE.WebSell.Application.AuthenticationModule.Abstract;
using STE.WebSell.Application.AuthenticationModule.Dtos;
using STE.WebSell.Application.AuthenticationModule.Dtos.PermissionDto;
using STE.WebSell.Application.AuthenticationModule.Implements;
using STE.WebSell.Domain.Entities;
using System.Net;

namespace STE.WebSell.API.Controllers
{
    [Authorize]
    [Route("api/permission")]
    [ApiController]
    public class PermissionController : ApiControllerBase
    {
        private readonly IPermissionService _permissionService;

        public PermissionController(IPermissionService permissionService)
        {
            _permissionService = permissionService;
        }
        /// <summary>
        /// Xem danh sách người dùng
        /// </summary>
        /// <returns></returns>
        [HttpGet("find-all")]
        public APIResponse<List<PermissionDetailDto>> FindAll()
            => new(_permissionService.FindAllPermission());

        /// <summary>
        /// Lấy permissionKey của người dùng hiện tại
        /// </summary>
        /// <returns></returns>
        [HttpGet("get-permissions")]
        public APIResponse<List<string>> GetAllPermission()
            => new(_permissionService.GetPermission());
    }
}
