﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;
using STE.Utils;
using STE.Utils.ConstantVariables.User;
using STE.Utils.RolePermission.Constant;
using STE.WebAPIBase.Controllers;
using STE.WebAPIBase.Filters;
using STE.WebSell.Application.AuthenticationModule.Abstract;
using STE.WebSell.Application.AuthenticationModule.Dtos.UserDto;
using STE.WebSell.Application.CustomerManagerModule.Abstract;
using STE.WebSell.Application.CustomerManagerModule.Dtos.DeliveryAddress;
using STE.WebSell.Domain.Entities;
using System.Net;

namespace STE.WebSell.API.Controllers
{
    [Authorize]
    [Route("api/user")]
    [ApiController]
    public class UsersController : ApiControllerBase
    {
        private readonly IUserService _userService;
        private readonly IUserBusinessCustomerService _userBusinessCustomerService;
        private readonly IUserRestaurantService _userRestaurantService;
        private readonly IUserCustomerService _userCustomerService;
        private readonly IDeliveryAddressCustomerService _deliveryAddressCustomerService;

        public UsersController(IUserService userService,
                               IUserBusinessCustomerService userBusinessCustomerService,
                               IUserRestaurantService userRestaurantService,
                               IDeliveryAddressCustomerService deliveryAddressCustomerService,
                               IUserCustomerService userCustomerService)
        {
            _userService = userService;
            _userBusinessCustomerService = userBusinessCustomerService;
            _userRestaurantService = userRestaurantService;
            _deliveryAddressCustomerService = deliveryAddressCustomerService;
            _userCustomerService = userCustomerService;
        }

        /// <summary>
        /// register customer
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("register")]
        public APIResponse Register([FromBody] CreateUserDto input)
        {
            _userCustomerService.CreateUser(input);
            return new();
        }

        /// <summary>
        /// Xem danh sách người dùng
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("find-all")]
        //[PermissionFilter(PermissionKeys.UserTable)]
        public APIResponse<PagingResult<UserDto>> FindAll([FromQuery] FilterUserDto input)
            => new(_userService.FindAll(input));

        /// <summary>
        /// Tìm kiếm người dùng theo Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(APIResponse<User>), (int)HttpStatusCode.OK)]
        public APIResponse FindById(int id)
            => new(_userService.FindById(id));

        /// <summary>
        /// Thêm User
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("add")]
        //[PermissionFilter(PermissionKeys.UserCreate)]
        public APIResponse Add([FromBody] CreateUserDto input)
        {                
            _userService.CreateUser(input);
            return new();
        }

        /// <summary>
        /// Thêm User doanh nghiệp
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("add-by-business")]
        public APIResponse AddByBusiness([FromBody] CreateUserDto input)
        {
            _userBusinessCustomerService.CreateUser(input);
            return new();
        }

        /// <summary>
        /// Thêm User restaurant
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("add-by-restaurant")]

        public APIResponse AddByRestaurant([FromBody] CreateUserDto input)
        {
            _userRestaurantService.CreateUser(input);
            return new();
        }

        /// <summary>
        /// Cập nhật User
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut("update")]
        //[PermissionFilter(PermissionKeys.UserUpdate)]
        public APIResponse Update([FromBody] UpdateUserDto input)
        {
            _userService.Update(input);
            return new();
        }

        [HttpPut("update-by-customer")]
        //[PermissionFilter(PermissionKeys.UserUpdate)]
        public APIResponse UpdateByCustomer([FromBody] UpdateUserDto input)
        {
            _userCustomerService.Update(input);
            return new();
        }

        /// <summary>
        /// Xóa User
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("delete/{id}")]
        //[PermissionFilter(PermissionKeys.UserDelete)]
        public APIResponse Delete(int id)
        {
            _userService.Delete(id);
            return new();
        }

        /// <summary>
        /// Thay đổi trạng thái tai khoản
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("change-status/{id}")]
        //[PermissionFilter(PermissionKeys.UserChangeStatus)]
        public APIResponse ChangeStatus(int id)
        {
            _userService.ChangeStatus(id);
            return new();
        }

        /// <summary>
        /// Tự thay đổi mật khẩu
        /// </summary>
        /// <returns></returns>
        [HttpPut("change-password")]
        public APIResponse ChangePassword(ChangePasswordDto input)
        {
            _userService.ChangePassword(input);
            return new();
        }

        [HttpPut("set-password")]
        public APIResponse SetPassword(SetPasswordUserDto input)
        {
            _userService.SetPassword(input);
            return new();
        }

        #region Delivery Address
        /// <summary>
        /// Thêm địa chỉ nhà hàng
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("delivery-address/create")]
        public APIResponse<DeliveryAddressDto> CreateDeliveryAddress(CreateDeliveryAddressDto input)
               => new(_deliveryAddressCustomerService.Create(input));

        /// <summary>
        /// Danh sách 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("delivery-address/find-all")]
        public APIResponse<PagingResult<DeliveryAddressDto>> FindAllDeliveryAddress([FromQuery] FilterDeliveryAddressDto input)
                => new(_deliveryAddressCustomerService.FindAllDeliveryAddress(input));

        /// <summary>
        /// Lấy thông tin chi tiết địa chỉ giao hàng
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("delivery-address/find-by-user/{id}")]
        public APIResponse<IEnumerable<DeliveryAddressDto>> FindByUser(int id)
                => new(_deliveryAddressCustomerService.FindByUser(id));

        /// <summary>
        /// Cập nhập thông tin địa chỉ giao hàng
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut("delivery-address/update")]
        public APIResponse UpdateDeliveryAddress(UpdateDeliveryAddressDto input)
        {
            _deliveryAddressCustomerService.Update(input);
            return new();
        }

        /// <summary>
        /// Xóa thông tin địa chỉ giao hàng
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("delivery-address/delete/{id}")]
        public APIResponse DeleteDeliveryAddress(int id)
        {
            _deliveryAddressCustomerService.Delete(id);
            return new();
        }
        #endregion
    }
}
