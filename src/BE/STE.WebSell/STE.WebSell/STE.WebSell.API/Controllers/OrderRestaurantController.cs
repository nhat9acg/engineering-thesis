﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;
using STE.Utils;
using STE.WebAPIBase.Controllers;
using STE.WebSell.Application.OrderModule.Abstract;
using STE.WebSell.Application.OrderModule.Dtos.Order;
using STE.WebSell.Application.OrderModule.Dtos.OrderRestaurant;

namespace STE.WebSell.API.Controllers
{
    [Authorize]
    [Route("api/order/restaurant")]
    [ApiController]
    public class OrderRestaurantController : ApiControllerBase
    {
        private readonly IRestaurantOrderService _restaurantOrderService;

        public OrderRestaurantController(IRestaurantOrderService restaurantOrderService)
        {
            _restaurantOrderService = restaurantOrderService;
        }

        /// <summary>
        /// Lấy danh sách order theo nhà hàng
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("find-all")]
        public APIResponse<PagingResult<OrderDto>> FindAll([FromQuery] FilterOrderRestaurantDto input)
            => new(_restaurantOrderService.FindAll(input));

        /// <summary>
        /// Tìm theo Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public APIResponse<OrderDto> FindById(int id)
            => new(_restaurantOrderService.FindById(id));

        /// <summary>
        /// Thêm đơn
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("add")]
        public APIResponse<OrderDto> Add([FromBody] CreateOrderRestaurantDto input)
            => new(_restaurantOrderService.Create(input));

        /// <summary>
        /// Cập nhật đơn
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut("update")]
        public APIResponse<OrderDto> Update([FromBody] UpdateOrderDto input)
            => new(_restaurantOrderService.Update(input));

        /// <summary>
        /// Hủy đơn hàng
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut("cancel-order")]
        public APIResponse CalcelOrder([FromBody] CancelOrResponseOrderDto input)
        {
            _restaurantOrderService.CancelOrder(input);
            return new();
        }

        /// <summary>
        /// Phản hồi đơn hàng
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut("response-order")]
        public APIResponse ResponseOrder([FromBody] CancelOrResponseOrderDto input)
        {
            _restaurantOrderService.ResponseOrder(input);
            return new();
        }
    }
}
