﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;
using STE.Utils;
using STE.WebAPIBase.Controllers;
using STE.WebSell.Application.ShippingProviderModule.Abstract;
using STE.WebSell.Application.ShippingProviderModule.Dtos;

namespace STE.WebSell.API.Controllers
{
    [Authorize]
    [Route("api/shipping-provider")]
    [ApiController]
    public class ShippingProviderController : ApiControllerBase
    {
        private readonly IShippingProviderService _service;

        public ShippingProviderController(IShippingProviderService service)
        {
            _service = service;
        }

        /// <summary>
        /// Lấy danh sách đơn vị vận chuyển
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("find-all")]
        public APIResponse<PagingResult<ShippingProviderDto>> FindAll([FromQuery] FilterShippingProviderDto input)
            => new(_service.FindAll(input));

        /// <summary>
        /// Tìm theo Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public APIResponse<ShippingProviderDto> FindById(int id)
            => new(_service.FindById(id));

        /// <summary>
        /// Thêm đơn vị vận chuyển
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("add")]
        public APIResponse<ShippingProviderDto> Add([FromBody] CreateShippingProviderDto input)
            => new(_service.Create(input));

        /// <summary>
        /// Cập nhật đơn vị vận chuyển
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut("update")]
        public APIResponse<ShippingProviderDto> Update([FromBody] UpdateShippingProviderDto input)
            => new(_service.Update(input));

        /// <summary>
        /// xóa đơn vị vận chuyển
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("delete/{id}")]
        public APIResponse Delete(int id)
        {
            _service.Delete(id);
            return new();
        }

        /// <summary>
        /// Cập nhật trạng thái đơn vị vận chuyển
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("change-status/{id}")]
        public APIResponse ChangeStatus(int id)
        {
            _service.ChangeStatus(id);
            return new();
        }
    }
}
