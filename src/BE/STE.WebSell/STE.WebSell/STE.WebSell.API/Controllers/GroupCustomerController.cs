﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;
using STE.Utils;
using STE.WebAPIBase.Controllers;
using STE.WebSell.Application.CustomerManagerModule.Abstract;
using STE.WebSell.Application.CustomerManagerModule.Dtos.Group;
using STE.WebSell.Application.CustomerManagerModule.Dtos.GroupCustomerBusinessCustomer;

namespace STE.WebSell.API.Controllers
{
    //[Authorize]
    [Route("api/group-customer")]
    [ApiController]
    public class GroupCustomerController : ApiControllerBase
    {
        private readonly IGroupCustomerService _groupCustomerService;
        public GroupCustomerController(IGroupCustomerService groupCustomerService)
        {
            _groupCustomerService = groupCustomerService;
        }

        #region Group customer
        /// <summary>
        /// Cập nhập nhóm khách hàng
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut("update")]
        public APIResponse UpdateGroupCustomer(UpdateGroupCustomerDto input)
        {
            _groupCustomerService.Update(input);
            return new();
        }

        /// <summary>
        /// Thêm nhóm khách hàng
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public APIResponse<GroupCustomerDto> CreateGroupCustomer(CreateGroupCustomerDto input)
               => new(_groupCustomerService.Create(input));

        /// <summary>
        /// Tìm kiếm nhóm khách hàng theo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("find/{id}")]
        public APIResponse FindByIdGroupCustomer(int id)
               => new(_groupCustomerService.FindById(id));

        /// <summary>
        /// Tìm kiếm tất cả nhóm khách hàng
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("find")]
        public APIResponse<PagingResult<GroupCustomerDto>> FindAllGroupCustomer([FromQuery] FilterGroupCustomerDto input)
               => new(_groupCustomerService.FindAll(input));

        /// <summary>
        /// Xóa nhóm khách hàng
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("delete/{id}")]
        public APIResponse DeleteGroupCustomer(int id)
        {
            _groupCustomerService.Delete(id);
            return new();
        }
        #endregion
    }
}
