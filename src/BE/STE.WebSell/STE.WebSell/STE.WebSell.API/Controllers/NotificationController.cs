﻿

using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MimeKit;
using MimeKit.Text;
using Polly;
using STE.ApplicationBase.Common;
using STE.Utils;
using STE.WebAPIBase.Controllers;
using STE.WebSell.Application.NotificationModule.Abstract;
using STE.WebSell.Application.NotificationModule.Dtos;
using STE.WebSell.Application.NotificationModule.Dtos.Media;
using STE.WebSell.Application.OrderModule.Dtos.Order;
using STE.WebSell.Domain.Entities;

namespace STE.WebSell.API.Controllers
{
    [Authorize]
    [Route("api/notification")]
    [ApiController]
    public class NotificationController : ApiControllerBase
    {
        private readonly IEmailService _emailService;
        private readonly ISystemNotificationService _systemNotificationService;

        public NotificationController(IEmailService emailService, ISystemNotificationService systemNotificationService)
        {
            _emailService = emailService;
            _systemNotificationService = systemNotificationService;
        }

        [AllowAnonymous]
        [HttpGet("media")]
        public APIResponse<ResponseMediaDto> FindAllMedia([FromQuery] FilterSystemNotificationDto input)
           => new(_systemNotificationService.FindAllMedia(input));

        [HttpPost("media")]
        public APIResponse<ResponseMediaDto> Add([FromBody] List<string> images)
        {
            return new(_systemNotificationService.UpdateMedia(images));
        }

        [HttpPost("send")]
        public async Task<APIResponse> Add([FromBody] MailContent input)
        {
            //MailContent content = new MailContent
            //{
            //    To = "nhatvv18@gmail.com",
            //    Subject = "Kiểm tra thử",
            //    Body = "<p><strong>Xin chào</strong></p>"
            //};

            await _emailService.SendMail(input);
            return new();
        }

        [HttpGet("system")] 
        public APIResponse<ResponseSystemNotificationDto> FindAll([FromQuery] FilterSystemNotificationDto input)
            => new(_systemNotificationService.FindAll(input));

        [HttpPost("system")]
        public APIResponse Add([FromBody] List<ConfigKeysDto> input)
        {
            _systemNotificationService.Create(input);
            return new();
        }

        [HttpGet("notification")]
        public APIResponse<PagingResult<ResponseNotificationDto>> FindAllNotification([FromQuery] FilterSystemNotificationDto input)
          => new(_systemNotificationService.FindAllNotification(input));


        [HttpPost("read-notification/{notificationId}")]
        public async Task<APIResponse> Read(int notificationId)
        {
            await _systemNotificationService.ReadNotification(notificationId);
            return new();
        }

        [HttpGet("get-info-not-seen")]
        public APIResponse<ResponseInfoNotSeenNotificationDto> GetInfoNotSeen()
        => new(_systemNotificationService.GetInfoNotSeenNotification());
    }
}
