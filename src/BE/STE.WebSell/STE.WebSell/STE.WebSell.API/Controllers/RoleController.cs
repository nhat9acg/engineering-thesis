﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;
using STE.Utils;
using STE.Utils.RolePermission.Constant;
using STE.WebAPIBase.Controllers;
using STE.WebAPIBase.Filters;
using STE.WebSell.Application.AuthenticationModule.Abstract;
using STE.WebSell.Application.AuthenticationModule.Dtos;
using STE.WebSell.Application.AuthenticationModule.Dtos.PermissionDto;
using STE.WebSell.Application.AuthenticationModule.Dtos.RoleDto;
using STE.WebSell.Application.AuthenticationModule.Implements;
using STE.WebSell.Application.CustomerManagerModule.Dtos.BusinessCustomer;

namespace STE.WebSell.API.Controllers
{
    [Authorize]
    [Route("api/role")]
    [ApiController]
    public class RoleController : ApiControllerBase
    {
        private readonly IRoleService _roleService;
        public RoleController(IRoleService roleService)
        {
            _roleService = roleService;
        }

        /// <summary>
        /// Thêm Role
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("add")]
        //[PermissionFilter(PermissionKeys.UserTable)]
        public APIResponse<RoleDto> Add(CreateRolePermissionDto input)
                => new(_roleService.Add(input));

        /// <summary>
        /// update Role
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut("update")]
        public APIResponse<RoleDto> Update(UpdateRolePermissionDto input)
                => new(_roleService.Update(input));

        /// <summary>
        /// Xóa Role
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("delete/{id}")]
        public APIResponse Delete(int id)
        {
            _roleService.Delete(id);
            return new();
        }

        /// <summary>
        /// Find Role
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("find-by-id/{id}")]
        public APIResponse<RoleDto> FindById(int id)
       => new(_roleService.FindById(id));

        /// <summary>
        /// Xem danh sách Role
        /// </summary>
        /// <returns></returns>
        [HttpGet("find-all")]
        public APIResponse<PagingResult<RoleDto>> FindAll([FromQuery] FilterRoleDto input)
            => new(_roleService.FindAll(input));
    }
}
