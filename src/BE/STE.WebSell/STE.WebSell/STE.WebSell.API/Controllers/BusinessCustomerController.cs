﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;
using STE.Utils;
using STE.WebAPIBase.Controllers;
using STE.WebSell.Application.Common;
using STE.WebSell.Application.CustomerManagerModule.Abstract;
using STE.WebSell.Application.CustomerManagerModule.Dtos.BusinessCustomer;

namespace STE.WebSell.API.Controllers
{
    [Authorize]
    [Route("api/business-customer")]
    [ApiController]
    public class BusinessCustomerController : ApiControllerBase
    {
        private readonly IBusinessCustomerService _businessCustomerService;
        public BusinessCustomerController(IBusinessCustomerService businessCustomerService) 
        {
            _businessCustomerService = businessCustomerService;
        }

        /// <summary>
        /// Thêm khách hàng doanh nghiệp
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public APIResponse<BusinessCustomerDto> CreateBusinessCustomer(CreateBusinessCustomerDto input)
                => new(_businessCustomerService.Create(input));

        /// <summary>
        /// Danh sách khách hàng doanh nghiệp có phân trang
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("find")]
        public APIResponse<PagingResult<BusinessCustomerDto>> FindAllBusinessCustomer([FromQuery] FilterBusinessCustomerDto input)
                => new(_businessCustomerService.FindAll(input));

        /// <summary>
        /// Lấy thông tin khách hàng doanh nghiệp theo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public APIResponse<BusinessCustomerDto> FindByIdBusinessCustomer(int id)
                => new(_businessCustomerService.FindById(id));

        /// <summary>
        /// Cập nhập khách hàng doanh nghiệp
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut("update")]
        public APIResponse<BusinessCustomerDto> UpdateBusinessCustomer(UpdateBusinessCustomerDto input)
                => new(_businessCustomerService.Update(input));

        /// <summary>
        /// Xóa khách hàng doanh nghiệp
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("delete/{id}")]
        public APIResponse DeleteBusinessCustomer(int id)
        {
            _businessCustomerService.Delete(id);
            return new();
        }

        /// <summary>
        /// Lấy thông tin khách hàng doanh nghiệp đang đăng nhập
        /// </summary>
        /// <returns></returns>
        [HttpGet("info")]
        public APIResponse<BusinessCustomerDto> GetCurrentInfoBusinessCustomer()
                => new(_businessCustomerService.GetCurrentInfoBusinessCustomer());
    }
}
