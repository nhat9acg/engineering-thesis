﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;
using STE.Utils;
using STE.WebAPIBase.Controllers;
using STE.WebSell.Application.OrderModule.Abstract;
using STE.WebSell.Application.OrderModule.Dtos.Order;
using STE.WebSell.Application.OrderModule.Dtos.OrderBusinessCustomer;

namespace STE.WebSell.API.Controllers
{
    [Authorize]
    [Route("api/order/business-customer")]
    [ApiController]
    public class OrderBusinessCustomerController : ApiControllerBase
    {
        private readonly IBusinessCustomerOrderService _businessCustomerOrderService;

        public OrderBusinessCustomerController(IBusinessCustomerOrderService businessCustomerOrderService)
        {
            _businessCustomerOrderService = businessCustomerOrderService;
        }

        /// <summary>
        /// Lấy danh sách order theo doanh nghiệp
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("find-all")]
        public APIResponse<PagingResult<OrderDto>> FindAll([FromQuery] FilterOrderBusinessCustomerDto input)
            => new(_businessCustomerOrderService.FindAll(input));

        /// <summary>
        /// Tìm theo Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public APIResponse<OrderDto> FindById(int id)
            => new(_businessCustomerOrderService.FindById(id));

        /// <summary>
        /// Thêm đơn
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("add")]
        public APIResponse<OrderDto> Add([FromBody] CreateOrderBusinessCustomerDto input)
            => new(_businessCustomerOrderService.Create(input));

        /// <summary>
        /// Cập nhật đơn
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut("update")]
        public APIResponse<OrderDto> Update([FromBody] UpdateOrderBusinessCustomerDto input)
            => new(_businessCustomerOrderService.Update(input));

        /// <summary>
        /// Hủy đơn hàng
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut("cancel-order")]
        public APIResponse CalcelOrder([FromBody] CancelOrResponseOrderDto input)
        {
            _businessCustomerOrderService.CancelOrder(input);
            return new();
        }
    }
}
