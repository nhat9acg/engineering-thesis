﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;
using STE.Utils;
using STE.WebSell.Application.ProductModule.Abstract;
using STE.WebSell.Application.ProductModule.Dtos.Product;
using STE.WebSell.Application.ProductModule.Dtos.ProductCategory;

namespace STE.WebSell.API.Controllers
{
    [Authorize]
    [Route("api/product-manager")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductCategoryService _productCategoryService;
        private readonly IProductService _productManagerService;
        public ProductController(IProductService productManagerService, IProductCategoryService productCategoryService)
        {
            _productManagerService = productManagerService;
            _productCategoryService = productCategoryService;
        }

        /// <summary>
        /// thêm mới sản phẩm
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public APIResponse<ProductDto> CreateProduct(CreateProductDto input)
               => new(_productManagerService.Create(input));

        /// <summary>
        /// cập nhập sản phẩm
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut("update")]
        public APIResponse<ProductDto> UpdateProduct(UpdateProductDto input)
               => new(_productManagerService.Update(input));

        /// <summary>
        /// lấy thông tin sản phẩm theo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("find/{id}")]
        public APIResponse<ProductDto> FindByIdProduct(int id)
               => new(_productManagerService.FindById(id));

        /// <summary>
        /// lây danh sách sản phẩm
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("find")]
        public APIResponse<PagingResult<ProductDto>> FindProduct([FromQuery] FilterProductDto input)
        {
            var result = _productManagerService.FindAll(input);
             return new(result);
        }


        /// <summary>
        /// danh sach san pham cho khach khong login
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("find-anonymous")]
        public APIResponse<PagingResult<ProductDto>> FindProductForAnonymous([FromQuery] FilterProductDto input)
        {
            var result = _productManagerService.FindAll(input);
            return new(result);
        }

        /// <summary>
        /// Bật tắt active sản phẩm
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("active/{id}")]
        public APIResponse AcitveProduct(int id)
        {
            _productManagerService.ActiveProduct(id);
            return new();
        }
        /// <summary>
        /// Xóa danh sách sản phẩm
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("delete/{id}")]
        public APIResponse DeleteProduct(int id)
        {
            _productManagerService.Delete(id);
            return new();
        }
        /// <summary>
        /// thêm mới danh mục sản phẩm
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("product-category/create")]
        public APIResponse<ProductCategoryDto> CreateProductCategory(CreateProductCategoryDto input)
               => new(_productCategoryService.Create(input));

        /// <summary>
        /// cập nhập danh mục sản phẩm
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut("product-category/update")]
        public APIResponse<ProductCategoryDto> UpdateProductCategory(UpdateProductCategoryDto input)
               => new(_productCategoryService.Update(input));

        /// <summary>
        /// lấy thông tin danh mục sản phẩm theo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("product-category/find/{id}")]
        public APIResponse<ProductCategoryDto> FindByIdProductCategory(int id)
               => new(_productCategoryService.FindById(id));

        /// <summary>
        /// lấy danh sách thông tin danh mục sản phẩm
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet("product-category/find")]
        public APIResponse<PagingResult<ProductCategoryDto>> FindProductCategory([FromQuery] FilterProductCategoryDto input)
               => new(_productCategoryService.FindAll(input));

        /// <summary>
        /// Lấy tất cả danh sách danh mục sản phẩm kh phân trâng
        /// </summary>
        /// <returns></returns>
        [HttpGet("product-category/find-list")]
        public APIResponse<IEnumerable<ProductCategoryDto>> FindNoPagingProductCategory()
               => new(_productCategoryService.FindList());

        /// <summary>
        /// Xóa danh mục sản phẩm
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("product-category/delete/{id}")]
        public APIResponse DeleteProductCategory(int id)
        {
            _productCategoryService.Delete(id);
            return new();
        }

        /// <summary>
        /// Bật tắt active danh mục sản phẩm
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("product-category/active/{id}")]
        public APIResponse AcitveProductCategory(int id)
        {
            _productCategoryService.ActiveProductCategory(id);
            return new();
        }
    }
}
