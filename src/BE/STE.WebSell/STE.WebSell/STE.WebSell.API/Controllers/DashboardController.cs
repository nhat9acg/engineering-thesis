﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;
using STE.Utils;
using STE.WebSell.Application.DashboardModule.Abstract;
using STE.WebSell.Application.DashboardModule.Dtos;

namespace STE.WebSell.API.Controllers
{
    [Authorize]
    [Route("api/dashboard")]
    [ApiController]
    public class DashboardController : ControllerBase
    {
        private readonly IDashboardService _dashboardService;

        public DashboardController(IDashboardService dashboardService)
        {
            _dashboardService = dashboardService;
        }
        /// <summary>
        /// Dashboard
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("overview")]
        public APIResponse<DashboardDto> Dashboard([FromQuery] FilterDashboardDto input)
            => new(_dashboardService.Dashboard(input));

        /// <summary>
        /// Dashboard
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("customer")]
        public APIResponse<PagingResult<DashboardCustomerOrderDto>> CustomerOverview([FromQuery] FilterDashboardCustomerDto input)
            => new(_dashboardService.CustomerOverview(input));
    }
}
