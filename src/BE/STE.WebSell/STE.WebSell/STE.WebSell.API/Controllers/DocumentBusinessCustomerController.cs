﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;
using STE.Utils;
using STE.WebAPIBase.Controllers;
using STE.WebSell.Application.DocumentModule.Abstract;
using STE.WebSell.Application.DocumentModule.Dtos.Document;
using STE.WebSell.Application.DocumentModule.Dtos.DocumentBusinessCustomer;

namespace STE.WebSell.API.Controllers
{
    [Authorize]
    [Route("api/document/business-customer")]
    [ApiController]
    public class DocumentBusinessCustomerController : ApiControllerBase
    {
        private readonly IDocumentBusinessCustomerService _documentBusinessCustomerService;

        public DocumentBusinessCustomerController(IDocumentBusinessCustomerService documentBusinessCustomerService)
        {
            _documentBusinessCustomerService = documentBusinessCustomerService;
        }

        /// <summary>
        /// Danh sách tài liệu có phân trang cho khách doanh nghiệp xem
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("find-all")]
        public APIResponse<PagingResult<DocumentDto>> FindAllDocumentByBusinessCustomer([FromQuery] FilterDocumentBusinessCustomerDto input)
        {
            return new(_documentBusinessCustomerService.FindAll(input));
        }

        /// <summary>
        /// Lấy thông tin tài liệu theo id cho khách doanh nghiệp
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public APIResponse<DocumentDto> FindByIdDocumentByBusinessCustomer(int id)
                => new(_documentBusinessCustomerService.FindById(id));
    }
}
