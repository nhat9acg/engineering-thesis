﻿using Microsoft.AspNetCore.Mvc;
using STE.Utils;
using STE.WebSell.Application.OrderModule.Abstract;
using STE.WebSell.Application.OrderModule.Dtos.DayOff;

namespace STE.WebSell.API.Controllers
{
    [Route("api/dayoff")]
    [ApiController]
    public class DayOffController : ControllerBase
    {
        private readonly IDayOffService _dayOffService;

        public DayOffController(IDayOffService dayOffService)
        {
            _dayOffService = dayOffService;
        }

        /// <summary>
        /// Cập nhật ngày không sản xuất, ngày truyền trong dải StartDate -> EndDate nếu EndDate null thì lấy bằng StartDate
        /// Những ngày nào chưa có trong dải thì thêm, những ngày nào có rồi thì update
        /// </summary>
        [HttpPut("update")]
        public APIResponse UpdateDayOff(UpdateDayOffDto input)
        {
            _dayOffService.UpdateDayOff(input);
            return new();
        }

        /// <summary>
        /// Lấy ngày không sản xuất theo năm
        /// </summary>
        /// <returns></returns>
        [HttpGet("find-all")]
        public APIResponse<IEnumerable<DayOffDto>> GetDayOff(int year)
            => new(_dayOffService.GetDayOff(year));

        /// <summary>
        /// Xoá ngày không sản xuất, những ngày có trong giải sẽ bị xoá
        /// </summary>
        /// <returns></returns>
        [HttpDelete("delete")]
        public APIResponse DeleteDayOff(DateTime startDate, DateTime? endDate)
        {
            _dayOffService.DeleteDayOff(startDate, endDate);
            return new();
        }
    }
}
