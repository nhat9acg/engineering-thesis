﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;
using STE.ApplicationBase.CommonModule.Implements;
using STE.Utils;
using STE.WebSell.Application.ProductModule.Abstract;
using STE.WebSell.Application.ProductModule.Dtos.ProductPrice;

namespace STE.WebSell.API.Controllers
{
    /// <summary>
    /// Quản lý giá sản phẩm cho khách hàng
    /// </summary>
    [Route("api/product-price")]
    [ApiController]
    public class ProductPriceController : ControllerBase
    {
        private readonly IProductPriceService _productPriceService;
        private readonly IHttpContextAccessor _httpContext;
        private readonly IMapper _mapper;

        public ProductPriceController(IProductPriceService productPriceService,
            IHttpContextAccessor httpContext,
            IMapper mapper)
        {
            _productPriceService = productPriceService;
            _httpContext = httpContext;
            _mapper = mapper;
        }

        /// <summary>
        /// Thêm giá sản phẩm theo khách hàng
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("add")]
        public APIResponse AddProductPrice(List<CreateProductPriceDto> input)
        {
            _productPriceService.Add(input);
            return new();
        }

        /// <summary>
        /// cập nhập giá sản phẩm theo khách hàng
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut("update")]
        public APIResponse UpdateProductPrice(List<UpdateProductPriceDto> input)
        {
            _productPriceService.Update(input);
            return new();
        }

        /// <summary>
        /// hiển thị giá sản phẩm theo khách hàng
        /// </summary>
        /// <param name="id">id doanh nghiệp</param>
        /// <returns></returns>
        [HttpGet("find-by-business-customer")]
        public APIResponse<IEnumerable<ProductPriceDto>> FindProductPriceByBusinessCustomer(int id)
               => new(_productPriceService.FindProductPriceByBusinessCustomer(id));

        /// <summary>
        /// hiển thị danh sách thông tin sản phẩm và giá phân trang
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("find-all")]
        public APIResponse<PagingResult<ProductPriceDto>> FindAllProductPrice([FromQuery] FilterProductPriceDto input)
               => new(_productPriceService.FindAll(input));

        /// <summary>
        /// hiển thị danh sách thông tin sản phẩm và giá phân trang cho doanh nghiệp đang đăng nhập
        /// </summary>
        /// <returns></returns>
        [HttpGet("business-customer/find")]
        public APIResponse<PagingResult<ProductPriceDto>> FindAllProductPriceBusinessCustomer([FromQuery] FilterProductPriceBusinessCustomerDto input)
        {
            var filter = _mapper.Map<FilterProductPriceDto>(input);
            filter.BusinessCustomerId = CommonUtils.GetCurrentBusinessCustomerId(_httpContext);
            return new(_productPriceService.FindAll(filter));
        }

        /// <summary>
        /// Xóa danh sách giá sản phẩm 
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        [HttpDelete("delete")]
        public APIResponse DeleteProductPrice([FromQuery] List<int> ids)
        {
            _productPriceService.Delete(ids);
            return new();
        }
    }
}
