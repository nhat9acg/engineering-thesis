﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using STE.Utils.Net.MimeTypes;
using STE.WebAPIBase.Controllers;
using STE.WebAPIBase.Filters;
using STE.WebSell.Application.ExportExcelReportModule.Abstract;
using static OpenIddict.Abstractions.OpenIddictConstants;

namespace STE.WebSell.API.Controllers
{
    [Authorize]
    [Route("api/export-excel-Report")]
    [ApiController]
    public class ExportExcelReportController : ApiControllerBase
    {
        private readonly IExportExcelReportService _service;

        public ExportExcelReportController(IExportExcelReportService service)
        {
            _service = service;
        }

        /// <summary>
        /// Báo cáo tổng hợp tiền về sản phẩm
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        [HttpGet("synthetic-money-product")]
        public async Task<IActionResult> SyntheticMoneyProduct(DateTime? startDate, DateTime? endDate)
        {
            try
            {
                var result = await _service.SyntheticMoneyProduct(startDate, endDate);
                return File(result.fileData, MimeTypeNames.ApplicationVndOpenxmlformatsOfficedocumentSpreadsheetmlSheet, "BaoCaoTongHopTienVeSanPham.xlsx");
            }
            catch (Exception ex)
            {
                return Ok(ex);
            }
        }
    }
}
