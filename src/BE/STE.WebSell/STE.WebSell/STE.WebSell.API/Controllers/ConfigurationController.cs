﻿using Microsoft.AspNetCore.Mvc;
using STE.Utils;
using STE.WebSell.Application.ConfigurationModule.Abstract;
using STE.WebSell.Application.ConfigurationModule.Dtos;

namespace STE.WebAPIBase.Controllers
{
    [Route("api/configuration")]
    [ApiController]
    public class ConfigurationController : ApiControllerBase
    {
        private readonly IConfigurationService _configurationService;

        public ConfigurationController(IConfigurationService configurationService)
        {
            _configurationService = configurationService;
        }

        /// <summary>
        /// Lấy thông tin các config cho ứng dụng
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public APIResponse<ConfigurationDto> GetConfiguration() => new(_configurationService.GetConfiguration());
    }
}
