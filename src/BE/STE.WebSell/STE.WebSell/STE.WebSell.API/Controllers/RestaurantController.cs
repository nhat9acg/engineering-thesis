﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;
using STE.ApplicationBase.CommonModule.Implements;
using STE.Utils;
using STE.WebAPIBase.Controllers;
using STE.WebSell.Application.CustomerManagerModule.Abstract;
using STE.WebSell.Application.CustomerManagerModule.Dtos.DeliveryAddress;
using STE.WebSell.Application.CustomerManagerModule.Dtos.Restaurant;

namespace STE.WebSell.API.Controllers
{
    [Authorize]
    [Route("api/restaurant")]
    [ApiController]
    public class RestaurantController : ApiControllerBase
    {
        private readonly IRestaurantService _restaurantService;
        private readonly IDeliveryAddressService _deliveryAddressService;
        private readonly IHttpContextAccessor _httpContext;
        private readonly IMapper _mapper;

        public RestaurantController(IRestaurantService restaurantService,
            IDeliveryAddressService deliveryAddressService,
            IHttpContextAccessor httpContext, 
            IMapper mapper)
        {
            _restaurantService = restaurantService;
            _deliveryAddressService = deliveryAddressService;
            _httpContext = httpContext;
            _mapper = mapper;
        }

        #region restaurant
        /// <summary>
        /// Thêm mới nhà hàng
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public APIResponse<RestaurantDto> CreateRestaurant(CreateRestaurantDto input)
                => new(_restaurantService.Create(input));

        /// <summary>
        /// Lấy danh sách nhà hàng có phân trang
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("find")]
        public APIResponse<PagingResult<RestaurantDto>> FindAllRestaurant([FromQuery] FilterRestaurantDto input)
                => new(_restaurantService.FindAll(input));

        /// <summary>
        /// Lấy danh sách nhà hàng có phân trang cho doanh nghiệp tự xem
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("find-by-business-customer")]
        public APIResponse<PagingResult<RestaurantDto>> FindAllRestaurantByBusinessCustomer([FromQuery] FilterRestaurantByBusinessCustomerDto input)
        {
            var filter = _mapper.Map<FilterRestaurantDto>(input);
            filter.BusinessCustomerId = CommonUtils.GetCurrentBusinessCustomerId(_httpContext);
            return new(_restaurantService.FindAll(filter));
        }

        /// <summary>
        /// Lấy thông tin chi tiết nhà hàng
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public APIResponse<RestaurantDto> FindByIdRestaurant(int id)
                => new(_restaurantService.FindById(id));

        /// <summary>
        /// Cập nhập thông tin nhà hàng
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut("update")]
        public APIResponse<RestaurantDto> UpdateRestaurant(UpdateRestaurantDto input)
                => new(_restaurantService.Update(input));

        /// <summary>
        /// Xóa nhà hàng
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("delete/{id}")]
        public APIResponse DeleteRestaurant(int id)
        {
            _restaurantService.Delete(id);
            return new();
        }
        #endregion

        #region Delivery Address
        /// <summary>
        /// Thêm địa chỉ nhà hàng
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("delivery-address/create")]
        public APIResponse<DeliveryAddressDto> CreateDeliveryAddress(CreateDeliveryAddressDto input)
               => new(_deliveryAddressService.Create(input));

        /// <summary>
        /// Danh sách khách hàng doanh nghiệp có phân trang
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("delivery-address/find-all")]
        public APIResponse<PagingResult<DeliveryAddressDto>> FindAllDeliveryAddress([FromQuery] FilterDeliveryAddressDto input)
                => new(_deliveryAddressService.FindAllDeliveryAddress(input));

        /// <summary>
        /// Lấy thông tin chi tiết địa chỉ giao hàng
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("delivery-address/find-by-restaurant/{id}")]
        public APIResponse<IEnumerable<DeliveryAddressDto>> FindDeliveryAddressByRestaurantId(int id)
                => new(_deliveryAddressService.FindByRestaurant(id));

        /// <summary>
        /// Cập nhập thông tin địa chỉ giao hàng
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut("delivery-address/update")]
        public APIResponse UpdateDeliveryAddress(UpdateDeliveryAddressDto input)
        {
            _deliveryAddressService.Update(input);
            return new();
        }

        /// <summary>
        /// Xóa thông tin địa chỉ giao hàng
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("delivery-address/delete/{id}")]
        public APIResponse DeleteDeliveryAddress(int id)
        {
            _deliveryAddressService.Delete(id);
            return new();
        }
        #endregion

        /// <summary>
        /// Lấy thông tin nhà đang đăng nhập
        /// </summary>
        /// <returns></returns>
        [HttpGet("info")]
        public APIResponse<RestaurantDto> GetCurrentInfoRestaurant()
                => new(_restaurantService.GetCurrentInfoRestaurant());
    }
}
