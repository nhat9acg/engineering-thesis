﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;
using STE.Utils;
using STE.WebAPIBase.Controllers;
using STE.WebSell.Application.ChatModule.Abstract;
using STE.WebSell.Application.ChatModule.Dtos;

namespace STE.WebSell.API.Controllers
{
    [Authorize]
    [Route("api/chat")]
    [ApiController]
    public class ChatController : ApiControllerBase
    {
        private readonly IChatService _service;

        public ChatController(IChatService service)
        {
            _service = service;
        }

        [HttpGet("find")]
        public APIResponse<PagingResult<ChatGroup>> FindByUserId([FromQuery] FilterChatDto input)
            => new(_service.FindByUserId(input));

        [HttpPost("add")]
        public async Task<APIResponse> Add([FromBody] RequestChatDto input)
        {
            await _service.Create(input);
            return new();
        }
    }
}
