﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;
using STE.Utils;
using STE.WebAPIBase.Controllers;
using STE.WebSell.Application.OrderModule.Abstract;
using STE.WebSell.Application.OrderModule.Dtos.Order;

namespace STE.WebSell.API.Controllers
{
    [Authorize]
    [Route("api/order")]
    [ApiController]
    public class OrderController : ApiControllerBase
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        /// <summary>
        /// Lấy danh sách order
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("find-all")]
        public APIResponse<PagingResult<OrderDto>> FindAll([FromQuery] FilterOrderDto input)
            => new(_orderService.FindAll(input));

        /// <summary>
        /// Tìm theo Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public APIResponse<OrderDto> FindById(int id)
            => new(_orderService.FindById(id));

        /// <summary>
        /// Thêm đơn
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("add")]
        public APIResponse<OrderDto> Add([FromBody] CreateOrderDto input)
            => new(_orderService.Create(input));

        /// <summary>
        /// Cập nhật đơn
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut("update")]
        public APIResponse<OrderDto> Update([FromBody] UpdateOrderDto input)
            => new(_orderService.Update(input));

        /// <summary>
        /// xóa đơn hàng ở trạng thái mới tạo hoặc huỷ
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("delete/{id}")]
        public APIResponse Delete(int id)
        {
            _orderService.Delete(id);
            return new();
        }

        /// <summary>
        /// Chuyển trạng thái order sang đang xử lý
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPut("processing-order/{id}")]
        public APIResponse ProcessingOrder(int id)
        {
            _orderService.ProcessingOrder(id);
            return new();
        }

        /// <summary>
        /// Xác nhận gửi hàng
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut("confirm-order")]
        public APIResponse<OrderDto> ConfirmOrder([FromBody] UpdateOrderDto input)
            => new(_orderService.CompleteOrder(input));

        /// <summary>
        /// Hủy đơn hàng
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut("cancel-order")]
        public APIResponse CalcelOrder([FromBody] CancelOrResponseOrderDto input)
        {
            _orderService.CancelOrder(input);
            return new();
        }

        /// <summary>
        /// Phản hồi đơn hàng
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut("response-order")]
        public APIResponse ResponseOrder([FromBody] CancelOrResponseOrderDto input)
        {
            _orderService.ResponseOrder(input);
            return new();
        }
    }
}
