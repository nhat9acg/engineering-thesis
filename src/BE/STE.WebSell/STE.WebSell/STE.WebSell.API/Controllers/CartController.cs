﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;
using STE.Utils;
using STE.WebAPIBase.Controllers;
using STE.WebSell.Application.CartModule.Abstract;
using STE.WebSell.Application.CartModule.Dtos;

namespace STE.WebSell.API.Controllers
{
    [Authorize]
    [Route("api/cart")]
    [ApiController]
    public class CartController : ApiControllerBase
    {
        private readonly ICartService _cartService;

        public CartController(ICartService cartService)
        {
            _cartService = cartService;
        }

        [HttpPost("create")]
        public APIResponse<CartDto> CreateCart(CreateCartDto input)
                => new(_cartService.Create(input));

        [HttpGet("find")]
        public APIResponse<PagingResult<CartDto>> FindAllCart([FromQuery] FilterCartDto input)
                => new(_cartService.FindAll(input));

        [HttpGet("{id}")]
        public APIResponse<CartDto> FindByIdCart(int id)
                => new(_cartService.FindById(id));

        [HttpPut("update")]
        public APIResponse UpdateCart(UpdateCartDto input)
        {
            _cartService.Update(input);
            return new();
        }

        [HttpDelete("delete")]
        public APIResponse DeleteCart(int id)
        {
            _cartService.Delete(id);
            return new();
        }
    }
}
