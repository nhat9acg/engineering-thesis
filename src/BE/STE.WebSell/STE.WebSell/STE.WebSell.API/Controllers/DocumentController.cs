﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;
using STE.Utils;
using STE.WebAPIBase.Controllers;
using STE.WebSell.Application.DocumentModule.Abstract;
using STE.WebSell.Application.DocumentModule.Dtos.Document;

namespace STE.WebSell.API.Controllers
{
    [Authorize]
    [Route("api/document")]
    [ApiController]
    public class DocumentController : ApiControllerBase
    {
        private readonly IDocumentService _documentService;

        public DocumentController(IDocumentService documentService)
        {
            _documentService = documentService;
        }

        /// <summary>
        /// Thêm tài liệu
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public APIResponse<DocumentDto> CreateDocument(CreateDocumentDto input)
                => new(_documentService.Create(input));

        /// <summary>
        /// Danh sách tài liệu có phân trang
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("find")]
        public APIResponse<PagingResult<DocumentDto>> FindAllDocument([FromQuery] FilterDocumentDto input)
                => new(_documentService.FindAll(input));

        /// <summary>
        /// Lấy thông tin tài liệu theo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public APIResponse<DocumentDto> FindByIdDocument(int id)
                => new(_documentService.FindById(id));

        /// <summary>
        /// Cập nhập tài liệu
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut("update")]
        public APIResponse UpdateDocument(UpdateDocumentDto input)
        {
            _documentService.Update(input);
            return new();
        }

        /// <summary>
        /// Xóa tài liệu
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("delete/{id}")]
        public APIResponse DeleteDocument(int id)
        {
            _documentService.Delete(id);
            return new();
        }
    }
}
