﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using STE.Utils;
using STE.WebAPIBase.Controllers;
using STE.WebSell.Application.DemoModule.Abstract;
using STE.WebSell.Application.DemoModule.Dtos;

namespace STE.WebSellAPI.Controllers
{
    [Authorize]
    [Route("api/values")]
    [ApiController]
    public class ValuesController : ApiControllerBase
    {
        private readonly IValueService _valueService;

        public ValuesController(IValueService valueService)
        {
            _valueService = valueService;
        }

        [HttpGet]
        public APIResponse<int> Get()
            => new(_valueService.GetValue());

        [HttpGet("{id}")]
        public APIResponse<string> Get(int id) => new("value");

        [HttpPost]
        public APIResponse Post([FromForm] InputValueDto input) => new();

        [HttpPut("{id}")]
        public APIResponse Put(int id, [FromBody] string value) => new();

        [HttpDelete("{id}")]
        public APIResponse Delete(int id) => new();
    }
}
