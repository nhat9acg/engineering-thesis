﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;
using STE.Utils;
using STE.WebAPIBase.Controllers;
using STE.WebSell.Application.OrderModule.Abstract;
using STE.WebSell.Application.OrderModule.Dtos.Order;
using STE.WebSell.Application.OrderModule.Dtos.OrderCustomer;

namespace STE.WebSell.API.Controllers
{
    [Authorize]
    [Route("api/order/customer")]
    [ApiController]
    public class OrderCustomerController : ApiControllerBase
    {
        private readonly ICustomerOrderService _customerOrderService;

        public OrderCustomerController(ICustomerOrderService customerOrderService)
        {
            _customerOrderService = customerOrderService;
        }

        /// <summary>
        /// Lấy danh sách order
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("find-all")]
        public APIResponse<PagingResult<CustomerOrderDto>> FindAll([FromQuery] FilterOrderDto input)
            => new(_customerOrderService.FindAll(input));

        /// <summary>
        /// Tìm theo Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public APIResponse<CustomerOrderDto> FindById(int id)
            => new(_customerOrderService.FindById(id));

        /// <summary>
        /// Thêm đơn
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("add")]
        public async Task<APIResponse<int>> Add([FromBody] CreateCustomerOrderDto input)
        {
           
            return new(await _customerOrderService.Create(input));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        [HttpPut("has-paid/{orderId}")]
        public async Task<APIResponse> HasPaid(int orderId)
        {
            await _customerOrderService.HasPaid(orderId);
            return new();
        }
    }
}
