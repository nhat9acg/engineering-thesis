﻿using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;
using STE.Utils;
using STE.WebSell.Application.ProductModule.Abstract;
using STE.WebSell.Application.ProductModule.Dtos.CalculationUnit;

namespace STE.WebSell.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CalculationUnitController : ControllerBase
    {
        private readonly ICalculationUnitService _calculationUnitService;

        public CalculationUnitController(ICalculationUnitService calculationUnitService)
        {
            _calculationUnitService = calculationUnitService;
        }
        /// <summary>
        /// thêm mới đơn vị tính
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPost("create")]
        public APIResponse<CalculationUnitDto> CreateCalculationUnit(CreateCalculationUnitDto input)
               => new(_calculationUnitService.Create(input));
        /// <summary>
        /// cập nhập đơn vị tính
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpPut("update")]
        public APIResponse<CalculationUnitDto> UpdateCalculationUnit(UpdateCalculationUnitDto input)
               => new(_calculationUnitService.Update(input));
        /// <summary>
        /// lấy thông tin đơn vị tính theo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("find/{id}")]
        public APIResponse<CalculationUnitDto> FindByIdCalculationUnit(int id)
               => new(_calculationUnitService.FindById(id));
        /// <summary>
        /// lấy danh sách đơn vị tính
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("find")]
        public APIResponse<PagingResult<CalculationUnitDto>> FindCalculationUnits([FromQuery] FilterCalculationUnitDto input)
               => new(_calculationUnitService.FindAll(input));
        /// <summary>
        /// xóa đơn vị tính
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("delete/{id}")]
        public APIResponse DeleteCalculationUnit(int id)
        {
            _calculationUnitService.Delete(id);
            return new();
        }
    }
}
