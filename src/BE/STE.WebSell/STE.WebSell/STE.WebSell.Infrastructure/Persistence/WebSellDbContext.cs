﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using STE.InfrastructureBase.Persistence;
using STE.Utils.ConstantVariables.Database;
using STE.Utils.ConstantVariables.Order;
using STE.Utils.ConstantVariables.Shared;
using STE.Utils.ConstantVariables.User;
using STE.WebSell.Domain.Entities;
using STE.WebSell.Domain.Enums;

namespace STE.WebSell.Infrastructure.Persistence
{
    public partial class WebSellDbContext : ApplicationDbContext<User>
    {
        public DbSet<Cart> Carts { get; set; }
        public DbSet<UserAddress> UserAddresses { get; set; }
        public override DbSet<User> Users { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<RolePermission> RolePermissions { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<BusinessCustomer> BusinessCustomers { get; set; }
        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<DeliveryAddress> DeliveryAddresses { get; set; }
        public DbSet<GroupCustomer> GroupCustomers { get; set; }
        public DbSet<GroupCustomerBusinessCustomer> GroupCustomerBusinessCustomers { get; set; }
        public DbSet<ProductCategory> ProductCategories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductPrice> ProductPrices { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderDetail> OrderDetails { get; set; }
        public DbSet<CalculationUnit> CalculationUnits { get; set; }
        public DbSet<ShippingProvider> ShippingProviders { get; set; }
        public DbSet<HistoryUpdate> HistoryUpdates { get; set; }
        public DbSet<DayOff> DayOffs { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<DocumentRecipient> DocumentRecipients { get; set; }
        public DbSet<SystemNotification> SystemNotifications { get; set; }
        public DbSet<SystemNotificationType> SystemNotificationTypes { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<Chat> Chats { get; set; }
        public DbSet<Media> Medias { get; set; }

        public WebSellDbContext() : base()
        {
        }

        public WebSellDbContext(DbContextOptions<WebSellDbContext> options, IHttpContextAccessor httpContextAccessor) : base(options, httpContextAccessor)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema(DbSchemas.Default);

            #region Order & OrderDetail
            modelBuilder.Entity<Order>(entity =>
            {
                entity.Property(e => e.OrderDate).HasDefaultValueSql("getdate()");
                entity.Property(e => e.CreatedDate).HasDefaultValueSql("getdate()");
                entity.Property(e => e.Status).HasDefaultValue(OrderStatus.NEW);
            });

            modelBuilder.Entity<OrderDetail>()
                        .HasOne(orderDetail => orderDetail.Order)
                        .WithMany(order => order.OrderDetails)
                        .HasForeignKey(e => e.OrderId)
                        .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<OrderDetail>()
                      .HasOne(orderDetail => orderDetail.Product)
                      .WithMany(product => product.OrderDetails)
                      .HasForeignKey(e => e.ProductId)
                      .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<Order>()
                      .HasOne(order => order.DeliveryAddress)
                      .WithMany(deliveryAddress => deliveryAddress.Orders)
                      .HasForeignKey(e => e.DeliveryAddressId)
                      .OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Order>()
                        .HasOne(order => order.Restaurant)
                        .WithMany(restaurant => restaurant.Orders)
                        .HasForeignKey(e => e.RestaurantId)
                        .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Order>()
                       .HasOne(order => order.User)
                       .WithMany(user => user.Orders)
                       .HasForeignKey(e => e.UserId)
                       .OnDelete(DeleteBehavior.SetNull);

            modelBuilder.Entity<Order>()
                        .HasOne(order => order.ShippingProvider)
                        .WithMany()
                        .HasForeignKey(e => e.ShippingProviderId)
                        .OnDelete(DeleteBehavior.SetNull);
            #endregion

            modelBuilder.Entity<ShippingProvider>(entity =>
            {
                entity.Property(e => e.Status).HasDefaultValue(CommonStatus.ACTIVE);
                entity.Property(e => e.CreatedDate).HasDefaultValueSql("getdate()");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("User");
                entity.Property(e => e.Status)
                    .HasColumnName("Status")
                    .HasDefaultValue(UserStatus.ACTIVE);
                entity.Property(e => e.CreatedDate).HasDefaultValueSql("getdate()");
            });

            modelBuilder.Entity<BusinessCustomer>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasDefaultValueSql("getdate()");
            });

            modelBuilder.Entity<Restaurant>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasDefaultValueSql("getdate()");
            });

            modelBuilder.Entity<DeliveryAddress>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasDefaultValueSql("getdate()");
            });

            modelBuilder.Entity<GroupCustomer>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasDefaultValueSql("getdate()");
            });

            modelBuilder.Entity<ProductCategory>(entity =>
            {
                entity.Property(e => e.Status).HasDefaultValue(ActiveStatus.ACTIVE);
                entity.Property(e => e.CreatedDate).HasDefaultValueSql("getdate()");
            });

            #region Product
            modelBuilder.Entity<Product>(entity =>
            {
                entity.Property(e => e.Status).HasDefaultValue(ActiveStatus.ACTIVE);
                entity.Property(e => e.CreatedDate).HasDefaultValueSql("getdate()");
            });

            modelBuilder.Entity<Product>()
                        .HasOne(product => product.ProductCategory)
                        .WithMany(productCategory => productCategory.Products)
                        .HasForeignKey(product => product.ProductCategoryId)
                        .OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<Product>()
                        .HasOne(product => product.CalculationUnit)
                        .WithMany(calculationUnit => calculationUnit.Products)
                        .HasForeignKey(e => e.CalculationUnitId);
            #endregion

            #region Product Price
            modelBuilder.Entity<ProductPrice>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasDefaultValueSql("getdate()");
            });

            modelBuilder.Entity<ProductPrice>()
                        .HasOne(productPrice => productPrice.BusinessCustomer)
                        .WithMany(businessCustomer => businessCustomer.ProductPrices)
                        .HasForeignKey(productPrice => productPrice.BusinessCustomerId);

            modelBuilder.Entity<ProductPrice>()
                        .HasOne(productPrice => productPrice.Product)
                        .WithMany(product => product.ProductPrices)
                        .HasForeignKey(productPrice => productPrice.ProductId);
            #endregion

            modelBuilder.Entity<RolePermission>()
                .HasOne(e => e.Role)
                .WithMany()
                .HasForeignKey(e => e.RoleId);

            modelBuilder.Entity<UserRole>()
                .HasOne<Role>()
                .WithMany()
                .HasForeignKey(e => e.RoleId);

            modelBuilder.Entity<UserRole>()
                .HasOne<User>()
                .WithMany()
                .HasForeignKey(e => e.UserId);

            #region restaurant
            modelBuilder.Entity<Restaurant>()
                        .HasOne(restaurant => restaurant.BusinessCustomer)
                        .WithMany(businessCustomer => businessCustomer.Restaurants)
                        .HasForeignKey(e => e.BusinessCustomerId);

            modelBuilder.Entity<DeliveryAddress>()
                        .HasOne(deliveryAddress => deliveryAddress.Restaurant)
                        .WithMany(restaurant => restaurant!.DeliveryAddresses)
                        .HasForeignKey(deliveryAddress => deliveryAddress.RestaurantId)
                        .OnDelete(DeleteBehavior.Restrict);

            modelBuilder.Entity<DeliveryAddress>()
                        .HasOne(deliveryAddress => deliveryAddress.User)
                        .WithMany(user => user!.DeliveryAddresses)
                        .HasForeignKey(deliveryAddress => deliveryAddress.UserId)
                        .OnDelete(DeleteBehavior.Restrict);
            #endregion

            modelBuilder.Entity<GroupCustomerBusinessCustomer>()
                        .HasOne<BusinessCustomer>()
                        .WithMany()
                        .HasForeignKey(e => e.BusinessCustomerId);

            modelBuilder.Entity<GroupCustomerBusinessCustomer>()
                        .HasOne<GroupCustomer>()
                        .WithMany()
                        .HasForeignKey(e => e.GroupCustomerId);
            #region HistoryUpdate

            modelBuilder.Entity<HistoryUpdate>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasDefaultValueSql("getdate()");
            });
            #endregion

            #region Cart
            modelBuilder.Entity<Cart>(entity =>
            {
                entity.Property(e => e.Status).HasDefaultValue(ActiveStatus.ACTIVE);
                entity.Property(e => e.CreatedDate).HasDefaultValueSql("getdate()");
            });
            modelBuilder.Entity<Cart>()
                .HasOne(cart => cart.Product)
                .WithMany(product => product.Carts)
                .HasForeignKey(cart => cart.ProductId);
            #endregion

            #region chat
            modelBuilder.Entity<Chat>(entity =>
            {
                entity.Property(e => e.Status).HasDefaultValue(ActiveStatus.ACTIVE);
                entity.Property(e => e.CreatedDate).HasDefaultValueSql("getdate()");
            });
            //Cấu hình liên kết giữa Chat và User(người gửi)
            modelBuilder.Entity<Chat>()
                .HasOne(c => c.SenderUser)
                .WithMany(u => u.SenderChats)
                .HasForeignKey(c => c.SenderUserId)
                .OnDelete(DeleteBehavior.Restrict);

            // Cấu hình liên kết giữa Chat và User (người nhận)
            modelBuilder.Entity<Chat>()
                .HasOne(c => c.ReceiverUser)
                .WithMany(u => u.ReceiverChats)
                .HasForeignKey(c => c.ReceiverUserId)
                .OnDelete(DeleteBehavior.Restrict);
            #endregion

            modelBuilder.Entity<Media>(entity =>
            {
                entity.Property(e => e.Status).HasDefaultValue(MediaStatusEnum.Activated);
            });


            #region User address
            modelBuilder.Entity<UserAddress>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasDefaultValueSql("getdate()");
            });
            modelBuilder.Entity<UserAddress>()
                .HasOne(address => address.User)
                .WithMany(user => user.UserAddresses)
                .HasForeignKey(address => address.UserId);
            #endregion

            #region System Notification
            modelBuilder.Entity<SystemNotificationType>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasDefaultValueSql("getdate()");
            });
            modelBuilder.Entity<SystemNotification>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasDefaultValueSql("getdate()");
            });

            modelBuilder.Entity<SystemNotificationType>()
                .HasOne(s => s.SystemNotification)
                .WithMany(s => s.SystemNotificationType)
                .HasForeignKey(s => s.SystemNotificationId);

            modelBuilder.Entity<Notification>()
                .HasOne(s => s.User)
                .WithMany(s => s.Notifications)
                .HasForeignKey(s => s.UserId);
            modelBuilder.Entity<Notification>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasDefaultValueSql("getdate()");
                entity.Property(e => e.IsRead).HasDefaultValue(false);
            });
            #endregion

            modelBuilder.Entity<Document>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasDefaultValueSql("getdate()");
            });

            modelBuilder.Entity<DocumentRecipient>(entity =>
            {
                entity.Property(e => e.CreatedDate).HasDefaultValueSql("getdate()");
            });
            modelBuilder.Entity<DocumentRecipient>()
                .HasOne(e => e.BusinessCustomer)
                .WithMany()
                .HasForeignKey(e => e.BusinessCustomerId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<DocumentRecipient>()
                .HasOne(e => e.Document)
                .WithMany(e => e.DocumentRecipients)
                .HasForeignKey(e => e.DocumentId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.SeedData();
            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }
    }
}
