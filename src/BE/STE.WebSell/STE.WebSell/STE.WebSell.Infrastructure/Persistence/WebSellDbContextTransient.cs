﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace STE.WebSell.Infrastructure.Persistence
{
    public class WebSellDbContextTransient : WebSellDbContext
    {
        public WebSellDbContextTransient(DbContextOptions<WebSellDbContext> options, IHttpContextAccessor httpContextAccessor) : base(options, httpContextAccessor)
        {
        }
    }
}
