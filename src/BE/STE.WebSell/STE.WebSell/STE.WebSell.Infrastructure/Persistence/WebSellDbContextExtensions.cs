﻿using EPIC.Utils.Security;
using Microsoft.EntityFrameworkCore;
using STE.Utils.ConstantVariables.User;
using STE.WebSell.Domain.Entities;

namespace STE.WebSell.Infrastructure.Persistence
{
    public static class WebSellDbContextExtensions
    {
        public static void SeedData(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasData(
                new User { Id = 1, Username = "admin", Password = CryptographyUtils.CreateMD5("123qwe"), FullName = "admin", UserType = UserTypes.ADMIN, Status = UserStatus.ACTIVE });
        }
    }
}
