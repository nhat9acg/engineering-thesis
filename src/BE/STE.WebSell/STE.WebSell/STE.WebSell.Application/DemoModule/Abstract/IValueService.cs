﻿namespace STE.WebSell.Application.DemoModule.Abstract
{
    public interface IValueService
    {
        int GetValue();
    }
}
