﻿using Microsoft.AspNetCore.Http;
using STE.ApplicationBase.Common.Validations;

namespace STE.WebSell.Application.DemoModule.Dtos
{
    public class InputValueDto
    {
        [IntegerRange(AllowableValues = new int[] { 1, 2, 3 })]
        public int Type { get; set; }

        [CustomRequired(AllowEmptyStrings = false)]
        public string Name { get; set; }

        [FileExtention]
        [FileMaxLength]
        public IFormFile File { get; set; }
    }
}
