﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using STE.ApplicationBase.Common;
using STE.Utils.ConstantVariables.Shared;
using STE.Utils.CustomException;
using STE.Utils.Linq;
using STE.WebSell.Application.Common;
using STE.WebSell.Application.CustomerManagerModule.Abstract;
using STE.WebSell.Application.CustomerManagerModule.Dtos.DeliveryAddress;
using STE.WebSell.Domain.Entities;

namespace STE.WebSell.Application.CustomerManagerModule.Implement
{
    public class DeliveryAddressService : ServiceBase, IDeliveryAddressService
    {
        public DeliveryAddressService(ILogger<DeliveryAddressService> logger, IHttpContextAccessor httpContext) : base(logger, httpContext) { }

        public virtual PagingResult<DeliveryAddressDto> FindAllDeliveryAddress(FilterDeliveryAddressDto input)
        {
            var result = new PagingResult<DeliveryAddressDto>();
            var restaurantQuery = _dbContext.DeliveryAddresses
                                                        .Where(b => !b.Deleted
                                                                && (input.RestaurantId == null || input.RestaurantId == b.RestaurantId)
                                                                && (input.UserId == null || input.UserId == b.UserId)
                                                                );

            result.TotalItems = restaurantQuery.Count();
            restaurantQuery = restaurantQuery.OrderDynamic(input.Sort);

            if (input.PageSize != -1)
            {
                restaurantQuery = restaurantQuery.Skip(input.GetSkip()).Take(input.PageSize);
            }
            result.Items = _mapper.Map<IEnumerable<DeliveryAddressDto>>(restaurantQuery);
            return result;
        }

        public virtual void Update(UpdateDeliveryAddressDto input)
        {
            var deliveryAddress = _dbContext.DeliveryAddresses.FirstOrDefault(d => !d.Deleted && d.Id == input.Id)
                ?? throw new UserFriendlyException(ErrorCode.DeliveryAddressNotFound);
            deliveryAddress.Address = input.Address;
            deliveryAddress.IsDefault = input.IsDefault;
            deliveryAddress.Receiver = input.Receiver;
            deliveryAddress.Phone = input.Phone;
            UpdateOrtherDeliveryDefault(deliveryAddress, input.IsDefault);
            _dbContext.SaveChanges();
        }

        public virtual void UpdateDefault(int deliveryAddressId, bool isDefault)
        {
            var deliveryAddress = _dbContext.DeliveryAddresses.FirstOrDefault(d => !d.Deleted && d.Id == deliveryAddressId)
                ?? throw new UserFriendlyException(ErrorCode.DeliveryAddressNotFound);
            deliveryAddress.IsDefault = isDefault;
            UpdateOrtherDeliveryDefault(deliveryAddress, isDefault);
            _dbContext.SaveChanges();
        }

        public IEnumerable<DeliveryAddressDto> FindByRestaurant(int restaurantId)
        {
            var deliveryAddressQuery = _dbContext.DeliveryAddresses.Include(d => d.Restaurant)
                                                                   .Where(d => !d.Deleted && d.RestaurantId == restaurantId)
                                                                   .AsEnumerable();
            var result = _mapper.Map<IEnumerable<DeliveryAddressDto>>(deliveryAddressQuery);
            return result;
        }

        public virtual void Delete(int id)
        {
            var deliveryAddress = _dbContext.DeliveryAddresses.FirstOrDefault(d => !d.Deleted && d.Id == id)
                                            ?? throw new UserFriendlyException(ErrorCode.DeliveryAddressNotFound);
            if (deliveryAddress.IsDefault && deliveryAddress.RestaurantId.HasValue)
            {
                UpdateLastDeliveryToDefault(deliveryAddress.RestaurantId.Value);
            }
            deliveryAddress.Deleted = true;
            _dbContext.SaveChanges();
        }

        /// <summary>
        /// Cập nhật các địa chỉ khác có là mặc định
        /// </summary>
        /// <param name="deliveryAddress"></param>
        /// <param name="updateIsDefault"></param>
        private void UpdateOrtherDeliveryDefault(DeliveryAddress deliveryAddress, bool updateIsDefault)
        {
            if (updateIsDefault)
            {
                //các địa chỉ khác thành không phải mặc định
                foreach (var item in _dbContext.DeliveryAddresses.Where(d => d.RestaurantId == deliveryAddress.RestaurantId
                    && !d.Deleted && d.Id != deliveryAddress.Id && d.IsDefault).AsEnumerable())
                {
                    item.IsDefault = false;
                }
            }
            else if(deliveryAddress.RestaurantId.HasValue)
            {
                UpdateLastDeliveryToDefault(deliveryAddress.RestaurantId.Value);
            }
        }

        /// <summary>
        /// tìm địa chỉ cuối cùng làm mặc định
        /// </summary>
        /// <param name="restaurantId"></param>
        private void UpdateLastDeliveryToDefault(int restaurantId)
        {
            //var deliveryAddresLast = _dbContext.DeliveryAddresses
            //    .LastOrDefault(d => !d.Deleted && d.RestaurantId == restaurantId);
            var deliveryAddresLast = _dbContext.DeliveryAddresses
                    .Where(d => !d.Deleted && d.RestaurantId == restaurantId)
                    .OrderByDescending(d => d.Id)
                    .LastOrDefault();
            if (deliveryAddresLast != null)
            {
                deliveryAddresLast.IsDefault = true;
            }
        }

        public virtual DeliveryAddressDto Create(CreateDeliveryAddressDto input)
        {
            var restaurant = _dbContext.Restaurants.FirstOrDefault(r => r.Id == input.RestaurantId && !r.Deleted)
                             ?? throw new UserFriendlyException(ErrorCode.RestaurantNotFound);
            var deliveryAddress = _mapper.Map<DeliveryAddress>(input);

            if (!_dbContext.DeliveryAddresses.Any(d => d.RestaurantId == input.RestaurantId && !d.Deleted))
            {
                // Nếu đây là bản ghi đầu tiên, thiết lập IsDefault thành true
                deliveryAddress.IsDefault = true;
            }
            else if (input.IsDefault)
            {
                // Nếu đây là bản ghi thứ hai trở đi và input.IsDefault là true
                // Cập nhật tất cả các bản ghi khác của DeliveryAddress liên quan đến nhà hàng thành false
                var otherDeliveryAddresses = _dbContext.DeliveryAddresses
                    .Where(d => d.RestaurantId == input.RestaurantId && !d.Deleted && d.IsDefault)
                    .ToList();

                foreach (var address in otherDeliveryAddresses)
                {
                    address.IsDefault = false;
                }
            }

            var result = _dbContext.DeliveryAddresses.Add(deliveryAddress).Entity;
            _dbContext.SaveChanges();

            return _mapper.Map<DeliveryAddressDto>(result);
        }
    }
}
