﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using STE.ApplicationBase.Common;
using STE.ApplicationBase.CommonModule.Implements;
using STE.EntitiesBase.Entities;
using STE.Utils.ConstantVariables.Shared;
using STE.Utils.CustomException;
using STE.Utils.Linq;
using STE.WebSell.Application.Common;
using STE.WebSell.Application.CustomerManagerModule.Abstract;
using STE.WebSell.Application.CustomerManagerModule.Dtos.BusinessCustomer;
using STE.WebSell.Application.CustomerManagerModule.Dtos.Restaurant;
using STE.WebSell.Domain.Entities;

namespace STE.WebSell.Application.CustomerManagerModule.Implement
{
    public class RestaurantService : ServiceBase, IRestaurantService
    {
        public RestaurantService(ILogger<RestaurantService> logger, IHttpContextAccessor httpContext) : base(logger, httpContext)
        {
        }

        public RestaurantDto Create(CreateRestaurantDto input)
        {
            var restaurant = _mapper.Map<Restaurant>(input);
            var businessCustomerFind = _dbContext.BusinessCustomers.FirstOrDefault(b => b.Id == input.BusinessCustomerId && !b.Deleted)
                                                 ?? throw new UserFriendlyException(ErrorCode.BusinessCustomerNotFound);
            var result = _dbContext.Restaurants.Add(restaurant).Entity;
            _dbContext.SaveChanges();
            return _mapper.Map<RestaurantDto>(result);
        }

        public RestaurantDto Update(UpdateRestaurantDto input)
        {
            var restaurant = _dbContext.Restaurants.FirstOrDefault(r => r.Id == input.Id && !r.Deleted)
                             ?? throw new UserFriendlyException(ErrorCode.RestaurantNotFound);
            _mapper.Map(input, restaurant);
            _dbContext.SaveChanges();
            return _mapper.Map<RestaurantDto>(restaurant);
        }

        public RestaurantDto FindById(int id)
        {
            var restaurant = _dbContext.Restaurants.FirstOrDefault(r => r.Id == id && !r.Deleted)
                             ?? throw new UserFriendlyException(ErrorCode.RestaurantNotFound);
            restaurant.BusinessCustomer = _dbContext.BusinessCustomers.FirstOrDefault(b => b.Id == restaurant.BusinessCustomerId)
                             ?? throw new UserFriendlyException(ErrorCode.BusinessCustomerNotFound);
            return _mapper.Map<RestaurantDto>(restaurant);
        }

        public PagingResult<RestaurantDto> FindAll(FilterRestaurantDto input)
        {
            var result = new PagingResult<RestaurantDto>();
            var restaurantQuery = _dbContext.Restaurants.Include(r => r.BusinessCustomer)
                                                        .Where(b => !b.Deleted
                                                                && (input.BusinessCustomerId == null || input.BusinessCustomerId == b.BusinessCustomerId)
                                                                && (input.Keyword == null
                                                                || (b.Name != null && b.Name.ToLower().Contains(input.Keyword.ToLower()))
                                                                || (b.ContactPerson != null && b.ContactPerson.ToLower().Contains(input.Keyword.ToLower()))
                                                                || (b.Phone != null && b.Phone.ToLower().Contains(input.Keyword.ToLower()))));

            result.TotalItems = restaurantQuery.Count();
            restaurantQuery = restaurantQuery.OrderDynamic(input.Sort);

            if (input.PageSize != -1)
            {
                restaurantQuery = restaurantQuery.Skip(input.GetSkip()).Take(input.PageSize);
            }
            result.Items = _mapper.Map<IEnumerable<RestaurantDto>>(restaurantQuery);
            return result;
        }

        public void Delete(int id)
        {
            var restaurant = _dbContext.Restaurants.FirstOrDefault(r => r.Id == id && !r.Deleted)
                             ?? throw new UserFriendlyException(ErrorCode.RestaurantNotFound);
            restaurant.Deleted = true;
            foreach (var deliveryAddress in _dbContext.DeliveryAddresses.Where(d => !d.Deleted && d.RestaurantId == id))
            {
                deliveryAddress.Deleted = true;
            }
            _dbContext.SaveChanges();
        }

        public RestaurantDto GetCurrentInfoRestaurant()
        {
            var restaurantId = CommonUtils.GetCurrentRestaurantId(_httpContext);
            var restaurant = _dbContext.Restaurants.Include(r => r.BusinessCustomer).FirstOrDefault(r => r.Id == restaurantId && !r.Deleted) ?? throw new UserFriendlyException(ErrorCode.RestaurantNotFound);
            var result = _mapper.Map<RestaurantDto>(restaurant);
            return _mapper.Map<RestaurantDto>(restaurant);
        }
    }
}
