﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using STE.ApplicationBase.Common;
using STE.ApplicationBase.CommonModule.Implements;
using STE.EntitiesBase.Entities;
using STE.Utils.ConstantVariables.Customer;
using STE.Utils.ConstantVariables.Shared;
using STE.Utils.CustomException;
using STE.Utils.Linq;
using STE.WebSell.Application.Common;
using STE.WebSell.Application.CustomerManagerModule.Abstract;
using STE.WebSell.Application.CustomerManagerModule.Dtos.BusinessCustomer;
using STE.WebSell.Domain.Entities;
using System.Numerics;
using System.Security.Cryptography.X509Certificates;

namespace STE.WebSell.Application.CustomerManagerModule.Implement
{
    public class BusinessCustomerService : ServiceBase, IBusinessCustomerService
    {
        public BusinessCustomerService(ILogger<BusinessCustomerService> logger, IHttpContextAccessor httpContext) : base(logger, httpContext) { }
        public BusinessCustomerDto Create(CreateBusinessCustomerDto input)
        {
            var businessCustomer = _mapper.Map<BusinessCustomer>(input);
            var transaction = _dbContext.Database.BeginTransaction();
            var result = _dbContext.BusinessCustomers.Add(businessCustomer).Entity;
            _dbContext.SaveChanges();
            if (input.isRestaurant)
            {
                var restaurant = new Restaurant
                {
                    BusinessCustomerId = result.Id,
                    Name = result.FullName,
                    Phone = result.Phone,
                    Address = result.Address,
                    Status = CommonStatus.ACTIVE,
                    Email = result.Email
                };
                _dbContext.Restaurants.Add(restaurant);
                _dbContext.SaveChanges();
            }
            transaction.Commit();
           
            return _mapper.Map<BusinessCustomerDto>(result);
        }

        public BusinessCustomerDto Update(UpdateBusinessCustomerDto input)
        {
            var businessCustomer = _dbContext.BusinessCustomers.FirstOrDefault(b => !b.Deleted && b.Id == input.Id)
                                   ?? throw new UserFriendlyException(ErrorCode.BusinessCustomerNotFound);

            _mapper.Map(input, businessCustomer);
            _dbContext.SaveChanges();
            return _mapper.Map<BusinessCustomerDto>(businessCustomer);
        }

        public BusinessCustomerDto FindById(int id)
        {
            var businessCustomer = _dbContext.BusinessCustomers.FirstOrDefault(b => b.Id == id && !b.Deleted)
                                   ?? throw new UserFriendlyException(ErrorCode.BusinessCustomerNotFound);
            return _mapper.Map<BusinessCustomerDto>(businessCustomer);
        }

        public PagingResult<BusinessCustomerDto> FindAll(FilterBusinessCustomerDto input)
        {
            var result = new PagingResult<BusinessCustomerDto>();
            var businessCustomerQuery = _dbContext.BusinessCustomers
                                       .Where(b => !b.Deleted
                                             && (input.Keyword == null
                                                || b.FullName.ToLower().Contains(input.Keyword.ToLower())
                                                || b.ShortName.ToLower().Contains(input.Keyword.ToLower())
                                                || b.ContactPerson.ToLower().Contains(input.Keyword.ToLower())
                                                || b.Phone.ToLower().Contains(input.Keyword.ToLower())
                                                || b.Email.ToLower().Contains(input.Keyword.ToLower())
                                                || b.Website.ToLower().Contains(input.Keyword.ToLower())));

            businessCustomerQuery = businessCustomerQuery.OrderDynamic(input.Sort);

            if (input.PageSize != -1)
            {
                businessCustomerQuery = businessCustomerQuery.Skip(input.GetSkip()).Take(input.PageSize);
            }
            result.Items = _mapper.Map<IEnumerable<BusinessCustomerDto>>(businessCustomerQuery);
            return result;
        }

        public void Delete(int id)
        {
            var businessCustomer = _dbContext.BusinessCustomers.FirstOrDefault(b => b.Id == id && !b.Deleted)
                                   ?? throw new UserFriendlyException(ErrorCode.BusinessCustomerNotFound);

            if (_dbContext.Restaurants.Any(r => r.BusinessCustomerId == id && !r.Deleted)
                 || _dbContext.GroupCustomerBusinessCustomers.Any(g => g.BusinessCustomerId == id && !g.Deleted)
                 || _dbContext.ProductPrices.Any(p => p.BusinessCustomerId == id))
                throw new UserFriendlyException(ErrorCode.BusinessCustomerIsBeingUsed);

            businessCustomer.Deleted = true;
            _dbContext.SaveChanges();
        }

        public BusinessCustomerDto GetCurrentInfoBusinessCustomer()
        {
            var bussinessCustomerId = CommonUtils.GetCurrentBusinessCustomerId(_httpContext);
            var businessCustomer = _dbContext.BusinessCustomers.FirstOrDefault(bc => bc.Id == bussinessCustomerId) ?? throw new UserFriendlyException(ErrorCode.BusinessCustomerNotFound);
            return _mapper.Map<BusinessCustomerDto>(businessCustomer);
        }
    }
}
