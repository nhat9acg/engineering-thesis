﻿using EPIC.Utils.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using STE.ApplicationBase.Common;
using STE.ApplicationBase.CommonModule.Implements;
using STE.Utils.ConstantVariables.Shared;
using STE.Utils.ConstantVariables.User;
using STE.Utils.CustomException;
using STE.WebSell.Application.AuthenticationModule.Abstract;
using STE.WebSell.Application.AuthenticationModule.Dtos.UserDto;
using STE.WebSell.Application.AuthenticationModule.Implements;
using STE.WebSell.Application.CustomerManagerModule.Abstract;
using STE.WebSell.Application.CustomerManagerModule.Dtos.DeliveryAddress;
using STE.WebSell.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using STE.Utils.Linq;

namespace STE.WebSell.Application.CustomerManagerModule.Implement
{
    public class DeliveryAddressCustomerService : DeliveryAddressService, IDeliveryAddressCustomerService
    {
        public DeliveryAddressCustomerService(ILogger<DeliveryAddressService> logger, IHttpContextAccessor httpContext) : base(logger, httpContext)
        {
        }

        public override DeliveryAddressDto Create(CreateDeliveryAddressDto input)
        {
            int userId = CommonUtils.GetCurrentUserId(_httpContext);
            var user = _dbContext.Users.FirstOrDefault(r => r.Id == userId && !r.Deleted)
                             ?? throw new UserFriendlyException(ErrorCode.UserNotFound);
            var deliveryAddress = _mapper.Map<DeliveryAddress>(input);

            if (!_dbContext.DeliveryAddresses.Any(d => d.UserId == userId && !d.Deleted))
            {
                // Nếu đây là bản ghi đầu tiên, thiết lập IsDefault thành true
                deliveryAddress.IsDefault = true;
            }
            else if (input.IsDefault)
            {
                // Nếu đây là bản ghi thứ hai trở đi và input.IsDefault là true
                // Cập nhật tất cả các bản ghi khác của DeliveryAddress liên quan đến nhà hàng thành false
                var otherDeliveryAddresses = _dbContext.DeliveryAddresses
                    .Where(d => d.UserId == userId && !d.Deleted && d.IsDefault)
                    .ToList();

                foreach (var address in otherDeliveryAddresses)
                {
                    address.IsDefault = false;
                }
            }

            var result = _dbContext.DeliveryAddresses.Add(deliveryAddress).Entity;
            _dbContext.SaveChanges();

            return _mapper.Map<DeliveryAddressDto>(result);
        }

        public IEnumerable<DeliveryAddressDto> FindByUser(int userId)
        {
            var deliveryAddressQuery = _dbContext.DeliveryAddresses.Include(d => d.User)
                                                                   .Where(d => !d.Deleted && d.UserId == userId)
                                                                   .AsEnumerable();
            var result = _mapper.Map<IEnumerable<DeliveryAddressDto>>(deliveryAddressQuery);
            return result;
        }

        public override void Update(UpdateDeliveryAddressDto input)
        {
            var deliveryAddress = _dbContext.DeliveryAddresses.FirstOrDefault(d => !d.Deleted && d.Id == input.Id)
                ?? throw new UserFriendlyException(ErrorCode.DeliveryAddressNotFound);
            deliveryAddress.Address = input.Address;
            deliveryAddress.IsDefault = input.IsDefault;
            deliveryAddress.Receiver = input.Receiver;
            deliveryAddress.Phone = input.Phone;
            UpdateOrtherDeliveryDefault(deliveryAddress, input.IsDefault);
            _dbContext.SaveChanges();
        }

        private void UpdateOrtherDeliveryDefault(DeliveryAddress deliveryAddress, bool updateIsDefault)
        {
            if (updateIsDefault)
            {
                //các địa chỉ khác thành không phải mặc định
                foreach (var item in _dbContext.DeliveryAddresses.Where(d => d.UserId == deliveryAddress.UserId
                    && !d.Deleted && d.Id != deliveryAddress.Id && d.IsDefault).AsEnumerable())
                {
                    item.IsDefault = false;
                }
            }
            else if (deliveryAddress.UserId.HasValue)
            {
                UpdateLastDeliveryToDefault(deliveryAddress.UserId.Value);
            }
        }

        /// <summary>
        /// tìm địa chỉ cuối cùng làm mặc định
        /// </summary>
        /// <param name="userId"></param>
        private void UpdateLastDeliveryToDefault(int userId)
        {
          
            var deliveryAddresLast = _dbContext.DeliveryAddresses
                    .Where(d => !d.Deleted && d.UserId == userId)
                    .OrderByDescending(d => d.Id)
                    .LastOrDefault();
            if (deliveryAddresLast != null)
            {
                deliveryAddresLast.IsDefault = true;
            }
        }

        public override void UpdateDefault(int deliveryAddressId, bool isDefault)
        {
            var deliveryAddress = _dbContext.DeliveryAddresses.FirstOrDefault(d => !d.Deleted && d.Id == deliveryAddressId)
                ?? throw new UserFriendlyException(ErrorCode.DeliveryAddressNotFound);
            deliveryAddress.IsDefault = isDefault;
            UpdateOrtherDeliveryDefault(deliveryAddress, isDefault);
            _dbContext.SaveChanges();
        }

    }
}
