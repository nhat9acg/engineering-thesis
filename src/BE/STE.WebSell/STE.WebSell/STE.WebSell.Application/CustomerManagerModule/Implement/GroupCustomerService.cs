﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using STE.ApplicationBase;
using STE.ApplicationBase.Common;
using STE.ApplicationBase.CommonModule.Implements;
using STE.Utils.ConstantVariables.Shared;
using STE.Utils.CustomException;
using STE.Utils.Linq;
using STE.WebSell.Application.Common;
using STE.WebSell.Application.CustomerManagerModule.Abstract;
using STE.WebSell.Application.CustomerManagerModule.Dtos.Group;
using STE.WebSell.Application.CustomerManagerModule.Dtos.GroupCustomerBusinessCustomer;
using STE.WebSell.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.CustomerManagerModule.Implement
{
    public class GroupCustomerService : ServiceBase, IGroupCustomerService
    {
        public GroupCustomerService(ILogger<GroupCustomerService> logger, IHttpContextAccessor httpContext) : base(logger, httpContext) { }

        public GroupCustomerDto Create(CreateGroupCustomerDto input)
        {
            var transaction = _dbContext.Database.BeginTransaction();
            var inputInsert = _dbContext.GroupCustomers.Add(_mapper.Map<GroupCustomer>(input)).Entity;
            _dbContext.SaveChanges();
            foreach (var item in input.BusinessCustomerIds)
            {
                //lay thong tin cua doanh nghiep
                var businessCustomer = _dbContext.BusinessCustomers.FirstOrDefault(b => b.Id == item && !b.Deleted)
                    ?? throw new UserFriendlyException(ErrorCode.BusinessCustomerNotFound);

                    _dbContext.GroupCustomerBusinessCustomers.Add(new GroupCustomerBusinessCustomer
                    {
                        GroupCustomerId = inputInsert.Id,
                        BusinessCustomerId = item,
                    });
                    _dbContext.SaveChanges();
            }
         
            transaction.Commit();

            return _mapper.Map<GroupCustomerDto>(inputInsert);
        }

        public GroupCustomerDto Update(UpdateGroupCustomerDto input)
        {
            var groupCustomer = _dbContext.GroupCustomers.FirstOrDefault(g => !g.Deleted && g.Id == input.Id)
                    ?? throw new UserFriendlyException(ErrorCode.GroupCustomerNotFound);
            var isValidBusinessCustomerIds = input.BusinessCustomerIds?.All(id =>
            _dbContext.BusinessCustomers.Any(c => c.Id == id)) ?? false;
            if (!isValidBusinessCustomerIds)
            {
                throw new UserFriendlyException(ErrorCode.BusinessCustomerNotFound);
            }

            _mapper.Map(input, groupCustomer);

            var oldBusinessCustomerIds = _dbContext.GroupCustomerBusinessCustomers
                .Where(t => t.GroupCustomerId == input.Id)
                .Select(x => x.BusinessCustomerId)
                .ToList();

            var newBusinessCustomerIds = input.BusinessCustomerIds ?? new List<int>();

            // Tìm các business customers cần xóa
            var removedBusinessCustomerIds = oldBusinessCustomerIds.Except(newBusinessCustomerIds).ToList();

            // Tìm các business customers cần thêm mới
            var addedBusinessCustomerIds = newBusinessCustomerIds.Except(oldBusinessCustomerIds).ToList();

            // Xóa các business customers không còn được truyền vào
            if (removedBusinessCustomerIds.Any())
            {
                var removedBusinessCustomers = _dbContext.GroupCustomerBusinessCustomers
                    .Where(x => x.GroupCustomerId == input.Id && removedBusinessCustomerIds.Contains(x.BusinessCustomerId))
                    .ToList();

                _dbContext.GroupCustomerBusinessCustomers.RemoveRange(removedBusinessCustomers);
            }

            // Thêm các business customers mới
            if (addedBusinessCustomerIds.Any())
            {
                var addedBusinessCustomers = addedBusinessCustomerIds.Select(x => new GroupCustomerBusinessCustomer
                {
                    GroupCustomerId = input.Id,
                    BusinessCustomerId = x
                });

                _dbContext.GroupCustomerBusinessCustomers.AddRange(addedBusinessCustomers);
            }

            _dbContext.SaveChanges();

            return _mapper.Map<GroupCustomerDto>(groupCustomer);
        }

        public GroupCustomerDto FindById(int id)
        {
            var groupCustomer = _dbContext.GroupCustomers.FirstOrDefault(g => !g.Deleted && g.Id == id)
                                ?? throw new UserFriendlyException(ErrorCode.GroupCustomerNotFound);
            var result = _mapper.Map<GroupCustomerDto>(groupCustomer);
            result.BusinessCustomerIds = _dbContext.GroupCustomerBusinessCustomers.Where(g => g.GroupCustomerId == id && !g.Deleted).Select(r => r.BusinessCustomerId).ToList();
           
            if (result == null)
            {
                throw new UserFriendlyException(ErrorCode.GroupCustomerNotFound);
            }
            return result;
        }

        public PagingResult<GroupCustomerDto> FindAll(FilterGroupCustomerDto input)
        {
            var result = new PagingResult<GroupCustomerDto>();
            var groupCustomerQuery = _dbContext.GroupCustomers
                                               .Where(b => !b.Deleted
                                                      && (input.Keyword == null
                                                          || b.Name.ToLower().Contains(input.Keyword.ToLower())));

            result.TotalItems = groupCustomerQuery.Count();

            groupCustomerQuery = groupCustomerQuery.OrderDynamic(input.Sort);

            if (input.PageSize != -1)
            {
                groupCustomerQuery = groupCustomerQuery.Skip(input.GetSkip()).Take(input.PageSize);
            }

            result.Items = _mapper.Map<IEnumerable<GroupCustomerDto>>(groupCustomerQuery);
            foreach (var item in result.Items)
            {
                item.BusinessCustomerIds = _dbContext.GroupCustomerBusinessCustomers.Where(g => g.GroupCustomerId == item.Id && !g.Deleted).Select(r => r.BusinessCustomerId).ToList();
            }
            return result;
        }

        public void Delete(int id)
        {
            var groupCustomer = _dbContext.GroupCustomers.FirstOrDefault(g => !g.Deleted && g.Id == id)
                                ?? throw new UserFriendlyException(ErrorCode.GroupCustomerNotFound);
            var groupCustomerBusinessCustomer = _dbContext.GroupCustomerBusinessCustomers.Where(g => g.GroupCustomerId == id && !g.Deleted);
            foreach (var item in groupCustomerBusinessCustomer)
            {
                item.Deleted = true;
            }
            groupCustomer.Deleted = true;
            _dbContext.SaveChanges();
        }
    }
}
