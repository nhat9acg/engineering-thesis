﻿using STE.ApplicationBase.Common;
using STE.WebSell.Application.CustomerManagerModule.Dtos.DeliveryAddress;

namespace STE.WebSell.Application.CustomerManagerModule.Abstract
{
    public interface IDeliveryAddressService
    {
        PagingResult<DeliveryAddressDto> FindAllDeliveryAddress(FilterDeliveryAddressDto input);
        /// <summary>
        /// Tạo địa chỉ giao hàng
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        DeliveryAddressDto Create(CreateDeliveryAddressDto input);

        /// <summary>
        /// Xóa địa chỉ giao hàng
        /// </summary>
        /// <param name="id"></param>
        void Delete(int id);

        /// <summary>
        /// lấy danh sách địa chỉ giao hàng
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        IEnumerable<DeliveryAddressDto> FindByRestaurant(int restaurantId);

        /// <summary>
        /// cập nhập thông tin địa chỉ giao hàng
        /// </summary>
        /// <param name="input"></param>
        void Update(UpdateDeliveryAddressDto input);

        /// <summary>
        /// cập nhập mặc định
        /// </summary>
        /// <param name="deliveryAddressId"></param>
        /// <param name="isDefault"></param>
        void UpdateDefault(int deliveryAddressId, bool isDefault);
    }
}
