﻿using STE.ApplicationBase.Common;
using STE.WebSell.Application.Common;
using STE.WebSell.Application.CustomerManagerModule.Dtos.Group;
using STE.WebSell.Application.CustomerManagerModule.Dtos.GroupCustomerBusinessCustomer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.CustomerManagerModule.Abstract
{
    public interface IGroupCustomerService
    {
        GroupCustomerDto Create(CreateGroupCustomerDto input);
        void Delete(int id);
        PagingResult<GroupCustomerDto> FindAll(FilterGroupCustomerDto input);
        GroupCustomerDto FindById(int id);
        GroupCustomerDto Update(UpdateGroupCustomerDto input);
    }
}
