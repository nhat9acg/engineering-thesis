﻿using STE.ApplicationBase.Common;
using STE.WebSell.Application.CustomerManagerModule.Dtos.Restaurant;

namespace STE.WebSell.Application.CustomerManagerModule.Abstract
{
    public interface IRestaurantService
    {
        /// <summary>
        /// Tạo thông tin nhà hàng
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        RestaurantDto Create(CreateRestaurantDto input);

        /// <summary>
        /// Xóa nhà hàng
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        void Delete(int id);

        /// <summary>
        /// xem phân trang nhà hàng
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        PagingResult<RestaurantDto> FindAll(FilterRestaurantDto input);

        /// <summary>
        /// Tìm kiếm nhà hàng theo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        RestaurantDto FindById(int id);

        /// <summary>
        /// Cập nhập thông tin nhà hàng
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        RestaurantDto Update(UpdateRestaurantDto input);
        RestaurantDto GetCurrentInfoRestaurant();
    }
}
