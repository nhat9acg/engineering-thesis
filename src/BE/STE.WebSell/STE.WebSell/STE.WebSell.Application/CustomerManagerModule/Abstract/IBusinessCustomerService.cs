﻿using STE.ApplicationBase.Common;
using STE.WebSell.Application.CustomerManagerModule.Dtos.BusinessCustomer;

namespace STE.WebSell.Application.CustomerManagerModule.Abstract
{
    public interface IBusinessCustomerService
    {
        /// <summary>
        /// Tạo thông tin khách hàng doanh nghiệp
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        BusinessCustomerDto Create(CreateBusinessCustomerDto input);

        /// <summary>
        /// Xóa doanh nghiệp
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        void Delete(int id);

        /// <summary>
        /// Find all phân trang doanh nghiệp
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        PagingResult<BusinessCustomerDto> FindAll(FilterBusinessCustomerDto input);

        /// <summary>
        /// Tìm doanh nghiệp theo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        BusinessCustomerDto FindById(int id);

        /// <summary>
        /// Cập nhập doanh nghiệp
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        BusinessCustomerDto Update(UpdateBusinessCustomerDto input);
        BusinessCustomerDto GetCurrentInfoBusinessCustomer();
    }
}
