﻿using STE.WebSell.Application.AuthenticationModule.Abstract;
using STE.WebSell.Application.CustomerManagerModule.Dtos.DeliveryAddress;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.CustomerManagerModule.Abstract
{
    public interface IDeliveryAddressCustomerService : IDeliveryAddressService
    {
        IEnumerable<DeliveryAddressDto> FindByUser(int userId);
    }
}
