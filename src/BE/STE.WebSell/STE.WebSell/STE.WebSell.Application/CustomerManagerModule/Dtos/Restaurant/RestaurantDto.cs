﻿using STE.WebSell.Application.CustomerManagerModule.Dtos.BusinessCustomer;
using STE.WebSell.Application.CustomerManagerModule.Dtos.DeliveryAddress;

namespace STE.WebSell.Application.CustomerManagerModule.Dtos.Restaurant
{
    public class RestaurantDto
    {
        public int Id { get; set; }
        public int BusinessCustomerId { get; set; }
        public int Status { get; set; }
        public BusinessCustomerDto BusinessCustomer { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
    }
}
