﻿using STE.ApplicationBase.Common.Validations;

namespace STE.WebSell.Application.CustomerManagerModule.Dtos.Restaurant
{
    public class CreateRestaurantDto
    {
        public int BusinessCustomerId { get; set; }
        public int Status { get; set; }

        private string _name;
        [CustomRequired(AllowEmptyStrings = false)]
        public string Name
        {
            get => _name; 
            set => _name = value?.Trim();
        }

        private string _phone;
        [CustomRequired(AllowEmptyStrings = false)]
        public string Phone
        {
            get => _phone;
            set => _phone = value?.Trim();
        }

        private string _email;
        [CustomRequired(AllowEmptyStrings = false)]
        public string Email
        {
            get => _email;
            set => _email = value?.Trim();
        }

        private string _address;
        /// <summary>
        /// địa chỉ
        /// </summary>
        [CustomRequired(AllowEmptyStrings = false)]
        public string Address
        {
            get => _address;
            set => _address = value?.Trim();
        }
    }
}
