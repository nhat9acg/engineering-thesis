﻿using STE.ApplicationBase.Common;

namespace STE.WebSell.Application.CustomerManagerModule.Dtos.Restaurant
{
    /// <summary>
    /// Filter cho doanh nghiệp tự xem nhà hàng
    /// </summary>
    public class FilterRestaurantByBusinessCustomerDto : PagingRequestBaseDto
    {
    }
}
