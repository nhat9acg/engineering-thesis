﻿using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;

namespace STE.WebSell.Application.CustomerManagerModule.Dtos.Restaurant
{
    public class FilterRestaurantDto : PagingRequestBaseDto
    {
        [FromQuery(Name = "businessCustomerId")]
        public int? BusinessCustomerId { get; set; }
    }
}
