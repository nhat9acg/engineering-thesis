﻿namespace STE.WebSell.Application.CustomerManagerModule.Dtos.Restaurant
{
    public class UpdateRestaurantDto : CreateRestaurantDto
    {
        public int Id { get; set; }
    }
}
