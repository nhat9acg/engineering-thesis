﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ConstrainedExecution;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.CustomerManagerModule.Dtos.GroupCustomerBusinessCustomer
{
    public class CreateGroupCustomerBusinessCustomerDto
    {
        public int GroupCustomerId { get; set; }
        public List<int> BusinessCustomerIds { get; set; }
    }
}
