﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.CustomerManagerModule.Dtos.GroupCustomerBusinessCustomer
{
    public class UpdateGroupCustomerBusinessCustomerDto : CreateGroupCustomerBusinessCustomerDto
    {
        public int Id { get; set; }
    }
}
