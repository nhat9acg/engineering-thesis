﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.CustomerManagerModule.Dtos.GroupCustomerBusinessCustomer
{
    public class GroupCustomerBusinessCustomerDto
    {
        public int Id { get; set; }
        public int BusinessCustomerId { get; set; }
        public int GroupCustomerId { get; set; }
    }
}
