﻿using STE.WebSell.Application.CustomerManagerModule.Dtos.GroupCustomerBusinessCustomer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.CustomerManagerModule.Dtos.Group
{
    public class GroupCustomerDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<int> BusinessCustomerIds { get; set; }
    }
}
