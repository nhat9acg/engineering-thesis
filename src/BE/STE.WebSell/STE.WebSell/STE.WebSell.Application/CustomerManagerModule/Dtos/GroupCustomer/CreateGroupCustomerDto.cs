﻿using STE.ApplicationBase.Common.Validations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.CustomerManagerModule.Dtos.Group
{
    public class CreateGroupCustomerDto
    {
        [CustomRequired(AllowEmptyStrings = false)]
        private string _name;
        public string Name 
        { 
            get => _name; 
            set => _name = value?.Trim(); 
        }
        public List<int> BusinessCustomerIds { get; set; }

    }
}