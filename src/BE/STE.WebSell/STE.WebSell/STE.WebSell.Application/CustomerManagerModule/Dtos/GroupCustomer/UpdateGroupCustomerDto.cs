﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.CustomerManagerModule.Dtos.Group
{
    public class UpdateGroupCustomerDto : CreateGroupCustomerDto
    {
        public int Id { get; set; }
    }
}
