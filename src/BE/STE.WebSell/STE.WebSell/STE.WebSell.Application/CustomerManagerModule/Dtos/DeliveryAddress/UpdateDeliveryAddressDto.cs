﻿namespace STE.WebSell.Application.CustomerManagerModule.Dtos.DeliveryAddress
{
    public class UpdateDeliveryAddressDto : CreateDeliveryAddressDto
    {
        public int Id { get; set; }
    }
}
