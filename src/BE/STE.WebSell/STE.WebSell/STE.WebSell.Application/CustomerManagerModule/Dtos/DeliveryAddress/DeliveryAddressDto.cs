﻿using STE.ApplicationBase.Common.Validations;
using STE.WebSell.Application.AuthenticationModule.Dtos.UserDto;
using STE.WebSell.Application.CustomerManagerModule.Dtos.Restaurant;

namespace STE.WebSell.Application.CustomerManagerModule.Dtos.DeliveryAddress
{
    public class DeliveryAddressDto
    {
        public int Id { get; set; }

        /// <summary>
        /// Id nhà hàng
        /// </summary>
        public int? RestaurantId { get; set; }

        public RestaurantDto Restaurant { get; set; }

        public int? UserId { get; set; }

        public UserDto User { get; set; }

        /// <summary>
        /// Địa chỉ giao hàng của nhà hàng
        /// </summary>
        private string _address;

        private string _receiver;

        private string _phone;

        public string Address 
        { 
            get => _address; 
            set => _address = value?.Trim(); 
        }

        public string Receiver
        {
            get => _receiver;
            set => _receiver = value?.Trim();
        }

        public string Phone
        {
            get => _phone;
            set => _phone = value?.Trim();
        }

        /// <summary>
        /// Có là mặc định
        /// </summary>
        public bool IsDefault { get; set; }
    }
}
