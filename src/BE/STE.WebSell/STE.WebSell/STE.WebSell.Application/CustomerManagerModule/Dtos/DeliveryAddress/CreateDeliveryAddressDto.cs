﻿using STE.ApplicationBase.Common.Validations;

namespace STE.WebSell.Application.CustomerManagerModule.Dtos.DeliveryAddress
{
    public class CreateDeliveryAddressDto
    {
        /// <summary>
        /// Id của user
        /// </summary>
        public int? UserId { get; set; }
        /// <summary>
        /// Id của nhà hàng
        /// </summary>
        public int? RestaurantId { get; set; }
        public bool IsDefault { get; set; }

        private string _address;

        private string _receiver;

        private string _phone;
        /// <summary>
        /// Địa chỉ giao hàng của nhà hàng
        /// </summary>
        [CustomRequired(AllowEmptyStrings = false)]
        public string Address
        {
            get => _address;
            set => _address = value?.Trim();
        }

        [CustomRequired(AllowEmptyStrings = false)]
        public string Receiver
        {
            get => _receiver;
            set => _receiver = value?.Trim();
        }

        [CustomRequired(AllowEmptyStrings = false)]
        public string Phone
        {
            get => _phone;
            set => _phone = value?.Trim();
        }

    }
}
