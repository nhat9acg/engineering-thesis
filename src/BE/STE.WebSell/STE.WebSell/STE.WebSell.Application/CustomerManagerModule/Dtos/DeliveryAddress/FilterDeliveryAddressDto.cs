﻿using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.CustomerManagerModule.Dtos.DeliveryAddress
{
    public class FilterDeliveryAddressDto : PagingRequestBaseDto
    {
        [FromQuery(Name = "restaurantId")]
        public int? RestaurantId { get; set; }
        [FromQuery(Name = "userId")]
        public int? UserId { get; set; }
    }
}
