﻿using STE.ApplicationBase.Common.Validations;

namespace STE.WebSell.Application.CustomerManagerModule.Dtos.BusinessCustomer
{
    public class CreateBusinessCustomerDto
    {
        private string _fullName;
        /// <summary>
        /// Tên đầy đủ của khách hàng doanh nghiệp
        /// </summary>
        [CustomRequired(AllowEmptyStrings = false)]
        public string FullName
        {
            get => _fullName;
            set => _fullName = value?.Trim();
        }

        private string _address;
        /// <summary>
        /// Tên viết tắt
        /// </summary>
        [CustomRequired(AllowEmptyStrings = false)]
        public string Address
        {
            get => _address;
            set => _address = value?.Trim();
        }

        private string _taxCode;
        /// <summary>
        /// Mã số thuế
        /// </summary>
        public string TaxCode
        {
            get => _taxCode;
            set => _taxCode = value?.Trim();
        }

        private string _phone;
        /// <summary>
        /// Số điện thoại
        /// </summary>
        
        [CustomRequired(AllowEmptyStrings = false)]
        public string Phone
        {
            get => _phone;
            set => _phone = value?.Trim();
        }

        private string _email;
        /// <summary>
        /// Địa chỉ email
        /// </summary>
        public string Email
        {
            get => _email;
            set => _email = value?.Trim();
        }

        private string _language;
        /// <summary>
        /// Ngôn ngữ
        /// </summary>
        public string Language
        {
            get => _language;
            set => _language = value?.Trim();
        }
        public bool isRestaurant { get; set; }
    }
}
