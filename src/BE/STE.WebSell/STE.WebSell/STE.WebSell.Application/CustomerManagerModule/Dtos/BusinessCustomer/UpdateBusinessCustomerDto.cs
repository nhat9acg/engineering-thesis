﻿using Microsoft.AspNetCore.Mvc;

namespace STE.WebSell.Application.CustomerManagerModule.Dtos.BusinessCustomer
{
    public class UpdateBusinessCustomerDto : CreateBusinessCustomerDto
    {
        public int Id { get; set; }
    }
}
