﻿namespace STE.WebSell.Application.CustomerManagerModule.Dtos.BusinessCustomer
{
    public class BusinessCustomerDto
    {
        public int Id { get; set; }

        /// <summary>
        /// Tên đầy đủ của khách hàng doanh nghiẹp
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Địa chỉ
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Mã số thuế
        /// </summary>
        public string TaxCode { get; set; }

        /// <summary>
        /// Số điện thoại
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Địa chỉ email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Ngôn ngữ
        /// </summary>
        public string Language { get; set; }
    }
}
