﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using STE.ApplicationBase;
using STE.WebSell.Infrastructure.Persistence;

namespace STE.WebSell.Application.Common
{
    public abstract class ServiceBase : ServiceBase<WebSellDbContext>
    {
        protected ServiceBase(ILogger logger, IHttpContextAccessor httpContext) : base(logger, httpContext)
        {
        }
    }
}
