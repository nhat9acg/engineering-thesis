﻿using Microsoft.AspNetCore.Http;
using STE.Utils.Localization;

namespace STE.WebSell.Application.Common.Localization
{
    public class JVFLocalization : LocalizationBase
    {
        public JVFLocalization(IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor, "STE.Application.Common.Localization.SourceFiles")
        {
        }
    }
}
