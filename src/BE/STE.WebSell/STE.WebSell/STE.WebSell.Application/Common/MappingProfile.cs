﻿using AutoMapper;
using STE.WebSell.Application.AuthenticationModule.Dtos.RoleDto;
using STE.WebSell.Application.AuthenticationModule.Dtos.UserDto;
using STE.WebSell.Application.CartModule.Dtos;
using STE.WebSell.Application.ChatModule.Dtos;
using STE.WebSell.Application.CustomerManagerModule.Dtos.BusinessCustomer;
using STE.WebSell.Application.CustomerManagerModule.Dtos.DeliveryAddress;
using STE.WebSell.Application.CustomerManagerModule.Dtos.Group;
using STE.WebSell.Application.CustomerManagerModule.Dtos.GroupCustomerBusinessCustomer;
using STE.WebSell.Application.CustomerManagerModule.Dtos.Restaurant;
using STE.WebSell.Application.DocumentModule.Dtos.Document;
using STE.WebSell.Application.DocumentModule.Dtos.DocumentBusinessCustomer;
using STE.WebSell.Application.NotificationModule.Dtos;
using STE.WebSell.Application.OrderModule.Dtos.Order;
using STE.WebSell.Application.OrderModule.Dtos.OrderBusinessCustomer;
using STE.WebSell.Application.OrderModule.Dtos.OrderCustomer;
using STE.WebSell.Application.OrderModule.Dtos.OrderRestaurant;
using STE.WebSell.Application.ProductModule.Dtos.CalculationUnit;
using STE.WebSell.Application.ProductModule.Dtos.Product;
using STE.WebSell.Application.ProductModule.Dtos.ProductCategory;
using STE.WebSell.Application.ProductModule.Dtos.ProductPrice;
using STE.WebSell.Application.ShippingProviderModule.Dtos;
using STE.WebSell.Domain.Entities;

namespace STE.WebSell.Application.Common
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            #region Role Permission
            CreateMap<RoleDto, Role>().ReverseMap();
            CreateMap<CreateRolePermissionDto, Role>().ReverseMap();

            #endregion

            #region Business Customer
            CreateMap<CreateBusinessCustomerDto, BusinessCustomer>().ReverseMap();
            CreateMap<BusinessCustomerDto, BusinessCustomer>().ReverseMap();
            CreateMap<UpdateBusinessCustomerDto, BusinessCustomer>().ReverseMap();
            #endregion

            #region Restaurant
            CreateMap<CreateRestaurantDto, Restaurant>().ReverseMap();
            CreateMap<UpdateRestaurantDto, Restaurant>().ReverseMap();
            CreateMap<RestaurantDto, Restaurant>().ReverseMap();
            CreateMap<Restaurant, RestaurantDto>()
                        .ForMember(dest => dest.BusinessCustomer, opt => opt.MapFrom(src => src.BusinessCustomer)).ReverseMap();
            CreateMap<FilterRestaurantByBusinessCustomerDto, FilterRestaurantDto>();
            #endregion

            #region Delivery Address
            CreateMap<CreateDeliveryAddressDto, DeliveryAddress>().ReverseMap();
            CreateMap<UpdateDeliveryAddressDto, DeliveryAddress>().ReverseMap();
            CreateMap<DeliveryAddressDto, DeliveryAddress>().ReverseMap();
            #endregion

            #region Group customer
            CreateMap<GroupCustomerDto, GroupCustomer>().ReverseMap();
            CreateMap<CreateGroupCustomerDto, GroupCustomer>().ReverseMap();
            CreateMap<UpdateGroupCustomerDto, GroupCustomer>().ReverseMap();
            #endregion

            #region Group Customer Business Customer
            CreateMap<CreateGroupCustomerBusinessCustomerDto, GroupCustomerBusinessCustomer>().ReverseMap();
            CreateMap<UpdateGroupCustomerBusinessCustomerDto, GroupCustomerBusinessCustomer>().ReverseMap();
            CreateMap<GroupCustomerBusinessCustomerDto, GroupCustomerBusinessCustomer>().ReverseMap();
            #endregion

            #region Product category
            CreateMap<ProductCategoryDto, ProductCategory>().ReverseMap();
            CreateMap<CreateProductCategoryDto, ProductCategory>().ReverseMap();
            CreateMap<UpdateProductCategoryDto, ProductCategory>().ReverseMap();
            CreateMap<ParentProductCategoryDto, ProductCategory>().ReverseMap();
            CreateMap<ViewProductCategoryDto, ProductCategory>().ReverseMap();
            #endregion

            #region Product
            CreateMap<Product, ProductDto>()
                .ForMember(p => p.CalculationUnitName, opt => opt.MapFrom(c => c.CalculationUnit.Name))
                .ForMember(p => p.ProductCategoryName, opt => opt.MapFrom(c => c.ProductCategory.Name)).ReverseMap();
            CreateMap<CreateProductDto, Product>().ReverseMap();
            CreateMap<UpdateProductDto, Product>().ReverseMap();
            //CreateMap<ViewProductDto, Product>().ReverseMap();
            #endregion

            #region Product Price
            CreateMap<ProductPriceDto, ProductPrice>().ReverseMap();
            CreateMap<CreateProductPriceDto, ProductPrice>().ReverseMap();
            CreateMap<UpdateProductPriceDto, ProductPrice>().ReverseMap();
            CreateMap<FilterProductPriceBusinessCustomerDto, FilterProductDto>().ReverseMap();
            #endregion

            CreateMap<CreateUserDto, User>().ReverseMap();
            CreateMap<UpdateUserDto, User>().ReverseMap();
            CreateMap<UserDto, User>().ReverseMap();

            #region Order & OrderDetail
            CreateMap<CreateOrderDto, Order>().ReverseMap();
            CreateMap<UpdateOrderDto, Order>().ReverseMap();

            CreateMap<Order, CustomerOrderDto>()
              .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.User.Id))
              .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.User.FullName))
              .ForMember(dest => dest.DeliveryAddressId, opt => opt.MapFrom(src => src.DeliveryAddress.Id))
              .ForMember(dest => dest.DeliveryAddress, opt => opt.MapFrom(src => src.DeliveryAddress.Address))
              .ForMember(dest => dest.ShippingProviderName, opt => opt.MapFrom(src => src.ShippingProvider.Name)).ReverseMap();

            CreateMap<Order, OrderDto>()
                .ForMember(dest => dest.BusinessCustomerId, opt => opt.MapFrom(src => src.Restaurant.BusinessCustomer.Id))
                .ForMember(dest => dest.BusinessCustomerName, opt => opt.MapFrom(src => src.Restaurant.BusinessCustomer.FullName))
                .ForMember(dest => dest.RestaurantId, opt => opt.MapFrom(src => src.Restaurant.Id))
                .ForMember(dest => dest.RestaurantName, opt => opt.MapFrom(src => src.Restaurant.Name))
                .ForMember(dest => dest.DeliveryAddressId, opt => opt.MapFrom(src => src.DeliveryAddress.Id))
                .ForMember(dest => dest.DeliveryAddress, opt => opt.MapFrom(src => src.DeliveryAddress.Address))
                .ForMember(dest => dest.ShippingProviderName, opt => opt.MapFrom(src => src.ShippingProvider.Name)).ReverseMap();

            CreateMap<CreateOrderDetailDto, OrderDetail>().ReverseMap();
            CreateMap<OrderDetail, OrderDetailDto>()
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.Product.Name))
                .ForMember(dest => dest.ProductCategoryName, opt => opt.MapFrom(src => src.Product.ProductCategory.Name))
                .ForMember(dest => dest.CalculationUnit, opt => opt.MapFrom(src => src.Product.CalculationUnit.Name))
                .ReverseMap();

            CreateMap<FilterOrderBusinessCustomerDto, FilterOrderDto>().ReverseMap();
            CreateMap<CreateOrderBusinessCustomerDto, CreateOrderDto>()
                .ForMember(dest => dest.OrderDetails, opt => opt.MapFrom(src => src.OrderDetails))
                .ReverseMap();
            CreateMap<UpdateOrderBusinessCustomerDto, UpdateOrderDto>().ReverseMap();

            CreateMap<FilterOrderRestaurantDto, FilterOrderDto>().ReverseMap();
            CreateMap<CreateOrderRestaurantDto, CreateOrderDto>()
                .ForMember(dest => dest.OrderDetails, opt => opt.MapFrom(src => src.OrderDetails))
                .ReverseMap();
            CreateMap<UpdateOrderRestaurantDto, UpdateOrderDto>().ReverseMap();
            CreateMap<CreateOrderDetailRestaurantDto, CreateOrderDetailDto>().ReverseMap();
            #endregion

            #region Calculation Unit
            CreateMap<CalculationUnit, CreateCalculationUnitDto>().ReverseMap();
            CreateMap<CalculationUnit, UpdateCalculationUnitDto>().ReverseMap();
            CreateMap<CalculationUnit, CalculationUnitDto>().ReverseMap();
            #endregion

            CreateMap<ShippingProvider, ShippingProviderDto>().ReverseMap();
            CreateMap<ShippingProvider, CreateShippingProviderDto>().ReverseMap();
            CreateMap<ShippingProviderDto, CreateShippingProviderDto>().ReverseMap();
            CreateMap<ShippingProvider, UpdateShippingProviderDto>().ReverseMap();
            CreateMap<HistoryUpdate, CancelOrResponseOrderDto>().ReverseMap();
            CreateMap<HistoryUpdate, HistoryUpdateOrderDto>().ReverseMap();

            CreateMap<Document, DocumentDto>().ReverseMap();
            CreateMap<FilterDocumentBusinessCustomerDto, FilterDocumentDto>().ReverseMap();

            #region Cart
            CreateMap<Cart, CreateCartDto>().ReverseMap();
            CreateMap<Cart, UpdateCartDto>().ReverseMap();
            CreateMap<Cart, CartDto>().ReverseMap();
            #endregion

            CreateMap<Notification, ResponseNotificationDto>().ReverseMap();

            //CreateMap<Chat, RequestChatDto>().ReverseMap();
            //CreateMap<Chat, ResponseChatDto>().ReverseMap();
        }
    }
}
