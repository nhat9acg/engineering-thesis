﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using STE.ApplicationBase.Common;
using STE.ApplicationBase.CommonModule.Implements;
using STE.Utils.Linq;
using STE.WebSell.Application.ChatModule.Abstract;
using STE.WebSell.Application.ChatModule.Dtos;
using STE.WebSell.Application.Common;
using STE.WebSell.Application.NotificationModule.Abstract;
using STE.WebSell.Application.SignalR;
using STE.WebSell.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace STE.WebSell.Application.ChatModule.Implements
{
    public class ChatService : ServiceBase, IChatService
    {
        private readonly ISignalRBroadcastService _signalRBroadcastService;
        private readonly ISystemNotificationService _systemNotificationService;
        public ChatService(
                ILogger<ChatService> logger, 
                IHttpContextAccessor httpContext,
                ISystemNotificationService systemNotificationService,
                ISignalRBroadcastService signalRBroadcastService
            ) : base(logger, httpContext)
        {
            _signalRBroadcastService = signalRBroadcastService;
            _systemNotificationService = systemNotificationService;
        }

        public async Task Create(RequestChatDto input)
        {
            _logger.LogInformation($"{nameof(Create)}: input = {JsonSerializer.Serialize(input)}");
            var userId = CommonUtils.GetCurrentUserId(_httpContext);
            //var insert = _mapper.Map<Chat>(input);
            //insert.SenderUserId = userId;
            var codeFind = _dbContext.Chats.Where(e => !e.Deleted
                            && ((e.SenderUserId == userId && e.ReceiverUserId == input.ReceiverUserId)
                                || (e.ReceiverUserId == userId && e.SenderUserId == input.ReceiverUserId))
                            ).Select(e => e.Code).FirstOrDefault();
            var insert = new Chat
            {
                SenderUserId = userId,
                ReceiverUserId = input.ReceiverUserId,
                Message = input.Message
            };

            var result = _dbContext.Add(insert);
            if (codeFind != 0)
            {
                result.Entity.Code = codeFind;
            }
            else
            {
                result.Entity.Code = _dbContext.Chats.Where(e => !e.Deleted).Select(e => e.Code).DefaultIfEmpty(0).Max() + 1;
            }

            _dbContext.SaveChanges();
            await _signalRBroadcastService.BroadcastSendMessage(userId, result.Entity.Code);
            await _systemNotificationService.SendNotificationNewMessage(insert.ReceiverUserId);
        }

        public PagingResult<ChatGroup> FindByUserId(FilterChatDto input)
        {
            _logger.LogInformation($"{nameof(FindByUserId)}: input = {JsonSerializer.Serialize(input)}");
            var userId = CommonUtils.GetCurrentUserId(_httpContext);

            var query = _dbContext.Chats
                        .Where(sp => !sp.Deleted && (userId == sp.SenderUserId || userId == sp.ReceiverUserId))
                        .GroupBy(c => c.Code)
                        .Select(grp => new ChatGroup
                        {
                            Id = grp.OrderByDescending(e => e.Id).FirstOrDefault().Id,
                            Code = grp.Key,
                            SenderUserId = userId,
                            ReceiverUserName = _dbContext.Users.FirstOrDefault(e => !e.Deleted 
                                && (e.Id == grp.FirstOrDefault(e => e.SenderUserId == userId).ReceiverUserId 
                                    || e.Id == grp.FirstOrDefault(e => e.ReceiverUserId == userId).SenderUserId)).FullName,
                            ReceiverUserId = grp.Where(e => e.SenderUserId == userId).Select(e => e.ReceiverUserId).FirstOrDefault()
                                        != 0 ? grp.Where(e => e.SenderUserId == userId).Select(e => e.ReceiverUserId).FirstOrDefault()
                                        : grp.Where(e => e.ReceiverUserId == userId).Select(e => e.SenderUserId).FirstOrDefault(),
                            ChatDetail = grp.OrderByDescending(chat => chat.Id).Take(input.NumberMessage).OrderBy(chat => chat.Id)
                            .Select(e => new ChatDetailDto
                            {
                                Id = e.Id,
                                SenderUserId = e.SenderUserId,
                                ReceiverUserId = e.ReceiverUserId,
                                Message = e.Message,
                                SentByMe = (e.SenderUserId == userId),
                                Status = e.Status,
                                CreatedDate = e.CreatedDate
                            }).ToList()
                        });

            var result = new PagingResult<ChatGroup>();


            if (query.Count() == 0)
            {
                var insert = new Chat
                {
                    SenderUserId = 1,
                    ReceiverUserId = userId,
                    Message = "Xin chào quý khách, quý khách cần chúng tôi tư vấn gì ạ"
                };

                var resultAdd = _dbContext.Add(insert);
                var maxCode = _dbContext.Chats
                 .Where(e => !e.Deleted)
                 .Max(e => (int?)e.Code) ?? 0 + 1;
                resultAdd.Entity.Code = maxCode;
                _dbContext.SaveChanges();

                query = _dbContext.Chats
                        .Where(sp => !sp.Deleted && (userId == sp.SenderUserId || userId == sp.ReceiverUserId))
                        .GroupBy(c => c.Code)
                        .Select(grp => new ChatGroup
                        {
                            Id = grp.OrderByDescending(e => e.Id).FirstOrDefault().Id,
                            SenderUserId = userId,
                            ReceiverUserId = grp.Where(e => e.SenderUserId == userId).Select(e => e.ReceiverUserId).FirstOrDefault()
                                        != 0 ? grp.Where(e => e.SenderUserId == userId).Select(e => e.ReceiverUserId).FirstOrDefault()
                                        : grp.Where(e => e.ReceiverUserId == userId).Select(e => e.SenderUserId).FirstOrDefault(),
                            ChatDetail = grp.OrderByDescending(chat => chat.Id).Take(input.NumberMessage).OrderBy(chat => chat.Id).Select(e => new ChatDetailDto
                            {
                                Id = e.Id,
                                SenderUserId = e.SenderUserId,
                                ReceiverUserId = e.ReceiverUserId,
                                Message = e.Message,
                                SentByMe = (e.SenderUserId == userId),
                                Status = e.Status,
                                CreatedDate = e.CreatedDate
                            }).ToList()
                        });
            }
            //query = query.OrderDynamic(input.Sort);
            result.TotalItems = query.Count();
            if (input.PageSize != -1)
            {
                query = query.OrderByDescending(i => i.Id).Skip(input.GetSkip()).Take(input.PageSize);
            }
            result.Items = query;
            return result;
        }

        //public void ReadNotification(int notificationId)
        //{
        //    _logger.LogInformation($"{nameof(ReadNotification)}: notificationId = {notificationId}");
        //    var notificationFind = _dbContext.Notifications.FirstOrDefault(e => e.Id == notificationId && !e.Deleted) ?? throw new UserFriendlyException(ErrorCode.NotificationNotFound);
        //    if (notificationFind.IsRead)
        //    {
        //        return;
        //    }
        //    notificationFind.IsRead = true;
        //    _dbContext.SaveChanges();
        //}
    }
}
