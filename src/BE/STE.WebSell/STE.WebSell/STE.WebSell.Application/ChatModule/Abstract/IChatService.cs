﻿using STE.ApplicationBase.Common;
using STE.WebSell.Application.ChatModule.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.ChatModule.Abstract
{
    public interface IChatService
    {
        Task Create(RequestChatDto input);
        PagingResult<ChatGroup> FindByUserId(FilterChatDto input);
    }
}
