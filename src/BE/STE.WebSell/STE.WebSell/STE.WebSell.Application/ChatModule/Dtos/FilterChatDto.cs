﻿using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.ChatModule.Dtos
{
    public class FilterChatDto : PagingRequestBaseDto
    {
        [FromQuery(Name = "numberMessage")]
        public int NumberMessage { get; set; }
    }
}
