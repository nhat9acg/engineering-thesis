﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.ChatModule.Dtos
{
    public class RequestChatDto
    {
        public int ReceiverUserId { get; set; }
        public string Message { get; set; }
    }
}
