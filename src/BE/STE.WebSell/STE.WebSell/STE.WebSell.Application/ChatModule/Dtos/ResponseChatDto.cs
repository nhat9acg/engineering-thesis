﻿using STE.WebSell.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.ChatModule.Dtos
{
    public class ResponseChatDto
    {
        public int Id { get; set; }
        public int ReceiverUserId { get; set; }
        public int SenderUserId { get; set; }
        public string Message { get; set; }
        public bool SentByMe { get; set; }
        public int Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public IEnumerable<ChatDetailDto> ChatDetail { get; set; }
    }

    public class ChatDetailDto
    {
        public int Id { get; set; }
        public int ReceiverUserId { get; set; }
        public int SenderUserId { get; set; }
        public string Message { get; set; }
        public bool SentByMe { get; set; }
        public int Status { get; set; }
        public DateTime? CreatedDate { get; set; }
    }

    public class ChatGroup
    {
        public int SenderUserId { get; set; }
        public int ReceiverUserId { get; set; }
        public int Code { get; set; }
        public string ReceiverUserName { get; set; }
        public int Id { get; set; }
        public IEnumerable<ChatDetailDto> ChatDetail { get; set; }
    }
}
