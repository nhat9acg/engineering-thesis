﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using STE.ApplicationBase.Common;
using STE.ApplicationBase.CommonModule.Implements;
using STE.Utils.ConstantVariables.User;
using STE.Utils.Linq;
using STE.WebSell.Application.Common;
using STE.WebSell.Application.DashboardModule.Abstract;
using STE.WebSell.Application.DashboardModule.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace STE.WebSell.Application.DashboardModule.Implement
{
    public class DashboardService : ServiceBase, IDashboardService
    {
        public DashboardService(ILogger<DashboardService> logger, IHttpContextAccessor httpContext) : base(logger, httpContext)
        {
        }

        public PagingResult<DashboardCustomerOrderDto> CustomerOverview(FilterDashboardCustomerDto input)
        {
            var userType = CommonUtils.GetCurrentUserType(_httpContext);
            int? restaurantId = null;
            int? businessCustomerId = null;
            if (userType == UserTypes.BUSINESS_CUSTOMER)
            {
                businessCustomerId = CommonUtils.GetCurrentBusinessCustomerId(_httpContext);
            }
            else if (userType == UserTypes.RESTAURANT)
            {
                restaurantId = CommonUtils.GetCurrentRestaurantId(_httpContext);
            }
            _logger.LogInformation($"{nameof(CustomerOverview)}: input = {JsonSerializer.Serialize(input)}, userType = {userType}, restaurantId = {restaurantId}, businessCustomerId = {businessCustomerId}");
            var result = new PagingResult<DashboardCustomerOrderDto>();
            var orderQuery = _dbContext.Restaurants
                             .Include(r => r.BusinessCustomer)
                             .Include(r => r.Orders)
                             .Where(o => !o.Deleted
                             && ((restaurantId != null && o.Id == restaurantId)
                             || (businessCustomerId != null && o.BusinessCustomerId == businessCustomerId)
                             || (restaurantId == null && businessCustomerId == null)
                             ))
                             .Select(g => new DashboardCustomerOrderDto
                             {
                                 RestaurantName = g.Name,
                                 BusinessCustomerName = g.BusinessCustomer.FullName,
                                 TotalValue = _dbContext.OrderDetails.Where(od => od.Order.RestaurantId == g.Id).Sum(od => od.Quantity * od.UnitPrice)
                             });

            result.TotalItems = orderQuery.Count();
            orderQuery = orderQuery.OrderDynamic(input.Sort);

            if (input.PageSize != -1)
            {
                orderQuery = orderQuery.Skip(input.GetSkip()).Take(input.PageSize);
            }
            var listItem = new List<DashboardCustomerOrderDto>();

            result.Items = orderQuery;
            return result;
        }

        public DashboardDto Dashboard(FilterDashboardDto input)
        {
            var userType = CommonUtils.GetCurrentUserType(_httpContext);
            int? restaurantId = null;
            int? businessCustomerId = null;
            if (userType == UserTypes.BUSINESS_CUSTOMER)
            {
                businessCustomerId = CommonUtils.GetCurrentBusinessCustomerId(_httpContext);
            }
            else if (userType == UserTypes.RESTAURANT)
            {
                restaurantId = CommonUtils.GetCurrentRestaurantId(_httpContext);
            }
            _logger.LogInformation($"{nameof(Dashboard)}: input = {JsonSerializer.Serialize(input)}, userType = {userType}, restaurantId = {restaurantId}, businessCustomerId = {businessCustomerId}");

            //Số đơn hàng trong 1 ngày
            var orderInDay = _dbContext.Orders.Include(o => o.OrderDetails).Include(o => o.Restaurant).ThenInclude(r => r.BusinessCustomer)
                                              .Where(o => !o.Deleted
                                               && ((restaurantId != null && o.RestaurantId == restaurantId)
                                               || (businessCustomerId != null && o.Restaurant.BusinessCustomerId == businessCustomerId)
                                               || (restaurantId == null && businessCustomerId == null)))
                                              .Count();
            //Số tiền trong 1 ngày
            var moneyInDay = _dbContext.OrderDetails.Include(od => od.Order).ThenInclude(o => o.Restaurant).ThenInclude(r => r.BusinessCustomer)
                                                .Where(o => !o.Order.Deleted
                                                && ((restaurantId != null && o.Order.RestaurantId == restaurantId)
                                                || (businessCustomerId != null && o.Order.Restaurant.BusinessCustomerId == businessCustomerId)
                                                || (restaurantId == null && businessCustomerId == null)))
                                                .Sum(od => od.Quantity * od.UnitPrice);
            var orderQuery = _dbContext.Orders.Include(o => o.Restaurant).Include(o => o.OrderDetails)
                             .Where(o => !o.Deleted
                              && ((restaurantId != null && o.RestaurantId == restaurantId)
                             || (businessCustomerId != null && o.Restaurant.BusinessCustomerId == businessCustomerId)
                             || (restaurantId == null && businessCustomerId == null)
                             )
                             && (input.StartDate == null || o.OrderDate.Date >= input.StartDate.Value.Date)
                             && (input.EndDate == null || o.OrderDate.Date <= input.EndDate.Value.Date)
                             && (input.RestaurantId == null || o.RestaurantId == input.RestaurantId)
                             && (input.BusinessCustomerId == null || o.Restaurant.BusinessCustomerId == input.BusinessCustomerId)
                             && (input.Status == null || input.Status.Contains(o.Status)));
            //Số đơn hàng lũy kế
            var totalOrders = orderQuery.Count();
            //Số tiền lũy kế
            var totalMoney = _dbContext.OrderDetails.Include(od => od.Order).ThenInclude(o => o.Restaurant)
                              .Where(od => !od.Order.Deleted
                              && ((restaurantId != null && od.Order.RestaurantId == restaurantId)
                              || (businessCustomerId != null && od.Order.Restaurant.BusinessCustomerId == businessCustomerId)
                              || (restaurantId == null && businessCustomerId == null))
                              && (input.StartDate == null || od.Order.OrderDate.Date >= input.StartDate.Value.Date)
                              && (input.EndDate == null || od.Order.OrderDate.Date <= input.EndDate.Value.Date)
                              && (input.RestaurantId == null || od.Order.RestaurantId == input.RestaurantId)
                              && (input.BusinessCustomerId == null || od.Order.Restaurant.BusinessCustomerId == input.BusinessCustomerId)
                              && (input.Status == null || input.Status.Contains(od.Order.Status)))
                              .Sum(od => od.Quantity * od.UnitPrice);

            //Biểu đồ tiền
            var moneyChartQuery = orderQuery.Select(x => new
            {
                Date = x.OrderDate.Date,
                TotalValue = x.OrderDetails.Sum(x => x.Quantity * x.UnitPrice),
            });

            //Biểu đồ số đơn hàng
            var orderChartQuery = orderQuery.GroupBy(x => x.OrderDate.Date)
                            .Select(x => new
                            {
                                Date = x.Key,
                                TotalOrders = x.Count()
                            });

            //Nếu không truyền vào startDate và endDate thì mặc định xem của tháng hiện tại
            var startDate = input.StartDate == null ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1) : input.StartDate.Value;

            int lastDayOfMonth = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
            var endDate = input.EndDate == null ? new DateTime(DateTime.Now.Year, DateTime.Now.Month, lastDayOfMonth) : input.EndDate.Value;

            // Liệt kê ngày trong khoảng thời gian đã chọn
            var dates = Enumerable.Range(0, (endDate.Date - startDate.Date).Days + 1)
                      .Select(offset => startDate.Date.AddDays(offset));

            // Chèn thêm các ngày chưa có trong dữ liệu
            var moneyChart = dates.GroupJoin(
                                        moneyChartQuery,
                                        date => date,
                                        order => order.Date,
                                        (date, orderGroup) => new DashboardChartDto
                                        {
                                            Date = date,
                                            TotalValue = orderGroup.Sum(o => o.TotalValue),
                                        }).OrderBy(o => o.Date);
            var orderChart = dates.GroupJoin(
                                    orderChartQuery,
                                    date => date,
                                    order => order.Date,
                                    (date, orderGroup) => new DashboardChartDto
                                    {
                                        Date = date,
                                        TotalValue = orderGroup.Sum(o => o.TotalOrders)
                                    }).OrderBy(o => o.Date);

            return new DashboardDto
            {
                OrderInDay = orderInDay,
                MoneyInDay = moneyInDay,
                TotalMoney = totalMoney,
                OrderQuantity = totalOrders,
                OrderChart = orderChart,
                MoneyChart = moneyChart
            };
        }
    }
}
