﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.DashboardModule.Dtos
{
    public class DashboardDto
    {
        /// <summary>
        /// Số đơn hàng trong ngày
        /// </summary>
        public int OrderInDay { get; set; }
        /// <summary>
        /// Tổng số đơn hàng (Số đơn lũy kế)
        /// </summary>
        public int OrderQuantity { get; set; }
        /// <summary>
        /// Số tiền trong ngày
        /// </summary>
        public double MoneyInDay { get; set; }

        /// <summary>
        /// Số tiền lũy kế
        /// </summary>
        public double TotalMoney { get; set; }
        public IEnumerable<DashboardChartDto> OrderChart { get; set; }
        public IEnumerable<DashboardChartDto> MoneyChart { get; set; }
    }

    public class DashboardChartDto
    {
        public DateTime Date { get; set; }
        public double TotalValue { get; set; }
    }
}
