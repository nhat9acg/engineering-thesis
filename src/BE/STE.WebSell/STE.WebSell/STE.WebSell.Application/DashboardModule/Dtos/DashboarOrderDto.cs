﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.DashboardModule.Dtos
{
    public class DashboarOrderDto
    {
        public int Id { get; set; }
        public decimal OrderMoney { get; set; }
    }
}
