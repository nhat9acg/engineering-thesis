﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.DashboardModule.Dtos
{
    public class DashboardCustomerOrderDto
    {
        public string RestaurantName { get; set; }
        public string BusinessCustomerName { get; set; }
        public double TotalValue { get; set; }
    }
}
