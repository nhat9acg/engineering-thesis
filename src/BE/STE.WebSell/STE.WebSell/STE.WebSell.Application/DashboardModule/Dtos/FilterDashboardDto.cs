﻿using DocumentFormat.OpenXml.Wordprocessing;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.DashboardModule.Dtos
{
    public class FilterDashboardDto
    {
        [FromQuery(Name = "startDate")]
        public DateTime? StartDate { get; set; }
        [FromQuery(Name = "endDate")]
        public DateTime? EndDate { get; set; }
        [FromQuery(Name = "businessCustomerId")]
        public int? BusinessCustomerId { get; set; }
        [FromQuery(Name = "restaurantId")]
        public int? RestaurantId { get; set; }
        [FromQuery(Name = "status")]
        public List<int> Status { get; set; }
    }
}
