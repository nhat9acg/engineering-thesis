﻿using STE.ApplicationBase.Common;
using STE.WebSell.Application.DashboardModule.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.DashboardModule.Abstract
{
    public interface IDashboardService
    {
        public DashboardDto Dashboard(FilterDashboardDto input);
        public PagingResult<DashboardCustomerOrderDto> CustomerOverview(FilterDashboardCustomerDto input);
    }
}
