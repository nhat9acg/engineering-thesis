﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using STE.ApplicationBase.CommonModule.Implements;
using STE.Utils.RolePermission;
using STE.WebSell.Application.AuthenticationModule.Abstract;
using STE.WebSell.Application.AuthenticationModule.Dtos.PermissionDto;
using STE.WebSell.Application.AuthenticationModule.Dtos.RoleDto;
using STE.WebSell.Application.Common;

namespace STE.WebSell.Application.AuthenticationModule.Implements
{
    public class PermissionService : ServiceBase, IPermissionService
    {
        public PermissionService(ILogger<PermissionService> logger, IHttpContextAccessor httpContext) : base(logger, httpContext)
        {
        }

        public List<string> GetPermission()
        {
            var result = new List<string>();
            var userId = CommonUtils.GetCurrentUserId(_httpContext);
            var userType = CommonUtils.GetCurrentUserType(_httpContext);
            var userRoles = _dbContext.UserRoles.Where(e => !e.Deleted);

            var query = from userRole in userRoles
                        join role in _dbContext.Roles.Where(e => !e.Deleted) on userRole.RoleId equals role.Id
                        join rolePermission in _dbContext.RolePermissions on role.Id equals rolePermission.RoleId
                        where role.UserType == userType && userRole.UserId == userId
                        select rolePermission.PermissionKey;

            result = query.Distinct().ToList();
            return result;
        }

        public List<PermissionDetailDto> FindAllPermission()
        {
            var result = new List<PermissionDetailDto>();
            foreach (var item in PermissionConfig.Configs)
            {
                result.Add(new PermissionDetailDto
                {
                    Key = item.Key,
                    ParentKey = item.Value.ParentKey,
                    Label = L(item.Value.LName),
                    Icon = item.Value.Icon
                });
            }
            return result;
        }

        public List<PermissionDetailDto> CreateRolePermission(CreateRolePermissionDto input)
        {
            throw new NotImplementedException();
        }
    }
}
