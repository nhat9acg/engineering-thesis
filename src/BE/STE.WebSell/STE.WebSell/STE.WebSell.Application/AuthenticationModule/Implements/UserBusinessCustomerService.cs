﻿using EPIC.Utils.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using STE.ApplicationBase.CommonModule.Implements;
using STE.Utils.ConstantVariables.Shared;
using STE.Utils.ConstantVariables.User;
using STE.Utils.CustomException;
using STE.WebSell.Application.AuthenticationModule.Abstract;
using STE.WebSell.Application.AuthenticationModule.Dtos.UserDto;
using STE.WebSell.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace STE.WebSell.Application.AuthenticationModule.Implements
{
    public class UserBusinessCustomerService : UserService, IUserBusinessCustomerService
    {
        public UserBusinessCustomerService(ILogger<UserService> logger, IHttpContextAccessor httpContext) : base(logger, httpContext)
        {
        }

        public override void CreateUser(CreateUserDto input)
        {
            _logger.LogInformation($"{nameof(CreateUser)}: input = {JsonSerializer.Serialize(input)}");
            var userId = CommonUtils.GetCurrentUserId(_httpContext);
            input.Password = CryptographyUtils.CreateMD5(input.Password);
            var transaction = _dbContext.Database.BeginTransaction();
            var user = _mapper.Map<User>(input);


            if (input.Status == null)
            {
                user.Status = UserStatus.ACTIVE;
            }
            user.UserType = UserTypes.BUSINESS_CUSTOMER;

            var result = _dbContext.Users.Add(user);


            _dbContext.SaveChanges();

            //Thêm role
            if (input.RoleIds != null)
            {
                foreach (var item in input.RoleIds)
                {
                    var role = _dbContext.Roles.FirstOrDefault(e => e.Id == item) ?? throw new UserFriendlyException(ErrorCode.RoleNotFound);
                    _dbContext.UserRoles.Add(new UserRole
                    {
                        UserId = result.Entity.Id,
                        RoleId = item
                    });
                }
                _dbContext.SaveChanges();
            }
            transaction.Commit();
        }

    }
}
