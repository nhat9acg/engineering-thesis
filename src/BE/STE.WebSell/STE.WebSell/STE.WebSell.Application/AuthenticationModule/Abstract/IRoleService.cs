﻿using STE.ApplicationBase.Common;
using STE.WebSell.Application.AuthenticationModule.Dtos.RoleDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.AuthenticationModule.Abstract
{
    public interface IRoleService
    {
        RoleDto Add(CreateRolePermissionDto input);
        RoleDto Update(UpdateRolePermissionDto input);
        RoleDto FindById(int id);
        void Delete(int id);
        PagingResult<RoleDto> FindAll(FilterRoleDto input);
    }
}
