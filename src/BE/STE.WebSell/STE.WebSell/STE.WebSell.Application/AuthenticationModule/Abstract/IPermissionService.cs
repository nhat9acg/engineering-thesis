﻿using STE.Utils.RolePermission;
using STE.WebSell.Application.AuthenticationModule.Dtos.PermissionDto;
using STE.WebSell.Application.AuthenticationModule.Dtos.RoleDto;

namespace STE.WebSell.Application.AuthenticationModule.Abstract
{
    public interface IPermissionService
    {
        List<string> GetPermission();
        List<PermissionDetailDto> FindAllPermission();
    }
}
