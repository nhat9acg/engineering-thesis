﻿using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.AuthenticationModule.Dtos.RoleDto
{
    public class FilterRoleDto : PagingRequestBaseDto
    {
        [FromQuery(Name = "userType")]
        public int? UserType { get; set; }
    }
}
