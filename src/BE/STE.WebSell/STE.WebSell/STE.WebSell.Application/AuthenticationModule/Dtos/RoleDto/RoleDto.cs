﻿using STE.Utils.ConstantVariables.User;
using STE.WebSell.Application.AuthenticationModule.Dtos.PermissionDto;
using STE.WebSell.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.AuthenticationModule.Dtos.RoleDto
{
    public class RoleDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Tên Role
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// <see cref="UserTypes"/>
        /// </summary>
        public int UserType { get; set; }
        /// <summary>
        /// Mô tả
        /// </summary>
        public string Description { get; set; }
        public List<RolePermission> PermissionDetails { get; set; }
    }
}
