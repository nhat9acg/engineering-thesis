﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.AuthenticationModule.Dtos.RoleDto
{
    public class UpdateRolePermissionDto : CreateRolePermissionDto
    {
        public int Id { get; set; }
    }
}
