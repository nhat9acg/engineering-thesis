﻿namespace STE.WebSell.Application.AuthenticationModule.Dtos.UserDto
{
    public class UpdateUserDto : CreateUserDto
    {
        public int Id { get; set; }
    }
}
