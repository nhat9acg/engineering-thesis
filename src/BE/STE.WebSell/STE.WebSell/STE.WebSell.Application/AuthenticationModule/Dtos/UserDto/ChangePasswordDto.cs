﻿using STE.ApplicationBase.Common.Validations;

namespace STE.WebSell.Application.AuthenticationModule.Dtos.UserDto
{
    public class ChangePasswordDto
    {
        private string _oldPassword;
        [CustomMaxLength(128)]
        [CustomRequired(AllowEmptyStrings = false)]
        public string OldPassword
        {
            get => _oldPassword;
            set => _oldPassword = value?.Trim();
        }

        private string _newPassword;
        [CustomMaxLength(128)]
        [CustomRequired(AllowEmptyStrings = false)]
        public string NewPassword
        {
            get => _newPassword;
            set => _newPassword = value?.Trim();
        }
    }
}
