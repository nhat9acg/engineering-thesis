﻿using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;

namespace STE.WebSell.Application.AuthenticationModule.Dtos.UserDto
{
    public class FilterUserDto : PagingRequestBaseDto
    {
        [FromQuery(Name = "username")]
        public string Username { get; set; }
        [FromQuery(Name = "fullname")]
        public string FullName { get; set; }
        [FromQuery(Name = "status")]
        public int? Status { get; set; }
        [FromQuery(Name = "businessCustomerId")]
        public int? BusinessCustomerId { get; set; }
        [FromQuery(Name = "restaurantId")]
        public int? RestaurantId { get; set; }
    }
}
