﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using STE.ApplicationBase;
using STE.ApplicationBase.Common;
using STE.ApplicationBase.CommonModule.Implements;
using STE.Utils.ConstantVariables.Shared;
using STE.Utils.CustomException;
using STE.Utils.Linq;
using STE.WebSell.Application.CartModule.Abstract;
using STE.WebSell.Application.CartModule.Dtos;
using STE.WebSell.Application.Common;
using STE.WebSell.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace STE.WebSell.Application.CartModule.Implements
{
    public class CartService : ServiceBase, ICartService
    {
        public CartService(ILogger<CartService> logger, IHttpContextAccessor httpContext) : base(logger, httpContext) { }

        public CartDto Create(CreateCartDto input)
        {
            _logger.LogInformation($"{nameof(Create)}: input = {JsonSerializer.Serialize(input)}");

            var userId = CommonUtils.GetCurrentUserId(_httpContext);

            var existingCart = _dbContext.Carts
                .FirstOrDefault(p => p.ProductId == input.ProductId && !p.Deleted && p.Status == 1 && p.UserId == userId);

            if (existingCart == null)
            {
                var newCart = _mapper.Map<Cart>(input);
                newCart.UserId = userId;
                var result = _dbContext.Add(newCart).Entity;
            }
            else
            {
                existingCart.Quantity += input.Quantity;
            }

            _dbContext.SaveChanges();

            return null;
        }

        public CartDto Update(UpdateCartDto input)
        {
            _logger.LogInformation($"{nameof(Update)}: input = {JsonSerializer.Serialize(input)}");
            var product = _dbContext.Carts.FirstOrDefault(p => p.Id == input.Id && !p.Deleted);
            _mapper.Map(input, product);
            _dbContext.SaveChanges();
            return _mapper.Map<CartDto>(product);
        }

        public CartDto FindById(int id)
        {
            _logger.LogInformation($"{nameof(FindById)}: id = {id}");
            var product = _dbContext.Carts.FirstOrDefault(p => p.Id == id && !p.Deleted);
            var result = _mapper.Map<CartDto>(product);
            return _mapper.Map<CartDto>(product);
        }

        public PagingResult<CartDto> FindAll(FilterCartDto input)
        {
            _logger.LogInformation($"{nameof(FindAll)}: input = {JsonSerializer.Serialize(input)}");
            var result = new PagingResult<CartDto>();
            int useId = CommonUtils.GetCurrentUserId(_httpContext);
            var cartQuery = _dbContext.Carts.Include(c => c.Product).ThenInclude(p => p.ProductCategory)
                                            .Include(c => c.Product).ThenInclude(p => p.CalculationUnit)
                                            .Where(p => !p.Deleted && (p.UserId == useId) && p.Status == 1)
                                            .Select(o => new CartDto
                                            {
                                                Id = o.Id,
                                                Status = o.Status,
                                                ProductId = o.ProductId,
                                                Quantity = o.Quantity,
                                                UserId = o.UserId,
                                                ProductName = o.Product.Name,
                                                Image = o.Product.Image,
                                                Price = o.Product.DefaultPrice,
                                                Total = o.Product.DefaultPrice * o.Quantity,
                                                CalculationUnitId = o.Product.CalculationUnit.Id,
                                                CalculationUnitName = o.Product.CalculationUnit.Name,
                                                ProductCategoryName = o.Product.ProductCategory.Name,
                                                ProductCategoryId = o.Product.ProductCategory.Id,
                                            }); ;

            result.TotalItems = cartQuery.Count();
            cartQuery = cartQuery.OrderDynamic(input.Sort);

            if (input.PageSize != -1)
            {
                cartQuery = cartQuery.Skip(input.GetSkip()).Take(input.PageSize);
            }
            result.Items = _mapper.Map<IEnumerable<CartDto>>(cartQuery);
            return result;
        }

        public void Delete(int id)
        {
            _logger.LogInformation($"{nameof(Delete)}: id = {id}");
            var product = _dbContext.Carts.FirstOrDefault(p => p.Id == id && !p.Deleted);
                       
            product.Deleted = true;
            _dbContext.SaveChanges();
        }

        public void ChangeStatus(int id, int status)
        {
            _logger.LogInformation($"{nameof(ChangeStatus)}: id = {id}");
            var product = _dbContext.Carts.FirstOrDefault(p => p.Id == id && !p.Deleted);
            product.Status = status;
            _dbContext.SaveChanges();
        }
    }
}
