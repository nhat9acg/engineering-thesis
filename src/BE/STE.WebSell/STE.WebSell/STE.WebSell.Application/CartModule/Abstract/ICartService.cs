﻿using STE.ApplicationBase.Common;
using STE.WebSell.Application.CartModule.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.CartModule.Abstract
{
    public interface ICartService
    {
        public CartDto Create(CreateCartDto input);
        CartDto FindById(int id);
        PagingResult<CartDto> FindAll(FilterCartDto input);
        CartDto Update(UpdateCartDto input);
        void ChangeStatus(int id, int status);
        void Delete(int id);
    }
}
