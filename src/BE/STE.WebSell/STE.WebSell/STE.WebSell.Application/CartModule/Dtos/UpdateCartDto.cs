﻿using STE.WebSell.Application.ProductModule.Dtos.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.CartModule.Dtos
{
    public class UpdateCartDto : CreateCartDto
    {
        public int Id { get; set; }
    }
}
