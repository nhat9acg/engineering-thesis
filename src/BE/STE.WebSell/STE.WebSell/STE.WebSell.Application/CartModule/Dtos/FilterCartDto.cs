﻿using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.CartModule.Dtos
{
    public class FilterCartDto : PagingRequestBaseDto
    {
        [FromQuery(Name = "productId")]
        public int? ProductId { get; set; }
    }
}
