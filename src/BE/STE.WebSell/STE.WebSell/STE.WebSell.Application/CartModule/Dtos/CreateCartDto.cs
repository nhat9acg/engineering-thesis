﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.CartModule.Dtos
{
    public class CreateCartDto
    {
        public int? ProductId { get; set; }
        public int Quantity { get; set; }
    }
}
