﻿using STE.WebSell.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.CartModule.Dtos
{
    public class CartDto
    {
        public int Id { get; set; }
        public int Status { get; set; }
        public int? ProductId { get; set; }
        public int Quantity { get; set; }
        public int? UserId { get; set; }
        public string ProductName { get; set; }
        public string Image { get; set; }
        public double? Total { get; set; }
        public double? Price { get; set; }
        public int ProductCategoryId { get; set; }
        public string ProductCategoryName { get; set; }
        public int CalculationUnitId { get; set; }
        public string CalculationUnitName { get; set; }

    }

}
