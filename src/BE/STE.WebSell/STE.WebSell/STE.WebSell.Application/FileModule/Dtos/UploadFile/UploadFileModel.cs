﻿using Microsoft.AspNetCore.Http;

namespace STE.WebSell.Application.FileModule.Dtos.UploadFile
{
    public class UploadFileModel
    {
        public IFormFile File { get; set; }
        public string Folder { get; set; }
    }
}
