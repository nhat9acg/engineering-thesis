﻿using STE.WebSell.Application.FileModule.Dtos.UploadFile;

namespace STE.WebSell.Application.FileModule.Abstract
{
    public interface IFileService
    {
        byte[] GetFile(string folder, string fileName);
        string UploadFile(UploadFileModel input);
    }
}
