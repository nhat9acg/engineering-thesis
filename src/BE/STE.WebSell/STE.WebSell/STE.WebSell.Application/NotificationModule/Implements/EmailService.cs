﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MimeKit;
using MimeKit.Text;
using STE.Utils;
using STE.WebSell.Application.NotificationModule.Abstract;
using STE.WebSell.Application.NotificationModule.Dtos;
using STE.WebSell.Application.OrderModule.Implements;
using STE.WebSell.Application.ProductModule.Implements;
using STE.WebSell.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.NotificationModule.Implements
{
    public class EmailService : IEmailService
    {
        public readonly IConfiguration _config;
        private readonly EmailConfiguration _emailConfig;
        private readonly ILogger<EmailService> logger;
        public EmailService(IConfiguration config, EmailConfiguration emailConfig, ILogger<EmailService> _logger) {
            _emailConfig = emailConfig;
            logger = _logger;
        }

        public async Task SendMail(MailContent mailContent)
        {
            var email = new MimeMessage();
            email.Sender = new MailboxAddress(_emailConfig.DisplayName, _emailConfig.From);
            email.From.Add(new MailboxAddress(_emailConfig.DisplayName, _emailConfig.From));
            email.To.Add(MailboxAddress.Parse(mailContent.To));

            email.Subject = mailContent.Subject;

            var builder = new BodyBuilder();

            if (mailContent.Type == "HTML")
            {
                builder.HtmlBody = ReplaceContent(mailContent.Body, mailContent.Data);
            }
            else if (mailContent.Type == "MARKDOWN")
            {
                builder.TextBody = WebUtility.HtmlEncode(ReplaceContent(mailContent.Body, mailContent.Data));
            }
            else
            {
                builder.TextBody = ReplaceContent(mailContent.Body, mailContent.Data);
            }

            // Thêm các tệp đính kèm nếu có
            if (mailContent.Attachments != null)
            {
                foreach (var attachment in mailContent.Attachments)
                {
                    builder.Attachments.Add(attachment);
                }
            }

            email.Body = builder.ToMessageBody();

            // dùng SmtpClient của MailKit
            using var smtp = new MailKit.Net.Smtp.SmtpClient();

            try
            {
                smtp.Connect(_emailConfig.SmtpServer, _emailConfig.Port, SecureSocketOptions.StartTls);
                smtp.Authenticate(_emailConfig.From, _emailConfig.Password);
                await smtp.SendAsync(email);
            }
            catch (Exception ex)
            {
                // Gửi mail thất bại, nội dung email sẽ lưu vào thư mục mailssave
                System.IO.Directory.CreateDirectory("mailssave");
                var emailsavefile = string.Format(@"mailssave/{0}.eml", Guid.NewGuid());
                await email.WriteToAsync(emailsavefile);

                logger.LogInformation("Lỗi gửi mail, lưu tại - " + emailsavefile);
                logger.LogError(ex.Message);
            }

            smtp.Disconnect(true);

            logger.LogInformation("send mail to " + mailContent.To);
        }

        private string ReplaceContent(string text, IDictionary<string, string> data)
        {
            foreach (var keyToReplace in data.Keys)
            {
                text = text.Replace("[" + keyToReplace + "]", data[keyToReplace]);
            }
            return text;
        }

        public async Task SendMailList(List<MailContent> mailContents)
        {
            using (var smtp = new MailKit.Net.Smtp.SmtpClient())
            {
                try
                {
                    smtp.Connect(_emailConfig.SmtpServer, _emailConfig.Port, SecureSocketOptions.StartTls);
                    smtp.Authenticate(_emailConfig.From, _emailConfig.Password);

                    foreach (var mailContent in mailContents)
                    {
                        var email = new MimeMessage();
                        email.Sender = new MailboxAddress(_emailConfig.DisplayName, _emailConfig.From);
                        email.From.Add(new MailboxAddress(_emailConfig.DisplayName, _emailConfig.From));
                        email.To.Add(MailboxAddress.Parse(mailContent.To));
                        email.Subject = mailContent.Subject;

                        var builder = new BodyBuilder();
                        builder.HtmlBody = mailContent.Body;
                        email.Body = builder.ToMessageBody();

                        await smtp.SendAsync(email);

                        logger.LogInformation("Sent email to " + mailContent.To);
                    }
                }
                catch (Exception ex)
                {
                    // Handle the exception
                    logger.LogError(ex.Message);
                }
                finally
                {
                    smtp.Disconnect(true);
                }
            }
        }

    }
}
