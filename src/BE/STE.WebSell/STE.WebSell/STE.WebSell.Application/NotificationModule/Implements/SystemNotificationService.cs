﻿
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using STE.ApplicationBase.Common;
using STE.WebSell.Application.Common;
using STE.WebSell.Application.NotificationModule.Abstract;
using STE.WebSell.Application.ShippingProviderModule.Dtos;
using System.Text.Json;
using STE.Utils.Linq;
using STE.WebSell.Application.NotificationModule.Dtos;
using Microsoft.EntityFrameworkCore;
using STE.Utils.ConstantVariables.HistoryUpdate;
using STE.Utils.ConstantVariables.Document;
using STE.WebSell.Application.AuthenticationModule.Dtos.UserDto;
using STE.WebSell.Application.DocumentModule.Dtos.Document;
using STE.WebSell.Domain.Entities;
using STE.Utils.ConstantVariables.Shared;
using STE.Utils.CustomException;
using Microsoft.AspNetCore.Mvc;
using System;
using AutoMapper;
using MimeKit;
using System.Net;
using STE.ApplicationBase.CommonModule.Implements;
using System.Net.Http;
using STE.WebSell.Application.SignalR;
using STE.WebSell.Application.NotificationModule.Dtos.Media;

namespace STE.WebSell.Application.NotificationModule.Implements
{
    public class SystemNotificationService : ServiceBase, ISystemNotificationService
    {
        private readonly IEmailService _emailService;
        private readonly ISignalRBroadcastService _signalRBroadcastService;
        public SystemNotificationService(
            IEmailService emailService, 
            ILogger<SystemNotificationService> logger,
              ISignalRBroadcastService signalRBroadcastService,
            IHttpContextAccessor httpContext
            ) : base(logger, httpContext)
        {
            _emailService = emailService;
            _signalRBroadcastService = signalRBroadcastService;
        }

        public ResponseMediaDto FindAllMedia(FilterSystemNotificationDto input)
        {
            _logger.LogInformation($"{nameof(FindAllMedia)}: input = {JsonSerializer.Serialize(input)}");

            var query = _dbContext.Medias.Select(m => m.Url);
            var result = new ResponseMediaDto();
            result.Images = query;
            return result;
        }

        public ResponseMediaDto UpdateMedia(List<string> images)
        {
            _logger.LogInformation($"{nameof(UpdateMedia)}: images = {JsonSerializer.Serialize(images)}");
            // Xóa tất cả các bản ghi hiện tại trong bảng Media
            var currentMedia = _dbContext.Medias;
            _dbContext.Medias.RemoveRange(currentMedia);
            _dbContext.SaveChanges();

            // Thêm mới các URL từ danh sách mới
            var newMediaList = images.Select(url => new Media { Url = url });
            _dbContext.Medias.AddRange(newMediaList);
            _dbContext.SaveChanges();

            var result = new ResponseMediaDto();
            result.Images = images;
            return result;
        }


        public ResponseSystemNotificationDto FindAll(FilterSystemNotificationDto input)
        {
            _logger.LogInformation($"{nameof(FindAll)}: input = {JsonSerializer.Serialize(input)}");

            var query = _dbContext.SystemNotifications
                                        .Include(s => s.SystemNotificationType)
                                        .Where(s => !s.Deleted && s.Key != null)
                                        .Select(e => new ConfigKeysDto
                                        {
                                            Id = e.Id,
                                            Key = e.Key,
                                            Description = e.Description,
                                            EmailContent = e.EmailContent,
                                            EmailContentType = e.EmailContentType,
                                            PushAppContent = e.PushAppContent,
                                            PushAppContentType = e.PushAppContentType,
                                            TitleAppContent = e.TitleAppContent,
                                            NotificationTemplateName = e.NotificationTemplateName,
                                            Actions = e.SystemNotificationType.Select(s => s.Action)
                                        });
            var result = new ResponseSystemNotificationDto();
            result.ConfigKeys = query;
            string predefinedDataJson = File.ReadAllText("../STE.WebSell.API/DataNotification/predefined.json");
            var predefinedData = JsonSerializer.Deserialize<List<ConfigKeysDto>>(predefinedDataJson);

            IEnumerable<ConfigKeysDto> predefinedDataAsIEnumerable = predefinedData;
            result.Value.Settings = query;

            return result;
        }

        public void Create(List<ConfigKeysDto> input)
        {
            _logger.LogInformation($"{nameof(Create)}: input = {JsonSerializer.Serialize(input)}");

            var transaction = _dbContext.Database.BeginTransaction();

            var mapperConfig = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<ConfigKeysDto, SystemNotification>();
                cfg.CreateMap<string, SystemNotificationType>()
                   .ForMember(dest => dest.Action, opt => opt.MapFrom(src => src));
            });

            var mapper = mapperConfig.CreateMapper();

            foreach (var item in input)
            {
                if (_dbContext.SystemNotifications.Any(e => e.Key == item.Key))
                {
                    var systemNotificationFind = _dbContext.SystemNotifications
                                                        .Include(s => s.SystemNotificationType)
                                                        .FirstOrDefault(s => !s.Deleted && s.Id == item.Id
                                                                        && s.Key == item.Key)
                                                        ?? throw new UserFriendlyException(ErrorCode.SystemNotificationNotFound);

                    mapper.Map(item, systemNotificationFind);

                    var systemTypeFind = _dbContext.SystemNotificationTypes.Where(s => !s.Deleted && s.SystemNotificationId == item.Id);
                    _dbContext.SystemNotificationTypes.RemoveRange(systemTypeFind);
                    _dbContext.SaveChanges();
                    foreach (var action in item.Actions)
                    {
                        var check = _dbContext.SystemNotificationTypes.Any(e => e.Action == action && e.SystemNotificationId == item.Id);
                        if (!check)
                        {
                            _dbContext.SystemNotificationTypes.Add(new SystemNotificationType
                            {
                                SystemNotificationId = item.Id ?? 0,
                                Action = action,
                            });
                        }
                    }

                    //systemNotificationFind.Description = item.Description;
                    //systemNotificationFind.EmailContent = item.EmailContent;
                    //systemNotificationFind.EmailContentType = item.EmailContentType;
                    //systemNotificationFind.NotificationTemplateName = item.NotificationTemplateName;
                    //systemNotificationFind.PushAppContent = item.PushAppContent;
                    //systemNotificationFind.PushAppContentType = item.PushAppContentType;
                    //systemNotificationFind.TitleAppContent = item.TitleAppContent;

                }
                else
                {
                    var newSystemNotification = mapper.Map<ConfigKeysDto, SystemNotification>(item);

                    _dbContext.SystemNotifications.Add(newSystemNotification);

                    foreach (var action in item.Actions)
                    {
                        var newSystemNotificationType = mapper.Map<string, SystemNotificationType>(action);
                        newSystemNotification.SystemNotificationType.Add(newSystemNotificationType);
                    }

                    //var result = _dbContext.SystemNotifications.Add(new SystemNotification
                    //            {
                    //                Description = item.Description,
                    //                EmailContent = item.EmailContent,
                    //                EmailContentType = item.EmailContentType,
                    //                NotificationTemplateName = item.NotificationTemplateName,
                    //                PushAppContent = item.PushAppContent,
                    //                PushAppContentType = item.PushAppContentType,
                    //                Key = item.Key,
                    //            });

                    //foreach (var action in item.Actions)
                    //{
                    //    _dbContext.SystemNotificationTypes.Add(new SystemNotificationType
                    //    {
                    //        SystemNotificationId = result.Entity.Id,
                    //        Action = action,
                    //    });
                    //}

                }
            }
            _dbContext.SaveChanges();
            transaction.Commit();

        }

        private string ReplaceContent(string text, IDictionary<string, string> data)
        {
            foreach (var keyToReplace in data.Keys)
            {
                text = text.Replace("[" + keyToReplace + "]", data[keyToReplace]);
            }
            return text;
        }

        public async Task SendNotificationCustomerOrder(int userId, int orderId)
        {
            // bo sung kiem tra them ...

            var userFind = await _dbContext.Users.FirstOrDefaultAsync(u => u.Id == userId && !u.Deleted) ?? throw new UserFriendlyException(ErrorCode.UserNotFound);

            var systemNotificationFind = await _dbContext.SystemNotifications.Include(i => i.SystemNotificationType)
                                                    .Where(e => e.Key == "DANG_KY_MUA_HANG_THANH_CONG")
                                                    .Select(e => new
                                                    {
                                                        Actions = e.SystemNotificationType.Select(e => e.Action),
                                                        Subject = e.TitleAppContent,
                                                        TypeEmail = e.EmailContentType,
                                                        TypeApp = e.PushAppContentType,
                                                        BodyEmail = e.EmailContent,
                                                        BodyApp = e.PushAppContent,
                                                    })
                                                    .FirstOrDefaultAsync();

            var orderFind = await _dbContext.Orders.Include(o => o.OrderDetails)
                                .FirstOrDefaultAsync(e => e.Id == orderId) ?? throw new UserFriendlyException(ErrorCode.OrderNotFound);
            var data = new Dictionary<string, string>
                {
                    { "CustomerName", userFind.FullName },
                    { "ProductQuantity", orderFind.OrderDetails.Count().ToString() },
                    { "TotalMoney", orderFind.OrderDetails.Sum(od => od.Quantity * od.UnitPrice).ToString() }
                };
            var notificationsToAdd = new List<Notification>();
            if (systemNotificationFind.Actions.Contains("SEND_EMAIL"))
            {
                var mailContent = new MailContent();
                mailContent.Subject = systemNotificationFind.Subject;
                mailContent.Type = systemNotificationFind.TypeEmail;
                mailContent.Body = systemNotificationFind.BodyEmail;
                mailContent.To = userFind.Email;
                mailContent.Data = data;

                await _emailService.SendMail(mailContent);
            }

            if (systemNotificationFind.Actions.Contains("PUSH_WEB"))
            {
                var insert = new Notification
                {
                    UserId = userId,
                    TitleAppContent = systemNotificationFind.Subject,
                    PushAppContent = systemNotificationFind.BodyApp,
                    PushAppContentType = systemNotificationFind.TypeApp
                };

                if (systemNotificationFind.TypeApp == "HTML")
                {
                    insert.PushAppContent = ReplaceContent(systemNotificationFind.BodyApp, data);
                }
                else if (systemNotificationFind.TypeApp == "MARKDOWN")
                {
                    insert.PushAppContent = WebUtility.HtmlEncode(ReplaceContent(systemNotificationFind.BodyApp, data));
                }

                await _dbContext.Notifications.AddAsync(insert);
                await _dbContext.SaveChangesAsync();
                await _signalRBroadcastService.ReadNotification(0);
            }
        }

        public async Task SendNotificationNewMessage(int userId)
        {
            // bo sung kiem tra them ...

            var userFind = await _dbContext.Users.FirstOrDefaultAsync(u => u.Id == userId && !u.Deleted) ?? throw new UserFriendlyException(ErrorCode.UserNotFound);

            var systemNotificationFind = await _dbContext.SystemNotifications.Include(i => i.SystemNotificationType)
                                                    .Where(e => e.Key == "TIN_NHAN_MOI")
                                                    .Select(e => new
                                                    {
                                                        Actions = e.SystemNotificationType.Select(e => e.Action),
                                                        Subject = e.TitleAppContent,
                                                        TypeEmail = e.EmailContentType,
                                                        TypeApp = e.PushAppContentType,
                                                        BodyEmail = e.EmailContent,
                                                        BodyApp = e.PushAppContent,
                                                    })
                                                    .FirstOrDefaultAsync();

            var data = new Dictionary<string, string>
                {
                    { "CustomerName", userFind.FullName },
                };
            var notificationsToAdd = new List<Notification>();
            if (systemNotificationFind.Actions.Contains("SEND_EMAIL"))
            {
                var mailContent = new MailContent();
                mailContent.Subject = systemNotificationFind.Subject;
                mailContent.Type = systemNotificationFind.TypeEmail;
                mailContent.Body = systemNotificationFind.BodyEmail;
                mailContent.To = userFind.Email;
                mailContent.Data = data;

                await _emailService.SendMail(mailContent);
            }

            if (systemNotificationFind.Actions.Contains("PUSH_WEB"))
            {
                var insert = new Notification
                {
                    UserId = userId,
                    TitleAppContent = systemNotificationFind.Subject,
                    PushAppContent = systemNotificationFind.BodyApp,
                    PushAppContentType = systemNotificationFind.TypeApp
                };

                if (systemNotificationFind.TypeApp == "HTML")
                {
                    insert.PushAppContent = ReplaceContent(systemNotificationFind.BodyApp, data);
                }
                else if (systemNotificationFind.TypeApp == "MARKDOWN")
                {
                    insert.PushAppContent = WebUtility.HtmlEncode(ReplaceContent(systemNotificationFind.BodyApp, data));
                }

                await _dbContext.Notifications.AddAsync(insert);
                await _dbContext.SaveChangesAsync();
                await _signalRBroadcastService.NotificationNewMessage(userId);

            }
        }

        public PagingResult<ResponseNotificationDto> FindAllNotification(FilterSystemNotificationDto input)
        {
            _logger.LogInformation($"{nameof(FindAllNotification)}: input = {JsonSerializer.Serialize(input)}");
            var userId = CommonUtils.GetCurrentUserId(_httpContext);
            var query = _dbContext.Notifications.Where(sp => !sp.Deleted && userId == sp.UserId && (input.Keyword == null || sp.TitleAppContent.ToLower().Contains(input.Keyword.ToLower())));
            var result = new PagingResult<ResponseNotificationDto>();

            result.TotalItems = query.Count();
            query = query.OrderDynamic(input.Sort);

            if (input.PageSize != -1)
            {
                query = query.Skip(input.GetSkip()).Take(input.PageSize);
            }
            result.Items = _mapper.Map<List<ResponseNotificationDto>>(query);
            return result;
        }

        public async Task ReadNotification(int notificationId)
        {
            _logger.LogInformation($"{nameof(ReadNotification)}: notificationId = {notificationId}");
            var notificationFind = await _dbContext.Notifications.FirstOrDefaultAsync(e => e.Id == notificationId && !e.Deleted) ?? throw new UserFriendlyException(ErrorCode.NotificationNotFound);
            if(notificationFind.IsRead)
            {
                return;
            }
            notificationFind.IsRead = true;
            await _dbContext.SaveChangesAsync();
            await _signalRBroadcastService.ReadNotification(notificationId);

        }

        public ResponseInfoNotSeenNotificationDto GetInfoNotSeenNotification()
        {
            var userId = CommonUtils.GetCurrentUserId(_httpContext);
            _logger.LogInformation($"{nameof(GetInfoNotSeenNotification)}: userId = {userId}");
            var result = new ResponseInfoNotSeenNotificationDto();
            result.NotificationNumber = _dbContext.Notifications
                                            .Where(sp => !sp.Deleted 
                                                && userId == sp.UserId && !sp.IsRead)
                                            .Count();
            result.CartNumber = _dbContext.Carts.Where(c => !c.Deleted && userId == c.UserId && c.Status == 1).Count();
           return result;
        }
    }
}