﻿using STE.WebSell.Application.OrderModule.Dtos.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.NotificationModule.Dtos
{
    public class ResponseSystemNotificationDto
    {
        public IEnumerable<ConfigKeysDto> ConfigKeys { get; set; }
        public ValueDto Value { get; set; }

        public ResponseSystemNotificationDto()
        {
            Value = new ValueDto(); // Khởi tạo đối tượng ValueDto trong hàm khởi tạo
        }
    }
    public class ConfigKeysDto
    {
        public int? Id { get; set; }
        public string Description { get; set; }
        public string EmailContent { get; set; }
        public string EmailContentType { get; set; }
        public string Key { get; set; }
        public string NotificationTemplateName { get; set; }
        public string PushAppContent { get; set; }
        public string PushAppContentType { get; set; }
        public string TitleAppContent { get; set; }
        public IEnumerable<string> Actions { get; set; } 
    }

    public class ValueDto
    {
        public IEnumerable<ConfigKeysDto> Settings { get; set; }
    }
}
