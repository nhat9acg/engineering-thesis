﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.NotificationModule.Dtos
{
    public class ResponseNotificationDto
    {
        public int Id { get; set; }
        public string PushAppContent { get; set; }
        public string PushAppContentType { get; set; }
        public string TitleAppContent { get; set; }
        public bool IsRead { get; set; }
        public DateTime CreatedDate { get; set; }
    }

    public class ResponseInfoNotSeenNotificationDto
    {
        public int UserId { get; set; }
        public int NotificationNumber { get; set; }
        public int MessageNumber { get; set; }
        public int CartNumber { get; set; }
    }
}
