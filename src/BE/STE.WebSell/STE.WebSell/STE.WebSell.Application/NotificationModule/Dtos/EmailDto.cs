﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.NotificationModule.Dtos
{
    public class MailContent
    {
        public string To { get; set; } = string.Empty;              // Địa chỉ gửi đến
        public string Subject { get; set; } = string.Empty;         // Chủ đề (tiêu đề email)
        public string Body { get; set; } = string.Empty;            // Nội dung (hỗ trợ HTML) của email
        public Dictionary<string, string> Data { get; set; }
        public string Type { get; set; } = "HTML";
        public List<string> Attachments { get; set; }
    }
}
