﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.NotificationModule.Dtos.Media
{
    public class ResponseMediaDto
    {
        public IEnumerable<string> Images { get; set; }
    }
}
