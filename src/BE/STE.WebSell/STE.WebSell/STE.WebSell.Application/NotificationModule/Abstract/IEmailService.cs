﻿using STE.WebSell.Application.NotificationModule.Dtos;
using STE.WebSell.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.NotificationModule.Abstract
{
    public interface IEmailService
    {
        Task SendMail(MailContent mailContent);
    }
}
