﻿using STE.ApplicationBase.Common;
using STE.WebSell.Application.NotificationModule.Dtos;
using STE.WebSell.Application.NotificationModule.Dtos.Media;
using STE.WebSell.Application.ShippingProviderModule.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.NotificationModule.Abstract
{
    public interface ISystemNotificationService
    {
        ResponseMediaDto FindAllMedia(FilterSystemNotificationDto input);
        ResponseMediaDto UpdateMedia(List<string> images);
        ResponseSystemNotificationDto FindAll(FilterSystemNotificationDto input);
        void Create(List<ConfigKeysDto> input);
        Task SendNotificationCustomerOrder(int userId, int orderId);
        Task SendNotificationNewMessage(int userId);
        PagingResult<ResponseNotificationDto> FindAllNotification(FilterSystemNotificationDto input);
        Task ReadNotification(int notificationId);
        ResponseInfoNotSeenNotificationDto GetInfoNotSeenNotification();
    }
}
