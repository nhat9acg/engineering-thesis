﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using STE.ApplicationBase.Common;
using STE.Utils.ConstantVariables.Shared;
using STE.Utils.CustomException;
using STE.Utils.Linq;
using STE.WebSell.Application.Common;
using STE.WebSell.Application.ShippingProviderModule.Abstract;
using STE.WebSell.Application.ShippingProviderModule.Dtos;
using STE.WebSell.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace STE.WebSell.Application.ShippingProviderModule.Implements
{
    public class ShippingProviderService : ServiceBase, IShippingProviderService
    {
        public ShippingProviderService(ILogger<ShippingProviderService> logger, IHttpContextAccessor httpContext) : base(logger, httpContext)
        {
        }

        public void ChangeStatus(int id)
        {
            _logger.LogInformation($"{nameof(FindAll)}: input = {id}");
            var result = _dbContext.ShippingProviders.FirstOrDefault(sp => sp.Id == id && !sp.Deleted) ?? throw new UserFriendlyException(ErrorCode.ProductNotFound);
            if (result.Status == CommonStatus.ACTIVE)
            {
                result.Status = CommonStatus.DEACTIVE;
            } 
            else if (result.Status == CommonStatus.DEACTIVE)
            {
                result.Status = CommonStatus.ACTIVE;
            }
            _dbContext.SaveChanges();
        }

        public ShippingProviderDto Create(CreateShippingProviderDto input)
        {
            _logger.LogInformation($"{nameof(Create)}: input = {JsonSerializer.Serialize(input)}");
            var insert = _mapper.Map<ShippingProvider>(input);
            _dbContext.ShippingProviders.Add(insert);
            _dbContext.SaveChanges();
            return _mapper.Map<ShippingProviderDto>(input);
        }

        public void Delete(int id)
        {
            _logger.LogInformation($"{nameof(Create)}: input = {id}");
            var result = _dbContext.ShippingProviders.FirstOrDefault(sp => sp.Id == id && !sp.Deleted) ?? throw new UserFriendlyException(ErrorCode.ProductNotFound);
            result.Deleted = true;
            _dbContext.SaveChanges();
        }

        public PagingResult<ShippingProviderDto> FindAll(FilterShippingProviderDto input)
        {
            _logger.LogInformation($"{nameof(FindAll)}: input = {JsonSerializer.Serialize(input)}");
            var query = _dbContext.ShippingProviders.Where(sp => !sp.Deleted && (input.Status == null || sp.Status == input.Status) && (input.Keyword == null || sp.Name.ToLower().Contains(input.Keyword.ToLower())));
            var result = new PagingResult<ShippingProviderDto>();
            result.TotalItems = query.Count();

            query = query.OrderDynamic(input.Sort);

            if (input.PageSize != -1)
            {
                query = query.Skip(input.GetSkip()).Take(input.PageSize);
            }
            result.Items = _mapper.Map<List<ShippingProviderDto>>(query);
            return result;
        }

        public ShippingProviderDto FindById(int id)
        {
            _logger.LogInformation($"{nameof(FindById)}: input = {id}");
            var result = _dbContext.ShippingProviders.FirstOrDefault(sp => sp.Id == id && !sp.Deleted) ?? throw new UserFriendlyException(ErrorCode.ProductNotFound);
            return _mapper.Map<ShippingProviderDto>(result);
        }

        public ShippingProviderDto Update(UpdateShippingProviderDto input)
        {
            _logger.LogInformation($"{nameof(FindById)}: input = {JsonSerializer.Serialize(input)}");
            var result = _dbContext.ShippingProviders.FirstOrDefault(sp => sp.Id == input.Id && !sp.Deleted) ?? throw new UserFriendlyException(ErrorCode.ProductNotFound);
            result.Name = input.Name;
            _dbContext.SaveChanges();
            return _mapper.Map<ShippingProviderDto>(input);
        }
    }
}
