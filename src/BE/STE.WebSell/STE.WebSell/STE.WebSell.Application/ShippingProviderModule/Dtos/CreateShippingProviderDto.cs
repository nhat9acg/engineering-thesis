﻿using STE.Utils.ConstantVariables.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.ShippingProviderModule.Dtos
{
    public class CreateShippingProviderDto
    {
        /// <summary>
        /// Tên đơn vị vận chuyển
        /// </summary>
        [Required]
        [MaxLength(256)]
        public string Name { get; set; } = null!;
    }
}
