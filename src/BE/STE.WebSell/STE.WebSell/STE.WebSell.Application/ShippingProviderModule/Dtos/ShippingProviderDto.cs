﻿using STE.Utils.ConstantVariables.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.ShippingProviderModule.Dtos
{
    public class ShippingProviderDto
    {
        public int Id { get; set; }

        /// <summary>
        /// Tên đơn vị vận chuyển
        /// </summary>
        public string Name { get; set; } = null!;

        /// <summary>
        /// Trạng thái
        /// <see cref="CommonStatus"/>
        /// </summary>
        public int Status { get; set; }
    }
}
