﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.ShippingProviderModule.Dtos
{
    public class UpdateShippingProviderDto : CreateShippingProviderDto
    {
        public int Id { get; set; }
    }
}
