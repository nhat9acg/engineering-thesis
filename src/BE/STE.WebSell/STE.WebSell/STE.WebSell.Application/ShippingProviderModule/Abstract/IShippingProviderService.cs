﻿using STE.ApplicationBase.Common;
using STE.WebSell.Application.ShippingProviderModule.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.ShippingProviderModule.Abstract
{
    public interface IShippingProviderService
    {
        ShippingProviderDto Create(CreateShippingProviderDto input);
        ShippingProviderDto Update(UpdateShippingProviderDto input);
        ShippingProviderDto FindById(int id);
        void Delete(int id);
        PagingResult<ShippingProviderDto> FindAll(FilterShippingProviderDto input);
        void ChangeStatus(int id);
    }
}
