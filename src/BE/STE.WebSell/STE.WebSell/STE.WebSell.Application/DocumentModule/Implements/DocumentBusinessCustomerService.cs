﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using STE.ApplicationBase.Common;
using STE.ApplicationBase.CommonModule.Implements;
using STE.Utils.ConstantVariables.Shared;
using STE.Utils.CustomException;
using STE.WebSell.Application.AuthenticationModule.Dtos.UserDto;
using STE.WebSell.Application.DocumentModule.Abstract;
using STE.WebSell.Application.DocumentModule.Dtos.Document;
using STE.WebSell.Application.DocumentModule.Dtos.DocumentBusinessCustomer;
using System.Text.Json;

namespace STE.WebSell.Application.DocumentModule.Implements
{
    public class DocumentBusinessCustomerService : DocumentService, IDocumentBusinessCustomerService
    {
        public DocumentBusinessCustomerService(ILogger<DocumentBusinessCustomerService> logger, IHttpContextAccessor httpContext) : base(logger, httpContext)
        {
        }

        public PagingResult<DocumentDto> FindAll(FilterDocumentBusinessCustomerDto input)
        {
            _logger.LogInformation($"{nameof(FindAll)}: input = {JsonSerializer.Serialize(input)}");
            var filter = _mapper.Map<FilterDocumentDto>(input);
            filter.BusinessCustomerIds = new List<int> { CommonUtils.GetCurrentBusinessCustomerId(_httpContext) };
            return base.FindAll(filter);
        }

        public override DocumentDto FindById(int id)
        {
            _logger.LogInformation($"{nameof(FindById)}: id = {id}");
            int businessCustomerId = CommonUtils.GetCurrentBusinessCustomerId(_httpContext);
            var documentFind = _dbContext.Documents.Where(d => d.Id == id
                    && d.DocumentRecipients.Any(r => r.BusinessCustomerId == businessCustomerId) && !d.Deleted)
                .Select(d => new DocumentDto
                {
                    Id = d.Id,
                    Title = d.Title,
                    FileUrl = d.FileUrl,
                    SendType = d.SendType,
                    NumberCustomerSent = d.DocumentRecipients.Count(),
                    NumberCustomerView = d.DocumentRecipients.Where(r => r.IsView).Count(),
                    CreatedDate = d.CreatedDate,
                    CreatedBy = _dbContext.Users.Select(u => new UserByDto
                    {
                        Id = u.Id,
                        Username = u.Username,
                        FullName = u.FullName,
                        UserType = u.UserType
                    }).FirstOrDefault()
                })
                .FirstOrDefault() ?? throw new UserFriendlyException(ErrorCode.DocumentNotFound);

            var recipient = _dbContext.DocumentRecipients.FirstOrDefault(r => r.DocumentId == documentFind.Id && r.BusinessCustomerId == businessCustomerId);
            if (recipient != null)
            {
                recipient.IsView = true;
            }
            _dbContext.SaveChanges();
            return _mapper.Map<DocumentDto>(documentFind);
        }
    }
}
