﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using STE.ApplicationBase.Common;
using STE.Utils.ConstantVariables.Shared;
using STE.Utils.CustomException;
using STE.WebSell.Application.Common;
using STE.WebSell.Application.CustomerManagerModule.Implement;
using STE.WebSell.Application.DocumentModule.Abstract;
using STE.WebSell.Application.DocumentModule.Dtos.Document;
using STE.WebSell.Domain.Entities;
using STE.Utils.Linq;
using STE.WebSell.Application.AuthenticationModule.Dtos.UserDto;
using System.Text.Json;
using STE.Utils.ConstantVariables.Document;

namespace STE.WebSell.Application.DocumentModule.Implements
{
    public class DocumentService : ServiceBase, IDocumentService
    {
        public DocumentService(ILogger<DocumentService> logger, IHttpContextAccessor httpContext) : base(logger, httpContext)
        {
        }

        public virtual DocumentDto Create(CreateDocumentDto input)
        {
            _logger.LogInformation($"{nameof(Create)}: input = {JsonSerializer.Serialize(input)}");
            var transaction = _dbContext.Database.BeginTransaction();
            var document = _dbContext.Documents.Add(new Document
            {
                Title = input.Title,
                FileUrl = input.FileUrl,
                SendType = input.SendType,
            });
            _dbContext.SaveChanges();
            if (input.SendType == DocumentSendTypes.ALL_CUSTOMER)
            {
                input.BusinessCustomerIds = _dbContext.BusinessCustomers
                    .Where(b => !b.Deleted)
                    .Select(b => b.Id)
                    .ToList();
            }
            if (input.BusinessCustomerIds != null)
            {
                foreach (var businessCustomerId in input.BusinessCustomerIds)
                {
                    _dbContext.DocumentRecipients.Add(new DocumentRecipient
                    {
                        DocumentId = document.Entity.Id,
                        BusinessCustomerId = businessCustomerId,
                    });
                }
                _dbContext.SaveChanges();
            }
            transaction.Commit();
            var documentView = _dbContext.Documents.Select(d => new DocumentDto
            {
                Id = d.Id,
                Title = d.Title,
                FileUrl = d.FileUrl,
                SendType = d.SendType,
                NumberCustomerSent = d.DocumentRecipients.Count(),
                NumberCustomerView = d.DocumentRecipients.Where(r => r.IsView).Count(),
                CreatedDate = d.CreatedDate,
                CreatedBy = _dbContext.Users.Select(u => new UserByDto
                {
                    Id = u.Id,
                    Username = u.Username,
                    FullName = u.FullName,
                    UserType = u.UserType
                }).FirstOrDefault()
            }).FirstOrDefault();
            return documentView;
        }

        public virtual void Delete(int id)
        {
            _logger.LogInformation($"{nameof(Delete)}: id = {id}");
            var documentFind = _dbContext.Documents.FirstOrDefault(d => d.Id == id && !d.Deleted)
                                   ?? throw new UserFriendlyException(ErrorCode.DocumentNotFound);
            documentFind.Deleted = true;
            _dbContext.SaveChanges();
        }

        public virtual PagingResult<DocumentDto> FindAll(FilterDocumentDto input)
        {
            _logger.LogInformation($"{nameof(FindAll)}: input = {JsonSerializer.Serialize(input)}");
            var result = new PagingResult<DocumentDto>();
            var documentQuery = _dbContext.Documents
                                        .Include(d => d.DocumentRecipients)
                                            .ThenInclude(r => r.BusinessCustomer)
                                        .Where(d => !d.Deleted
                                             && (input.Keyword == null
                                                || d.Title.ToLower().Contains(input.Keyword.ToLower()))
                                             && (input.BusinessCustomerIds == null
                                                || d.DocumentRecipients.Any(b => input.BusinessCustomerIds.Contains(b.BusinessCustomerId))))
                                        .Select(d => new DocumentDto
                                        {
                                            Id = d.Id,
                                            Title = d.Title,
                                            FileUrl = d.FileUrl,
                                            SendType = d.SendType,
                                            NumberCustomerSent = d.DocumentRecipients.Count(),
                                            NumberCustomerView = d.DocumentRecipients.Where(r => r.IsView).Count(),
                                            CreatedDate = d.CreatedDate,
                                            IsView = d.DocumentRecipients.Any(r => r.IsView),
                                            CreatedBy = _dbContext.Users.Select(u => new UserByDto
                                            {
                                                Id = u.Id,
                                                Username = u.Username,
                                                FullName = u.FullName,
                                                UserType = u.UserType,
                                            }).FirstOrDefault(),
                                            BusinessCustomerIds = _dbContext.DocumentRecipients.Where(g => g.DocumentId == d.Id).Select(r => r.BusinessCustomerId).ToList(),

                                        });

            documentQuery = documentQuery.OrderDynamic(input.Sort);

            if (input.PageSize != -1)
            {
                documentQuery = documentQuery.Skip(input.GetSkip()).Take(input.PageSize);
            }
           
            result.TotalItems = documentQuery.Count();
            result.Items = documentQuery;
            return result;
        }

        public virtual DocumentDto FindById(int id)
        {
            _logger.LogInformation($"{nameof(FindById)}: id = {id}");
            var documentFind = _dbContext.Documents
                .Where(d => d.Id == id && !d.Deleted)
                .Select(d => new DocumentDto
                {
                    Id = d.Id,
                    Title = d.Title,
                    FileUrl = d.FileUrl,
                    SendType = d.SendType,
                    NumberCustomerSent = d.DocumentRecipients.Count(),
                    NumberCustomerView = d.DocumentRecipients.Where(r => r.IsView).Count(),
                    CreatedDate = d.CreatedDate,
                    BusinessCustomerIds = _dbContext.DocumentRecipients.Where(g => g.DocumentId == d.Id).Select(r => r.BusinessCustomerId),
                    CreatedBy = _dbContext.Users.Select(u => new UserByDto
                    {
                        Id = u.Id,
                        Username = u.Username,
                        FullName = u.FullName,
                        UserType = u.UserType
                    }).FirstOrDefault()
                })
                .FirstOrDefault() ?? throw new UserFriendlyException(ErrorCode.DocumentNotFound);
            return documentFind;
        }

        public virtual void Update(UpdateDocumentDto input)
        {
            _logger.LogInformation($"{nameof(Update)}: input = {JsonSerializer.Serialize(input)}");
            var documentFind = _dbContext.Documents.FirstOrDefault(d => d.Id == input.Id && !d.Deleted)
                                   ?? throw new UserFriendlyException(ErrorCode.DocumentNotFound);

            documentFind.Title = input.Title;
            documentFind.FileUrl = input.FileUrl;

            foreach (var recipient in documentFind.DocumentRecipients)
            {
                recipient.IsView = false;
            }

            if(input.SendType == DocumentSendTypes.SOME_CUSTOMER)
            {
                var isValidBusinessCustomerIds = input.BusinessCustomerIds?.All(id =>
                _dbContext.BusinessCustomers.Any(c => c.Id == id)) ?? false;
                if (!isValidBusinessCustomerIds)
                {
                    throw new UserFriendlyException(ErrorCode.BusinessCustomerNotFound);
                }

                var oldBusinessCustomerIds = _dbContext.DocumentRecipients
                    .Where(t => t.DocumentId == input.Id)
                    .Select(x => x.BusinessCustomerId)
                    .ToList();

                var newBusinessCustomerIds = input.BusinessCustomerIds ?? new List<int>();

                var removedBusinessCustomerIds = oldBusinessCustomerIds.Except(newBusinessCustomerIds).ToList();

                var addedBusinessCustomerIds = newBusinessCustomerIds.Except(oldBusinessCustomerIds).ToList();

                if (removedBusinessCustomerIds.Any())
                {
                    var removedBusinessCustomers = _dbContext.DocumentRecipients
                        .Where(x => x.DocumentId == input.Id && removedBusinessCustomerIds.Contains(x.BusinessCustomerId))
                        .ToList();

                    _dbContext.DocumentRecipients.RemoveRange(removedBusinessCustomers);
                }

                if (addedBusinessCustomerIds.Any())
                {
                    var addedBusinessCustomers = addedBusinessCustomerIds.Select(x => new DocumentRecipient
                    {
                        DocumentId = input.Id,
                        BusinessCustomerId = x
                    });

                    _dbContext.DocumentRecipients.AddRange(addedBusinessCustomers);
                }
            }

            _dbContext.SaveChanges();
        }
    }
}
