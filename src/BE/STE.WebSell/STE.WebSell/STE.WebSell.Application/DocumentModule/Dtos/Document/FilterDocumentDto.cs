﻿using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;

namespace STE.WebSell.Application.DocumentModule.Dtos.Document
{
    public class FilterDocumentDto : PagingRequestBaseDto
    {
        /// <summary>
        /// Tìm theo danh sách khách
        /// </summary>
        [FromQuery(Name = "businessCustomerIds")]
        public IEnumerable<int> BusinessCustomerIds { get; set; }
    }
}
