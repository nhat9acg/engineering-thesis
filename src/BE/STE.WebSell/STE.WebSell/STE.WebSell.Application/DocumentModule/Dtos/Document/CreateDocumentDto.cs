﻿using STE.ApplicationBase.Common.Validations;
using STE.Utils.ConstantVariables.Document;

namespace STE.WebSell.Application.DocumentModule.Dtos.Document
{
    public class CreateDocumentDto : DocumentBaseDto
    {
        /// <summary>
        /// Danh sách khách hàng nhận khi chọn kiểu là gửi cho một số khách
        /// </summary>
        public IEnumerable<int> BusinessCustomerIds { get; set; }

        /// <summary>
        /// Kiểu gửi 1: tất cả khách, 2: một số khách (1 hoặc nhóm) <see cref="DocumentSendTypes"/>
        /// </summary>
        [IntegerRange(AllowableValues = new int[] { DocumentSendTypes.ALL_CUSTOMER, DocumentSendTypes.SOME_CUSTOMER })]
        public int SendType { get; set; }
    }
}
