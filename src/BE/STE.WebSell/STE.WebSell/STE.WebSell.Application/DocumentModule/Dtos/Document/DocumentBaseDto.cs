﻿using STE.ApplicationBase.Common.Validations;
using STE.Utils.ConstantVariables.Document;

namespace STE.WebSell.Application.DocumentModule.Dtos.Document
{
    public class DocumentBaseDto
    {
        private string _title;
        /// <summary>
        /// Tiêu đề
        /// </summary>
        [CustomRequired]
        [CustomMaxLength(128)]
        public string Title
        {
            get => _title;
            set => _title = value?.Trim();
        }

        private string _fileUrl;
        /// <summary>
        /// Đường dẫn file
        /// </summary>
        [CustomRequired]
        [CustomMaxLength(512)]
        public string FileUrl
        {
            get => _fileUrl;
            set => _fileUrl = value?.Trim();
        }
    }
}
