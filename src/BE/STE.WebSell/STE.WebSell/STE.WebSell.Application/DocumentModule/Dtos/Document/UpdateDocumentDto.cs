﻿using STE.ApplicationBase.Common.Validations;
using System.ComponentModel.DataAnnotations;

namespace STE.WebSell.Application.DocumentModule.Dtos.Document
{
    public class UpdateDocumentDto : DocumentBaseDto
    {
        public int Id { get; set; }
        public int SendType { get; set; }
        public IEnumerable<int> BusinessCustomerIds { get; set; }
    }
}
