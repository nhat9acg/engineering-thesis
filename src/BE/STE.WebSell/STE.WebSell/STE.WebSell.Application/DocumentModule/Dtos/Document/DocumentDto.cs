﻿using STE.Utils.ConstantVariables.Document;
using STE.WebSell.Application.AuthenticationModule.Dtos.UserDto;

namespace STE.WebSell.Application.DocumentModule.Dtos.Document
{
    public class DocumentDto
    {
        public int Id { get; set; }
        /// <summary>
        /// Tiêu đề tài liệu
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Đường dẫn file tài liệu
        /// </summary>
        public string FileUrl { get; set; }
        /// <summary>
        /// Kiểu gửi <see cref="DocumentSendTypes"/>
        /// </summary>
        public int SendType { get; set; }
        /// <summary>
        /// Số lượng khách hàng đã gửi
        /// </summary>
        public int NumberCustomerSent { get; set; }
        /// <summary>
        /// Số lượng khách hàng đã xem
        /// </summary>
        public int NumberCustomerView { get; set; }
        public DateTime? CreatedDate { get; set; }
        public UserByDto CreatedBy { get; set; }
        public bool IsView { get; set; }
        /// <summary>
        /// Danh sách khách hàng nhận khi chọn kiểu là gửi cho một số khách
        /// </summary>
        public IEnumerable<int> BusinessCustomerIds { get; set; }
    }
}
