﻿using STE.ApplicationBase.Common;
using STE.WebSell.Application.DocumentModule.Dtos.Document;

namespace STE.WebSell.Application.DocumentModule.Abstract
{
    public interface IDocumentService
    {
        /// <summary>
        /// thêm mới tài liệu
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        DocumentDto Create(CreateDocumentDto input);

        /// <summary>
        /// xóa tài liệu
        /// </summary>
        /// <param name="id"></param>
        void Delete(int id);

        /// <summary>
        /// lấy danh sách tài liệu
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        PagingResult<DocumentDto> FindAll(FilterDocumentDto input);

        /// <summary>
        /// lấy thông tin chi tiết tài liệu
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        DocumentDto FindById(int id);

        /// <summary>
        /// cập nhập tài liệu
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        void Update(UpdateDocumentDto input);
    }
}
