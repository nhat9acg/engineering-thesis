﻿using STE.ApplicationBase.Common;
using STE.WebSell.Application.DocumentModule.Dtos.Document;
using STE.WebSell.Application.DocumentModule.Dtos.DocumentBusinessCustomer;

namespace STE.WebSell.Application.DocumentModule.Abstract
{
    public interface IDocumentBusinessCustomerService : IDocumentService
    {
        PagingResult<DocumentDto> FindAll(FilterDocumentBusinessCustomerDto input);
    }
}
