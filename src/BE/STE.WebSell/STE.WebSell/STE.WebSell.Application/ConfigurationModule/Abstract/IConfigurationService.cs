﻿using STE.WebSell.Application.ConfigurationModule.Dtos;

namespace STE.WebSell.Application.ConfigurationModule.Abstract
{
    public interface IConfigurationService
    {
        ConfigurationDto GetConfiguration();
    }
}
