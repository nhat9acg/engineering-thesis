﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using STE.WebSell.Application.Common;
using STE.WebSell.Application.ConfigurationModule.Abstract;
using STE.WebSell.Application.ConfigurationModule.Dtos;

namespace STE.WebSell.Application.ConfigurationModule.Implements
{
    public class ConfigurationService : ServiceBase, IConfigurationService
    {
        public ConfigurationService(ILogger<ConfigurationService> logger, IHttpContextAccessor httpContext) : base(logger, httpContext)
        {
        }

        public ConfigurationDto GetConfiguration()
        {
            var configuration = new ConfigurationDto()
            {
                Dictionary = _localization.Dictionary
            };
            return configuration;
        }
    }
}
