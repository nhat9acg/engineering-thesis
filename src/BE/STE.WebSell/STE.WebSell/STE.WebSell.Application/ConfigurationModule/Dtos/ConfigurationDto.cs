﻿namespace STE.WebSell.Application.ConfigurationModule.Dtos
{
    public class ConfigurationDto
    {
        public Dictionary<string, Dictionary<string, string>> Dictionary { get; set; }
    }
}
