﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using STE.WebSell.Application.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.SignalR
{
    public class SignalRBroadcastService : ServiceBase, ISignalRBroadcastService
    {
        private readonly IHubContext<WebSellHub> _hub;

        public SignalRBroadcastService(ILogger<SignalRBroadcastService> logger,
            IHubContext<WebSellHub> hub, IHttpContextAccessor httpContext) : base(logger, httpContext)
        {
            _hub = hub;
        }

        public async Task BroadcastSendMessage(int userId, int code)
        {
            //await _hub.Clients.Group(code.ToString())
            //        .SendAsync("ReceiveMessage", code);
            await _hub.Clients.All.SendAsync("ReceiveMessage", userId);
        }

        public async Task NotificationNewMessage(int userId)
        {
            await _hub.Clients.All.SendAsync("NewMessage", userId);
        }

        public async Task ReadNotification(int notificationId)
        {
            await _hub.Clients.All.SendAsync("ReadNotification", notificationId);
        }
    }
}
