﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.SignalR
{
    public interface ISignalRBroadcastService
    {
       Task BroadcastSendMessage(int userId, int code);
       Task NotificationNewMessage(int userId);
       Task ReadNotification(int notificationId);
    }
}
