﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using STE.Utils;
using STE.WebSell.Infrastructure.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.SignalR
{
    public class WebSellHub : Hub
    {
        private readonly ILogger _logger;
        protected readonly WebSellDbContext _dbContext;

        public WebSellHub(
            ILogger<WebSellHub> logger,
            WebSellDbContext dbContext
            )
        {
            _logger = logger;
            _dbContext = dbContext;
        }

        public override async Task OnConnectedAsync()
        {
            // Ghi log hoặc in thông báo khi có kết nối mới
            Console.WriteLine($"Client connected: {Context.ConnectionId}");

            // xu ly group nhom 
            //int userId = int.Parse(Context.User.FindFirst(UserClaimTypes.UserId)?.Value);
            //string code = (_dbContext.Chats.Where(e => e.SenderUserId == userId).Select(c => c.Code).FirstOrDefault()).ToString();
            //await Groups.AddToGroupAsync(Context.ConnectionId, code);

            // Gửi thông báo tới tất cả client rằng có một client mới đã kết nối
            await Clients.All.SendAsync("UserConnected", Context.ConnectionId);

            // Gọi phương thức OnConnectedAsync trong lớp cha nếu cần thiết
            await base.OnConnectedAsync();
        }

        //public override async Task OnDisconnectedAsync(Exception exception)
        //{
        //    // Ghi log hoặc in thông báo khi một kết nối bị mất
        //    Console.WriteLine($"Client disconnected: {Context.ConnectionId}");

        //    // Gửi thông báo tới tất cả client rằng có một client đã ngắt kết nối
        //    await Clients.All.SendAsync("UserDisconnected", Context.ConnectionId);

        //    // Gọi phương thức OnDisconnectedAsync trong lớp cha nếu cần thiết
        //    await base.OnDisconnectedAsync(exception);
        //}
    }
}
