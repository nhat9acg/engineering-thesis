﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using STE.ApplicationBase.Common;
using STE.Utils.ConstantVariables.Shared;
using STE.Utils.CustomException;
using STE.Utils.Linq;
using STE.WebSell.Application.Common;
using STE.WebSell.Application.NotificationModule.Implements;
using STE.WebSell.Application.ProductModule.Abstract;
using STE.WebSell.Application.ProductModule.Dtos.Product;
using STE.WebSell.Domain.Entities;
using System.Text.Json;

namespace STE.WebSell.Application.ProductModule.Implements
{
    public class ProductService : ServiceBase, IProductService
    {
        //private readonly EmailService _emailService;
        //EmailService emailService
        public ProductService(ILogger<ProductService> logger, IHttpContextAccessor httpContext) : base(logger, httpContext) 
        {
            //_emailService = emailService;
        }

        public ProductDto Create(CreateProductDto input)
        {
            _logger.LogInformation($"{nameof(Create)}: input = {JsonSerializer.Serialize(input)}");
            var result = _dbContext.Add(_mapper.Map<Product>(input)).Entity;
            _dbContext.SaveChanges();
            return _mapper.Map<ProductDto>(result);
        }

        public ProductDto Update(UpdateProductDto input)
        {
            _logger.LogInformation($"{nameof(Update)}: input = {JsonSerializer.Serialize(input)}");
            var product = _dbContext.Products.FirstOrDefault(p => p.Id == input.Id && !p.Deleted)
                          ?? throw new UserFriendlyException(ErrorCode.ProductNotFound);
            _mapper.Map(input, product);
            _dbContext.SaveChanges();
            return _mapper.Map<ProductDto>(product);
        }

        public ProductDto FindById(int id)
        {
            _logger.LogInformation($"{nameof(FindById)}: id = {id}");
            var product = _dbContext.Products.Include(e => e.ProductCategory)
                                             .Include(e => e.CalculationUnit)
                                             .Include(e => e.ProductPrices)
                                             .FirstOrDefault(p => p.Id == id && !p.Deleted)
                          ?? throw new UserFriendlyException(ErrorCode.ProductNotFound);
            var result = _mapper.Map<ProductDto>(product);
            return _mapper.Map<ProductDto>(product);
        }

        public PagingResult<ProductDto> FindAll(FilterProductDto input)
        {
            _logger.LogInformation($"{nameof(FindAll)}: input = {JsonSerializer.Serialize(input)}");
            var result = new PagingResult<ProductDto>();
            var productQuery = _dbContext.Products.Include(p => p.ProductCategory)
                                                  .Include(p => p.CalculationUnit)
                                                  .Where(p => !p.Deleted
                                                      && (input.Name == null || p.Name.ToLower().Contains(input.Name.ToLower()))
                                                      && (input.Code == null || p.Code.ToLower().Contains(input.Code.ToLower()))
                                                      && (input.ProductCategoryIds == null || input.ProductCategoryIds.Count() == 0 || input.ProductCategoryIds.Contains(p.ProductCategoryId ?? 0))
                                                      && (input.ProductCategoryId == null || p.ProductCategory.Id == input.ProductCategoryId));

            result.TotalItems = productQuery.Count();
            productQuery = productQuery.OrderDynamic(input.Sort);

            if (input.PageSize != -1)
            {
                productQuery = productQuery.Skip(input.GetSkip()).Take(input.PageSize);
            }
            result.Items = _mapper.Map<IEnumerable<ProductDto>>(productQuery);

            //string recipientEmail = "vuvan10082001@gmail.com";
            //string message = "This is a notification.";
            //string subject = "Notification";
            //await _emailService.SendEmailAsync(recipientEmail, subject, message);

            return result;
        }

        public void Delete(int id)
        {
            _logger.LogInformation($"{nameof(Delete)}: id = {id}");
            var product = _dbContext.Products.FirstOrDefault(p => p.Id == id && !p.Deleted)
                        ?? throw new UserFriendlyException(ErrorCode.ProductNotFound);
            product.Deleted = true;
            _dbContext.SaveChanges();
        }

        public void ActiveProduct(int id)
        {
            _logger.LogInformation($"{nameof(ActiveProduct)}: id = {id}");
            var product = _dbContext.Products.FirstOrDefault(p => p.Id == id && !p.Deleted)
                                  ?? throw new UserFriendlyException(ErrorCode.ProductNotFound);
            product.Status = (product.Status == ActiveStatus.ACTIVE) ? ActiveStatus.DEACTIVE : ActiveStatus.ACTIVE;
            _dbContext.SaveChanges();
        }
    }
}
