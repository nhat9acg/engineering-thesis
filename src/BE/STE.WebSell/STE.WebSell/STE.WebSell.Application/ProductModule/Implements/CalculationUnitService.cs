﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using STE.ApplicationBase.Common;
using STE.Utils.ConstantVariables.Shared;
using STE.Utils.CustomException;
using STE.Utils.Linq;
using STE.WebSell.Application.Common;
using STE.WebSell.Application.ProductModule.Abstract;
using STE.WebSell.Application.ProductModule.Dtos.CalculationUnit;
using STE.WebSell.Domain.Entities;
using System.Text.Json;

namespace STE.WebSell.Application.ProductModule.Implements
{
    public class CalculationUnitService : ServiceBase, ICalculationUnitService
    {
        public CalculationUnitService(ILogger<CalculationUnitService> logger, IHttpContextAccessor httpContext) : base(logger, httpContext) { }
        public CalculationUnitDto Create(CreateCalculationUnitDto input)
        {
            _logger.LogInformation($"{nameof(Create)}: input = {JsonSerializer.Serialize(input)}");
            var result = _dbContext.Add(_mapper.Map<CalculationUnit>(input)).Entity;
            _dbContext.SaveChanges();
            return _mapper.Map<CalculationUnitDto>(result);
        }

        public void Delete(int id)
        {
            _logger.LogInformation($"{nameof(Delete)}: input = {JsonSerializer.Serialize(id)}");
            var check = _dbContext.CalculationUnits.FirstOrDefault(p => p.Id == id && !p.Deleted)
                        ?? throw new UserFriendlyException(ErrorCode.CalculationUnitNotFound);
            check.Deleted = true;
            _dbContext.SaveChanges();
        }

        public PagingResult<CalculationUnitDto> FindAll(FilterCalculationUnitDto input)
        {
            _logger.LogInformation($"{nameof(FindAll)}: input = {JsonSerializer.Serialize(input)}");
            var result = new PagingResult<CalculationUnitDto>();
            var calculationUnitQuery = _dbContext.CalculationUnits
                                         .Where(p => !p.Deleted
                                                 && (input.Name == null || p.Name.ToLower().Contains(input.Name.ToLower())));
            result.TotalItems = calculationUnitQuery.Count();
            calculationUnitQuery = calculationUnitQuery.OrderDynamic(input.Sort);

            if (input.PageSize != -1)
            {
                calculationUnitQuery = calculationUnitQuery.Skip(input.GetSkip()).Take(input.PageSize);
            }
            result.Items = _mapper.Map<IEnumerable<CalculationUnitDto>>(calculationUnitQuery);
            return result;
        }

        public CalculationUnitDto FindById(int id)
        {
            _logger.LogInformation($"{nameof(FindById)}: input = {JsonSerializer.Serialize(id)}");
            var calculationUnit = _dbContext.CalculationUnits.FirstOrDefault(p => p.Id == id && !p.Deleted)
                          ?? throw new UserFriendlyException(ErrorCode.CalculationUnitNotFound);
            var result = _mapper.Map<CalculationUnitDto>(calculationUnit);
            return _mapper.Map<CalculationUnitDto>(result);
        }

        public CalculationUnitDto Update(UpdateCalculationUnitDto input)
        {
            _logger.LogInformation($"{nameof(Update)}: input = {JsonSerializer.Serialize(input)}");
            var calculationUnit = _dbContext.CalculationUnits.FirstOrDefault(p => p.Id == input.Id && !p.Deleted)
                          ?? throw new UserFriendlyException(ErrorCode.CalculationUnitNotFound);
            //_ = _dbContext.ProductCategories.FirstOrDefault(p => p.Id == input.ProductCategoryId && !p.Deleted)
            //                             ?? throw new UserFriendlyException(ErrorCode.ProductCategoryNotFound);
            _mapper.Map(input, calculationUnit);
            _dbContext.SaveChanges();
            return _mapper.Map<CalculationUnitDto>(calculationUnit);
        }
    }
}
