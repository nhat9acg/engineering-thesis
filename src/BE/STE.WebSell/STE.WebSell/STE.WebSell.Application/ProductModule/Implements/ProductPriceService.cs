﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using STE.ApplicationBase.Common;
using STE.Utils.ConstantVariables.HistoryUpdate;
using STE.Utils.ConstantVariables.Shared;
using STE.Utils.Linq;
using STE.WebSell.Application.Common;
using STE.WebSell.Application.ProductModule.Abstract;
using STE.WebSell.Application.ProductModule.Dtos.ProductPrice;
using STE.WebSell.Domain.Entities;
using System.Text.Json;

namespace STE.WebSell.Application.ProductModule.Implements
{
    public class ProductPriceService : ServiceBase, IProductPriceService
    {
        public ProductPriceService(ILogger<ProductPriceService> logger, IHttpContextAccessor httpContext) : base(logger, httpContext) { }
        public void Add(List<CreateProductPriceDto> input)
        {
            _logger.LogInformation($"{nameof(Add)}: input = {JsonSerializer.Serialize(input)}");
            _dbContext.ProductPrices.AddRange(_mapper.Map<List<ProductPrice>>(input));
            _dbContext.SaveChanges();
        }

        public void Delete(List<int> ids)
        {
            _logger.LogInformation($"{nameof(Delete)}: input = {JsonSerializer.Serialize(ids)}");
            foreach (var id in ids)
            {
                var productPrice = _dbContext.ProductPrices.FirstOrDefault(p => p.Id == id);
                _dbContext.Remove(productPrice);
            }
            _dbContext.SaveChanges();
        }

        public PagingResult<ProductPriceDto> FindAll(FilterProductPriceDto input)
        {
            _logger.LogInformation($"{nameof(FindAll)}: input = {JsonSerializer.Serialize(input)}");
            var result = new PagingResult<ProductPriceDto>();
            var productPriceQuery = _dbContext.Products
                                                    .Include(pd => pd.ProductPrices)
                                                        .ThenInclude(pp => pp.BusinessCustomer)
                                                    .Where(p => p.Status == CommonStatus.ACTIVE && !p.Deleted)
                                                    .Select(pd =>
                                                    new ProductPriceDto
                                                    {
                                                        Id = pd.ProductPrices.FirstOrDefault(pp => pp.BusinessCustomerId == input.BusinessCustomerId).Id,
                                                        ProductId = pd.Id,
                                                        BusinessCustomerId = pd.ProductPrices.FirstOrDefault(pp => pp.BusinessCustomerId == input.BusinessCustomerId).BusinessCustomer.Id,
                                                        DefaultPrice = pd.DefaultPrice,
                                                        Price = pd.ProductPrices.FirstOrDefault(pp => pp.BusinessCustomerId == input.BusinessCustomerId).Price,
                                                    });


            result.TotalItems = productPriceQuery.Count();
            productPriceQuery = productPriceQuery.OrderDynamic(input.Sort);

            if (input.PageSize != -1)
            {
                productPriceQuery = productPriceQuery.Skip(input.GetSkip()).Take(input.PageSize);
            }
            result.Items = _mapper.Map<IEnumerable<ProductPriceDto>>(productPriceQuery);
            return result;
        }

        public IEnumerable<ProductPriceDto> FindProductPriceByBusinessCustomer(int businessCustomerId)
        {
            _logger.LogInformation($"{nameof(FindProductPriceByBusinessCustomer)}: input = {JsonSerializer.Serialize(businessCustomerId)}");
            var productPriceQuery = _dbContext.ProductPrices.Include(p => p.Product).ThenInclude(pd => pd.ProductCategory)
                                                            .Include(p => p.Product).ThenInclude(pd => pd.CalculationUnit)
                                                            .Include(p => p.BusinessCustomer)
                                                            .Where(p => p.BusinessCustomerId == businessCustomerId)
                                                            .AsEnumerable();
            return _mapper.Map<IEnumerable<ProductPriceDto>>(productPriceQuery);
        }

        public void Update(List<UpdateProductPriceDto> input)
        {
            _logger.LogInformation($"{nameof(Update)}: input = {JsonSerializer.Serialize(input)}");
            var productPricesToUpdate = (
                from item in input
                join productPrice in _dbContext.ProductPrices on new { item.BusinessCustomerId, item.ProductId, item.Id } equals new { productPrice.BusinessCustomerId, productPrice.ProductId, productPrice.Id }
                select new { Item = item, ProductPrice = productPrice }
            );

            foreach (var update in productPricesToUpdate)
            {
                _mapper.Map(update.Item, update.ProductPrice);

                _dbContext.HistoryUpdates.Add(new HistoryUpdate
                {
                    Table = TableUpdate.PRODUCT_PRICE,
                    Column = ColumnUpdate.PRICE,
                    Action = Actions.UPDATE,
                    ReferId = update.Item.Id,
                    OldValue = update.ProductPrice.Price.ToString(),
                    NewValue = update.Item.Price.ToString()
                });
            }

            //lấy ra những bảng giá để add
            var productPricesToAdd = input.Where(item => !productPricesToUpdate.Any(update => update.Item == item));

            _dbContext.ProductPrices.AddRange(_mapper.Map<IEnumerable<ProductPrice>>(productPricesToAdd));

            _dbContext.SaveChanges();
        }
    }
}
