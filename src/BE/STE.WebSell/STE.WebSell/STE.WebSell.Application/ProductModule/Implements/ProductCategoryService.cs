﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using STE.ApplicationBase.Common;
using STE.Utils.ConstantVariables.Shared;
using STE.Utils.CustomException;
using STE.Utils.Linq;
using STE.WebSell.Application.Common;
using STE.WebSell.Application.ProductModule.Abstract;
using STE.WebSell.Application.ProductModule.Dtos.ProductCategory;
using STE.WebSell.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.ProductModule.Implements
{
    public class ProductCategoryService : ServiceBase, IProductCategoryService
    {
        public ProductCategoryService(ILogger<ProductCategoryService> logger, IHttpContextAccessor httpContext) : base(logger, httpContext) {}
        
        public ProductCategoryDto Create(CreateProductCategoryDto input)
        {
            if(input.ParentId != null)
            _ = _dbContext.ProductCategories.FirstOrDefault(p => p.Id == input.ParentId && !p.Deleted)
                                            ?? throw new UserFriendlyException(ErrorCode.ParentProductCategoryNotFound);

            var productCategory = new ProductCategory
            {
                Name = input.Name,
                Code = input.Code,
                Avatar = input.Avatar,
                ParentId = input.ParentId,
            };
            var result = _dbContext.ProductCategories.Add(productCategory).Entity;
            _dbContext.SaveChanges();
            return _mapper.Map<ProductCategoryDto>(result);
        }

        public ProductCategoryDto Update(UpdateProductCategoryDto input) 
        {
            var productCategory = _dbContext.ProductCategories.FirstOrDefault(p => p.Id == input.Id && !p.Deleted)
                                  ?? throw new UserFriendlyException(ErrorCode.ProductCategoryNotFound);

            if(input.ParentId != null)
            _ = _dbContext.ProductCategories.FirstOrDefault(p => p.Id == input.ParentId && !p.Deleted)
                                            ?? throw new UserFriendlyException(ErrorCode.ParentProductCategoryNotFound);
            _mapper.Map(input, productCategory);
            _dbContext.SaveChanges();
            return _mapper.Map<ProductCategoryDto>(productCategory);
        }

        public ProductCategoryDto FindById(int id) 
        {
            var productCategory = _dbContext.ProductCategories.FirstOrDefault(p => p.Id == id && !p.Deleted)
                                  ?? throw new UserFriendlyException(ErrorCode.ProductCategoryNotFound);
;
            return _mapper.Map<ProductCategoryDto>(productCategory);
        }

        public PagingResult<ProductCategoryDto> FindAll(FilterProductCategoryDto input) 
        {
            var result = new PagingResult<ProductCategoryDto>();
            var productCategoryQuery = _dbContext.ProductCategories.Where(p => !p.Deleted
                                                                          && (input.Keyword == null
                                                                              || p.Name.ToLower().Contains(input.Keyword.ToLower())
                                                                              || p.Code.ToLower().Contains(input.Keyword.ToLower())
                                                                              || p.Avatar.ToLower().Contains(input.Keyword.ToLower())));

            result.TotalItems = productCategoryQuery.Count();
            productCategoryQuery = productCategoryQuery.OrderDynamic(input.Sort);

            if (input.PageSize != -1)
            {
                productCategoryQuery = productCategoryQuery.Skip(input.GetSkip()).Take(input.PageSize);
            }
            result.Items = _mapper.Map<IEnumerable<ProductCategoryDto>>(productCategoryQuery);
            foreach(var item in result.Items)
            {
                var parentProductCategory = _dbContext.ProductCategories.FirstOrDefault(p => p.Id == item.ParentId && !p.Deleted);
                item.ParentProductCategory = _mapper.Map<ParentProductCategoryDto>(parentProductCategory);
            }
            return result;
        }

        public IEnumerable<ProductCategoryDto> FindList()
        {
            var productCategoryQuery = _dbContext.ProductCategories.Where(p => !p.Deleted && p.Status == ActiveStatus.ACTIVE)
                                                                   .OrderByDescending(p => p.Id)
                                                                   .AsEnumerable();

            var result = _mapper.Map<IEnumerable<ProductCategoryDto>>(productCategoryQuery);
            foreach (var item in result)
            {
                var parentProductCategory = _dbContext.ProductCategories.FirstOrDefault(p => p.Id == item.ParentId && !p.Deleted);
                item.ParentProductCategory = _mapper.Map<ParentProductCategoryDto>(parentProductCategory);
            }
            return result;
        }

        public void Delete(int id)
        {
            var productCategory = _dbContext.ProductCategories.FirstOrDefault(p => p.Id == id && !p.Deleted)
                                  ?? throw new UserFriendlyException(ErrorCode.ProductCategoryNotFound);
            if (_dbContext.Products.Any(p => p.ProductCategoryId == id && !p.Deleted))
            {
                throw new UserFriendlyException(ErrorCode.ProductCategoryIsBeingUsedByProduct);
            }
            else if(_dbContext.ProductCategories.Any(p => p.ParentId == id && !p.Deleted))
            {
                throw new UserFriendlyException(ErrorCode.ProductCategoryIsBeingUsedAsParent);
            }
            
            productCategory.Deleted = true;
            _dbContext.SaveChanges();
        }
        public void ActiveProductCategory(int id)
        {
            var productCategory = _dbContext.ProductCategories.FirstOrDefault(p => p.Id == id && !p.Deleted)
                                  ?? throw new UserFriendlyException(ErrorCode.ProductCategoryNotFound);
            productCategory.Status = (productCategory.Status == ActiveStatus.ACTIVE) ? ActiveStatus.DEACTIVE : ActiveStatus.ACTIVE;
            _dbContext.SaveChanges();
        }
    }
}
