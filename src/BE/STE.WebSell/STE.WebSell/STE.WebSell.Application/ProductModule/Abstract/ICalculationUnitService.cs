﻿using STE.ApplicationBase.Common;
using STE.WebSell.Application.ProductModule.Dtos.CalculationUnit;

namespace STE.WebSell.Application.ProductModule.Abstract
{
    public interface ICalculationUnitService
    {
        /// <summary>
        /// thêm mới đơn vị tính
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public CalculationUnitDto Create(CreateCalculationUnitDto input);

        /// <summary>
        /// lấy thông tin danh mục đơn vị tính theo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        CalculationUnitDto FindById(int id);

        /// <summary>
        /// lấy danh sách phân trang của danh mục đơn vị tính
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        PagingResult<CalculationUnitDto> FindAll(FilterCalculationUnitDto input);
        /// <summary>
        /// cập nhập thông tin danh mục đơn vị tính
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        CalculationUnitDto Update(UpdateCalculationUnitDto input);

        /// <summary>
        /// Xóa danh mục đơn vị tính
        /// </summary>
        /// <param name="id"></param>
        void Delete(int id);
    }
}
