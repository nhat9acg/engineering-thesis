﻿using STE.ApplicationBase.Common;
using STE.WebSell.Application.ProductModule.Dtos.Product;

namespace STE.WebSell.Application.ProductModule.Abstract
{
    public interface IProductService
    {
        /// <summary>
        /// thêm mới sản phẩm
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        ProductDto Create(CreateProductDto input);

        /// <summary>
        /// xóa sản phẩm
        /// </summary>
        /// <param name="id"></param>
        void Delete(int id);

        /// <summary>
        /// lấy danh sách phân trang của sản phẩm
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        //PagingResult<ProductDto> FindAll(FilterProductDto input);
        PagingResult<ProductDto> FindAll(FilterProductDto input);

        /// <summary>
        /// lấy thông tin sản phẩm theo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ProductDto FindById(int id);

        /// <summary>
        /// cập nhập thông tin sản phẩm
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        ProductDto Update(UpdateProductDto input);

        /// <summary>
        /// Bật tắt active product
        /// </summary>
        /// <param name="id"></param>
        void ActiveProduct(int id);
    }
}
