﻿using STE.ApplicationBase.Common;
using STE.WebSell.Application.ProductModule.Dtos.ProductCategory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.ProductModule.Abstract
{
    public interface IProductCategoryService
    {
        /// <summary>
        /// thê mới danh mục sản phẩm
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        ProductCategoryDto Create(CreateProductCategoryDto input);

        /// <summary>
        /// lấy thông tin danh muc san pham theo id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        ProductCategoryDto FindById(int id);

        /// <summary>
        /// lay danh sách phân trang của danh mục sản phẩm
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        PagingResult<ProductCategoryDto> FindAll(FilterProductCategoryDto input);

        /// <summary>
        /// cập nhập thông tin danh mục sản phẩm
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        ProductCategoryDto Update(UpdateProductCategoryDto input);

        /// <summary>
        /// lấy danh sách danh mục sản phẩm đang trong trạng thái hoạt động
        /// </summary>
        /// <returns></returns>
        IEnumerable<ProductCategoryDto> FindList();
        
        /// <summary>
        /// Xóa danh mục sản phẩm
        /// </summary>
        /// <param name="id"></param>
        void Delete(int id);

        

        /// <summary>s
        /// Bật tắt active product
        /// </summary>
        /// <param name="id"></param>
        void ActiveProductCategory(int id);
    }
}
