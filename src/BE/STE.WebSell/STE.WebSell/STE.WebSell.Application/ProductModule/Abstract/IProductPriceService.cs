﻿using STE.ApplicationBase.Common;
using STE.WebSell.Application.ProductModule.Dtos.ProductPrice;

namespace STE.WebSell.Application.ProductModule.Abstract
{
    public interface IProductPriceService
    {
        /// <summary>
        /// Cập nhập giá sản phẩm theo khách hàng
        /// </summary>
        /// <param name="input"></param>
        void Update(List<UpdateProductPriceDto> input);
        /// <summary>
        /// Lấy dữ liệu phân trong trong bảng giá
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        PagingResult<ProductPriceDto> FindAll(FilterProductPriceDto input);

        /// <summary>
        /// lấy thông tin giá sản phẩm theo id
        /// </summary>
        /// <param name="BusinessCustomerId"></param>
        /// <returns></returns>
        IEnumerable<ProductPriceDto> FindProductPriceByBusinessCustomer(int BusinessCustomerId);
        /// <summary>
        /// Thêm giá sản phẩm theo khách hàng
        /// </summary>
        void Add(List<CreateProductPriceDto> input);

        /// <summary>
        /// Xoá giá của sản phẩm theo khách hàng doanh nghiệp
        /// </summary>
        void Delete(List<int> ids);
    }
}
