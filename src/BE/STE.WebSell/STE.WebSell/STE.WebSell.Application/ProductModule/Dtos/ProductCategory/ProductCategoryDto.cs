﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.ProductModule.Dtos.ProductCategory
{
    public class ProductCategoryDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Avatar { get; set; }
        public int Status { get; set; }
        public int? ParentId { get; set; }
        public ParentProductCategoryDto ParentProductCategory { get; set; }
    }

    public class ParentProductCategoryDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Avatar { get; set; }
        public int Status { get; set; }
    }
}
