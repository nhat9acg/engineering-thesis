﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.ProductModule.Dtos.ProductCategory
{
    public class CreateProductCategoryDto
    {
        /// <summary>
        /// tên danh mục sản phẩm
        /// </summary>
        private string _name;
        public string Name 
        { 
            get => _name; 
            set => _name = value?.Trim(); 
        }

        /// <summary>
        /// mã danh mục sản phẩm
        /// </summary>
        private string _code;
        public string Code 
        { 
            get => _code; 
            set => _code = value?.Trim(); 
        }

        /// <summary>
        /// link ảnh avatar
        /// </summary>
        private string _avatar;
        public string Avatar 
        { 
            get => _avatar; 
            set => _avatar = value?.Trim(); 
        }
        public int? ParentId { get; set; }
    }
}
