﻿using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;

namespace STE.WebSell.Application.ProductModule.Dtos.CalculationUnit
{
    public class FilterCalculationUnitDto : PagingRequestBaseDto
    {
        [FromQuery(Name = "name")]
        public string Name { get; set; }
    }
}
