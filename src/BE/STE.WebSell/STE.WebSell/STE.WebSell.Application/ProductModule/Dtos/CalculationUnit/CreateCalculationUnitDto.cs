﻿using System.ComponentModel.DataAnnotations;

namespace STE.WebSell.Application.ProductModule.Dtos.CalculationUnit
{
    public class CreateCalculationUnitDto
    {
        /// <summary>
        /// Tên đơn vị tính
        /// </summary>
        private string _name;
        [Required(AllowEmptyStrings = false)]
        public string Name
        {
            get => _name;
            set => _name = value?.Trim();
        }
    }
}
