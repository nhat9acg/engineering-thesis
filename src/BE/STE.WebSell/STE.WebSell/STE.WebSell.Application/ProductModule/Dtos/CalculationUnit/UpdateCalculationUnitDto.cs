﻿namespace STE.WebSell.Application.ProductModule.Dtos.CalculationUnit
{
    public class UpdateCalculationUnitDto : CreateCalculationUnitDto
    {
        public int Id { get; set; }
    }
}
