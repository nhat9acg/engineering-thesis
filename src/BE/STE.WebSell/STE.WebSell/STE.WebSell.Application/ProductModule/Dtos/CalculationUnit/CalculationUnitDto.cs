﻿namespace STE.WebSell.Application.ProductModule.Dtos.CalculationUnit
{
    public class CalculationUnitDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
