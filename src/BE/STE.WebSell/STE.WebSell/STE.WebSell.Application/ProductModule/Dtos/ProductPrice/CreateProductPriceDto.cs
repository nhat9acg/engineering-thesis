﻿using STE.ApplicationBase.Common.Validations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.ProductModule.Dtos.ProductPrice
{
    public class CreateProductPriceDto
    {
        /// <summary>
        /// Id khách hàng doanh nghiệp
        /// </summary>
        public int BusinessCustomerId { get; set; }

        /// <summary>
        /// Id sản phẩm
        /// </summary>
        public int ProductId { get; set; }

        /// <summary>
        /// Giá sản phẩm
        /// </summary>
        public double Price { get; set; }
    }
}
