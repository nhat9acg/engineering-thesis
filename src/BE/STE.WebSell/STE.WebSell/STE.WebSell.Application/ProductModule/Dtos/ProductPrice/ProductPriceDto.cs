﻿using STE.WebSell.Application.CustomerManagerModule.Dtos.BusinessCustomer;
using STE.WebSell.Application.ProductModule.Dtos.Product;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.ProductModule.Dtos.ProductPrice
{
    public class ProductPriceDto
    {
        public int? Id { get; set; }
        public int? BusinessCustomerId { get; set; }
        public int ProductId { get; set; }
        public double? Price { get; set; }
        public double DefaultPrice { get; set; }
        public ProductDto Product { get; set; }
        public BusinessCustomerDto BusinessCustomer { get; set; }
    }


}
