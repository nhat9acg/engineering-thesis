﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.ProductModule.Dtos.ProductPrice
{
    public class DeleteProductPriceDto
    {
        public int Id { get; set; }
        public bool IsDeleted { get; set; }
    }
}
