﻿using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;

namespace STE.WebSell.Application.ProductModule.Dtos.ProductPrice
{
    public class FilterProductPriceDto : PagingRequestBaseDto
    {
        [FromQuery(Name = "businessCustomerId")]
        public int? BusinessCustomerId { get; set; }
    }
}
