﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.ProductModule.Dtos.Product
{
    public class UpdateProductDto : CreateProductDto
    {
        public int Id { get; set; }
    }
}
