﻿using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;

namespace STE.WebSell.Application.ProductModule.Dtos.Product
{
    public class FilterProductDto : PagingRequestBaseDto
    {
        [FromQuery(Name = "code")]
        public string Code { get; set; }

        [FromQuery(Name = "name")]
        public string Name { get; set; }

        [FromQuery(Name = "productCategory")]
        public int? ProductCategoryId { get; set; }
        [FromQuery(Name = "productCategoryIds")]
        public List<int> ProductCategoryIds { get; set; }
    }
}
