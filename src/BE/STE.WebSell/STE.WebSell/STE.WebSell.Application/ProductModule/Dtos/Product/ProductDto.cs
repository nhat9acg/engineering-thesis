﻿namespace STE.WebSell.Application.ProductModule.Dtos.Product
{
    public class ProductDto
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int Status { get; set; }
        public int ProductCategoryId { get; set; }
        public string ProductCategoryName { get; set; }
        public string Image { get; set; }
        public int CalculationUnitId { get; set; }
        public string CalculationUnitName { get; set; }
        public bool AllowOrder { get; set; }
        public double DefaultPrice { get; set; }
        public double Price { get; set; }
        public string Description { get; set; }
    }

    public class ViewProductCategoryDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Avatar { get; set; }
        public int Status { get; set; }
    }
}
