﻿using System.ComponentModel.DataAnnotations;

namespace STE.WebSell.Application.ProductModule.Dtos.Product
{
    public class CreateProductDto
    {
        /// <summary>
        /// mã sản phẩm
        /// </summary>
        private string _code;
        public string Code
        {
            get => _code;
            set => _code = value?.Trim();
        }

        /// <summary>
        /// Tên sản phẩm
        /// </summary>
        private string _name;
        public string Name
        {
            get => _name;
            set => _name = value?.Trim();
        }

        public int? ProductCategoryId { get; set; }

        private string _image;
        [MaxLength(512)]
        public string Image
        {
            get => _image;
            set => _image = value?.Trim();
        }

        private string _description;
        [MaxLength(500)]
        public string Description
        {
            get => _description;
            set => _description = value?.Trim();
        }

        public int CalculationUnitId { get; set; }
        public bool AllowOrder { get; set; }
        public double DefaultPrice { get; set; }
    }
}
