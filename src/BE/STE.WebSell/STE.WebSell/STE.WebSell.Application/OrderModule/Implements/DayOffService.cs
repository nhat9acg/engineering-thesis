﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using STE.WebSell.Application.Common;
using STE.WebSell.Application.OrderModule.Abstract;
using STE.WebSell.Application.OrderModule.Dtos.DayOff;
using STE.WebSell.Domain.Entities;

namespace STE.WebSell.Application.OrderModule.Implements
{
    public class DayOffService : ServiceBase, IDayOffService
    {
        public DayOffService(ILogger<OrderService> logger, IHttpContextAccessor httpContext) : base(logger, httpContext)
        {
        }

        public void UpdateDayOff(UpdateDayOffDto input)
        {
            input.EndDate ??= input.StartDate;
            for (DateTime start = input.StartDate.Date; start <= input.EndDate.Value.Date; start = start.AddDays(1))
            {
                var dateFind = _dbContext.DayOffs.FirstOrDefault(d => d.Date == start);
                if (dateFind != null)
                {
                    dateFind.Note = input.Note;
                }
                else
                {
                    _dbContext.DayOffs.Add(new DayOff
                    {
                        Date = start.Date,
                        Note = input.Note
                    });
                }
            }
            _dbContext.SaveChanges();
        }

        public void DeleteDayOff(DateTime startDate, DateTime? endDate)
        {
            endDate ??= startDate;
            _dbContext.DayOffs.RemoveRange(_dbContext.DayOffs.Where(d => d.Date >= startDate.Date && d.Date <= endDate.Value.Date));
            _dbContext.SaveChanges();
        }

        public IEnumerable<DayOffDto> GetDayOff(int year)
        {
            return _dbContext.DayOffs.Where(d => d.Date.Year == year)
                .Select(d => new DayOffDto
                {
                    Id = d.Id,
                    Date = d.Date,
                    Note = d.Note
                })
                .AsEnumerable();
        }
    }
}
