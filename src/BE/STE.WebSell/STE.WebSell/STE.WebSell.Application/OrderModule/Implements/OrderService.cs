﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using STE.ApplicationBase.Common;
using STE.Utils.ConstantVariables.HistoryUpdate;
using STE.Utils.ConstantVariables.Order;
using STE.Utils.ConstantVariables.Shared;
using STE.Utils.CustomException;
using STE.Utils.Linq;
using STE.WebSell.Application.AuthenticationModule.Dtos.UserDto;
using STE.WebSell.Application.Common;
using STE.WebSell.Application.OrderModule.Abstract;
using STE.WebSell.Application.OrderModule.Dtos.Order;
using STE.WebSell.Domain.Entities;
using System.Text.Json;

namespace STE.WebSell.Application.OrderModule.Implements
{
    public class OrderService : ServiceBase, IOrderService
    {
        public OrderService(ILogger<OrderService> logger, IHttpContextAccessor httpContext) : base(logger, httpContext)
        {
        }

        public OrderDto CompleteOrder(UpdateOrderDto input)
        {
            _logger.LogInformation($"{nameof(CompleteOrder)}: input = {JsonSerializer.Serialize(input)}");
            var order = _dbContext.Orders.FirstOrDefault(e => e.Id == input.Id && !e.Deleted) ?? throw new UserFriendlyException(ErrorCode.OrderNotFound);
            var historyUpdate = new HistoryUpdate()
            {
                Table = TableUpdate.ORDER,
                Action = Actions.UPDATE,
                Column = ColumnUpdate.STATUS,
                ReferId = order.Id,
                OldValue = order.Status.ToString(),
                NewValue = OrderStatus.COMPLETED.ToString()
            };
            _dbContext.HistoryUpdates.Add(historyUpdate);
            order.TrackingCode = input.TrackingCode;
            order.ShippingProviderId = input.ShippingProviderId;
            order.Status = OrderStatus.COMPLETED;
            order.EstimatedShippingDate = input.EstimatedShippingDate;
            order.HasPaid = true;
            _dbContext.SaveChanges();
            return _mapper.Map<OrderDto>(order);
        }

        public OrderDto Create(CreateOrderDto input)
        {
            _logger.LogInformation($"{nameof(Create)}: input = {JsonSerializer.Serialize(input)}");
            var restaurant = _dbContext.Restaurants.FirstOrDefault(r => r.Id == input.RestaurantId && !r.Deleted) ?? throw new UserFriendlyException(ErrorCode.RestaurantNotFound);
            var businessCustomer = _dbContext.BusinessCustomers.FirstOrDefault(bc => bc.Id == restaurant.BusinessCustomerId && !bc.Deleted) ?? throw new UserFriendlyException(ErrorCode.BusinessCustomerNotFound);

            var dayOff = _dbContext.DayOffs.Where(e => e.Date == input.OrderDate);
            if (dayOff.Any())
            {
                throw new UserFriendlyException(ErrorCode.OrderCannotOnDayOff);
            }

            var insertOrder = new Order()
            {
                RestaurantId = input.RestaurantId,
                Description = input.Description,
                DeliveryAddressId = input.DeliveryAddressId,
                OrderDate = input.OrderDate ?? DateTime.Now,
                InvoiceAddress = input.InvoiceAddress
            };
            var result = _dbContext.Orders.Add(insertOrder);
            _dbContext.SaveChanges();

            if (input.OrderDetails != null)
            {
                foreach (var item in input.OrderDetails)
                {
                    var product = _dbContext.Products.FirstOrDefault(p => p.Id == item.ProductId && !p.Deleted) ?? throw new UserFriendlyException(ErrorCode.ProductNotFound);
                    var productPrice = _dbContext.ProductPrices.FirstOrDefault(price => price.ProductId == item.ProductId && price.BusinessCustomerId == businessCustomer.Id);
                    _dbContext.OrderDetails.Add(new OrderDetail
                    {
                        OrderId = result.Entity.Id,
                        ProductId = item.ProductId,
                        Quantity = item.Quantity,
                        UnitPrice = item.UnitPrice == null ? (productPrice == null ? product.DefaultPrice : productPrice.Price) : item.UnitPrice.Value
                    });
                }
            }
            _dbContext.SaveChanges();

            var historyUpdate = new HistoryUpdate()
            {
                Table = TableUpdate.ORDER,
                Action = Actions.ADD,
                ReferId = result.Entity.Id,
            };
            _dbContext.HistoryUpdates.Add(historyUpdate);
            _dbContext.SaveChanges();
            return _mapper.Map<OrderDto>(result.Entity);
        }

        public virtual void Delete(int id)
        {
            _logger.LogInformation($"{nameof(Delete)}: id = {id}");
            //Xóa đơn hàng
            var order = _dbContext.Orders.FirstOrDefault(e => e.Id == id && !e.Deleted) ?? throw new UserFriendlyException(ErrorCode.OrderNotFound);
            if (order.Status != OrderStatus.NEW && order.Status != OrderStatus.CANCEL)
            {
                throw new UserFriendlyException(ErrorCode.OrderCannotDelete);
            }
            order.Deleted = true;

            // Xóa chi tiết đơn hàng
            var orderDetails = _dbContext.OrderDetails.Where(e => e.OrderId == id);
            if (orderDetails.Any()) _dbContext.OrderDetails.RemoveRange(orderDetails);
            _dbContext.SaveChanges();
        }

        public virtual PagingResult<OrderDto> FindAll(FilterOrderDto input)
        {
            _logger.LogInformation($"{nameof(FindAll)}: input = {JsonSerializer.Serialize(input)}");
            var result = new PagingResult<OrderDto>();

            var orders = _dbContext.Orders.Include(o => o.Restaurant)
                                            .ThenInclude(r => r.BusinessCustomer)
                                          .Include(o => o.OrderDetails)
                                            .ThenInclude(od => od.Product)
                                        .Include(o => o.DeliveryAddress)
                                        .Include(o => o.ShippingProvider)
                                        .Where(order => !order.Deleted && (input.Status == null || order.Status == input.Status)
                                            && order.UserId == null
                                            && ((input.StartDate == null && input.EndDate == null) || (order.OrderDate.Date <= input.EndDate.Value.Date) && (order.OrderDate.Date >= input.StartDate.Value.Date))
                                            && (input.BusinessCustomerId == null || order.Restaurant.BusinessCustomer.Id == input.BusinessCustomerId)
                                            && (input.RestaurentIds == null || input.RestaurentIds.Count() == 0 || input.RestaurentIds.Contains(order.RestaurantId)))
                                        .Select(o => new OrderDto
                                        {
                                            Id = o.Id,
                                            BusinessCustomerId = o.Restaurant.BusinessCustomerId,
                                            BusinessCustomerName = o.Restaurant.BusinessCustomer.FullName,
                                            OrderDate = o.OrderDate,
                                            EstimatedShippingDate = o.EstimatedShippingDate,
                                            RestaurantId = o.RestaurantId,
                                            RestaurantName = o.Restaurant.Name,
                                            DeliveryAddressId = o.DeliveryAddressId,
                                            DeliveryAddress = o.DeliveryAddress.Address,
                                            InvoiceAddress = o.InvoiceAddress,
                                            TrackingCode = o.TrackingCode,
                                            Status = o.Status,
                                            Description = o.Description,
                                            ShippingProviderId = o.ShippingProvider.Id,
                                            ShippingProviderName = o.ShippingProvider.Name,
                                            OrderDetails = _mapper.Map<IEnumerable<OrderDetailDto>>(o.OrderDetails),
                                            TotalMoney = o.OrderDetails.Sum(od => od.Quantity * od.UnitPrice)
                                        });

            result.TotalItems = orders.Count();
            orders = orders.OrderDynamic(input.Sort);

            if (input.PageSize != -1)
            {
                orders = orders.Skip(input.GetSkip()).Take(input.PageSize);
            }
            result.Items = orders.ToList();
            return result;
        }

        public virtual OrderDto FindById(int id)
        {
            _logger.LogInformation($"{nameof(FindById)}: id = {id}");

            var order = _dbContext.Orders.Include(o => o.Restaurant).ThenInclude(r => r.BusinessCustomer)
                                         .Include(o => o.OrderDetails).ThenInclude(od => od.Product).ThenInclude(p => p.CalculationUnit)
                                         .Include(o => o.OrderDetails).ThenInclude(od => od.Product).ThenInclude(p => p.ProductCategory)
                                         .Include(o => o.DeliveryAddress)
                                         .Include(o => o.ShippingProvider)
                                             .FirstOrDefault(e => e.Id == id && !e.Deleted) ?? throw new UserFriendlyException(ErrorCode.OrderNotFound);
            var result = _mapper.Map<OrderDto>(order);

            result.OrderDetails = _mapper.Map<IEnumerable<OrderDetailDto>>(order.OrderDetails);
            //lịch sử phản hồi
            var historyUpdate = _dbContext.HistoryUpdates.Where(h => h.Table == TableUpdate.ORDER && h.ReferId == order.Id)
                .Select(h => new HistoryUpdateOrderDto
                {
                    Id = h.Id,
                    Table = h.Table,
                    Action = h.Action,
                    Column = h.Column,
                    OldValue = h.OldValue,
                    NewValue = h.NewValue,
                    Note = h.Note,
                    ReferId = h.ReferId,
                    CreatedBy = h.CreatedBy,
                    CreatedDate = h.CreatedDate,
                    CreatedUserBy = _dbContext.Users.Select(u => new UserByDto
                    {
                        Id = u.Id,
                        FullName = u.FullName,
                        Username = u.Username,
                        UserType = u.UserType
                    }).FirstOrDefault(u => u.Id == h.CreatedBy)
                });
            result.HistoryUpdates = historyUpdate;
            return result;
        }


        public void ProcessingOrder(int id)
        {
            _logger.LogInformation($"{nameof(ProcessingOrder)}: id = {id}");
            var order = _dbContext.Orders.FirstOrDefault(e => e.Id == id && !e.Deleted) ?? throw new UserFriendlyException(ErrorCode.OrderNotFound);
            var historyUpdate = new HistoryUpdate()
            {
                Table = TableUpdate.ORDER,
                Action = Actions.UPDATE,
                Column = ColumnUpdate.STATUS,
                ReferId = order.Id,
                OldValue = order.Status.ToString(),
                NewValue = OrderStatus.PROCESSING.ToString()
            };
            _dbContext.HistoryUpdates.Add(historyUpdate);
            order.Status = OrderStatus.PROCESSING;
            _dbContext.SaveChanges();
        }

        public OrderDto Update(UpdateOrderDto input)
        {
            _logger.LogInformation($"{nameof(Update)}: input = {JsonSerializer.Serialize(input)}");
            var order = _dbContext.Orders.Include(o => o.Restaurant).ThenInclude(r => r.BusinessCustomer)
                                             .FirstOrDefault(e => e.Id == input.Id && !e.Deleted) ?? throw new UserFriendlyException(ErrorCode.OrderNotFound);

            order.Description = input.Description;
            order.DeliveryAddressId = input.DeliveryAddressId;

            if (input.OrderDetails == null)
            {
                throw new UserFriendlyException(ErrorCode.OrderNotFound);
            }

            var inputList = input.OrderDetails.Select(e => e.ProductId);
            //Danh sách xóa
            var removeList = _dbContext.OrderDetails.Where(e => e.OrderId == input.Id && !inputList.Contains(e.ProductId));
            _dbContext.OrderDetails.RemoveRange(removeList);

            //Cập nhật và thêm
            foreach (var item in input.OrderDetails)
            {
                var detail = _dbContext.OrderDetails.FirstOrDefault(e => e.ProductId == item.ProductId && e.OrderId == order.Id);
                if (detail != null)
                {
                    //không sửa giá
                    detail.Quantity = item.Quantity;
                }
                else
                {
                    var product = _dbContext.Products.FirstOrDefault(e => e.Id == item.ProductId) ?? throw new UserFriendlyException(ErrorCode.ProductNotFound);
                    var productPrice = _dbContext.ProductPrices.FirstOrDefault(price => price.ProductId == item.ProductId && price.BusinessCustomerId == order.Restaurant.BusinessCustomer.Id);

                    _dbContext.OrderDetails.Add(new OrderDetail
                    {
                        OrderId = order.Id,
                        ProductId = item.ProductId,
                        Quantity = item.Quantity,
                        UnitPrice = productPrice == null ? product.DefaultPrice : productPrice.Price
                    });
                }
            }
            _dbContext.SaveChanges();
            return _mapper.Map<OrderDto>(order);
        }

        public virtual void CancelOrder(CancelOrResponseOrderDto input)
        {
            _logger.LogInformation($"{nameof(CancelOrder)}: input = {JsonSerializer.Serialize(input)}");
            var order = _dbContext.Orders.FirstOrDefault(e => e.Id == input.OrderId && !e.Deleted) ?? throw new UserFriendlyException(ErrorCode.OrderNotFound);
            if (order.Status == OrderStatus.COMPLETED || order.Status == OrderStatus.CANCEL)
            {
                throw new UserFriendlyException(ErrorCode.OrderCannotCancel);
            }
            else
            {
                var historyUpdate = new HistoryUpdate()
                {
                    Table = TableUpdate.ORDER,
                    Action = Actions.UPDATE,
                    Column = ColumnUpdate.STATUS,
                    ReferId = input.OrderId,
                    OldValue = order.Status.ToString(),
                    NewValue = OrderStatus.CANCEL.ToString(),
                    Note = input.Note
                };
                _dbContext.HistoryUpdates.Add(historyUpdate);
                order.Status = OrderStatus.CANCEL;
            }
            _dbContext.SaveChanges();
        }

        public virtual void ResponseOrder(CancelOrResponseOrderDto input)
        {
            _logger.LogInformation($"{nameof(ResponseOrder)}: input = {JsonSerializer.Serialize(input)}");
            var order = _dbContext.Orders.FirstOrDefault(e => e.Id == input.OrderId && !e.Deleted) ?? throw new UserFriendlyException(ErrorCode.OrderNotFound);
            var historyUpdate = new HistoryUpdate()
            {
                Table = TableUpdate.ORDER,
                Action = Actions.RESPONSE,
                ReferId = input.OrderId,
                Note = input.Note
            };
            _dbContext.HistoryUpdates.Add(historyUpdate);
            _dbContext.SaveChanges();
        }
    }
}
