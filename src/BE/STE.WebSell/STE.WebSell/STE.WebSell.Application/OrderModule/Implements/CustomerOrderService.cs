﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using STE.ApplicationBase.Common;
using STE.ApplicationBase.CommonModule.Implements;
using STE.Utils.ConstantVariables.HistoryUpdate;
using STE.Utils.ConstantVariables.Shared;
using STE.Utils.CustomException;
using STE.Utils.Linq;
using STE.WebSell.Application.AuthenticationModule.Dtos.UserDto;
using STE.WebSell.Application.Common;
using STE.WebSell.Application.NotificationModule.Abstract;
using STE.WebSell.Application.OrderModule.Abstract;
using STE.WebSell.Application.OrderModule.Dtos.Order;
using STE.WebSell.Application.OrderModule.Dtos.OrderBusinessCustomer;
using STE.WebSell.Application.OrderModule.Dtos.OrderCustomer;
using STE.WebSell.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace STE.WebSell.Application.OrderModule.Implements
{
    public class CustomerOrderService : ServiceBase, ICustomerOrderService
    {
        private readonly ISystemNotificationService _systemNotificationService;
        public CustomerOrderService(ISystemNotificationService systemNotificationService, ILogger<BusinessCustomerOrderService> logger, IHttpContextAccessor httpContext) : base(logger, httpContext)
        {
            _systemNotificationService = systemNotificationService;
        }

        public async Task<int> Create(CreateCustomerOrderDto input)
        {
            _logger.LogInformation($"{nameof(Create)}: input = {JsonSerializer.Serialize(input)}");

           int userId = CommonUtils.GetCurrentUserId(_httpContext);
            var userFind = _dbContext.Users.FirstOrDefault(r => r.Id == userId && !r.Deleted) ?? throw new UserFriendlyException(ErrorCode.UserNotFound);
            var transaction = _dbContext.Database.BeginTransaction();
            var insertOrder = new Order()
            {
                UserId = userId,
                Description = input.Description,
                DeliveryAddressId = input.DeliveryAddressId,
                OrderDate = input.OrderDate ?? DateTime.Now,
                InvoiceAddress = input.InvoiceAddress,
            };
            var result = _dbContext.Orders.Add(insertOrder);
            _dbContext.SaveChanges();

            if (input.OrderDetails != null)
            {
                foreach (var item in input.OrderDetails)
                {
                    var carFind = _dbContext.Carts.FirstOrDefault(c => c.Id == item.CartId && !c.Deleted && c.Status == 1) ?? throw new UserFriendlyException(ErrorCode.CartNotFound);
                    carFind.Status = 2;
                    _dbContext.SaveChanges();
                    var product = _dbContext.Products.FirstOrDefault(p => p.Id == item.ProductId && !p.Deleted) ?? throw new UserFriendlyException(ErrorCode.ProductNotFound);
                    _dbContext.OrderDetails.Add(new OrderDetail
                    {
                        OrderId = result.Entity.Id,
                        ProductId = item.ProductId,
                        Quantity = item.Quantity,
                        UnitPrice = product.DefaultPrice
                    });
                }
            }

            var historyUpdate = new HistoryUpdate()
            {
                Table = TableUpdate.ORDER,
                Action = Actions.ADD,
                ReferId = result.Entity.Id,
            };
            _dbContext.HistoryUpdates.Add(historyUpdate);
            _dbContext.SaveChanges();
            transaction.Commit();
            await _systemNotificationService.SendNotificationCustomerOrder(userId, result.Entity.Id);
            return result.Entity.Id;
        }

        public async Task HasPaid(int orderId)
        {
            _logger.LogInformation($"{nameof(HasPaid)}: orderId = {orderId}");
            int userId = CommonUtils.GetCurrentUserId(_httpContext);
            var userFind = _dbContext.Users.FirstOrDefault(r => r.Id == userId && !r.Deleted) ?? throw new UserFriendlyException(ErrorCode.UserNotFound);
            var orderFind = _dbContext.Orders.FirstOrDefault(o => o.Id == orderId) ?? throw new UserFriendlyException(ErrorCode.OrderNotFound);
            orderFind.HasPaid = true;
            await _dbContext.SaveChangesAsync();
        }

        public PagingResult<CustomerOrderDto> FindAll(FilterOrderDto input)
        {
            _logger.LogInformation($"{nameof(FindAll)}: input = {JsonSerializer.Serialize(input)}");
            var result = new PagingResult<CustomerOrderDto>();
            int userId = CommonUtils.GetCurrentUserId(_httpContext);
            var userFind = _dbContext.Users.FirstOrDefault(u => u.Id == userId && !u.Deleted && u.UserType == 4);
            var orders = _dbContext.Orders.Include(o => o.User)
                                          .Include(o => o.OrderDetails)
                                            .ThenInclude(od => od.Product)
                                        .Include(o => o.DeliveryAddress)
                                        .Include(o => o.ShippingProvider)
                                        .Where(order => !order.Deleted && (input.Status == null || order.Status == input.Status)
                                             && order.UserId != null
                                             && (userFind == null || userFind.Id == order.UserId)
                                             && ((input.StartDate == null && input.EndDate == null) || (order.OrderDate.Date <= input.EndDate.Value.Date) 
                                             && (order.OrderDate.Date >= input.StartDate.Value.Date)))
                                        .Select(o => new CustomerOrderDto
                                        {
                                            Id = o.Id,
                                            UserId = o.User.Id,
                                            FullName = o.User.FullName,
                                            OrderDate = o.OrderDate,
                                            EstimatedShippingDate = o.EstimatedShippingDate,
                                            DeliveryAddressId = o.DeliveryAddressId,
                                            DeliveryAddress = o.DeliveryAddress.Address,
                                            InvoiceAddress = o.InvoiceAddress,
                                            TrackingCode = o.TrackingCode,
                                            Status = o.Status,
                                            Description = o.Description,
                                            ShippingProviderId = o.ShippingProvider.Id,
                                            ShippingProviderName = o.ShippingProvider.Name,
                                            OrderDetails = o.OrderDetails.Select(ed => new OrderDetailDto
                                            {
                                                Id = ed.Id,
                                                OrderId = ed.OrderId,
                                                UnitPrice = ed.UnitPrice,
                                                ProductId = ed.ProductId,
                                                ProductName = ed.Product.Name,
                                                ProductCategoryName = ed.Product.ProductCategory.Name,
                                                CalculationUnit = ed.Product.CalculationUnit.Name,
                                                Description = ed.Description,
                                                Quantity = ed.Quantity,
                                            }),
                                            TotalMoney = o.OrderDetails.Sum(od => od.Quantity * od.UnitPrice)
                                        });

            result.TotalItems = orders.Count();
            orders = orders.OrderDynamic(input.Sort);

            if (input.PageSize != -1)
            {
                orders = orders.Skip(input.GetSkip()).Take(input.PageSize);
            }
            result.Items = orders;
            return result;
        }

        public CustomerOrderDto FindById(int id)
        {
            _logger.LogInformation($"{nameof(FindById)}: id = {id}");

            var order = _dbContext.Orders.Include(o => o.User)
                                         .Include(o => o.OrderDetails).ThenInclude(od => od.Product).ThenInclude(p => p.CalculationUnit)
                                         .Include(o => o.OrderDetails).ThenInclude(od => od.Product).ThenInclude(p => p.ProductCategory)
                                         .Include(o => o.DeliveryAddress)
                                         .Include(o => o.ShippingProvider)
                                             .FirstOrDefault(e => e.Id == id && !e.Deleted) ?? throw new UserFriendlyException(ErrorCode.OrderNotFound);
            var result = _mapper.Map<CustomerOrderDto>(order);

            result.OrderDetails = _mapper.Map<IEnumerable<OrderDetailDto>>(order.OrderDetails);
            //lịch sử phản hồi
            var historyUpdate = _dbContext.HistoryUpdates.Where(h => h.Table == TableUpdate.ORDER && h.ReferId == order.Id)
                .Select(h => new HistoryUpdateOrderDto
                {
                    Id = h.Id,
                    Table = h.Table,
                    Action = h.Action,
                    Column = h.Column,
                    OldValue = h.OldValue,
                    NewValue = h.NewValue,
                    Note = h.Note,
                    ReferId = h.ReferId,
                    CreatedBy = h.CreatedBy,
                    CreatedDate = h.CreatedDate,
                    CreatedUserBy = _dbContext.Users.Select(u => new UserByDto
                    {
                        Id = u.Id,
                        FullName = u.FullName,
                        Username = u.Username,
                        UserType = u.UserType
                    }).FirstOrDefault(u => u.Id == h.CreatedBy)
                });
            result.HistoryUpdates = historyUpdate;
            return result;
        }
    }
}
