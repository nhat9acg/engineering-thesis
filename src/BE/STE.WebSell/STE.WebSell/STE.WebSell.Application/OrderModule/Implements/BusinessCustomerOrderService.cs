﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using STE.ApplicationBase.Common;
using STE.ApplicationBase.CommonModule.Implements;
using STE.Utils.ConstantVariables.Shared;
using STE.Utils.CustomException;
using STE.WebSell.Application.OrderModule.Abstract;
using STE.WebSell.Application.OrderModule.Dtos.Order;
using STE.WebSell.Application.OrderModule.Dtos.OrderBusinessCustomer;
using System.Text.Json;

namespace STE.WebSell.Application.OrderModule.Implements
{
    public class BusinessCustomerOrderService : OrderService, IBusinessCustomerOrderService
    {
        public BusinessCustomerOrderService(ILogger<BusinessCustomerOrderService> logger, IHttpContextAccessor httpContext) : base(logger, httpContext)
        {
        }

        public PagingResult<OrderDto> FindAll(FilterOrderBusinessCustomerDto input)
        {
            _logger.LogInformation($"{nameof(FindAll)}: input = {JsonSerializer.Serialize(input)}");
            var filter = _mapper.Map<FilterOrderDto>(input);
            int businessCustomerId = CommonUtils.GetCurrentBusinessCustomerId(_httpContext);
            filter.BusinessCustomerId = businessCustomerId;
            return base.FindAll(filter);
        }

        private void CheckHasOrder(int orderId)
        {
            int businessCustomerId = CommonUtils.GetCurrentBusinessCustomerId(_httpContext);
            if (!_dbContext.Orders.Any(o => o.Id == orderId && o.Restaurant.BusinessCustomer.Id == businessCustomerId))
            {
                throw new UserFriendlyException(ErrorCode.OrderNotFound);
            }
        }

        public OrderDto Create(CreateOrderBusinessCustomerDto input)
        {
            _logger.LogInformation($"{nameof(Create)}: input = {JsonSerializer.Serialize(input)}");
            int businessCustomerId = CommonUtils.GetCurrentBusinessCustomerId(_httpContext);
            if (!_dbContext.Restaurants.Any(r => r.BusinessCustomer.Id == businessCustomerId && r.Id == input.RestaurantId))
            {
                throw new UserFriendlyException(ErrorCode.RestaurantNotFound);
            }
            if (!_dbContext.DeliveryAddresses.Any(r => r.Restaurant.BusinessCustomer.Id == businessCustomerId && r.Id == input.DeliveryAddressId))
            {
                throw new UserFriendlyException(ErrorCode.DeliveryAddressNotFound);
            }
            var inputCreate = _mapper.Map<CreateOrderDto>(input);
            return Create(inputCreate);
        }

        public OrderDto Update(UpdateOrderBusinessCustomerDto input)
        {
            _logger.LogInformation($"{nameof(Update)}: input = {JsonSerializer.Serialize(input)}");
            CheckHasOrder(input.Id);
            var inputUpdate = _mapper.Map<UpdateOrderDto>(input);
            return Update(inputUpdate);
        }

        public override void Delete(int id)
        {
            _logger.LogInformation($"{nameof(Delete)}: id = {id}");
            CheckHasOrder(id);
            base.Delete(id);
        }

        public override OrderDto FindById(int id)
        {
            _logger.LogInformation($"{nameof(FindById)}: id = {id}");
            CheckHasOrder(id);
            return base.FindById(id);
        }

        public override void ResponseOrder(CancelOrResponseOrderDto input)
        {
            _logger.LogInformation($"{nameof(ResponseOrder)}: input = {JsonSerializer.Serialize(input)}");
            CheckHasOrder(input.OrderId);
            base.ResponseOrder(input);
        }

        public override void CancelOrder(CancelOrResponseOrderDto input)
        {
            _logger.LogInformation($"{nameof(CancelOrder)}: input = {JsonSerializer.Serialize(input)}");
            CheckHasOrder(input.OrderId);
            base.CancelOrder(input);
        }
    }
}
