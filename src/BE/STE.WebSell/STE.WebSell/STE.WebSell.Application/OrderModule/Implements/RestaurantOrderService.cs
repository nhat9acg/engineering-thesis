﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using STE.ApplicationBase.Common;
using STE.ApplicationBase.CommonModule.Implements;
using STE.Utils.ConstantVariables.Shared;
using STE.Utils.CustomException;
using STE.WebSell.Application.OrderModule.Abstract;
using STE.WebSell.Application.OrderModule.Dtos.Order;
using STE.WebSell.Application.OrderModule.Dtos.OrderRestaurant;
using System.Text.Json;

namespace STE.WebSell.Application.OrderModule.Implements
{
    public class RestaurantOrderService : OrderService, IRestaurantOrderService
    {
        public RestaurantOrderService(ILogger<RestaurantOrderService> logger, IHttpContextAccessor httpContext) : base(logger, httpContext)
        {
        }

        public OrderDto Create(CreateOrderRestaurantDto input)
        {
            _logger.LogInformation($"{nameof(Create)}: input = {JsonSerializer.Serialize(input)}");
            var inputCreate = _mapper.Map<CreateOrderDto>(input);
            inputCreate.RestaurantId = CommonUtils.GetCurrentRestaurantId(_httpContext);
            return Create(inputCreate);
        }

        private void CheckHasOrder(int orderId)
        {
            int restaurantId = CommonUtils.GetCurrentRestaurantId(_httpContext);
            if (!_dbContext.Orders.Any(o => o.Id == orderId && o.Restaurant.Id == restaurantId))
            {
                throw new UserFriendlyException(ErrorCode.OrderNotFound);
            }
        }

        public OrderDto Update(UpdateOrderRestaurantDto input)
        {
            _logger.LogInformation($"{nameof(Update)}: input = {JsonSerializer.Serialize(input)}");
            CheckHasOrder(input.Id);
            var inputUpdate = _mapper.Map<UpdateOrderDto>(input);
            inputUpdate.RestaurantId = CommonUtils.GetCurrentRestaurantId(_httpContext);
            return Update(inputUpdate);
        }

        public PagingResult<OrderDto> FindAll(FilterOrderRestaurantDto input)
        {
            _logger.LogInformation($"{nameof(FindAll)}: input = {JsonSerializer.Serialize(input)}");
            int restaurantId = CommonUtils.GetCurrentRestaurantId(_httpContext);
            var filter = _mapper.Map<FilterOrderDto>(input);
            filter.RestaurentIds = new List<int?> { restaurantId };
            return base.FindAll(filter);
        }

        public override void Delete(int id)
        {
            _logger.LogInformation($"{nameof(Delete)}: id = {id}");
            CheckHasOrder(id);
            base.Delete(id);
        }

        public override OrderDto FindById(int id)
        {
            _logger.LogInformation($"{nameof(FindById)}: id = {id}");
            CheckHasOrder(id);
            return base.FindById(id);
        }

        public override void ResponseOrder(CancelOrResponseOrderDto input)
        {
            _logger.LogInformation($"{nameof(ResponseOrder)}: input = {JsonSerializer.Serialize(input)}");
            CheckHasOrder(input.OrderId);
            base.ResponseOrder(input);
        }

        public override void CancelOrder(CancelOrResponseOrderDto input)
        {
            _logger.LogInformation($"{nameof(CancelOrder)}: input = {JsonSerializer.Serialize(input)}");
            CheckHasOrder(input.OrderId);
            base.CancelOrder(input);
        }
    }
}
