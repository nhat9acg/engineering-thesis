﻿using STE.Utils.ConstantVariables.Order;
using STE.WebSell.Application.OrderModule.Dtos.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.OrderModule.Dtos.OrderCustomer
{
    public class CustomerOrderDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// ID khách hàng
        /// </summary>
        public int? UserId { get; set; }
        /// <summary>
        /// Tên khách hàng 
        /// </summary>
        public string FullName { get; set; } = null!;
        /// <summary>
        /// Trạng thái
        /// <see cref="OrderStatus"/>
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// Ghi chú
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Id địa chỉ
        /// </summary>
        public int? DeliveryAddressId { get; set; }
        /// <summary>
        /// Địa chỉ giao hàng
        /// </summary>
        public string DeliveryAddress { get; set; }
        public double TotalMoney { get; set; }
        /// <summary>
        /// Thời gian giao hàng dự kiến
        /// </summary>
        public DateTime? EstimatedShippingDate { get; set; }
        /// <summary>
        /// Ngày đặt hàng
        /// </summary>
        public DateTime OrderDate { get; set; }
        /// <summary>
        /// Địa chỉ giao hàng trên hóa đơn
        /// </summary>
        public string InvoiceAddress { get; set; }
        public int? InvoiceAddressId { get; set; }
        /// <summary>
        /// Mã tracking
        /// </summary>
        public string TrackingCode { get; set; }
        public int? ShippingProviderId { get; set; }
        public string ShippingProviderName { get; set; }
        public bool? HasPaid { get; set; }

        public IEnumerable<OrderDetailDto> OrderDetails { get; set; }
        public IEnumerable<HistoryUpdateOrderDto> HistoryUpdates { get; set; }
    }
}
