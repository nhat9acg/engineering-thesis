﻿using STE.WebSell.Application.OrderModule.Dtos.Order;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.OrderModule.Dtos.OrderCustomer
{
    public class CreateCustomerOrderDto
    {
        private string _description;
        /// <summary>
        /// Ghi chú
        /// </summary>
        public string Description
        {
            get => _description;
            set => _description = value?.Trim();
        }
        /// <summary>
        /// Địa chỉ giao hàng
        /// </summary>
        public int DeliveryAddressId { get; set; }
        /// <summary>
        /// Ngày đặt hàng
        /// </summary>
        public DateTime? OrderDate { get; set; }

        private string _invoiceAddress;
        /// <summary>
        /// Địa chỉ trên hóa đơn
        /// </summary>
        public string InvoiceAddress
        {
            get => _invoiceAddress;
            set => _invoiceAddress = value?.Trim();
        }

        public List<CreateOrderDetailDto> OrderDetails { get; set; }
    }
}
