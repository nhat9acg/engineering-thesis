﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.OrderModule.Dtos.OrderCustomer
{
    public class CreateCustomerOrderDetailDto
    {
        /// <summary>
        /// Id product
        /// </summary>
        public int ProductId { get; set; }
        /// <summary>
        /// Số lượng
        /// </summary>
        public int Quantity { get; set; }
    }
}
