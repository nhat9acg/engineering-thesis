﻿using STE.Utils.ConstantVariables.HistoryUpdate;
using STE.WebSell.Application.AuthenticationModule.Dtos.UserDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.OrderModule.Dtos.Order
{
    public class HistoryUpdateOrderDto
    {
        public int Id { get; set; }
        /// <summary>
        /// bảng update
        /// <see cref="TableUpdate"/>
        /// </summary>
        public int Table { get; set; }
        /// <summary>
        /// Trường update
        /// <see cref="ColumnUpdate"/>
        /// </summary>
        public string Column { get; set; }
        /// <summary>
        /// Hành động Update (1: thêm, 2: sửa, 3: xoá, 4: phản hồi)
        /// <see cref="Actions"/>
        /// </summary>
        public int Action { get; set; }
        /// <summary>
        /// Id tham chiếu đến bảng update
        /// </summary>
        public int ReferId { get; set; }
        /// <summary>
        /// Giá trị cũ
        /// </summary>
        public string OldValue { get; set; }
        /// <summary>
        /// Giá trị mới
        /// </summary>
        public string NewValue { get; set; }
        /// <summary>
        /// Ghi chú tổng quan
        /// </summary>
        public string Note { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        /// <summary>
        /// Thông tin về user tương tác
        /// </summary>
        public UserByDto CreatedUserBy { get; set; }
    }
}
