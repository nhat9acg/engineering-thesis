﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.OrderModule.Dtos.Order
{
    public class CancelOrResponseOrderDto
    {
        public int OrderId { get; set; }
        public string Note { get; set; }
    }
}
