﻿namespace STE.WebSell.Application.OrderModule.Dtos.Order
{
    public class CreateOrderDetailDto
    {
        /// <summary>
        /// Id giỏ hàng
        /// </summary>
        public int? CartId { get; set; }
        /// <summary>
        /// Id product
        /// </summary>
        public int ProductId { get; set; }
        /// <summary>
        /// Số lượng
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// Đơn giá
        /// </summary>
        public double? UnitPrice { get; set; }
    }
}
