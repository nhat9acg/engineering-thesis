﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.OrderModule.Dtos.Order
{
    public class OrderDetailDto
    {
        /// <summary>
        /// Id
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// Id Order
        /// </summary>
        public int OrderId { get; set; }
        /// <summary>
        /// Id product
        /// </summary>
        public int ProductId { get; set; }
        /// <summary>
        /// Tên sản phẩm
        /// </summary>
        public string ProductName { get; set; }
        /// <summary>
        /// Nhãn
        /// </summary>
        public string ProductCategoryName { get; set; }
        /// <summary>
        /// Đơn vị tính
        /// </summary>
        public string CalculationUnit { get; set; }
        /// <summary>
        /// Số lượng
        /// </summary>
        public int Quantity { get; set; }
        /// <summary>
        /// Đơn giá
        /// </summary>
        public double UnitPrice { get; set; }
        /// <summary>
        /// Ghi chú
        /// </summary>
        public string Description { get; set; }
    }
}
