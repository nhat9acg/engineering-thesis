﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.OrderModule.Dtos.Order
{
    public class UpdateOrderDto : CreateOrderDto
    {
        public int Id { get; set; }

        private string _trackingCode;
        /// <summary>
        /// Ghi chú
        /// </summary>
        public string TrackingCode
        {
            get => _trackingCode;
            set => _trackingCode = value?.Trim();
        }

        /// <summary>
        /// Đơn vị vận chuyển
        /// </summary>
        public int? ShippingProviderId { get; set; }
        public DateTime? EstimatedShippingDate { get; set; }
    }
}
