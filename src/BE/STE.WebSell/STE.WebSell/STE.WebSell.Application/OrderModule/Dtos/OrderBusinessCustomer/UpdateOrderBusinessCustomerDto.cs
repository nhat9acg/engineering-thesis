﻿namespace STE.WebSell.Application.OrderModule.Dtos.OrderBusinessCustomer
{
    public class UpdateOrderBusinessCustomerDto : CreateOrderBusinessCustomerDto
    {
        public int Id { get; set; }
    }
}
