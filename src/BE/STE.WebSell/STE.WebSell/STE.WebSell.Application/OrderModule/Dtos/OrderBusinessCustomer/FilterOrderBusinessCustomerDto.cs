﻿using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;

namespace STE.WebSell.Application.OrderModule.Dtos.OrderBusinessCustomer
{
    public class FilterOrderBusinessCustomerDto : PagingRequestBaseDto
    {
        [FromQuery(Name = "status")]
        public int? Status { get; set; }

        [FromQuery(Name = "startDate")]
        public DateTime? StartDate { get; set; }

        [FromQuery(Name = "endDate")]
        public DateTime? EndDate { get; set; }

        [FromQuery(Name = "restaurentIds")]
        public IEnumerable<int> RestaurentIds { get; set; }
    }
}
