﻿using STE.WebSell.Application.OrderModule.Dtos.OrderRestaurant;

namespace STE.WebSell.Application.OrderModule.Dtos.OrderBusinessCustomer
{
    public class CreateOrderBusinessCustomerDto : CreateOrderRestaurantDto
    {
        /// <summary>
        /// ID nhà hàng
        /// </summary>
        public int RestaurantId { get; set; }
    }
}
