﻿namespace STE.WebSell.Application.OrderModule.Dtos.OrderRestaurant
{
    public class CreateOrderDetailRestaurantDto
    {
        /// <summary>
        /// Id product
        /// </summary>
        public int ProductId { get; set; }
        /// <summary>
        /// Số lượng
        /// </summary>
        public int Quantity { get; set; }
    }
}
