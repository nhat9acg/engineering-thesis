﻿namespace STE.WebSell.Application.OrderModule.Dtos.OrderRestaurant
{
    public class UpdateOrderRestaurantDto : CreateOrderRestaurantDto
    {
        public int Id { get; set; }
    }
}
