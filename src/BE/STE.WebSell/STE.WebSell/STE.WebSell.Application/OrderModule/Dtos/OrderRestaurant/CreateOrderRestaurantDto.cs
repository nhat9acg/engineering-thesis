﻿namespace STE.WebSell.Application.OrderModule.Dtos.OrderRestaurant
{
    public class CreateOrderRestaurantDto
    {
        private string _description;
        /// <summary>
        /// Ghi chú
        /// </summary>
        public string Description
        {
            get => _description;
            set => _description = value?.Trim();
        }
        /// <summary>
        /// Địa chỉ giao hàng
        /// </summary>
        public int DeliveryAddressId { get; set; }
        /// <summary>
        /// Ngày đặt hàng
        /// </summary>
        public DateTime? OrderDate { get; set; }

        private string _invoiceAddress;
        /// <summary>
        /// Địa chỉ trên hóa đơn
        /// </summary>
        public string InvoiceAddress
        {
            get => _invoiceAddress;
            set => _invoiceAddress = value?.Trim();
        }

        public List<CreateOrderDetailRestaurantDto> OrderDetails { get; set; }
    }
}
