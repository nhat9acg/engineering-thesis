﻿using Microsoft.AspNetCore.Mvc;
using STE.ApplicationBase.Common;

namespace STE.WebSell.Application.OrderModule.Dtos.OrderRestaurant
{
    public class FilterOrderRestaurantDto : PagingRequestBaseDto
    {
        [FromQuery(Name = "status")]
        public int? Status { get; set; }

        [FromQuery(Name = "startDate")]
        public DateTime? StartDate { get; set; }

        [FromQuery(Name = "endDate")]
        public DateTime? EndDate { get; set; }
    }
}
