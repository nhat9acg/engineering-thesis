﻿namespace STE.WebSell.Application.OrderModule.Dtos.DayOff
{
    public class UpdateDayOffDto
    {
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        private string _note;
        public string Note
        {
            get => _note;
            set => _note = value?.Trim();
        }
    }
}
