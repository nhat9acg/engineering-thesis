﻿using STE.ApplicationBase.Common;
using STE.WebSell.Application.OrderModule.Dtos.Order;
using STE.WebSell.Application.OrderModule.Dtos.OrderRestaurant;

namespace STE.WebSell.Application.OrderModule.Abstract
{
    public interface IRestaurantOrderService : IOrderService
    {
        PagingResult<OrderDto> FindAll(FilterOrderRestaurantDto input);
        OrderDto Create(CreateOrderRestaurantDto input);
        OrderDto Update(UpdateOrderRestaurantDto input);
    }
}
