﻿using STE.ApplicationBase.Common;
using STE.WebSell.Application.OrderModule.Dtos.Order;
using STE.WebSell.Application.OrderModule.Dtos.OrderBusinessCustomer;

namespace STE.WebSell.Application.OrderModule.Abstract
{
    public interface IBusinessCustomerOrderService : IOrderService
    {
        PagingResult<OrderDto> FindAll(FilterOrderBusinessCustomerDto input);
        OrderDto Create(CreateOrderBusinessCustomerDto input);
        OrderDto Update(UpdateOrderBusinessCustomerDto input);
    }
}
