﻿using STE.ApplicationBase.Common;
using STE.WebSell.Application.OrderModule.Dtos.Order;
using STE.WebSell.Application.OrderModule.Dtos.OrderCustomer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.OrderModule.Abstract
{
    public interface ICustomerOrderService
    {
        Task<int> Create(CreateCustomerOrderDto input);
        Task HasPaid(int orderId);
        PagingResult<CustomerOrderDto> FindAll(FilterOrderDto input); 
        CustomerOrderDto FindById(int id);
    }
}
