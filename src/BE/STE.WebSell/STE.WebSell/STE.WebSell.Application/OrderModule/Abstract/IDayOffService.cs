﻿using STE.WebSell.Application.OrderModule.Dtos.DayOff;

namespace STE.WebSell.Application.OrderModule.Abstract
{
    public interface IDayOffService
    {
        /// <summary>
        /// Cập nhật ngày không làm việc
        /// </summary>
        /// <param name="input"></param>
        void UpdateDayOff(UpdateDayOffDto input);
        IEnumerable<DayOffDto> GetDayOff(int year);
        void DeleteDayOff(DateTime startDate, DateTime? endDate);
    }
}
