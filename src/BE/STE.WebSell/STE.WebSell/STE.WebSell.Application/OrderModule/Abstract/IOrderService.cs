﻿using STE.ApplicationBase.Common;
using STE.WebSell.Application.OrderModule.Dtos.Order;

namespace STE.WebSell.Application.OrderModule.Abstract
{
    public interface IOrderService
    {
        OrderDto Create(CreateOrderDto input);
        OrderDto Update(UpdateOrderDto input);
        void Delete(int id);
        OrderDto FindById(int id);
        PagingResult<OrderDto> FindAll(FilterOrderDto input);
        void ProcessingOrder(int id);
        OrderDto CompleteOrder(UpdateOrderDto input);
        void CancelOrder(CancelOrResponseOrderDto input);
        void ResponseOrder(CancelOrResponseOrderDto input);
    }
}
