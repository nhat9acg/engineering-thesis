﻿using ClosedXML.Excel;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using STE.ApplicationBase.CommonModule.Implements;
using STE.Utils.ConstantVariables.User;
using STE.WebSell.Application.Common;
using STE.WebSell.Application.ExportExcelReportModule.Abstract;
using STE.WebSell.Application.ExportExcelReportModule.Dtos;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.ExportExcelReportModule.Implements
{
    public class ExportExcelReportService : ServiceBase, IExportExcelReportService
    {
        public ExportExcelReportService(ILogger<ExportExcelReportService> logger, IHttpContextAccessor httpContext) : base(logger, httpContext)
        {
        }

        public async Task<ExportResultDto> SyntheticMoneyProduct(DateTime? startDate, DateTime? endDate)
        {
            var result = new ExportResultDto();
            var syntheticMoneyProduct = new List<SyntheticMoneyProductDto>();
            List<Task> tasks = new();
            ConcurrentBag<SyntheticMoneyProductDto> syntheticMoneyProductCurrentbag = new();

            var listItem = _dbContext.Products.Include(e => e.OrderDetails).ThenInclude(o => o.Order)
                                                .Include(e => e.ProductCategory)
                    .Where(e => !e.Deleted && e.OrderDetails.Any(od => od.Order.CreatedDate >= startDate.Value.Date && od.Order.CreatedDate <= endDate.Value.Date));

            foreach (var item in listItem)
            {
                var itemProduct = new SyntheticMoneyProductDto();
                itemProduct.Code = item.Name;
                itemProduct.Category = item.ProductCategory.Name;
                itemProduct.Total = item.OrderDetails.Sum(od => od.Quantity);
                itemProduct.PaymentAmount = (decimal)item.OrderDetails.Sum(od => od.Quantity * od.UnitPrice); 
                syntheticMoneyProductCurrentbag.Add(itemProduct);
            };

            await Task.WhenAll(tasks);

            syntheticMoneyProduct.AddRange(syntheticMoneyProductCurrentbag);
            using var workbook = new XLWorkbook();
            var worksheet = workbook.Worksheets.Add("VVN");
            var currentRow = 2;
            var stt = 0;
            worksheet.Cell(1, 3).Value = "Đặt mua";
            worksheet.Range("C1:E2").Style.Fill.BackgroundColor = XLColor.FromArgb(226, 239, 218);

            worksheet.Cell(currentRow, 1).Value = "STT";
            worksheet.Cell(currentRow, 2).Value = "Sản phẩm";
            worksheet.Cell(currentRow, 3).Value = "Danh mục";
            worksheet.Cell(currentRow, 4).Value = "Tổng số lượng";
            worksheet.Cell(currentRow, 5).Value = "Tổng doanh thu";

            worksheet.Column("A").Width = 10;
            worksheet.Column("B").Width = 40;
            worksheet.Column("C").Width = 40;
            worksheet.Column("D").Width = 20;
            worksheet.Column("E").Width = 20;

            foreach (var item in syntheticMoneyProduct)
            {
                currentRow++;
                stt = ++stt;
                worksheet.Cell(currentRow, 1).Value = stt;
                worksheet.Cell(currentRow, 2).Value = item.Code;
                worksheet.Cell(currentRow, 3).Value = item.Category;
                worksheet.Cell(currentRow, 4).Value = item.Total;
                worksheet.Cell(currentRow, 5).Value = item.PaymentAmount;

            }
            using var stream = new MemoryStream();
            workbook.SaveAs(stream);
            result.fileData = stream.ToArray();
            return result;
        }
    }
}
