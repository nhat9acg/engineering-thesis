﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.ExportExcelReportModule.Dtos
{
    public class ExportResultDto
    {
        public byte[] fileData { get; set; }
        public string filePath { get; set; }
        public string fileDownloadName { get; set; }
    }

    public class ResultFileDto
    {
        public Stream Stream { get; set; }
        public string FileName { get; set; }
    }
}
