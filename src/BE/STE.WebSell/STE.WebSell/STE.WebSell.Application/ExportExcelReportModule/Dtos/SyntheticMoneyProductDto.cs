﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.ExportExcelReportModule.Dtos
{
    public class SyntheticMoneyProductDto
    {
        public string Code { get; set; }
        public string Category { get; set; }
        public int Total { get; set; }
        public int Status { get; set; }
        public int OrderStatus { get; set; }
        public decimal PaymentAmount { get; set; }
    }
}
