﻿using STE.WebSell.Application.ExportExcelReportModule.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Application.ExportExcelReportModule.Abstract
{
    public interface IExportExcelReportService
    {
        /// <summary>
        /// bao cao tien vao san pham
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        Task<ExportResultDto> SyntheticMoneyProduct(DateTime? startDate, DateTime? endDate);
    }
}
