﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Domain.Enums
{
    public enum MediaStatusEnum
    {
        Activated = 1,
        Deactivate = 2
    }
}
