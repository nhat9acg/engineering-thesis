﻿using Microsoft.EntityFrameworkCore;
using STE.EntitiesBase.Entities;
using STE.Utils.ConstantVariables.Database;
using STE.Utils.ConstantVariables.User;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace STE.WebSell.Domain.Entities
{
    [Table(nameof(Role), Schema = DbSchemas.Default)]
    [Index(nameof(Deleted), nameof(Name), Name = $"IX_{nameof(Role)}")]
    public class Role : IRole
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(256)]
        public string Name { get; set; } = null!;
        /// <summary>
        /// <see cref="UserTypes"/>
        /// </summary>
        public int UserType { get; set; }

        [MaxLength(1024)]
        public string? Description { get; set; }

        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
        public bool Deleted { get; set; }
        public int? DeletedBy { get; set; }
    }
}
