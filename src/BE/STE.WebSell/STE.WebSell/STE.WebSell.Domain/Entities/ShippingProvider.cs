﻿using STE.EntitiesBase.Interfaces;
using STE.Utils.ConstantVariables.Database;
using STE.Utils.ConstantVariables.Shared;

namespace STE.WebSell.Domain.Entities
{
    [Table("ShippingProvider", Schema = DbSchemas.Default)]
    public class ShippingProvider : IFullAudited
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Tên đơn vị vận chuyển
        /// </summary>
        [Required]
        [MaxLength(256)]
        public string Name { get; set; } = null!;

        /// <summary>
        /// Trạng thái
        /// <see cref="CommonStatus"/>
        /// </summary>
        public int Status { get; set; }

        #region Audit
        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
        public bool Deleted { get; set; }
        public int? DeletedBy { get; set; }
        #endregion
    }
}
