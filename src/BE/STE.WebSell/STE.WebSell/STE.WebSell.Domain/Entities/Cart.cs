﻿using Microsoft.EntityFrameworkCore;
using STE.EntitiesBase.Interfaces;
using STE.Utils.ConstantVariables.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Domain.Entities
{
    [Table(nameof(Cart), Schema = DbSchemas.Default)]
    [Index(nameof(Deleted), nameof(ProductId), nameof(Status), Name = $"IX_{nameof(Cart)}")]
    public class Cart : IFullAudited
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int Status { get; set; }
        public int? ProductId { get; set; }
        public Product? Product { get; set; }
        public int Quantity { get; set; }
        public int? UserId { get; set; }
        public User? User { get; set; }

        #region audit
        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
        public bool Deleted { get; set; }
        public int? DeletedBy { get; set; }
        #endregion
    }
}
