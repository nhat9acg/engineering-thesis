﻿using Microsoft.EntityFrameworkCore;
using STE.EntitiesBase.Interfaces;
using STE.Utils.ConstantVariables.Database;
using STE.Utils.ConstantVariables.Order;

namespace STE.WebSell.Domain.Entities
{
    /// <summary>
    /// Đơn hàng
    /// </summary>
    [Table(nameof(Order), Schema = DbSchemas.Default)]
    [Index(nameof(Deleted), nameof(RestaurantId), Name = $"IX_{nameof(Order)}")]
    public class Order : IFullAudited
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int? RestaurantId { get; set; }
        public Restaurant? Restaurant { get; set; }
        public int? UserId { get; set; }
        public User? User { get; set; }
        /// <summary>
        /// Trạng thái đơn hàng
        /// <see cref="OrderStatus"/>
        /// </summary>
        public int Status { get; set; }
        /// <summary>
        /// Ghi chú đơn hàng
        /// </summary>
        [MaxLength(512)]
        public string? Description { get; set; }
        /// <summary>
        /// Địa chỉ giao hàng
        /// </summary>
        public int DeliveryAddressId { get; set; }
        public DeliveryAddress DeliveryAddress { get; set; } = null!;
        public string? InvoiceAddress { get; set; }
        /// <summary>
        /// Mã tracking
        /// </summary>
        [MaxLength(512)]
        [Unicode(false)]
        public string? TrackingCode { get; set; }
        /// <summary>
        /// Đơn vị vận chuyển
        /// </summary>
        public int? ShippingProviderId { get; set; }
        public ShippingProvider? ShippingProvider { get; set; }
        /// <summary>
        /// Thời gian giao hàng dự kiến
        /// </summary>
        public DateTime? EstimatedShippingDate { get; set; }
        /// <summary>
        /// Ngày đặt hàng
        /// </summary>
        public DateTime OrderDate { get; set; }
        public bool? HasPaid { get; set; }
        public List<OrderDetail> OrderDetails { get; } = new();

        #region Audit
        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
        public bool Deleted { get; set; }
        public int? DeletedBy { get; set; }
        #endregion
    }
}
