﻿using Microsoft.EntityFrameworkCore;
using STE.EntitiesBase.Interfaces;
using STE.Utils.ConstantVariables.Database;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace STE.WebSell.Domain.Entities
{
    /// <summary>
    /// Nhà hàng
    /// </summary>
    [Table(nameof(Restaurant), Schema = DbSchemas.Default)]
    [Index(nameof(Deleted), nameof(Name), nameof(Phone), Name = $"IX_{nameof(Restaurant)}")]
    public class Restaurant : IFullAudited
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int BusinessCustomerId { get; set; }
        public BusinessCustomer BusinessCustomer { get; set; } = null!;

        /// <summary>
        /// Tên nhà hàng
        /// </summary>
        [Required]
        [MaxLength(128)]
        public string? Name { get; set; }

        /// <summary>
        /// Số điện thoại
        /// </summary>
        [Required]
        [MaxLength(18)]
        [Unicode(false)]
        public string? Phone { get; set; }

        [Required]
        [MaxLength(30)]
        [Unicode(false)]
        public string Email { get; set; } = null!;

        [Required]
        [MaxLength(2024)]
        public string Address { get; set; } = null!;
        [Required]
        [MaxLength(3)]
        public int Status { get; set; }

        /// <summary>
        /// Người liên hệ
        /// </summary>
        [MaxLength(256)]
        public string? ContactPerson { get; set; }

        public List<DeliveryAddress> DeliveryAddresses { get; } = new();

        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
        public bool Deleted { get; set; }
        public int? DeletedBy { get; set; }

        public List<Order> Orders { get; } = new();
    }
}
