﻿using Microsoft.EntityFrameworkCore;
using STE.EntitiesBase.Interfaces;
using STE.Utils.ConstantVariables.Database;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace STE.WebSell.Domain.Entities
{
    /// <summary>
    /// Địa chỉ giao hàng
    /// </summary>
    [Table(nameof(DeliveryAddress), Schema = DbSchemas.Default)]
    [Index(nameof(Deleted), Name = $"IX_{nameof(DeliveryAddress)}")]
    public class DeliveryAddress : IFullAudited
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int? RestaurantId { get; set; }
        public Restaurant? Restaurant { get; set; }
        public int? UserId { get; set; }
        public User? User { get; set; }
        [Required]
        [MaxLength(256)]
        public string Receiver { get; set; } = null!;
        [Required]
        [MaxLength(18)]
        [Unicode(false)]
        public string Phone { get; set; } = null!;
       
        [Required]
        [MaxLength(1024)]
        public string Address { get; set; } = null!;
        public bool IsDefault { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
        public bool Deleted { get; set; }
        public int? DeletedBy { get; set; }
        public List<Order> Orders { get; } = new();
    }
}
