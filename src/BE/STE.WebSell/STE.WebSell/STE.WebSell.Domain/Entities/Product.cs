﻿using Microsoft.EntityFrameworkCore;
using STE.EntitiesBase.Interfaces;
using STE.Utils.ConstantVariables.Database;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace STE.WebSell.Domain.Entities
{
    [Table(nameof(Product), Schema = DbSchemas.Default)]
    [Index(nameof(Deleted), nameof(ProductCategoryId), nameof(Status), Name = $"IX_{nameof(Product)}")]
    public class Product : IFullAudited
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [MaxLength(128)]
        public string? Code { get;set; }

        [MaxLength(256)]
        public string? Name { get; set; }
        public int Status { get; set; }
        public int? ProductCategoryId { get; set; }
        public ProductCategory? ProductCategory { get; set; }
        public List<Cart> Carts { get; } = new();
        public int CalculationUnitId { get; set; }
        public CalculationUnit CalculationUnit { get; set; } = null!;
        [StringLength(500)]
        public string? Image { get; set; }
        public bool AllowOrder { get; set; } = true;
        [StringLength(500)]
        public string? Description { get; set; }
        public double DefaultPrice { get; set; }
        public List<OrderDetail> OrderDetails { get; } = new();
        public List<ProductPrice> ProductPrices { get; } = new();

        #region audit
        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
        public bool Deleted { get; set; }
        public int? DeletedBy { get; set; }
        #endregion
    }
}
