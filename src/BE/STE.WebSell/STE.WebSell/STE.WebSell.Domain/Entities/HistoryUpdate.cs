﻿using Microsoft.EntityFrameworkCore;
using STE.EntitiesBase.Interfaces;
using STE.Utils.ConstantVariables.Database;
using STE.Utils.ConstantVariables.HistoryUpdate;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace STE.WebSell.Domain.Entities
{
    /// <summary>
    /// Lịch sử cập nhật
    /// </summary>
    [Table(nameof(HistoryUpdate), Schema = DbSchemas.Default)]
    [Index(nameof(Table), nameof(ReferId), Name = $"IX_{nameof(HistoryUpdate)}")]
    public class HistoryUpdate : ICreatedBy
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        /// <summary>
        /// bảng update
        /// <see cref="TableUpdate"/>
        /// </summary>
        public int Table { get; set; }
        /// <summary>
        /// Trường update
        /// <see cref="ColumnUpdate"/>
        /// </summary>
        public string? Column { get; set; }
        /// <summary>
        /// Hành động Update
        /// <see cref="Actions"/>
        /// </summary>
        public int Action { get; set; }
        /// <summary>
        /// Id tham chiếu đến bảng update
        /// </summary>
        public int ReferId { get; set; }
        /// <summary>
        /// Giá trị cũ
        /// </summary>
        public string? OldValue { get; set; }
        /// <summary>
        /// Giá trị mới
        /// </summary>
        public string? NewValue { get; set; }
        /// <summary>
        /// Ghi chú tổng quan
        /// </summary>
        public string? Note { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
    }
}
