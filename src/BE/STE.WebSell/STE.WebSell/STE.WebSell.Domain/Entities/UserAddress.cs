﻿using Microsoft.EntityFrameworkCore;
using STE.EntitiesBase.Interfaces;
using STE.Utils.ConstantVariables.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace STE.WebSell.Domain.Entities
{

    [Table(nameof(UserAddress), Schema = DbSchemas.Default)]
    [Index(nameof(Deleted), Name = $"IX_{nameof(UserAddress)}")]
    public class UserAddress : IFullAudited
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int UserId { get; set; }
        [Required]
        [MaxLength(256)]
        public string Receiver { get; set; } = null!;
        [Required]
        [MaxLength(18)]
        [Unicode(false)]
        public string Phone { get; set; } = null!;
        public User User { get; set; } = null!;
        [Required]
        [MaxLength(1024)]
        public string Address { get; set; } = null!;
        public bool IsDefault { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
        public bool Deleted { get; set; }
        public int? DeletedBy { get; set; }
        //public List<Order> Orders { get; } = new();
    }
}
