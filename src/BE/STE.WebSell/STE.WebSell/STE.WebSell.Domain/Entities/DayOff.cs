﻿using Microsoft.EntityFrameworkCore;
using STE.Utils.ConstantVariables.Database;

namespace STE.WebSell.Domain.Entities
{
    [Table(nameof(DayOff), Schema = DbSchemas.Default)]
    [Index(nameof(Date), Name = $"IX_{nameof(DayOff)}")]
    public class DayOff
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Column(TypeName = "date")]
        public DateTime Date { get; set; }

        [MaxLength(512)]
        public string? Note { get; set; }
    }
}
