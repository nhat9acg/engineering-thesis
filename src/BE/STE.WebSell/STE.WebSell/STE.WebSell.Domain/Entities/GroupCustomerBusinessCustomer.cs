﻿using Microsoft.EntityFrameworkCore;
using STE.EntitiesBase.Interfaces;
using STE.Utils.ConstantVariables.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace STE.WebSell.Domain.Entities
{
    [Table(nameof(GroupCustomerBusinessCustomer), Schema = DbSchemas.Default)]
    [Index(nameof(Deleted), nameof(GroupCustomerId), nameof(BusinessCustomerId), Name = $"IX_{nameof(GroupCustomerBusinessCustomer)}")]
    public class GroupCustomerBusinessCustomer : IFullAudited
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int GroupCustomerId { get; set; }
        public int BusinessCustomerId { get; set; }
        public DateTime? DeletedDate { get; set; }
        public bool Deleted { get; set; }
        public int? DeletedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
    }
}
