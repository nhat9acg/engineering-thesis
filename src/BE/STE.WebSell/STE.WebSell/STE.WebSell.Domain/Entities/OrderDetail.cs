﻿using Microsoft.EntityFrameworkCore;
using STE.Utils.ConstantVariables.Database;

namespace STE.WebSell.Domain.Entities
{
    /// <summary>
    /// Chi tiết đơn hàng
    /// </summary>
    [Table(nameof(OrderDetail), Schema = DbSchemas.Default)]
    [Index(nameof(OrderId), nameof(ProductId), Name = $"IX_{nameof(OrderDetail)}")]
    public class OrderDetail
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int OrderId { get; set; }
        public Order Order { get; set; } = null!;
        public int ProductId { get; set; }
        public Product Product { get; set; } = null!;
        public double UnitPrice { get; set; }
        public int Quantity { get; set; }
        /// <summary>
        /// Ghi chú chi tiết đơn hàng
        /// </summary>
        [MaxLength(512)]
        public string? Description { get; set; }
    }
}
