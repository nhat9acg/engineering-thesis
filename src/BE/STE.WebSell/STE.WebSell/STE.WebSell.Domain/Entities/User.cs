﻿using Microsoft.EntityFrameworkCore;
using STE.EntitiesBase.Entities;
using STE.Utils.ConstantVariables.Database;
using STE.Utils.ConstantVariables.User;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace STE.WebSell.Domain.Entities
{
    /// <summary>
    /// User
    /// </summary>
    [Table(nameof(User), Schema = DbSchemas.Default)]
    [Index(nameof(Deleted), nameof(Username), Name = $"IX_{nameof(User)}")]
    public class User : IUser
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        [MaxLength(128)]
        [Unicode(false)]
        public string Username { get; set; } = null!;

        [Required]
        [MaxLength(128)]
        [Unicode(false)]
        public string Password { get; set; } = null!;

        [MaxLength(128)]
        [Unicode(false)]
        public string? Email { get; set; }

        [MaxLength(128)]
        [Unicode(false)]
        public string? Phone { get; set; }

        [MaxLength(256)]
        public string? FullName { get; set; }
        
        /// <summary>
        /// Loại user <see cref="UserTypes"/>
        /// </summary>
        public int UserType { get; set; }
        public int? BusinessCustomerId { get; set; }
        public int? RestaurantId { get; set; }

        /// <summary>
        /// Trạng thái user <see cref="UserStatus"/>
        /// </summary>
        [Column("Status")]
        public int Status { get; set; }
        public List<UserAddress> UserAddresses { get; } = new();
        public List<DeliveryAddress> DeliveryAddresses { get; } = new();
        public List<Order> Orders { get; } = new();
        public List<Notification> Notifications { get; } = new();
        public List<Chat> SenderChats { get; } = new();
        public List<Chat> ReceiverChats { get; } = new();
        #region audit
        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
        public int? DeletedBy { get; set; }
        public bool Deleted { get; set; }
        #endregion
    }
}
