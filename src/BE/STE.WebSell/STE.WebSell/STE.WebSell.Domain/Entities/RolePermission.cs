﻿using Microsoft.EntityFrameworkCore;
using STE.EntitiesBase.Entities;
using STE.Utils.ConstantVariables.Database;
using STE.Utils.RolePermission;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace STE.WebSell.Domain.Entities
{
    [Table(nameof(RolePermission), Schema = DbSchemas.Default)]
    [Index(nameof(RoleId), nameof(PermissionKey), Name = $"IX_{nameof(RolePermission)}")]
    public class RolePermission : IRolePermission<int>
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int RoleId { get; set; }
        public Role Role { get; set; } = null!;

        /// <summary>
        /// Tên permission <br/>
        /// <see cref="PermissionConfig"/>
        /// </summary>
        [Required]
        [MaxLength(128)]
        [Unicode(false)]
        public string PermissionKey { get; set; } = null!;

        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
    }
}
