﻿using STE.EntitiesBase.Interfaces;
using STE.Utils.ConstantVariables.Database;
using STE.Utils.ConstantVariables.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Domain.Entities
{
    [Table("SystemNotification", Schema = DbSchemas.Default)]
    public class SystemNotification : IFullAudited
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string NotificationTemplateName { get; set; } = null!;
        [Required]
        public string Key { get; set; } = null!;
        public string? Description { get; set; }
        public string? EmailContent { get; set; }
        public string? EmailContentType { get; set; }
        public string? PushAppContent { get; set; }
        public string? PushAppContentType { get; set; }
        public string? TitleAppContent { get; set; }

        #region Audit
        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
        public bool Deleted { get; set; }
        public int? DeletedBy { get; set; }
        #endregion
        public List<SystemNotificationType> SystemNotificationType { get; } = new();
    }
}
