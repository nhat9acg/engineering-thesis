﻿using Microsoft.EntityFrameworkCore;
using STE.EntitiesBase.Interfaces;
using STE.Utils.ConstantVariables.Database;

namespace STE.WebSell.Domain.Entities
{
    /// <summary>
    /// Khách hàng nhận tài liệu
    /// </summary>
    [Table(nameof(DocumentRecipient), Schema = DbSchemas.Default)]
    [Index(nameof(DocumentId), nameof(BusinessCustomerId), Name = $"IX_{nameof(DocumentRecipient)}")]
    public class DocumentRecipient : ICreatedBy, IModifiedBy
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        public int DocumentId { get; set; }
        public Document Document { get; set; } = null!;

        public int BusinessCustomerId { get; set; }
        public BusinessCustomer BusinessCustomer { get; set; } = null!;

        /// <summary>
        /// Khách đã xem
        /// </summary>
        public bool IsView { get; set; }

        #region audit
        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        #endregion
    }
}
