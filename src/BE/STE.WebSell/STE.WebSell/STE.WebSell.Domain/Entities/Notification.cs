﻿using STE.EntitiesBase.Interfaces;
using STE.Utils.ConstantVariables.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Domain.Entities
{
    [Table("Notification", Schema = DbSchemas.Default)]
    public class Notification : IFullAudited
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int UserId { get; set; }
        public User User { get; set; } = null!;
        public string? NotificationTemplateName { get; set; }
        public string? Key { get; set; }
        public string? Description { get; set; }
        public string? PushAppContent { get; set; }
        public string? PushAppContentType { get; set; }
        public string? TitleAppContent { get; set; }
        public bool IsRead { get; set; }

        #region Audit
        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
        public bool Deleted { get; set; }
        public int? DeletedBy { get; set; }
        #endregion
    }
}
