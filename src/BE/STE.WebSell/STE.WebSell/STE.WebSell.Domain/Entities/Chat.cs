﻿using Microsoft.EntityFrameworkCore;
using STE.EntitiesBase.Interfaces;
using STE.Utils.ConstantVariables.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace STE.WebSell.Domain.Entities
{
    [Table(nameof(Chat), Schema = DbSchemas.Default)]
    [Index(nameof(Deleted), nameof(SenderUserId), nameof(ReceiverUserId), nameof(Status), Name = $"IX_{nameof(Chat)}")]
    public class Chat : IFullAudited
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public int Status { get; set; }
        public int Code { get; set; }
        public string Message { get; set; } = null!;
        public int SenderUserId { get; set; }
        public User SenderUser { get; set; } = null!;
        public int ReceiverUserId { get; set; }
        public User ReceiverUser { get; set; } = null!;
        #region audit
        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
        public bool Deleted { get; set; }
        public int? DeletedBy { get; set; }
        #endregion
    }
}
