﻿using Microsoft.EntityFrameworkCore;
using STE.EntitiesBase.Interfaces;
using STE.Utils.ConstantVariables.Database;
using STE.Utils.ConstantVariables.Document;

namespace STE.WebSell.Domain.Entities
{
    /// <summary>
    /// Tài liệu
    /// </summary>
    [Table(nameof(Document), Schema = DbSchemas.Default)]
    [Index(nameof(Deleted), Name = $"IX_{nameof(Document)}")]
    public class Document : IFullAudited
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [MaxLength(128)]
        public string Title { get; set; } = null!;

        [MaxLength(512)]
        [Unicode(false)]
        public string FileUrl { get; set; } = null!;

        /// <summary>
        /// Kiểu gửi tài liệu <see cref="DocumentSendTypes"/>
        /// </summary>
        public int SendType { get; set; }
        public List<DocumentRecipient> DocumentRecipients { get; set; } = new();

        #region audit
        public DateTime? CreatedDate { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public int? ModifiedBy { get; set; }
        public DateTime? DeletedDate { get; set; }
        public bool Deleted { get; set; }
        public int? DeletedBy { get; set; }
        #endregion
    }
}
