﻿using STE.EntitiesBase.Interfaces;
using STE.WebSell.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STE.WebSell.Domain.Entities
{
    [Table("Media")]
    public class Media
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [Required]
        public string Url { get; set; } = string.Empty;
        public MediaStatusEnum Status { get; set; }
    }
}
