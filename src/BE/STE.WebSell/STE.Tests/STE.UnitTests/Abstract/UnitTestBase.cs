﻿using EPIC.Utils.Security;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Moq;
using STE.Utils;
using STE.Utils.ConstantVariables.User;
using STE.Utils.Localization;
using STE.WebAPIBase;
using STE.WebSell.Application.AuthenticationModule.Abstract;
using STE.WebSell.Application.AuthenticationModule.Implements;
using STE.WebSell.Application.Common;
using STE.WebSell.Application.Common.Localization;
using STE.WebSell.Application.ConfigurationModule.Abstract;
using STE.WebSell.Application.ConfigurationModule.Implements;
using STE.WebSell.Application.CustomerManagerModule.Abstract;
using STE.WebSell.Application.CustomerManagerModule.Implement;
using STE.WebSell.Application.DemoModule.Abstract;
using STE.WebSell.Application.DemoModule.Implements;
using STE.WebSell.Application.FileModule.Abstract;
using STE.WebSell.Application.FileModule.Dtos.Settings;
using STE.WebSell.Application.FileModule.Implements;
using STE.WebSell.Domain.Entities;
using STE.WebSell.Infrastructure.Persistence;
using System.Security.Claims;
using static OpenIddict.Abstractions.OpenIddictConstants;

namespace STE.UnitTests.Abstract
{
    public abstract class UnitTestBase
    {
        public IHost CreateHostBuilder(IHttpContextAccessor httpContextAccessor)
        {
            IHost host = Host.CreateDefaultBuilder()
                .ConfigureHostConfiguration(hostConfig =>
                {
                    hostConfig.AddEnvironmentVariables(prefix: "DOTNET_");
                })
                .ConfigureAppConfiguration(app =>
                {
                    var env = Environment.GetEnvironmentVariable("DOTNET_ENVIRONMENT");
                    app.AddJsonFile("appsettings.json");
                    if (env != null)
                    {
                        app.AddJsonFile($"appsettings.{env}.json");
                    }
                })
                .ConfigureServices((hostContext, services) =>
                {
                    //config file
                    services.Configure<FileConfig>(hostContext.Configuration.GetSection("FileConfig:File"));

                    //string assemblyName = Assembly.GetExecutingAssembly().GetName().Name;

                    //nếu có cấu hình redis
                    string? redisConnectionString = hostContext.Configuration.GetConnectionString("Redis");

                    string? connectionString = hostContext.Configuration.GetConnectionString("Default");

                    if (httpContextAccessor != null)
                    {
                        services.RemoveAll(typeof(IHttpContextAccessor));
                        services.AddSingleton(httpContextAccessor);
                    }
                    services.RemoveAll(typeof(WebSellDbContext));
                    services.RemoveAll(typeof(DbContextOptions<WebSellDbContext>));
                    services.AddDbContext<WebSellDbContext>(options =>
                    {
                        options.UseInMemoryDatabase("webselldb");
                    });

                    ProgramExtensions.ConfigureAutoMap(services);

                    services.AddSingleton<LocalizationBase, JVFLocalization>();
                    services.AddSingleton<MapErrorCodeBase>();
                    services.AddScoped<IConfigurationService, ConfigurationService>();
                    services.AddScoped<IValueService, ValueService>();
                    services.AddScoped<IUserService, UserService>();
                    services.AddScoped<IFileService, FileService>();
                    services.AddScoped<IBusinessCustomerService, BusinessCustomerService>();
                    services.AddScoped<IRestaurantService, RestaurantService>();
                    services.AddScoped<IDeliveryAddressService, DeliveryAddressService>();
                    services.AddScoped<IPermissionService, PermissionService>();
                })
                .Build();

            var dbContext = host.Services.GetService<WebSellDbContext>()!;
            dbContext.Database.EnsureDeleted();
            dbContext.Database.EnsureCreated();
            SeedData(dbContext);
            dbContext.SaveChanges();
            return host;
        }

        public virtual IHttpContextAccessor CreateHttpContextAdmin(int userId)
        {
            var httpContext = new DefaultHttpContext();
            var identity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.NameIdentifier, userId.ToString()),
                new Claim(UserClaimTypes.UserId, userId.ToString()),
                new Claim(UserClaimTypes.UserType, UserTypes.ADMIN.ToString()),
            });
            httpContext.User = new ClaimsPrincipal(identity);

            var mockHttpContextAccessor = new Mock<IHttpContextAccessor>();
            mockHttpContextAccessor.Setup(x => x.HttpContext).Returns(httpContext);
            return mockHttpContextAccessor.Object;
        }

        /// <summary>
        /// Giả lập dữ liệu để test
        /// </summary>
        /// <param name="dbContext"></param>
        protected virtual void SeedData(WebSellDbContext dbContext)
        {
            dbContext.Users.Add(new User
            {
                Id = 0,
                Username = "Test",
                Password = CryptographyUtils.CreateMD5("123456"),
                UserType = UserTypes.ADMIN,
            });
        }
    }
}
