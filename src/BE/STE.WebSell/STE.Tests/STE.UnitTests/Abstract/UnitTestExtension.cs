﻿using Microsoft.Extensions.Hosting;

namespace STE.UnitTests.Abstract
{
    public static class UnitTestExtension
    {
        /// <summary>
        /// Lấy service để test
        /// </summary>
        public static TService? GetServiceTest<TService>(this IHost host) where TService : class
        {
            return host.Services.GetService(typeof(TService)) as TService;
        }
    }
}
