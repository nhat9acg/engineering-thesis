﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using STE.UnitTests.Abstract;
using STE.WebSell.Application.CustomerManagerModule.Abstract;
using STE.WebSell.Application.CustomerManagerModule.Dtos.BusinessCustomer;
using STE.WebSell.Application.CustomerManagerModule.Dtos.DeliveryAddress;
using STE.WebSell.Application.CustomerManagerModule.Dtos.Restaurant;

namespace STE.UnitTests
{
    public class TestCustomer : UnitTestBase
    {
        [Fact]
        public void TestCreate()
        {
            IHost mockRequestAdmin = CreateHostBuilder(CreateHttpContextAdmin(1));
            var businessCustomerService = mockRequestAdmin.Services.GetRequiredService<IBusinessCustomerService>();
            var restaurantService = mockRequestAdmin.Services.GetRequiredService<IRestaurantService>();
            var deliveryAddressService = mockRequestAdmin.Services.GetRequiredService<IDeliveryAddressService>();

            var businessCustomer = businessCustomerService.Create(new CreateBusinessCustomerDto
            {
                Phone = Faker.Phone.Number(),
                Email = Faker.Internet.Email(),
                FullName = Faker.Company.Name(),
            });

            var bcFindAll = businessCustomerService.FindAll(new FilterBusinessCustomerDto
            {
                Keyword = businessCustomer.FullName,
                PageSize = 10,
                PageNumber = 1,
                Sort = null
            });

            businessCustomerService.Update(
            new UpdateBusinessCustomerDto
            {
                Phone = Faker.Phone.Number(),
                Email = Faker.Internet.Email(),
                FullName = Faker.Company.Name(),
            });

            businessCustomer = businessCustomerService.FindById(businessCustomer.Id);

            if (businessCustomer == null)
            {
                Assert.True(false);
                return;
            }

            var restaurant = restaurantService.Create(new CreateRestaurantDto
            {
                BusinessCustomerId = businessCustomer.Id,
                Name = Faker.Company.Name(),
                Phone = Faker.Phone.Number(),
            });

            var restaurantAll = restaurantService.FindAll(new FilterRestaurantDto
            {
                Keyword = restaurant.Name,
                PageSize = 10,
                PageNumber = 1,
                Sort = null
            });

            if (!(restaurantAll.Items.Where(o => o.Id == restaurant.Id).Count() == 1))
            {
                Assert.Fail($"{nameof(restaurantService.FindAll)}: không tìm thấy bản ghi vừa thêm hoặc bị lặp dữ liệu");
                return;
            }

            var restaurantFindId = restaurantService.FindById(restaurant.Id);
            if (businessCustomer == null)
            {
                Assert.Fail($"{nameof(restaurantService.FindById)}: không tìm thấy bản ghi vừa thêm hoặc bị lặp dữ liệu");
                return;
            }

            var deleveryAddress = deliveryAddressService.Create(new CreateDeliveryAddressDto
            {
                RestaurantId = restaurant.Id,
                Address = Faker.Address.StreetAddress(),
            });

            var deleveryAddress2 = deliveryAddressService.Create(new CreateDeliveryAddressDto
            {
                RestaurantId = restaurant.Id,
                Address = Faker.Address.StreetAddress(),
            });

            var deleveryAddress3 = deliveryAddressService.Create(new CreateDeliveryAddressDto
            {
                RestaurantId = restaurant.Id,
                Address = Faker.Address.StreetAddress(),
            });

            var deleveryAddressAll = deliveryAddressService.FindByRestaurant(restaurant.Id);
            if (!deleveryAddressAll.Any(d => d.Id == deleveryAddress.Id))
            {
                Assert.Fail($"{nameof(deliveryAddressService.FindByRestaurant)}");
            }

            deliveryAddressService.UpdateDefault(deleveryAddress.Id, false);

            var deleveryAddressAll2 = deliveryAddressService.FindByRestaurant(restaurant.Id);

            deliveryAddressService.UpdateDefault(deleveryAddress2.Id, true);

            var deleveryAddressAll3 = deliveryAddressService.FindByRestaurant(restaurant.Id);

            Assert.True(true);
        }
    }
}
