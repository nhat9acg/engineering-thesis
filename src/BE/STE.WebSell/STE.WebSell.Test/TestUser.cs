﻿using EPIC.Utils.Security;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using STE.UnitTests.Abstract;
using STE.Utils;
using STE.Utils.ConstantVariables.User;
using STE.Utils.Localization;
using STE.WebSell.Application.AuthenticationModule.Abstract;
using STE.WebSell.Application.AuthenticationModule.Dtos;
using STE.WebSell.Application.AuthenticationModule.Dtos.UserDto;
using STE.WebSell.Domain.Entities;
using STE.WebSell.Infrastructure.Persistence;
using System.Security.Claims;
using static OpenIddict.Abstractions.OpenIddictConstants;

namespace STE.UnitTests
{
    public class TestUser : UnitTestBase
    {
        [Fact]
        public void Test1()
        {
            IHost mockRequestAdmin = CreateHostBuilder(CreateHttpContextAdmin(1));
            var dbContext = mockRequestAdmin.Services.GetRequiredService<WebSellDbContext>();
            var httpContext = mockRequestAdmin.Services.GetRequiredService<IHttpContextAccessor>();


            var userService = mockRequestAdmin.Services.GetRequiredService<IUserService>();
            userService.CreateUser(new CreateUserDto
            {
                Username = "Test2",
                Password = CryptographyUtils.CreateMD5("123456"),
                UserType = UserTypes.ADMIN
            });

            var userInsert = dbContext!.Users.FirstOrDefault(o => o.Username == "Test2" && !o.Deleted);
            Assert.True(true);
        }

        protected override void SeedData(WebSellDbContext dbContext)
        {
            dbContext.Users.Add(new User
            {
                Id = 2,
                Username = "Test",
                Password = CryptographyUtils.CreateMD5("123456"),
                UserType = UserTypes.ADMIN,
            });
            dbContext.SaveChanges();
        }
    }
}