import { Component, Injector, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppAuthService } from '@shared/auth/app-auth.service';
import { LayoutService } from '@shared/auth/app.layout.service';
import { CrudComponentBase } from '@shared/crud-component-base';
import { UserService } from '@shared/services/user.service';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
  styles: [`
      :host ::ng-deep .pi-eye,
      :host ::ng-deep .pi-eye-slash {
          transform:scale(1.2);
          margin-right: 0.5rem;
          color: var(--primary-color) !important;
      }
  `],
  providers: [MessageService]
})
export class RegisterComponent extends CrudComponentBase {

  valCheck: string[] = ['remember'];
  checkbox: boolean = true;
  password!: string;

  constructor(
    injector: Injector,
    messageService: MessageService,
    public layoutService: LayoutService,
    public authService: AppAuthService,
    private _router: Router,
    // private ref: DynamicDialogRef,
    // private configDialog: DynamicDialogConfig,
    private _userService: UserService,
    // private _businessCustomerService: BusinessCustomerService,
    // private _restaurantService: RestaurantService,
    // private _roleService: RoleService,
    private fb: FormBuilder,
  ) {
    super(injector, messageService);
  }

  submitting = false;
  dark: boolean;
  postForm: FormGroup;
  user: any;

  ngOnInit(): void {
    this.authService.clear();
    this.postForm = this.fb.group({
      // id: [null, [Validators.required]],
      username: [null, [Validators.required]],
      fullName: [null, [Validators.required]],
      userType: [4, []], // customer 
      password: [null, [Validators.required]],
      repeatPassword: [null, [Validators.required]],
      email: [null, [Validators.required]],
      phone: [null, [Validators.required]],
    })

  }

  get f() {
    return this.postForm.controls;
  }

  onSubmit() {
    if (!this.checkInValidForm(this.postForm)) {
      this.authService.register(this.postForm.value).subscribe((response) => {
        console.log('response',response);
        if(response.message == "Ok") {
          this.messageSuccess("Đăng ký thành công", 1000);
          setTimeout(() => {
            this._router.navigate(['/account/login']);
          }, 3000);
        } else {
          this.messageError(response.message, 3000);
        }
       
      }, () => {
        this.messageError("Không thể tạo tài khoản", 3000);
      }
      );
    
    }
}
}
