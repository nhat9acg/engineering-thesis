import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AccountComponent } from './account.component';
import { LandingComponent } from './landing/landing.component';
import { RegisterComponent } from './register/register.component';
import { ListProductComponent } from './list-product/list-product.component';
import { SuccessfulTransactionComponent } from './successful-transaction/successful-transaction.component';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: AccountComponent,
                children: [
                    // { path: 'landing', component: LandingComponent },
                    { path: 'login', component: LoginComponent },
                    { path: 'register', component: RegisterComponent },
                    {
                        path: 'list-product',
                        component: ListProductComponent,
                    },
                ]
            },
            {
                path: 'landing',
                component: LandingComponent,
            },
            {
                path: 'successful-transaction/:id',
                component: SuccessfulTransactionComponent,
            },
        ])
    ],
    exports: [
        RouterModule
    ]
})
export class AccountRoutingModule { }
