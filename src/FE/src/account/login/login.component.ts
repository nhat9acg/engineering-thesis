import { Component, Injector, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AppConsts } from '@shared/AppConsts';
import { AppAuthService } from '@shared/auth/app-auth.service';
import { ConfigLanguage } from '@shared/consts/config-language';
import { SelectedLanguageService } from '@shared/services/selected-language.service';
import { MessageService } from 'primeng/api';

@Component({
    templateUrl: './login.component.html',
    styles: [`
      :host ::ng-deep .pi-eye,
      :host ::ng-deep .pi-eye-slash {
          transform:scale(1.2);
          margin-right: 0.5rem;
          color: var(--primary-color) !important;
      }
  `],
    providers: [MessageService]
})
export class LoginComponent implements OnInit {
    submitting = false;
    dark: boolean;
    constructor(
        public authService: AppAuthService,
        public router: Router,
        private selectedLanguageService: SelectedLanguageService,
		private translate: TranslateService,
    ) {}
    selectLanguage: boolean = false;
	ConfigLanguage = ConfigLanguage;
	selectedCountry = {
		code: '',
        name:'',
        img:'',
	};
    ngOnInit(): void {
        this.authService.clear();
        this.selectedCountry = AppConsts.localization;
        this.setLang();
    }

    switchLanguage(language) {		
		AppConsts.localization = this.selectedCountry;
		localStorage.setItem('selectedCountry', JSON.stringify(this.selectedCountry));
		//
		localStorage.setItem(AppConsts.localStorageLangKey, language.code);
		this.translate.setDefaultLang(language.code);
		this.translate.use(language.code);
		this.setLang(language.code);
	}

    setLang(lang?: string) {
		this.selectedLanguageService.setLanguage(lang || localStorage.getItem(AppConsts.localStorageLangKey) || AppConsts.defaultLang);
	}

    login(): void {
        this.submitting = true;
        this.authService.authenticate(() => (this.submitting = false));
    }
}
