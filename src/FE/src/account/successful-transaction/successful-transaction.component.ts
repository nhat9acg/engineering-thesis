import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudComponentBase } from '@shared/crud-component-base';
import { OrderService } from '@shared/services/order.service';
import { MessageService } from 'primeng/api';
import { BreadcrumbService } from 'src/app/layout/breadcrumb/breadcrumb.service';

@Component({
  selector: 'app-successful-transaction',
  templateUrl: './successful-transaction.component.html',
  styleUrls: ['./successful-transaction.component.scss']
})
export class SuccessfulTransactionComponent extends CrudComponentBase {

  constructor(
    injector: Injector,
    private routeActive: ActivatedRoute,
    messageService: MessageService,
    private breadcrumbService: BreadcrumbService,
    private router: Router,
    private _orderService: OrderService,
) {
    super(injector, messageService);
}
  orderId: number = +this.routeActive.snapshot.paramMap.get('id');
  ngOnInit(): void {
    this._orderService.hasPaid(this.orderId).subscribe((res) => {
      this.isLoading = false;
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);
    });
  }

  navigate() {
    this.router.navigate(['/order-management/retail-orders/view-detail/'+this.cryptEncode(this.orderId)]);
  }

}
