import { Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConsts } from '@shared/AppConsts';
import { AppAuthService } from '@shared/auth/app-auth.service';
import { LayoutService } from '@shared/auth/app.layout.service';
import { CrudComponentBase } from '@shared/crud-component-base';
import { IPageInfo } from '@shared/interfaces/base-interface';
import { IProductCategory } from '@shared/interfaces/product-category.interface';
import { Page } from '@shared/model/page';
import { User } from '@shared/model/user.model';
import { CartService } from '@shared/services/cart.service';
import { NotificationService } from '@shared/services/notification.service';
import { ProductCategoryService } from '@shared/services/product-category.service';
import { ProductService } from '@shared/services/product.service';
import { AppUtilsService } from '@shared/services/utils.service';
import { CookieService } from 'ngx-cookie-service';
import { ConfirmationService, MessageService, SelectItem } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { Galleria } from 'primeng/galleria';
import { ListProductComponent } from '../list-product/list-product.component';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { enc, HmacSHA256, HmacSHA512 } from 'crypto-js';
import * as moment from 'moment';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent extends CrudComponentBase implements OnInit {

  constructor(
    injector: Injector,
    messageService: MessageService,
    private _dialogService: DialogService,
    private confirmationService: ConfirmationService,
    private routeActive: ActivatedRoute,
    private _productService: ProductService,
    private _cartService: CartService,
    private _utilsService: AppUtilsService,
    public layoutService: LayoutService,
    private authService: AppAuthService,
    private _cookieService: CookieService,
    private notificationService: NotificationService,
    private _productCategoryService: ProductCategoryService,
    private sanitizer: DomSanitizer,
    public router: Router) {
    super(injector, messageService);
    this.userInfo = this.getUser();
  }
  userInfo: User;
  responsiveOptions = [
    {
      breakpoint: '1024px',
      numVisible: 3,
      numScroll: 3
    },
    {
      breakpoint: '768px',
      numVisible: 2,
      numScroll: 2
    },
    {
      breakpoint: '560px',
      numVisible: 1,
      numScroll: 1
    }
  ];
  products: any[] = [
  ];
  // page: any = {
  //   pageNumber: 0,
  //   pageSize: 0,
  // };
  sortOptions: SelectItem[] = [];
  sortOrder: number = 0;
  sortField: string = '';

  AppConsts = AppConsts;
  images: string[] = [];
  dataFilter: any = {
    field: "name",
  };
  isRowOdds: boolean = false;
  infoNotSeen: any = {
    notificationNumber: 0,
    messageNumber: 0,
    cartNumber: 0,
  };
  baseUrl: string

  ngOnInit(): void {
    this.baseUrl = this.AppConsts.remoteServiceBaseUrl ?? this.baseUrl;
    console.log("userInfo", this.userInfo);
    this.getCategory()
    this.setPage();
    this.getMedia();
    if (this.userInfo.user_type > 0) {
      this.notification();
    }

    this.sortOptions = [
      { label: 'Giá từ cao tới thấp', value: '!defaultPrice' },
      { label: 'Giá từ thấp tới cao', value: 'defaultPrice' }
    ];
  }

  public getImage(image: File | Blob | string): SafeUrl | string {
    if (image instanceof File || image instanceof Blob) {
      const objectURL = URL.createObjectURL(image);
      return this.sanitizer.bypassSecurityTrustUrl(objectURL);
    }
    return `${this.baseUrl}/${image}`;
  }

  private getMedia() {
    this.notificationService.getSystemMedia().subscribe(
      (res) => {
        this.isLoading = false;
        if (this.handleResponseInterceptor(res, '')) {
          this.images = res.data?.images || [];
        }
      }, (err) => {
        this.isLoading = false;
        console.log('Error-------', err);
        this.messageError('Có lỗi xảy ra. Vui lòng thử lại sau!');
      });
  }

  addCart(product) {
    let body = {
      productId: product.id,
      quantity: 1,
    }
    this._cartService.create(body).subscribe((res) => {
      this.isLoading = false;
      if (this.handleResponseInterceptor(res, 'Sản phẩm đã được thêm vào giỏ hàng')) {
        if (this.userInfo.user_type > 0) {
          this.notification();
        }
      }
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);
    });
  }

  detail(item) {
    this._dialogService.open(ListProductComponent, {
      // header: "",
      width: '95%',
      contentStyle: { "max-height": "95%", "overflow": "auto", "margin-bottom": "0px" },
      data: {
        productId: item?.id,
      },
    }).onClose.subscribe(result => {
      if (result) {
        this.setPage();
      }
    })
  }

  categories: IProductCategory[] = [];
  private getCategory() {
    let page = new Page();
    page.pageNumber = 0;
    page.pageSize = 8;

    this.page.keyword = this.keyword;
    this._productCategoryService.getAll(page).subscribe((res) => {
      this.isLoading = false;
      if (this.handleResponseInterceptor(res, '')) {
        this.categories = res.data?.items.map(item => {
          return { ...item, checked: false };
        });;
      }
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);
    });
  }

  public onCheckboxChange(item: any) {
    console.log('Checkbox value changed:', this.categories);
    this.setPage();
  }

  setPage(event?: IPageInfo) {
    this.isRowOdds = false;
    if (event) {
      this.page.pageNumber = event.page;
      this.page.pageSize = event.rows;
    }
    this.page.keyword = this.keyword;
    this.dataFilter.productCategoryIds = this.categories.filter(item => item.checked).map(item => item.id);
    // this.page.pageNumber = 1;
    // this.page.pageSize = 6;
    this._productService.getAllAnonymous(this.page, this.dataFilter).subscribe((res) => {
      if (res?.data?.items) {
        this.products = res.data?.items.map(item => {
          if (item.allowOrder) {
            item.inventoryStatus = 'INSTOCK';
          } else {
            item.inventoryStatus = 'OUTOFSTOCK'
          }
          return { ...item, rating: 5, discount: 18 };
        });

      };
    }, (err) => {
      console.log('Error-------', err);
    });
  }

  onSortChange(event: any) {
    const value = event.value;

    if (value.indexOf('!') === 0) {
      this.sortOrder = -1;
      this.sortField = value.substring(1, value.length);
    } else {
      this.sortOrder = 1;
      this.sortField = value;
    }
  }

  public checkKey(): boolean {
    return this.page.keyword.trim().length > 0;
  }

  onFilter(dv: DataView, event: Event) {
    // dv.filter((event.target as HTMLInputElement).value);
  }

  public login() {
    this.router.navigate(['/account/login'])
  }

  public register() {
    this.router.navigate(['/account/register'])
  }

  public cart() {
    if (this.userInfo?.user_type === 4) {
      this.router.navigate(['/cart'])
    } else {
      this.router.navigate(['/account/login'])
    }
  }
  public logout() {
    this._tokenService.clearToken();
    this._cookieService.delete(AppConsts.authorization.refreshToken, '/');
    this._tokenService.clearAllCookie();
    this.userInfo = this.getUser();
  }

  private notification(isLoading: boolean = true) {
    this.isLoading = isLoading;
    this.notificationService.GetInfoNotSeenNotification().subscribe((res) => {
      this.isLoading = false;
      if (this.handleResponseInterceptor(res, '')) {
        this.infoNotSeen = res.data;
      }
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);
    });
  }
}
