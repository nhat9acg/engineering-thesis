import { Component, Injector, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppConsts } from '@shared/AppConsts';
import { CrudComponentBase } from '@shared/crud-component-base';
import { User } from '@shared/model/user.model';
import { CartService } from '@shared/services/cart.service';
import { ProductService } from '@shared/services/product.service';
import { AppUtilsService } from '@shared/services/utils.service';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-list-product',
  templateUrl: './list-product.component.html',
  styleUrls: ['./list-product.component.scss']
})
export class ListProductComponent extends CrudComponentBase {

  constructor(
    injector: Injector,
    messageService: MessageService,
    private _utilsService: AppUtilsService,
    private _productService: ProductService,
    private configDialog: DynamicDialogConfig,
    private _cartService: CartService,
    private ref: DynamicDialogRef,
    public router: Router
  ) { 
    super(injector, messageService);
    this.userInfo = this.getUser();
    // this.product = this._utilsService.getItem();
  }
  userInfo: User;
  product: any;
  AppConsts = AppConsts;

  ngOnInit(): void {
    if( this.configDialog?.data?.productId) {
			this.init(this.configDialog?.data?.productId);
		}
  }
 
  public chat() {
    if(this.userInfo?.user_type === 4) {
      this.ref.close();
      this.router.navigate(['/chat'])
    } else {
      this.ref.close();
      this.router.navigate(['/account/login'])
    }
  }

  public cart() {
    if(this.userInfo?.user_type === 4) {
      this.ref.close();
      this.router.navigate(['/cart'])
    } else {
      this.ref.close();
      this.router.navigate(['/account/login'])
    }
  }

  addCart(product, quantity: number = 1) {
    let body = {
      productId: product.id,
      quantity: 1,
    }
    this._cartService.create(body).subscribe((res) => {
      this.isLoading = false;
      if (this.handleResponseInterceptor(res, 'Sản phẩm đã được thêm vào giỏ hàng')) {
      }
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);
    });
  }

  init(id) {
    this._productService.get(id).subscribe((res) => {
			this.isLoading = false;
				this.product = res?.data || [];
		}, (err) => {
		  this.isLoading = false;
		  console.log('Error-------', err);
		});
  }

  addToCart(): void {
  }

  submitReview(): void {
  
  }
}
