import { AppComponentBase } from '@shared/app-component-base';
import { ElementRef, Injector, ViewChild } from "@angular/core";
import * as moment from 'moment';
import { MessageService, ConfirmationService } from 'primeng/api';
import { Subject } from 'rxjs';
import { Page } from './model/page';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { AppConsts, TypeFormatDatesConst } from './AppConsts';
import { PermissionsService } from './services/permissions.service';
import { SimpleCrypt } from "ngx-simple-crypt";
import { FormArray, FormGroup } from '@angular/forms';
import { User } from './model/user.model';

/**
 * Component base cho tất cả app
 */
export abstract class CrudComponentBase extends AppComponentBase {

    private permissions: PermissionsService;

    constructor(
        injector: Injector,
        messageService: MessageService,

    ) {
        super(injector, messageService);
        this.permissions = injector.get(PermissionsService);
        this.userInfo = this.getUser();
        //Khởi tạo danh sách quyền
        if([1,2,3].includes( this.userInfo.user_type)) {
            this.permissions.getAllPermission();
        }
      
    }
    userInfo: User;
    AppConsts = AppConsts;
    simpleCrypt = new SimpleCrypt();
    
    formatCurrencyJP(value) {
        if(value) {
            return value.toLocaleString('jp-JP', {style: 'currency', currency: 'JPY'});
        }
        return 0;
    }
    
    validateInput(event) {
        if (event.key === '-') {
          event.preventDefault();
        }
    }
    
    cryptEncode(id): string {
        return this.simpleCrypt.encode(AppConsts.keyCrypt,'' + id);
    }

    cryptDecode(codeId) {
        return this.simpleCrypt.decode(AppConsts.keyCrypt, codeId);
    }

    subject: any = {
        keyword: new Subject(),
    };

    keyword = '';
    page = new Page();
	offset = 0;

    isLoading = false;
    submitted = false;

    activeIndex =0;

    // === CHECK PERMISSION
    isGranted(permissionNames = []): boolean {
        // return true;
        return this.permissions.isGrantedRoot(permissionNames);
    }

    changeKeyword() {
        this.subject.keyword.next();
    }
    
    formatDate(value, type:string = TypeFormatDatesConst.DMY) {
      
        // return (moment(value).isValid() && value instanceof Date) ? moment(value).format(type) : '';
        return moment(value).isValid() ? moment(value).format(type) : '';
    }
   
    protected convertLowerCase(string: string = '') {
        if (string.length > 0) {
            return string.charAt(0).toLocaleLowerCase() + string.slice(1);
        }
        return '';
    }

    // === CHUYỂN ĐỔI DỮ LIỆU TỪ CALENDAR ĐỂ LƯU VÀ DATABASE KO BỊ SAI TIMEZONE
    protected formatCalendarSendApi(fields, model) {
        for (let field of fields) {
            if (model[field] instanceof Date) model[field] = this.formatCalendarItemSendApi(model[field]);
        }
        return model;
    }

    protected formatCalendarItemSendApi(datetime: Date) {
        return moment(datetime, TypeFormatDatesConst.YMDHmsDB).format(TypeFormatDatesConst.YMDHmsDB);
    }
    //====

    // === CHUYỂN ĐỔI DỮ LIỆU ĐỂ HIỂN THỊ ĐƯỢC TRONG CALENDAR
    protected formatCalendarDisplays(fields, model) {
        for (let field of fields) {
            if (model[field] instanceof Date) model[field] = new Date(model[field]);
        }
        return model;
    }
    // ===

    // === XỬ LÝ DATA ĐẨY LÊN DATABASE ĐÚNG THEO interface
    protected setDataSendApi(data, model) {
        for (const [key, value] of Object.entries(model)) {
            model[key] = data[key];
        }
        return model;
    }

    getTableHeight(percent = 65) {
        return (this.screenHeight * (percent / 100) + 'px');
    }

    public getLocalStorage(key: string) {
        return JSON.parse(localStorage.getItem(key))
    }

    public setLocalStorage(data: any[], key: string) {
        return localStorage.setItem(key, JSON.stringify(data));
    }

    getPriceText(price: number) {
        let priceText: string;
        if (price < 1000000000) {
            priceText = '~' + (Math.floor((price / 1000000) * 10) / 10) + ' tr';
        } else {
            priceText = '~' + (Math.floor((price / 1000000000) * 10) / 10) + ' tỷ';
        }
        return priceText;
    }

    public checkInValidForm(formGroup: FormGroup): boolean {
        for (const i in formGroup.controls) {
            formGroup.controls[i].markAsDirty();
            formGroup.controls[i].updateValueAndValidity();
        }
        return formGroup.invalid;
    }

    public validateProductPriceForm(form: FormGroup): boolean {
        const items = form.get('items') as FormArray;
        items.controls.forEach((group: FormGroup) => {
          Object.keys(group.controls).forEach(field => {
            const control = group.get(field);
            control.markAsDirty();
            control.updateValueAndValidity();
          });
        });      
        return form.valid;
      }

    public validateOrderForm(form: FormGroup): boolean {
        Object.keys(form.controls).forEach(field => {
          const control = form.get(field);
          control.markAsDirty();
          control.updateValueAndValidity();
        });
      
        // const orderDetails = form.get('orderDetails') as FormArray;
        // orderDetails.controls.forEach((group: FormGroup) => {
        //   Object.keys(group.controls).forEach(field => {
        //     const control = group.get(field);
        //     control.markAsDirty();
        //     control.updateValueAndValidity();
        //   });
        // });    
        console.log('form', form);
        console.log('form.valid', form.valid);
          
        return form.valid;
      }
    
    getPageCurrentInfo() {
		return {page: this.page.pageNumber, rows: this.page.pageSize};
	}

    mapItemSendApi(model: Object, data: Object) {
        let dataMap: Object = {};
        for(const [key, value] of Object.entries(model)) {
            dataMap[key] = data[key];
        }
        return dataMap;
    }

    mapItemSendApis(model: Object, data = []) {
        let dataMap = [];
        for (let item of data) {
            dataMap.push(this.mapItemSendApi(model, item));
        }
        return dataMap;
    }

    formatDateMonth(value) {
        return (moment(value).isValid() && value) ? moment(value).format('DD/MM') : '';
    }

}
