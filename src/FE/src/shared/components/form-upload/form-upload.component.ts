import { Component, OnInit, ViewChild } from '@angular/core';
import { ContentTypeEView, EAcceptFile, ETypeHandleLinkYoutube } from '@shared/consts/base.consts';
import { HelpersService } from '@shared/services/helpers.service';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { FileUpload } from 'primeng/fileupload';
import { Observable, forkJoin } from 'rxjs';
import { AppConsts } from '@shared/AppConsts';
import { IDialogUploadFileConfig } from '@shared/interfaces/other.interface';
import { IResponseItem } from '@shared/interfaces/response.interface';
import { HandleLinkYoutubePipe } from '@shared/pipes/handle-link-youtube.pipe';
import { FileService } from '@shared/services/file.service';
import { Utils } from '@shared/utils';


@Component({
  selector: 'app-form-upload',
  templateUrl: './form-upload.component.html',
  styleUrls: ['./form-upload.component.scss'],
  providers: [HandleLinkYoutubePipe]
})
export class FormUploadComponent implements OnInit {

    constructor(
        public ref: DynamicDialogRef, 
        public config: DynamicDialogConfig,
        private _fileService: FileService,
        private _helpersService: HelpersService,
        private _handleYoutubePipe: HandleLinkYoutubePipe,
    ) {}

    // CONSTS
    EAcceptFile = EAcceptFile;
    ETypeHandleLinkYoutube = ETypeHandleLinkYoutube;
    endPointApiUpload = AppConsts.endPointApiUpload;
    Utils = Utils;
    //
    isUploading: boolean = false;
    dialogData: IDialogUploadFileConfig = {};
    inputValue: string;

    ngOnInit(): void {
        let data: IDialogUploadFileConfig = this.config.data;
        const chooseLabel = data?.accept === EAcceptFile.IMAGE ? "Chọn ảnh tải lên" : (data?.accept === EAcceptFile.VIDEO ? "Chọn video tải lên" : "Chọn file tải lên");
        this.dialogData  = {
            ...data,
            folderUpload: data.folderUpload,
            uploadServer: !(data.uploadServer === false),
            multiple: !(data.multiple === false),
            accept: data.accept || EAcceptFile.ALL,
            previewBeforeUpload: !(data.previewBeforeUpload === false),
            isChooseNow: !(data.isChooseNow === false),
            inputValue: data.inputValue,
            inputRequired: data.inputRequired,
            chooseLabel: data.chooseLabel || chooseLabel,
        };
        this.inputValue = this.dialogData.inputValue || '';
        //
        if(!this.dialogData.folderUpload) {
            this._helpersService.messageError('','Dev chưa thêm Tên thư mục upload!', 60000);
        }

        // ẨN MODAL KHI CHỌN FILE
        if(this.dialogData.isChooseNow) {
            const elements: any = document.querySelectorAll('.p-dialog-mask-scrollblocker');
            elements[elements.length - 1].style.opacity = 0;
        }
    }

    @ViewChild('pUpload') pUpload: FileUpload;
    
    fileInput: HTMLElement;
    ngAfterViewInit() {
        if(this.dialogData.isChooseNow) {
            this.pUpload.choose();
            // ẨN DIALOG_MODAL KHI CLICK CANCEL KHÔNG CHỌN FILE
            const elementPUpload = document.getElementsByClassName("e-file-upload");
            this.fileInput = elementPUpload[0].getElementsByTagName("input")[0];
            this.fileInput.addEventListener("cancel", this.hideDialogListen);
        }
    }

    hideDialogListen = () => {
        this.hideDialog()
    }
    
    hideDialog(): any {
        this.ref.close();
    }

    eventFile: Event;
    onSelectedFiles(event?: any) {
        // console.log('event', event);
        this.files = event.currentFiles;
        this.eventFile = event.originalEvent;
        if(this.dialogData.accept === EAcceptFile.VIDEO || Utils.checkLinkYoutube(this.inputValue)) {
            // Nếu chọn upload video thì remove đường dẫn 
            this.inputValue = '';
        }
        //
        if(this.dialogData.previewBeforeUpload === false) {
            this.config.data.isLoading = true;
            this.onUpload();
        } else {
            // HIỆN MODEL SAU KHI CHỌN XONG FILE
            const elements: any = document.querySelectorAll('.p-dialog-mask-scrollblocker');
            elements[elements.length - 1].style.opacity = 1;
            if(this.fileInput) {
                this.fileInput.removeEventListener("cancel", this.hideDialogListen);
            }
        }
    }

    changeValue(inputValue) {
        if(this._handleYoutubePipe.checkLinkYoutube(inputValue)) {
            this.inputValue = this._handleYoutubePipe.getLinkWatchYoutube(inputValue);
            this.files = [];
        }
    }

    fileType = (file: File) => {
        return file.type.split('/')[0] || EAcceptFile.IMAGE;
    }

    getBlobVideo = (file: File) => {
        const blobVideo = this._helpersService.sanitizer.bypassSecurityTrustUrl(URL.createObjectURL(file));
        return blobVideo || '';
    }

    files: File[]= [];
    preview(image) {
        // this._helpersService.dialogViewerRef(
        //     image,
        //     ContentTypeEView.IMAGE,
        // );
    }

    cropImage(fileImage, index) {
        // const ref = this._helpersService.dialogService.open(
        //     ECropImageComponent,
        //     {
        //         header: 'Cắt ảnh',
        //         width: 'auto',
        //         style: {'min-width': '300px', 'max-height': '100%'},
        //         data: {
        //             fileImage: fileImage,
        //             eventFile: this.eventFile,
        //         }
        //     }
        // ).onClose.subscribe((fileCrop) => {
        //     if(fileCrop) {
        //         this.files[index] = fileCrop;
        //     }
        // })
    }

    removeFile(index: number) {
        this.files.splice(index, 1);
    }

    onUpload() {
        if((this.dialogData.inputRequired && this.inputValue.trim()) || !this.dialogData.inputRequired) {
            if(this.dialogData.uploadServer && this.files.length > 0 && this.dialogData.folderUpload) {
                let uploadFilesProcess: Observable<IResponseItem<string>>[] = [];
                //
                let quantity: number = +this.dialogData.quantity;
                quantity = quantity > 0 ? quantity : undefined;  
                //
                this.files.forEach((file, index) => {
                    if((quantity && index < quantity) || !quantity) {
                        uploadFilesProcess.push(this._fileService.uploadFile(file, this.dialogData.folderUpload));
                    }
                });
                //
                this.isUploading = true;
                if(this.dialogData?.callback) {
                    // Gọi lại component gốc để tương tác
                    this.dialogData.callback();
                }
                //
                let fileUploads: string[] = [];
                forkJoin(uploadFilesProcess).subscribe(results => {
                    this.isUploading = false;
                    if(results && results?.length) {
                        fileUploads = results.map(file => file?.data)
                    } 
                    //
                    this.ref.close({
                        inputData: this.inputValue,
                        fileUrls: fileUploads, 
                    })
                }, (err) => {
                    this.isUploading = false;
                });
            } else {
                this.ref.close({
                    inputData: this.inputValue,
                    fileUrls: this.files, 
                })
            }
        } else {
            this._helpersService.messageError(`Vui lòng nhập ${this.dialogData.titleInput}`);
        }
    }
}
