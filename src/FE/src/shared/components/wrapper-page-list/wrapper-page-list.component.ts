import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { localStorageKeyConst } from '@shared/AppConsts';

@Component({
  selector: 'wrapper-page-list',
  templateUrl: './wrapper-page-list.component.html',
  styleUrls: ['./wrapper-page-list.component.scss']
})
export class WrapperPageListComponent implements OnInit {

    constructor() { }

    @Input() pageName: string;
    @Input() scrollHeight: number;
    @Input() type: string;

    @Output() _countScrollHeight: EventEmitter<number> = new EventEmitter<number>();

    ngOnInit(): void {}

    @ViewChild('pageEl') pageEl: ElementRef<HTMLElement>;

    ngAfterViewInit() {
        setTimeout(() => {
            // TÍNH CHIỀU CAO NỘI DUNG PAGE = WINDOWN_INNER_HEIGHT - (LAYOUT_TOP_HEIGHT(= TOPBAR + BREADCRUM) + PAGE_HEIGHT_INIT) (PAGE_HEIGHT_INIT: CHIỀU CAO PAGE(COMPONENT) KHI CHƯA CÓ NỘI DUNG) 
            let layoutTopHeight = +localStorage.getItem(localStorageKeyConst.layoutTopHeight);
            let pageHeightInit = this.pageEl.nativeElement.offsetHeight;
            this.scrollHeight = window.innerHeight - (layoutTopHeight + pageHeightInit + 5);
            console.log('pageEl____', layoutTopHeight, pageHeightInit, this.scrollHeight);
            this._countScrollHeight.emit(this.scrollHeight);
        }, 0);
    }

}
