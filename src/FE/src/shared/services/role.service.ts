import { HttpClient } from "@angular/common/http";
import { Inject, Injectable, Optional } from "@angular/core";
import { IResponse } from "@shared/interfaces/base-interface";
import { IRole } from "@shared/interfaces/role-manager.interface";
import { API_BASE_URL, ServiceProxyBase } from "@shared/service-proxies/service-proxies-base";
import { CookieService } from "ngx-cookie-service";
import { MessageService } from "primeng/api";
import { Observable } from "rxjs";

@Injectable()
export class RoleService extends ServiceProxyBase { 
    constructor(
        messageService: MessageService, 
        _cookieService: CookieService, 
        @Inject(HttpClient) http: HttpClient, 
        @Optional() @Inject(API_BASE_URL) baseUrl?: string
        ) {
        super(messageService, _cookieService, http, baseUrl);
    }

    /**
     * Gọi tất cả quyền
     * @param  
     * @returns 
     */
    findAllPermission(): Observable<IResponse<any>> {
        return this.requestGet<IResponse<IRole>>('/api/permission/find-all?');
    }

    /**
     * thêm role mới
     * @param body 
     * @returns 
     */
    create(body): Observable<IResponse<any>> {
        return this.requestPost<IResponse<IRole>>(body, '/api/role/add?')
    }

    /**
     * Cập nhật role 
     * @param body 
     * @returns 
     */
    update(body): Observable<IResponse<any>> {
        return this.requestPut<IResponse<IRole>>(body, '/api/role/update?')
    }

    /**
     * Xóa role
     * @param id 
     * @returns 
     */
    delete(id): Observable<IResponse<any>> {
        return this.requestPut<IResponse<any>>(null, `/api/role/delete/${id}?`)
    }

    /**
     * danh sach role
     * @param  
     * @returns 
     */
    getAllRole(userType?: number): Observable<IResponse<any>> {
        let url_ = '/api/role/find-all?';
        if (userType) url_ += this.convertParamUrl("userType", userType);
        return this.requestGet<IResponse<IRole>>(url_);
    }

    /**
     * Thông tin role theo id
     * @param IRole 
     * @returns 
     */
    getRoleById(id: number): Observable<IResponse<any>> {
        return this.requestGet<IResponse<IRole>>(`/api/role/find-by-id/${id}?`)
    }

}