import { HttpClient } from "@angular/common/http";
import { Inject, Injectable, Optional } from "@angular/core";
import { IResponse } from "@shared/interfaces/base-interface";
import { IProductCategory } from "@shared/interfaces/product-category.interface";
import { Page } from "@shared/model/page";
import { API_BASE_URL, ServiceProxyBase } from "@shared/service-proxies/service-proxies-base";
import { CookieService } from "ngx-cookie-service";
import { MessageService } from "primeng/api";
import { Observable } from "rxjs";

@Injectable()
export class ProductCategoryService extends ServiceProxyBase { 
    private endPoint = '/api/product-manager/product-category'

    constructor(
        messageService: MessageService, 
        _cookieService: CookieService, 
        @Inject(HttpClient) http: HttpClient, 
        @Optional() @Inject(API_BASE_URL) baseUrl?: string
        ) {
        super(messageService, _cookieService, http, baseUrl);
    }
    /**
     * danh sach product category
     * @param page 
     * @returns 
     */
    getAll(page: Page): Observable<any> {
        let url_ = `${this.endPoint}/find?`;
        if (page.keyword) url_ += this.convertParamUrl("keyword", page.keyword);
        url_ += this.convertParamUrl('pageSize', page.pageSize);
        url_ += this.convertParamUrl('pageNumber', page.getPageNumber());
        //
        return this.requestGetAnonymous<IResponse<IProductCategory>>(url_);
    }

    /**
     * them product category
     * @param 
     * @returns 
     */
    getAllNoPaging():Observable<any> {
        let url_ = `${this.endPoint}/find-list?`;
        return this.requestGet<IResponse<IProductCategory>>(url_);
    }

    /**
     * them product category
     * @param body 
     * @returns 
     */
    create(body): Observable<IResponse<any>> {
        return this.requestPost<IResponse<any>>(body, `${this.endPoint}/create?`);
    }

    /**
     * update product category
     * @param body 
     * @returns 
     */
    update(body): Observable<IResponse<any>> {
        return this.requestPut<IResponse<any>>(body, `${this.endPoint}/update?`);
    }

    /**
     * delete product category
     * @param id 
     * @returns 
     */
    delete(id): Observable<IResponse<any>> {
        let url_ = `${this.endPoint}/delete/${id}?`;
        return this.requestDelete<IResponse<any>>(url_);
    }

    /**
     * Đổi trạng thái product category
     * @param id 
     * @returns 
     */
    changeStatus(id): Observable<IResponse<any>>{
        return this.requestPut<IResponse<any>>(null, `/api/product-manager/product-category/active/${id}?`);
    }
}