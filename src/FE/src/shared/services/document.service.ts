import { AppConsts, UserConst } from '@shared/AppConsts';
import {
    mergeMap as _observableMergeMap,
    catchError as _observableCatch,
} from "rxjs/operators";
import {
    Observable,
    throwError as _observableThrow,
    of as _observableOf,
} from "rxjs";
import { Injectable, Inject, Optional, Injector } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Page } from "@shared/model/page";
import { MessageService } from 'primeng/api';
import { CookieService } from 'ngx-cookie-service';
import { API_BASE_URL, ServiceProxyBase } from '@shared/service-proxies/service-proxies-base';


@Injectable()
export class DocumentService extends ServiceProxyBase {
    constructor(
        messageService: MessageService,
        _cookieService: CookieService,
        @Inject(HttpClient) http: HttpClient,
        @Optional() @Inject(API_BASE_URL) baseUrl?: string
    ) {
        super(messageService, _cookieService, http, baseUrl);
    }

    /**
     * them 
     * @param body 
     * @returns 
     */
    create(body): Observable<any> {
        return this.requestPost(body, "/api/document/create");
    }

    /**
     * cap nhat 
     * @param body 
     * @returns 
     */
    update(body): Observable<any> {
        return this.requestPut(body, "/api/document/update");
    }

    /**
     * xoa 
     * @param id 
     * @returns 
     */
    delete(id: number): Observable<void> {
        let url_ = `/api/document/delete/${id}`;
        return this.requestDelete(url_);
    }

    /**
     * chi tiet 
     * @param id 
     * @returns 
     */
    get(id: number): Observable<any> {
        let user: { [key: string]: any } = this.getUser();
        let urlMap: { [key: number]: string } = {
            [UserConst.BUSINESS_CUSTOMER]: "/api/document/business-customer/" + id,
            [UserConst.ADMIN]: "/api/document/" + id,
        };
        let url_ = urlMap[user.user_type];
        return this.requestGet(url_);
    }

    /**
     * danh sach 
     * @param page 
     * @returns 
     */
    getAll(page: Page): Observable<any> {

        let user: { [key: string]: any } = this.getUser();
        let urlMap: { [key: number]: string } = {
            [UserConst.BUSINESS_CUSTOMER]: "/api/document/business-customer/find-all?",
            [UserConst.ADMIN]: "/api/document/find?"
        };
        
        let url_ = urlMap[user.user_type];
        if(page.keyword) url_ += this.convertParamUrl("keyword", page.keyword);
        url_ += this.convertParamUrl('pageSize', page.pageSize);
        url_ += this.convertParamUrl('pageNumber', page.getPageNumber());
        return this.requestGet(url_);
    }
}