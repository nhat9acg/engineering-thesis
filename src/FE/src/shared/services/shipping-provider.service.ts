import { HttpClient } from "@angular/common/http";
import { Inject, Injectable, Optional } from "@angular/core";
import { IResponse } from "@shared/interfaces/base-interface";
import { Page } from "@shared/model/page";
import { API_BASE_URL, ServiceProxyBase } from "@shared/service-proxies/service-proxies-base";
import { CookieService } from "ngx-cookie-service";
import { MessageService } from "primeng/api";
import { Observable } from "rxjs";


@Injectable()
export class ShippingProviderService extends ServiceProxyBase { 
    private endPoint = '/api/shipping-provider'

    constructor(
        messageService: MessageService, 
        _cookieService: CookieService, 
        @Inject(HttpClient) http: HttpClient, 
        @Optional() @Inject(API_BASE_URL) baseUrl?: string
        ) {
        super(messageService, _cookieService, http, baseUrl);
    }

    getAll(page: Page): Observable<any> {
        let url_ = `${this.endPoint}/find-all?`;
        if (page.keyword) url_ += this.convertParamUrl("keyword", page.keyword);
        url_ += this.convertParamUrl('pageSize', page.pageSize);
        url_ += this.convertParamUrl('pageNumber', page.getPageNumber());
        //
        return this.requestGet<IResponse<any>>(url_);
    }

    getAllNoPaging(): Observable<any> {
        let url_ = `${this.endPoint}/find-all?`;
        url_ += this.convertParamUrl('pageSize', -1);
        return this.requestGet<IResponse<any>>(url_);
    }

}