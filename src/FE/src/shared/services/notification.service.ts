import { AppConsts } from '@shared/AppConsts';
import {
    mergeMap as _observableMergeMap,
    catchError as _observableCatch,
    map,
} from "rxjs/operators";
import {
    Observable,
    throwError as _observableThrow,
    of as _observableOf,
} from "rxjs";
import { Injectable, Inject, Optional, Injector } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Page } from "@shared/model/page";
import { MessageService } from 'primeng/api';
import { CookieService } from 'ngx-cookie-service';
import { API_BASE_URL, ServiceProxyBase } from '@shared/service-proxies/service-proxies-base';

@Injectable()
export class NotificationService extends ServiceProxyBase {
    constructor(
        messageService: MessageService,
        _cookieService: CookieService,
        @Inject(HttpClient) http: HttpClient,
        @Optional() @Inject(API_BASE_URL) baseUrl?: string
    ) {
        super(messageService, _cookieService, http, baseUrl);
    }

    getSystemMedia(): Observable<any> {
      return this.requestGetAnonymous(`/api/notification/media`);
    }
    createOrUpdateMedia(body): Observable<any> {
      return this.requestPost(body, `/api/notification/media`);
    }

    getSystemNotification(): Observable<any> {
      return this.requestGet(`/api/notification/system`);
    }

    createOrUpdateSystemNotification(body): Observable<any> {
      return this.requestPost(body, `/api/notification/system`);
    }

    getNotification(): Observable<any> {
      let url_ = `/api/notification/notification?`
      url_ += this.convertParamUrl("pageSize", -1);
      return this.requestGet(url_);
    }

    readNotification(notificationId): Observable<any> {
      let url_ = `/api/notification/read-notification/${notificationId}`
      return this.requestPost(null,url_);
    }

    GetInfoNotSeenNotification(): Observable<any> {
      let url_ = `/api/notification/get-info-not-seen`
      return this.requestGet(url_);
    }
}