import { HttpClient } from "@angular/common/http";
import { Inject, Injectable, Optional } from "@angular/core";
import { IResponse } from "@shared/interfaces/base-interface";
import { IProduct } from "@shared/interfaces/product.interface";
import { Page } from "@shared/model/page";
import { API_BASE_URL, ServiceProxyBase } from "@shared/service-proxies/service-proxies-base";
import { CookieService } from "ngx-cookie-service";
import { MessageService } from "primeng/api";
import { Observable } from "rxjs";

@Injectable()
export class ProductService extends ServiceProxyBase { 
    private endPoint = '/api/product-manager'

    constructor(
        messageService: MessageService, 
        _cookieService: CookieService, 
        @Inject(HttpClient) http: HttpClient, 
        @Optional() @Inject(API_BASE_URL) baseUrl?: string
        ) {
        super(messageService, _cookieService, http, baseUrl);
    }

    /**
     * danh sach product
     * @param page 
     * @returns 
     */
    getAll(page: Page, dataFilter): Observable<IResponse<any>> {
        let url_ = `${this.endPoint}/find?`;
        if(page.keyword) url_ += this.convertParamUrl(dataFilter?.field, page.keyword);
        url_ += this.convertParamUrl('pageSize', page.pageSize);
        url_ += this.convertParamUrl('pageNumber', page.getPageNumber());
        //
        return this.requestGet<IResponse<IProduct>>(url_);
    }

    /**
     * danh sach product chua co api no
     * @param  
     * @returns 
     */
    getAllNoPaging(): Observable<IResponse<any>> {
        let url_ = `${this.endPoint}/find?`;
        url_ += this.convertParamUrl('pageSize', -1);
        //
        return this.requestGet<IResponse<IProduct>>(url_);
    }

    get(id: number): Observable<IResponse<any>> {
        let url_ = `${this.endPoint}/find/${id}`;
        return this.requestGetAnonymous<IResponse<IProduct>>(url_);
    }

    /**
     * them product 
     * @param body 
     * @returns 
     */
    create(body): Observable<IResponse<any>> {
        return this.requestPost<IResponse<any>>(body, `${this.endPoint}/create?`);
    }

    /**
     * update product 
     * @param body 
     * @returns 
     */
    update(body): Observable<IResponse<any>> {
        return this.requestPut<IResponse<any>>(body, `${this.endPoint}/update?`);
    }

    /**
     * delete product category
     * @param id 
     * @returns 
     */
    delete(id): Observable<IResponse<any>> {
        let url_ = `${this.endPoint}/delete/${id}`;
        return this.requestDelete<IResponse<any>>(url_);
    }
    
    /**
     * Đổi trạng thái product category
     * @param id 
     * @returns 
     */
    changeStatus(id): Observable<IResponse<any>>{
        return this.requestPut<IResponse<any>>(null, `${this.endPoint}/active/${id}?`);
    }

    getAllAnonymous(page: Page, dataFilter): Observable<IResponse<any>> {
        let url_ = `${this.endPoint}/find-anonymous?`;
        if(page.keyword) url_ += this.convertParamUrl(dataFilter?.field, page.keyword);
        if(dataFilter?.productCategoryIds && dataFilter?.productCategoryIds?.length > 0 ) {
            for (const id of dataFilter.productCategoryIds) {
                url_ += this.convertParamUrl("productCategoryIds", id);
            }
        } 
        url_ += this.convertParamUrl('pageSize', page?.pageSize);
        url_ += this.convertParamUrl('pageNumber', page.getPageNumber());
        return this.requestGetAnonymous<IResponse<IProduct>>(url_);
    }
}