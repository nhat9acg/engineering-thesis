import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { AppConsts } from '@shared/AppConsts';
import { Page } from '@shared/model/page';
import { CookieService } from 'ngx-cookie-service';
import { SimpleCrypt } from 'ngx-simple-crypt';

@Injectable({
    providedIn: 'root'
})
export class HelperService {

    currentLang: string;
    page = new Page();
    simpleCrypt = new SimpleCrypt();

    constructor() {
        this.currentLang = localStorage.getItem(AppConsts.localStorageLangKey) || AppConsts.defaultLang;
    }

    // Convert key language khi biết parentKey và key
    convertLangKey(parentLangKeys: string[], lang: any) {
        let keys: string[] = [];
        for (let parentKey of parentLangKeys) {
            if (lang[parentKey]) {
                for (let key of Object.keys(lang[parentKey])) {
                    keys.push([parentKey, key].join('.'));
                }
            }
        }
        return keys;
    }

    // Check valid Form before submit
    checkInValidForm(formGroup: FormGroup): boolean {
        for (const i in formGroup.controls) {
            formGroup.controls[i].markAsDirty();
            formGroup.controls[i].updateValueAndValidity();
        }
        return formGroup.invalid;
    }

    //
    getPageCurrentInfo() {
		return {page: this.page.pageNumber, rows: this.page.pageSize};
	}

    getLocalStorage(key: string) {
        return JSON.parse(localStorage.getItem(key))
    }

    setLocalStorage(data: any[], key: string) {
        return localStorage.setItem(key, JSON.stringify(data));
    }

}