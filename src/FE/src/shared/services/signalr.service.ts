import { CrudComponentBase } from '@shared/crud-component-base';
import { Inject, Injectable, Injector, Optional } from "@angular/core";
import * as signalR from "@microsoft/signalr";
import { HubConnection, HubConnectionBuilder, HttpTransportType, LogLevel } from '@microsoft/signalr';
import { Observable, Subject } from "rxjs";
import { TokenService } from '@shared/services/token.service';
import { API_BASE_URL, ServiceProxyBase } from "@shared/service-proxies/service-proxies-base";
import { HttpClient } from "@angular/common/http";
import { AppConsts } from '@shared/AppConsts';

@Injectable({
    providedIn: "root",
})
export class SignalrService {
    constructor(
        private _tokenService: TokenService,
    ) {
        this.baseUrl = AppConsts.remoteServiceBaseUrl;
    }
    private hubConnection: any;
    private baseUrl: string;

    isSignalRConnected: boolean;

    public startConnection() {
        return new Promise((resolve, reject) => {
          this.hubConnection = new HubConnectionBuilder()
            .withUrl(
              `${this.baseUrl}/hub/webSell`,
              {
                accessTokenFactory: () => this._tokenService.getToken(),
                skipNegotiation: true,
                transport: signalR.HttpTransportType.WebSockets
              }
              )
            .build();
    
          this.hubConnection.start().then(() => {
          
            this.isSignalRConnected = true;
            console.log("start", this.isSignalRConnected);
            this.hubConnection.onclose((error) => {
              this.isSignalRConnected = false;
              console.log("onclose", this.isSignalRConnected);
            });
            this.hubConnection.onreconnecting((error) => {
              this.isSignalRConnected = false;
              console.log("onreconnecting", this.isSignalRConnected);
            });
            this.hubConnection.onreconnected((connectionId) => {
              this.isSignalRConnected = true;
              console.log("onreconnected", this.isSignalRConnected);
            });
            resolve(true);
          })
          .catch((err: any) => {
            this.isSignalRConnected = false;
            console.log("catch", this.isSignalRConnected);
            reject(err);
          });
        });
      }

    public listen(eventName: string, callback: (...args: any[]) => void) {
        if (!this.hubConnection) {
            throw new Error('SignalR connection is not established. Call startConnection() first.');
        }
        this.hubConnection.on(eventName, callback);
    }
}
