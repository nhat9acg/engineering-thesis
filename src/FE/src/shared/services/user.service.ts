import { HttpClient } from "@angular/common/http";
import { Inject, Injectable, Optional } from "@angular/core";
import { UserConst } from "@shared/AppConsts";
import { IResponse } from "@shared/interfaces/base-interface";
import { IFilterCustomer, IUser } from "@shared/interfaces/user-manager.interface";
import { Page } from "@shared/model/page";
import { API_BASE_URL, ServiceProxyBase } from "@shared/service-proxies/service-proxies-base";
import { CookieService } from "ngx-cookie-service";
import { MessageService } from "primeng/api";
import { Observable } from "rxjs";

@Injectable()
export class UserService extends ServiceProxyBase { 
    constructor(
        messageService: MessageService, 
        _cookieService: CookieService, 
        @Inject(HttpClient) http: HttpClient, 
        @Optional() @Inject(API_BASE_URL) baseUrl?: string
        ) {
        super(messageService, _cookieService, http, baseUrl);
    }

    /**
     * Thêm mới user
     * @param body 
     * @returns 
     */
    create(body,filterCustomer: IFilterCustomer): Observable<IResponse<any>> {
        let url_ = "/api/user/add?"
        if (filterCustomer?.businessCustomerId) {
            url_ = "/api/user/add-by-business";
        } else if (filterCustomer?.restaurantId) {
            url_ = "/api/user/add-by-restaurant";
        }
        return this.requestPost<IResponse<IUser>>(body, url_);
    }

    /**
     * Cập nhật user
     * @param body 
     * @returns 
     */
    update(body): Observable<IResponse<any>> {
        let url_ = '/api/user/update?'
        return this.requestPut<IResponse<IUser>>(body, url_)
    }

    /**
     * danh sach user
     * @param page 
     * @returns 
     */
    getAll(page: Page, filterCustomer: IFilterCustomer): Observable<IResponse<any>> {
        let url_ = '/api/user/find-all?';
        url_ += this.convertParamUrl("keyword", page.keyword);
        url_ += this.convertParamUrl('pageSize', page.pageSize);
        url_ += this.convertParamUrl('pageNumber', page.getPageNumber());
        if(filterCustomer?.businessCustomerId) {
            url_ += this.convertParamUrl("businessCustomerId", filterCustomer?.businessCustomerId);
        } 
        if(filterCustomer?.restaurantId) {
            url_ += this.convertParamUrl("restaurantId", filterCustomer?.restaurantId);
        } 
        //
        return this.requestGet<IResponse<IUser>>(url_);
    }

    /**
     * Xóa user
     * @param id 
     * @returns 
     */
    delete(id): Observable<IResponse<any>>{
        return this.requestPut<IResponse<any>>(null, `/api/user/delete/${id}?`);
    }

    /**
     * Đổi trạng thái user
     * @param id 
     * @returns 
     */
    changeStatus(id): Observable<IResponse<any>>{
        return this.requestPut<IResponse<any>>(null, `/api/user/change-status/${id}?`);
    }
}
