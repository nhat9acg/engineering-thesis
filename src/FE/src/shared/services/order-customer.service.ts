import { AppConsts, UserConst } from '@shared/AppConsts';
import {
    mergeMap as _observableMergeMap,
    catchError as _observableCatch,
} from "rxjs/operators";
import {
    Observable,
    throwError as _observableThrow,
    of as _observableOf,
} from "rxjs";
import { Injectable, Inject, Optional, Injector } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Page } from "@shared/model/page";
import { MessageService } from 'primeng/api';
import { CookieService } from 'ngx-cookie-service';
import { API_BASE_URL, ServiceProxyBase } from '@shared/service-proxies/service-proxies-base';
import { IResponse } from '@shared/interfaces/base-interface';
import { IBusinessCustomerDropdown, IRestaurant } from '@shared/interfaces/restaurant.interface';
import { IProduct } from '@shared/interfaces/product.interface';
import { IOrder } from '@shared/interfaces/order.interface';
import * as moment from 'moment';
import { IBusinessCustomer } from '@shared/interfaces/business-customer.interface';
import { Calendar } from '@shared/model/calendar.model';

@Injectable()
export class OrderCustomerService extends ServiceProxyBase {
    constructor(
        messageService: MessageService,
        _cookieService: CookieService,
        @Inject(HttpClient) http: HttpClient,
        @Optional() @Inject(API_BASE_URL) baseUrl?: string
    ) {
        super(messageService, _cookieService, http, baseUrl);
    }
 
    /**
     * them don hang
     * @param body 
     * @returns 
     */
    create(body): Observable<IResponse<any>> {
        let url_ = "/api/order/customer/add";
        return this.requestPost<IResponse<IOrder>>(body, url_);
    }

    /**
     * cap nhat don hang
     * @param body 
     * @returns 
     */
    update(body): Observable<IResponse<any>> {
        return this.requestPut<IResponse<IOrder>>(body, "/api/order/update");
    }

    /**
     * xoa don hang
     * @param id 
     * @returns 
     */
    delete(id: number): Observable<IResponse<any>> {
        let url_ = `/api/order/delete/${id}`;
        return this.requestDelete<IResponse<any>>(url_);
    }

    /**
     * xem don hang theo id
     * @param id 
     * @returns 
     */
    get(id: number): Observable<IResponse<any>> {
        let url_ = `/api/order/customer/${id}`;
        return this.requestGet<IResponse<IOrder>>(url_);
    }

    /**
     * lay danh sach don hang
     * @param page 
     * @returns 
     */
    getAll(page: Page, filter: any): Observable<IResponse<any>> {
        let url_ = '/api/order/customer/find-all?';
     
        if (filter.status) url_ += this.convertParamUrl("status", filter.status);
        if (filter.dates) {
            url_ += this.convertParamUrl('startDate', moment(filter.dates[0]).format('YYYY-MM-DD'));
            url_ += this.convertParamUrl('endDate', moment(filter.dates[1]).format('YYYY-MM-DD'));
            
        }
        url_ += this.convertParamUrl("keyword", page.keyword);
        url_ += this.convertParamUrl('pageSize', page.pageSize);
        url_ += this.convertParamUrl('pageNumber', page.getPageNumber());
        return this.requestGet<IResponse<IOrder>>(url_);
    }

    //CALL API KHAC

    /**
     * goi danh sach san pham
     * @param page 
     * @returns 
     */
    getAllProduct(businessCustomerId?): Observable<IResponse<any>> {
        let url_ = `/api/product-price/find-by-business-customer?`;
        if(businessCustomerId) url_ += this.convertParamUrl('id', businessCustomerId);
        return this.requestGet<IResponse<IProduct>>(url_);
    }

     /**
     * goi danh sach dia chi 
     * @param page 
     * @returns 
     */
     getAllAddress(): Observable<IResponse<any>> {
        let user: { [key: string]: any } = this.getUser();
        let url_ = `/api/user/delivery-address/find-by-user/${user.user_id}`;
        return this.requestGet<IResponse<IRestaurant>>(url_);
    }

    cancel(body){
        return this.requestPut<IResponse<any>>(body, `/api/order/cancel-order`);
    }

    response(body){
        return this.requestPut<IResponse<any>>(body, `/api/order/response-order`);
    }

    confirmOrder(body){
        return this.requestPut<IResponse<any>>(body, `/api/order/confirm-order`);
    }

    processing(id: number){
        let url_ = `/api/order/processing-order/${id}`;
        return this.requestPut<IResponse<any>>(null, url_);
    }
}