import { AppConsts } from '@shared/AppConsts';
import {
    mergeMap as _observableMergeMap,
    catchError as _observableCatch,
} from "rxjs/operators";
import {
    Observable,
    throwError as _observableThrow,
    of as _observableOf,
} from "rxjs";
import { Injectable, Inject, Optional, Injector } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Page } from "@shared/model/page";
import { MessageService } from 'primeng/api';
import { CookieService } from 'ngx-cookie-service';
import { API_BASE_URL, ServiceProxyBase } from '@shared/service-proxies/service-proxies-base';


@Injectable()
export class GroupCustomerService extends ServiceProxyBase {
    constructor(
        messageService: MessageService,
        _cookieService: CookieService,
        @Inject(HttpClient) http: HttpClient,
        @Optional() @Inject(API_BASE_URL) baseUrl?: string
    ) {
        super(messageService, _cookieService, http, baseUrl);
    }

    /**
     * them nhom khach hang
     * @param body 
     * @returns 
     */
    create(body): Observable<any> {
        return this.requestPost(body, "/api/group-customer/create");
    }

    /**
     * cap nhat nhom khach hang
     * @param body 
     * @returns 
     */
    update(body): Observable<any> {
        return this.requestPut(body, "/api/group-customer/update");
    }

    /**
     * xoa nhom khach hang
     * @param id 
     * @returns 
     */
    delete(id: number): Observable<void> {
        let url_ = `/api/group-customer/delete/${id}`;
        return this.requestDelete(url_);
    }

    /**
     * chi tiet nhom khach hang
     * @param id 
     * @returns 
     */
    get(id: number): Observable<any> {
        let url_ = "/api/group-customer/find/" + id;
        return this.requestGet(url_);
    }

    /**
     * danh sach nhom khach hang
     * @param page 
     * @returns 
     */
    getAll(page: Page): Observable<any> {
        let url_ = "/api/group-customer/find?";
        if(page.keyword) url_ += this.convertParamUrl("keyword", page.keyword);
        url_ += this.convertParamUrl('pageSize', page.pageSize);
        url_ += this.convertParamUrl('pageNumber', page.getPageNumber());
        return this.requestGet(url_);
    }

    getAllNoPaging():Observable<any> {
        let url_ = "/api/group-customer/find?";
        url_ += this.convertParamUrl('pageSize', -1);
        return this.requestGet(url_);
    }
}