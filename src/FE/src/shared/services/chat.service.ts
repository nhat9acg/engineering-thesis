import { HttpClient } from "@angular/common/http";
import { Inject, Injectable, Optional } from "@angular/core";
import { IResponse } from "@shared/interfaces/base-interface";
import { IProductPrice } from "@shared/interfaces/product-price.interface";
import { Page } from "@shared/model/page";
import { API_BASE_URL, ServiceProxyBase } from "@shared/service-proxies/service-proxies-base";
import { CookieService } from "ngx-cookie-service";
import { MessageService } from "primeng/api";
import { Observable } from "rxjs";

@Injectable()
export class ChatService extends ServiceProxyBase { 
    private endPoint = '/api/chat'

    constructor(
        messageService: MessageService, 
        _cookieService: CookieService, 
        @Inject(HttpClient) http: HttpClient, 
        @Optional() @Inject(API_BASE_URL) baseUrl?: string
        ) {
        super(messageService, _cookieService, http, baseUrl);
    }

    /**
     * danh sach 
     * @param page 
     * @returns 
     */
    getAll(page: Page, fieldFilters?: any): Observable<IResponse<any>> {
        let url_ = `${this.endPoint}/find?`;
        if(page.keyword) url_ += this.convertParamUrl("keyword", page.keyword);

        if(fieldFilters) {
            for(const [key, value] of Object.entries(fieldFilters)) {
                if(key == 'searchField') {
                    if(page.keyword) url_ += this.convertParamUrl(fieldFilters.searchField, page.keyword);
                } else {
                    if(value) url_ += this.convertParamUrl(key, `${value}`);
                } 
            }
        }
        url_ += this.convertParamUrl('Sort', 'id-asc');
        url_ += this.convertParamUrl('pageSize', page.pageSize);
        url_ += this.convertParamUrl('pageNumber', page.getPageNumber());
        url_ += this.convertParamUrl('numberMessage', 5);
        //
        return this.requestGet<IResponse<any>>(url_);
    }

    /**
     * create
     * @param body 
     * @returns 
     */
    create(body): Observable<IResponse<any>> {
        return this.requestPost<IResponse<any>>(body, `${this.endPoint}/add`);
    }
}