import { HttpClient } from "@angular/common/http";
import { Inject, Injectable, Optional } from "@angular/core";
import { AppConsts } from "@shared/AppConsts";
import { IResponse } from "@shared/interfaces/base-interface";
import { IResponseItem } from "@shared/interfaces/response.interface";
import { API_BASE_URL, ServiceProxyBase } from "@shared/service-proxies/service-proxies-base";
import { CookieService } from "ngx-cookie-service";
import { MessageService } from "primeng/api";
import { Observable } from "rxjs";

@Injectable()
export class FileService extends ServiceProxyBase {
    private fileEndpoint = `/api/file`;
    constructor(
        messageService: MessageService,
        _cookieService: CookieService,
        @Inject(HttpClient) http: HttpClient,
        @Optional() @Inject(API_BASE_URL) baseUrl?: string
    ) {
        super(messageService, _cookieService, http, baseUrl);
    }

    uploadFileGetUrl(file: File, folder = ''): Observable<IResponse<IResponse<any>>> {
        let url_: string = `/api/file/upload?folder=${folder}`;
        return this.requestPostFile(file, url_);
    }

    uploadFile(body: File, folderUpload: string): Observable<IResponseItem<string>> {
		let folderPath = `${AppConsts.folder}/${folderUpload}`;
        let url_: string = `${this.fileEndpoint}/upload?folder=${folderPath}`;
        return this.requestPostFile(body, url_);
    }
}