import { AppConsts } from '@shared/AppConsts';
import {
    mergeMap as _observableMergeMap,
    catchError as _observableCatch,
} from "rxjs/operators";
import {
    Observable,
    throwError as _observableThrow,
    of as _observableOf,
} from "rxjs";
import { Injectable, Inject, Optional, Injector } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Page } from "@shared/model/page";
import { MessageService } from 'primeng/api';
import { CookieService } from 'ngx-cookie-service';
import { API_BASE_URL, ServiceProxyBase } from '@shared/service-proxies/service-proxies-base';
import { IResponse } from '@shared/interfaces/base-interface';
import { IBusinessCustomer } from '@shared/interfaces/business-customer.interface';


@Injectable()
export class BusinessCustomerService extends ServiceProxyBase {
    constructor(
        messageService: MessageService,
        _cookieService: CookieService,
        @Inject(HttpClient) http: HttpClient,
        @Optional() @Inject(API_BASE_URL) baseUrl?: string
    ) {
        super(messageService, _cookieService, http, baseUrl);
    }

    /**
     * them doanh nghiep
     * @param body 
     * @returns 
     */
    
    create(body): Observable<IResponse<any>> {
        return this.requestPost<IResponse<IBusinessCustomer>>(body, "/api/business-customer/create");
    }

    /**
     * cap nhat doanh nghiep
     * @param body 
     * @returns 
     */
    update(body): Observable<IResponse<any>> {
        return this.requestPut<IResponse<IBusinessCustomer>>(body, "/api/business-customer/update");
    }

    /**
     * xoa doanh nghiep
     * @param id 
     * @returns 
     */
    delete(id: number): Observable<IResponse<any>> {
        let url_ = `/api/business-customer/delete/${id}`;
        return this.requestDelete<IResponse<any>>(url_);
    }

    /**
     * chi tiet doanh nghiep
     * @param id 
     * @returns 
     */
    get(id: number): Observable<IResponse<any>> {
        let url_ = "/api/business-customer/" + id;
        return this.requestGet<IResponse<IBusinessCustomer>>(url_);
    }

    /**
     * danh sach doanh nghiep
     * @param page 
     * @returns 
     */
    getAll(page: Page): Observable<IResponse<any>> {
        let url_ = "/api/business-customer/find?";
        url_ += this.convertParamUrl("keyword", page.keyword);
        url_ += this.convertParamUrl('pageSize', page.pageSize);
        url_ += this.convertParamUrl('pageNumber', page.getPageNumber());
        return this.requestGet<IResponse<IBusinessCustomer>>(url_);
    }

    getAllNoPaging():Observable<IResponse<any>> {
        let url_ = "/api/business-customer/find?";
        url_ += this.convertParamUrl('pageSize', -1);
        return this.requestGet<IResponse<IBusinessCustomer>>(url_);
    }
}