import { AppConsts, UserConst } from '@shared/AppConsts';
import {
    mergeMap as _observableMergeMap,
    catchError as _observableCatch,
} from "rxjs/operators";
import {
    Observable,
    throwError as _observableThrow,
    of as _observableOf,
} from "rxjs";
import { Injectable, Inject, Optional, Injector } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Page } from "@shared/model/page";
import { MessageService } from 'primeng/api';
import { CookieService } from 'ngx-cookie-service';
import { API_BASE_URL, ServiceProxyBase } from '@shared/service-proxies/service-proxies-base';
import { IResponse } from '@shared/interfaces/base-interface';
import { IBusinessCustomerDropdown, IRestaurant } from '@shared/interfaces/restaurant.interface';
import { IProduct } from '@shared/interfaces/product.interface';
import { IOrder } from '@shared/interfaces/order.interface';
import * as moment from 'moment';
import { IBusinessCustomer } from '@shared/interfaces/business-customer.interface';
import { Calendar } from '@shared/model/calendar.model';

@Injectable()
export class OrderService extends ServiceProxyBase {
    constructor(
        messageService: MessageService,
        _cookieService: CookieService,
        @Inject(HttpClient) http: HttpClient,
        @Optional() @Inject(API_BASE_URL) baseUrl?: string
    ) {
        super(messageService, _cookieService, http, baseUrl);
    }
 
    /**
     * them don hang
     * @param body 
     * @returns 
     */
    create(body): Observable<IResponse<any>> {
        let user: { [key: string]: any } = this.getUser();
        let urlMap: { [key: number]: string } = {
            [UserConst.ADMIN]: "/api/order/add",
            [UserConst.BUSINESS_CUSTOMER]: "/api/order/business-customer/add",
            [UserConst.RESTAURANT]: "/api/order/restaurant/add"
        };
        
        let url_ = urlMap[user.user_type];
        return this.requestPost<IResponse<IOrder>>(body, url_);
    }

    /**
     * cap nhat don hang
     * @param body 
     * @returns 
     */
    update(body): Observable<IResponse<any>> {
        return this.requestPut<IResponse<IOrder>>(body, "/api/order/update");
    }

      /**
     * thanh toan don hang
     * @param body 
     * @returns 
     */
    hasPaid(orderId): Observable<IResponse<any>> {
        return this.requestPut<IResponse<IOrder>>(null, `/api/order/customer/has-paid/${orderId}`);
    }

    /**
     * xoa don hang
     * @param id 
     * @returns 
     */
    delete(id: number): Observable<IResponse<any>> {
        let url_ = `/api/order/delete/${id}`;
        return this.requestDelete<IResponse<any>>(url_);
    }

    /**
     * xem don hang theo id
     * @param id 
     * @returns 
     */
    get(id: number): Observable<IResponse<any>> {
        let user: { [key: string]: any } = this.getUser();
        let urlMap: { [key: number]: string } = {
            [UserConst.ADMIN]: "/api/order/" + id,
            [UserConst.BUSINESS_CUSTOMER]: "/api/order/business-customer/" + id,
            [UserConst.RESTAURANT]: "/api/order/restaurant/" + id,
        };
        
        let url_ = urlMap[user.user_type];
        return this.requestGet<IResponse<IOrder>>(url_);
    }

    /**
     * lay danh sach don hang
     * @param page 
     * @returns 
     */
    getAll(page: Page, filter: any): Observable<IResponse<any>> {
        let user: { [key: string]: any } = this.getUser();
        let urlMap: { [key: number]: string } = {
            [UserConst.ADMIN]: "/api/order/find-all?",
            [UserConst.BUSINESS_CUSTOMER]: "/api/order/business-customer/find-all?",
            [UserConst.RESTAURANT]: "/api/order/restaurant/find-all?"
        };
        
        let url_ = urlMap[user.user_type];
     
        if (filter.status) url_ += this.convertParamUrl("status", filter.status);
        if (filter.businessCustomerId) url_ += this.convertParamUrl("businessCustomerId", filter.businessCustomerId);
        if (filter.dates) {
            url_ += this.convertParamUrl('startDate', moment(filter.dates[0]).format('YYYY-MM-DD'));
            url_ += this.convertParamUrl('endDate', moment(filter.dates[1]).format('YYYY-MM-DD'));
            
        }
        url_ += this.convertParamUrl("keyword", page.keyword);
        url_ += this.convertParamUrl('pageSize', page.pageSize);
        url_ += this.convertParamUrl('pageNumber', page.getPageNumber());
        return this.requestGet<IResponse<IOrder>>(url_);
    }

    //CALL API KHAC

    /**
     * goi danh sach doanh nghiep
     * @returns 
     */
    getAllBusinessCustomer(): Observable<IResponse<any>> {
        //API CHUA XU LY KHONG PHAN TRANG
        let url_ = "/api/business-customer/find?";
        url_ += this.convertParamUrl('pageSize', 999);
        url_ += this.convertParamUrl('pageNumber', 1);
        return this.requestGet<IResponse<IBusinessCustomerDropdown>>(url_);
    }

    /**
     * goi danh sach nha hang
     * @returns 
     */
    getAllRestaurant(id?: number): Observable<IResponse<any>> {
        let url_ = "/api/restaurant/find?";
        url_ += this.convertParamUrl('pageSize', -1);
        if(id) url_ += this.convertParamUrl('BusinessCustomerId', id);
        return this.requestGet<IResponse<IRestaurant>>(url_);
    }

    /**
     * goi danh sach san pham
     * @param page 
     * @returns 
     */
    getAllProduct(businessCustomerId?): Observable<IResponse<any>> {
        let url_ = `/api/product-price/find-by-business-customer?`;
        if(businessCustomerId) url_ += this.convertParamUrl('id', businessCustomerId);
        return this.requestGet<IResponse<IProduct>>(url_);
    }

     /**
     * goi danh sach dia chi nha hang
     * @param page 
     * @returns 
     */
     getAllAddress(id: number): Observable<IResponse<any>> {
        let url_ = `/api/restaurant/delivery-address/find-by-restaurant/${id}`;
        return this.requestGet<IResponse<IRestaurant>>(url_);
    }

    cancel(body){
        return this.requestPut<IResponse<any>>(body, `/api/order/cancel-order`);
    }

    response(body){
        return this.requestPut<IResponse<any>>(body, `/api/order/response-order`);
    }

    confirmOrder(body){
        return this.requestPut<IResponse<any>>(body, `/api/order/confirm-order`);
    }

    processing(id: number){
        let url_ = `/api/order/processing-order/${id}`;
        return this.requestPut<IResponse<any>>(null, url_);
    }

    /**
     * Info account
     * @returns 
     */
    getInfoAccount(): Observable<IResponse<any>> {
        let user: { [key: string]: any } = this.getUser();
        let urlMap: { [key: number]: string } = {
            [UserConst.BUSINESS_CUSTOMER]: "/api/business-customer/info?",
            [UserConst.RESTAURANT]: "/api/restaurant/info?"
        };
        
        let url_ = urlMap[user.user_type];
        return this.requestGet<IResponse<IBusinessCustomer>>(url_);
    }

    getCalendar(year: number): Observable<any> {
        let url_ = `/api/dayoff/find-all?`;
        url_ += this.convertParamUrl('year', year);
        return this.requestGet<IResponse<Calendar>>(url_);
    }
}