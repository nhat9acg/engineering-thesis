import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject } from 'rxjs';
import { IResponseLang } from '@shared/interfaces/base-interface';

@Injectable({
  providedIn: 'root'
})

export class SelectedLanguageService {
  
  constructor(
    private translateService: TranslateService,
  ) {}

  private $languageObservable: BehaviorSubject<IResponseLang> = new BehaviorSubject<IResponseLang>(null);

  public get getLanguage() {
    return this.$languageObservable.asObservable();
  }

  public setLanguage(lang: string): any {
    this.translateService.getTranslation(lang).subscribe((keys) => {
      this.$languageObservable.next({lang: lang, keys: keys });
    })
  }

  public getKeyLangs(parentKeys, lang, result = []) {
    // console.log('lang____', lang);
    for(let parentKey of parentKeys) {
      result[parentKey] = lang[parentKey];
    }
    return result;
  }
}
