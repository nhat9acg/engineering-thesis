

import { HttpClient } from "@angular/common/http";
import { Inject, Injectable, Optional } from "@angular/core";
import { IResponse } from "@shared/interfaces/base-interface";
import { IProductPrice } from "@shared/interfaces/product-price.interface";
import { Page } from "@shared/model/page";
import { API_BASE_URL, ServiceProxyBase } from "@shared/service-proxies/service-proxies-base";
import { CookieService } from "ngx-cookie-service";
import { MessageService } from "primeng/api";
import { Observable } from "rxjs";

@Injectable()
export class UserAddressService extends ServiceProxyBase { 
    private endPoint = '/api/user/delivery-address'

    constructor(
        messageService: MessageService, 
        _cookieService: CookieService, 
        @Inject(HttpClient) http: HttpClient, 
        @Optional() @Inject(API_BASE_URL) baseUrl?: string
        ) {
        super(messageService, _cookieService, http, baseUrl);
    }

    getAllAddresses(id: number): Observable<IResponse<any>> {
        let user: { [key: string]: any } = this.getUser();
        let url_ =`${this.endPoint}/find-by-user/${user.user_id}`;
        return this.requestGet<IResponse<any>>(url_);
    }
    
    /**
     * tao dia chi 
     * @param body 
     * @returns 
     */
    createAddress(body): Observable<IResponse<any>> {
        return this.requestPost<IResponse<any>>(body, `${this.endPoint}/create`);
    }
    
    /**
     * cap nhat dia chi 
     * @param body 
     * @returns 
     */
    updateAddress(body): Observable<IResponse<any>> {
        return this.requestPut<IResponse<any>>(body, `${this.endPoint}/update`);
    }
    
    /**cap nhat dia chi mac dinh
     * @param body 
     * @returns 
     */
    updateAddressDefault(body): Observable<IResponse<any>> {
        let url_ = `${this.endPoint}/update-default/${body.id}?`
        url_ += this.convertParamUrl('isDefault', true);
        return this.requestPut<IResponse<any>>(body, url_);
    }
    
    /**
     * xoa dia chi
     * @param id 
     * @returns 
     */
    deleteAddress(id: number): Observable<IResponse<any>> {
        let url_ = `${this.endPoint}/delete/${id}`;
        return this.requestDelete<IResponse<any>>(url_);
    }
}