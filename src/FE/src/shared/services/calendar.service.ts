import { HttpClient } from "@angular/common/http";
import { Inject, Injectable, Optional } from "@angular/core";
import { IResponse } from "@shared/interfaces/base-interface";
import { Calendar } from "@shared/model/calendar.model";
import { Page } from "@shared/model/page";
import { API_BASE_URL, ServiceProxyBase } from "@shared/service-proxies/service-proxies-base";
import { CookieService } from "ngx-cookie-service";
import { MessageService } from "primeng/api";
import { Observable } from "rxjs";


@Injectable()
export class CalendarService extends ServiceProxyBase { 
    private endPoint = '/api/dayoff'

    constructor(
        messageService: MessageService, 
        _cookieService: CookieService, 
        @Inject(HttpClient) http: HttpClient, 
        @Optional() @Inject(API_BASE_URL) baseUrl?: string
        ) {
        super(messageService, _cookieService, http, baseUrl);
    }

    getAll(year: number): Observable<any> {
        let url_ = `${this.endPoint}/find-all?`;
        url_ += this.convertParamUrl('year', year);
        return this.requestGet<IResponse<Calendar>>(url_);
    }

    update(body): Observable<any> {
        return this.requestPut(body, "/api/dayoff/update");
    }

    delete(body): Observable<any> {
        let url_ = "/api/dayoff/delete?";
        url_ += this.convertParamUrl('startDate', body.startDate);
        url_ += this.convertParamUrl('endDate', body.endDate);
        return this.requestDelete<IResponse<any>>(url_);
    }

}