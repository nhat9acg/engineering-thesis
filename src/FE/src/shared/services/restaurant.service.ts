import { AppConsts } from '@shared/AppConsts';
import {
    mergeMap as _observableMergeMap,
    catchError as _observableCatch,
} from "rxjs/operators";
import {
    Observable,
    throwError as _observableThrow,
    of as _observableOf,
} from "rxjs";
import { Injectable, Inject, Optional, Injector } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Page } from "@shared/model/page";
import { MessageService } from 'primeng/api';
import { CookieService } from 'ngx-cookie-service';
import { API_BASE_URL, ServiceProxyBase } from '@shared/service-proxies/service-proxies-base';
import { IResponse } from '@shared/interfaces/base-interface';
import { IBusinessCustomerDropdown, IRestaurant } from '@shared/interfaces/restaurant.interface';


@Injectable()
export class RestaurantService extends ServiceProxyBase {
    constructor(
        messageService: MessageService,
        _cookieService: CookieService,
        @Inject(HttpClient) http: HttpClient,
        @Optional() @Inject(API_BASE_URL) baseUrl?: string
    ) {
        super(messageService, _cookieService, http, baseUrl);
    }

    /**
     * them nha hang
     * @param body 
     * @returns 
     */
    create(body): Observable<IResponse<any>> {
        return this.requestPost<IResponse<IRestaurant>>(body, "/api/restaurant/create");
    }

    /**
     * cap nhat nha hang
     * @param body 
     * @returns 
     */
    update(body): Observable<IResponse<any>> {
        return this.requestPut<IResponse<IRestaurant>>(body, "/api/restaurant/update");
    }

    /**
     * xoa nha hang
     * @param id 
     * @returns 
     */
    delete(id: number): Observable<IResponse<any>> {
        let url_ = `/api/restaurant/delete/${id}`;
        return this.requestDelete<IResponse<any>>(url_);
    }

    /**
     * xem nha hang theo id
     * @param id 
     * @returns 
     */
    get(id: number): Observable<IResponse<any>> {
        let url_ = "/api/restaurant/" + id;
        return this.requestGet<IResponse<IRestaurant>>(url_);
    }

    /**
     * lay danh sach nha hang 
     * @param page 
     * @returns 
     */
    getAll(page: Page): Observable<IResponse<any>> {
        let url_ = "/api/restaurant/find?";
        url_ += this.convertParamUrl("keyword", page.keyword);
        url_ += this.convertParamUrl('pageSize', page.pageSize);
        url_ += this.convertParamUrl('pageNumber', page.getPageNumber());
        return this.requestGet<IResponse<IRestaurant>>(url_);
    }

    /**
     * danh sach nha hang khong phan trang
     * @param  
     * @returns 
     */
    getAllNoPaging(): Observable<IResponse<any>> {
        let url_ = "/api/restaurant/find?";
        url_ += this.convertParamUrl('pageSize', -1);
        return this.requestGet<IResponse<IRestaurant>>(url_);
    }

    /**
     * danh sach dia chi cua nha hang
     * @param restaurantId 
     * @returns 
     */
    getAllAddresses(restaurantId: number): Observable<IResponse<any>> {
        let url_ =`/api/restaurant/delivery-address/find-by-restaurant/${restaurantId}`;
        return this.requestGet<IResponse<any>>(url_);
    }

    /**
     * tao dia chi cho nha hang
     * @param body 
     * @returns 
     */
    createAddress(body): Observable<IResponse<any>> {
        return this.requestPost<IResponse<any>>(body, "/api/restaurant/delivery-address/create");
    }

    /**
     * cap nhat dia chi nha hang
     * @param body 
     * @returns 
     */
    updateAddress(body): Observable<IResponse<any>> {
        return this.requestPut<IResponse<any>>(body, "/api/restaurant/delivery-address/update");
    }

    /**cap nhat dia chi mac dinh
     * @param body 
     * @returns 
     */
    updateAddressDefault(body): Observable<IResponse<any>> {
        let url_ = `/api/restaurant/delivery-address/update-default/${body.id}?`
        url_ += this.convertParamUrl('isDefault', true);
        return this.requestPut<IResponse<any>>(body, url_);
    }

    /**
     * xoa dia chi nha hang
     * @param id 
     * @returns 
     */
    deleteAddress(id: number): Observable<IResponse<any>> {
        let url_ = `/api/restaurant/delivery-address/delete/${id}`;
        return this.requestDelete<IResponse<any>>(url_);
    }

    //CALL API KHAC

    /**
     * goi danh sach doanh nghiep
     * @returns 
     */
    getAllBusinessCustomer(): Observable<IResponse<any>> {
        //API CHUA XU LY KHONG PHAN TRANG
        let url_ = "/api/business-customer/find?";
        url_ += this.convertParamUrl('pageSize', 999);
        url_ += this.convertParamUrl('pageNumber', 1);
        return this.requestGet<IResponse<IBusinessCustomerDropdown>>(url_);
    }
}