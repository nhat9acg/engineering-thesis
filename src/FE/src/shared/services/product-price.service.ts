import { HttpClient } from "@angular/common/http";
import { Inject, Injectable, Optional } from "@angular/core";
import { IResponse } from "@shared/interfaces/base-interface";
import { IProductPrice } from "@shared/interfaces/product-price.interface";
import { Page } from "@shared/model/page";
import { API_BASE_URL, ServiceProxyBase } from "@shared/service-proxies/service-proxies-base";
import { CookieService } from "ngx-cookie-service";
import { MessageService } from "primeng/api";
import { Observable } from "rxjs";

@Injectable()
export class ProductPriceService extends ServiceProxyBase { 
    private endPoint = '/api/product-price'

    constructor(
        messageService: MessageService, 
        _cookieService: CookieService, 
        @Inject(HttpClient) http: HttpClient, 
        @Optional() @Inject(API_BASE_URL) baseUrl?: string
        ) {
        super(messageService, _cookieService, http, baseUrl);
    }

    /**
     * danh sach product
     * @param page 
     * @returns 
     */
    getAll(page: Page, fieldFilters: any): Observable<IResponse<any>> {
        let url_ = `${this.endPoint}/find-all?`;
        if(page.keyword) url_ += this.convertParamUrl("keyword", page.keyword);

        if(fieldFilters) {
            for(const [key, value] of Object.entries(fieldFilters)) {
                if(key == 'searchField') {
                    if(page.keyword) url_ += this.convertParamUrl(fieldFilters.searchField, page.keyword);
                } else {
                    if(value) url_ += this.convertParamUrl(key, `${value}`);
                } 
            }
        }

        url_ += this.convertParamUrl('pageSize', page.pageSize);
        url_ += this.convertParamUrl('pageNumber', page.getPageNumber());
        //
        return this.requestGet<IResponse<any>>(url_);
    }

    /**
     * them product category
     * @param body 
     * @returns 
     */
    create(body): Observable<IResponse<any>> {
        return this.requestPost<IResponse<any>>(body, `${this.endPoint}/add?`);
    }

    /**
     * update product 
     * @param body 
     * @returns 
     */
    update(body: IProductPrice[]): Observable<IResponse<any>> {
        return this.requestPut<IResponse<any>>(body, `${this.endPoint}/update?`);
    }
    

    /**
     * delete product category
     * @param body 
     * @returns 
     */
    delete(ids): Observable<IResponse<any>> {
        let url_ = `${this.endPoint}/delete?`;
        ids?.forEach(id => {
            url_ += this.convertParamUrl('ids', id);
        });
        return this.requestDelete<IResponse<any>>(url_);
    }
}