import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DateConversionService {
  convertDate(dateString: string): string {
    const date = new Date(dateString);
    date.setDate(date.getDate() + 1);
    date.setUTCHours(0, 0, 0, 0);
    const convertedDate = date.toISOString();
    return convertedDate;
  }
}