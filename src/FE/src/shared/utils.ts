import * as moment from "moment";
import { AppConsts, UserTypes } from "./AppConsts";
import { BaseConsts, ETypeUrlYoutube, UserType } from "./consts/base.consts";

export class Utils {

    public static getSessionStorage(key: string) {
        return JSON.parse(sessionStorage.getItem(key))
    }
    
    public static setSessionStorage(key: string, data: any, ) {
        sessionStorage.setItem(key, JSON.stringify(data));
    }
    
    public static removeSessionStorage(key: string) {
        sessionStorage.removeItem(key);
    }
    
    public static clearSessionStorage() {
        sessionStorage.clear();
    }
    
    // LOCAL STORAGE
    public static getLocalStorage(key: string) {
        return JSON.parse(localStorage.getItem(key))
    }
    
    public static setLocalStorage(key: string,data: any, ) {
        localStorage.setItem(key, JSON.stringify(data));
    }
    
    public static removeLocalStorage(key: string) {
        localStorage.removeItem(key);
    }
    
    public static clearLocalStorage() {
        localStorage.clear();
    }
    
    public static formatDateMonth(value) {
        return (moment(value).isValid() && value) ? moment(value).format('DD/MM') : '';
    }
    
    public static formatDate(value) {
        return (moment(value).isValid() && value) ? moment(value).format('DD/MM/YYYY') : '';
    }
    
    public static convertLowerCase(string: string = '') {
        if (string.length > 0) {
            return string.charAt(0).toLocaleLowerCase() + string.slice(1);
        }
        return '';
    }

    public static getUser() {
        return this.getLocalStorage(AppConsts.localStorageUser);
    }

    /**
     * đảo từ 1-12-2021 -> 2021-12-1
     * @param date
     * @returns
     */
    public static reverseDateString(date: string) {
        return date.split(/[-,/]/).reverse().join("-");
    }

    /**
     * tạo một thẻ a download file
     * @param fileName tên file
     * @param href đường dẫn
     */

    public static makeDownload(fileName: string, href: string) {
        let a = document.createElement("a");
        a.href = href;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();
        a.remove();
    }

    public static isPdfFile(file) {
        var parts = file.split('.');
        var typeFile = parts[parts.length - 1];
        switch (typeFile.toLowerCase()) {
            case 'pdf':
                return true;
        }
        return false;
    }

    public static replaceAll(str, find, replace) {
        var escapedFind=find.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
        return str.replace(new RegExp(escapedFind, 'g'), replace);
    }

    public static transformMoney(num: number, ...args: any[]): string {
        const value = `${num}`;
		if (value === '' || value === null || typeof value === 'undefined') {
			return '';
		}

		let locales = 'vi-VN';
		const cur = Number(value);

		if (args.length > 0) {
			locales = args[0];	
		}
        const result = new Intl.NumberFormat(locales).format(cur);
		return result === 'NaN' ? '' : result;
    }
    
    public static transformPercent(num: number, ...args: any[]): string {
        const value = `${num}`;
		if (value === '' || value === null || typeof value === 'undefined') {
			return '';
		}

		let locales = 'vi-VN';
		const cur = Number(value);

		if (args.length > 0) {
			locales = args[0];	
		}
        const result = new Intl.NumberFormat(locales).format(cur);
		return result === 'NaN' ? '' : this.replaceAll(result,'.', ',');
    }

    // BLOCK REQUEST API AFTER 3s
    public static doubleClickRequest(url): boolean {
        setTimeout(() => {
            Utils.removeLocalStorage(url); 
        }, 3000);
        //
        const beforeRequestTime = Utils.getLocalStorage(url);
		if(((new Date().getTime() - +beforeRequestTime) < 1500) && beforeRequestTime) {
			return true;
		} 
		Utils.setLocalStorage(url, new Date().getTime());
        //
        return false;
    }

    public static log(titleError:string, error?: any) {
        console.log(`%c ${titleError} `, 'background:black; color: red', error);
    }

    public static isExtensionImage(path: string) { 
        const extension = path.split('.').pop();
        return BaseConsts.imageExtensions.includes(extension);
    } 

    public static isExtensionVideo(path: string) { 
        const extension = path.split('.').pop();
        return BaseConsts.videoExtensions.includes(extension);
    } 

    public static checkLinkYoutube(link: string) {
        let isCheck: boolean = false;
        const urlYoutubes = {
            ...ETypeUrlYoutube,
            ORIGIN: "https://www.youtube.com"
        }
        //
        try {
            if(typeof link === 'string') {
                for(const [key, url] of Object.entries(urlYoutubes)) {
                    isCheck = link?.includes(url);
                    if(isCheck) break;
                }
            }
            //
            return isCheck;
        } catch (error) {
            this.log('checkLinkYoutube', error);
            return false;
        }
    }
}