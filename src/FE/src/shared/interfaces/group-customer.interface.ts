export interface IGroupCustomer {
    id?: number
    name?: string,
    businessCustomerIds: number[];
}