import { IBusinessCustomer } from "./business-customer.interface";

export interface IRestaurant {
    id?: number
    businessCustomerId: number,
    name: string,
    email: string;
    phone: string;
    address: string;
    status: number;
    businessCustomer?: IBusinessCustomer;
}

export interface IBusinessCustomerDropdown {
    id?: number
    labelName: string,
    address?: string
}

export interface IAddress {
    id?: number
    userId?: number,
    restaurantId?: number,
    address: string,
    receiver: string,
    isDefault?: boolean,
    phone: string,
    labelName?: string,
}