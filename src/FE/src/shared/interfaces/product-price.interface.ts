export interface IProductPrice {
    id: number | undefined,
    businessCustomerId: number;
    productId: number;
    price: number;
    unitPrice?: number;
    isDeleted?: boolean;
    image?: string;
    labelName?: string;
    product?: any
    checked: boolean;
}