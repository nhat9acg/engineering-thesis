export interface IUser {
    id: number | undefined;
    username: string;
    password: string;
    repeatPassword: string;
    fullName: string
    userType: number;
    roleIds: number[];
    businessCustomerId?: number;
    restaurantId?: number;
    roleType?: number;
    email?: string;
    phone?: string;
}

export interface IFilterCustomer {
    businessCustomerId?: number;
    restaurantId?: number;
}