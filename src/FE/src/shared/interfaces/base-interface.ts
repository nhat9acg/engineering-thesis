export interface IPageInfo {
    page?: number,
    rows?: number
}

export interface IBaseAction {
    data: any,
    label: string,
    icon: string,
    command: Function,
}

export interface IBaseListAction extends Array<IBaseAction[]> {}

// export interface

export interface IResponse<T> {
    code: number,
    data: T,
    message: string,
    status: number,
}

export interface IResponseLang {
    lang: string, 
    keys: string[],
}

export interface IDropdown {
    id?: number,
    labelName: string,
    code?: any
}
