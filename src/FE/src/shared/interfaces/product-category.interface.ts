export interface IProductCategory {
    id: number | undefined;
    name: string;
    code: string;
    avatar?: string;
    parentId?: number
    status?: number;
    checked?: boolean;
}