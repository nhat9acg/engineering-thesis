export interface IOrder {
    id?: number;
    restaurantId?: number;
    businessCustomerId?: number;
	userId?: number;
	fullName?: string
    description?: string;
    deliveryAddressId: number;
	invoiceAddress: string
    orderDetails?: IOrderDetail[];
    status?: number;
    totalMoney?: number;
	historyUpdates?: IHistoryOrder[];
	estimatedShippingDate?: string,
	shippingProviderName?: string,
	trackingCode?: string,
	restaurantName?: string,
	businessCustomerName?: string,
	orderDate?: Date,
	deliveryAddress?: string,
	hasPaid?: boolean,
}

export interface IHistoryOrder {
	createdDate?: Date,
	action?: number,
	actionName?: string,
	note?: string,
	oldValue?: string,
	newValue?: string,
	createdUserBy?: ICreatedUserBy,
}

export interface IHistoryView {
	actionName?: string,
	note?: string,
	date?: string ,
	createdUserBy?: ICreatedUserBy,
}
export interface ICreatedUserBy {
	username?: string,
}
  
export interface IOrderDetail {
	id?: number;
	orderId?: number;
	cartId?: number;
	productId?: number;
	quantity: number;
	description?: string;
	unitPrice?: number;
	image?: string;
	unit?: number;
	category?: string;
	productCategoryName?: string
	productName?: string,
	calculationUnitName?: string,
	calculationUnit?: string,
	currentValue?: number,
	price?: number,
	name?: string
}

export interface ILabelProduct {
	image?: string;
	labelName?: string;
}

export interface IOrderProduct {
	id: number;
	unitPrice: number;
	quantity: number;
}

export interface IShippingInfo {
	estimatedShippingDate?: string,
	shippingProviderName?: string,
	trackingCode?: string
}
