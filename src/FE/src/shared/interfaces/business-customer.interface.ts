export interface IBusinessCustomer {
    id?: number
    fullName: string,
    address: string,
    taxCode: string;
    phone: string;
    email: string;
    language: string;
    isRestaurant?: boolean;
}

export interface IColumn {
    field: string,
    header: string,
    class?: string,
    width: string;
    type?: string;
    isPin?: boolean;
    isResize?: boolean;
}

