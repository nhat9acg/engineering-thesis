export interface IProduct {
    id: number | undefined;
    name: string;
    code: string;
    productCategoryId?: number;
    image?: string;
    status?: number;
    defaultPrice: number;
    allowOrder: boolean;
    calculationUnitId: number;
    description?: string;
}