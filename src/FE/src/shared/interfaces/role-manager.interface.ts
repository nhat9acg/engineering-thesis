export interface IKeyPermission {
    // type: number,
    // name: string,
    key: string,
    label: string,
    parentKey?: string
    icon?: string,
}

export interface INodePermission {
    children?: INodePermission[],
    icon?: string,
    key: string,
    label: string,
    // name: string,
    parent?: INodePermission,
    parentKey?: string,
    styleClass?: string
    // type: number,
}

export interface IRole {
    id: number,
    name: string,
    userType: number,
    description?: string,
    permissionKeys?: string[],
    permissionKeysRemove?: string[],
}