import { HttpClient } from "@angular/common/http";
import { Inject, Injectable, Optional } from "@angular/core";
import { CookieService } from "ngx-cookie-service";
import { MessageService } from "primeng/api";
import { Observable } from "rxjs";
import { API_BASE_URL, ServiceProxyBase } from "./service-proxies-base";
import * as moment from "moment";
import { IResponse } from "@shared/interfaces/base-interface";
import { IBusinessCustomer } from "@shared/interfaces/business-customer.interface";
import { IRestaurant } from "@shared/interfaces/restaurant.interface";

@Injectable()
export class DashBoardServiceProxy extends ServiceProxyBase {
    // private secondaryEndPoint = `/api/bond/secondary`;
    constructor(messageService: MessageService, _cookieService: CookieService, @Inject(HttpClient) http: HttpClient, @Optional() @Inject(API_BASE_URL) baseUrl?: string) {
        super(messageService, _cookieService, http, baseUrl);
    }

    /**
     * TẠO PHÁT HÀNH THỨ CẤP
     * @param body
     * @returns
     */
    getInfoDashBoard(dataFilter?: any): Observable<any> {
        console.log('!!! dataFilter ', dataFilter);
        
        let url_ = "/api/dashboard/overview?";
        if (dataFilter) {
            if (dataFilter?.firstDate) url_ += this.convertParamUrl('startDate', moment(dataFilter.firstDate).format('YYYY-MM-DD'));
            if (dataFilter?.endDate) url_ += this.convertParamUrl('endDate', moment(dataFilter.endDate).format('YYYY-MM-DD'));
            if (dataFilter?.businessCustomerId) url_ += this.convertParamUrl('businessCustomerId', dataFilter?.businessCustomerId);
            if (dataFilter?.restaurantId) url_ += this.convertParamUrl('restaurantId', dataFilter?.restaurantId);
        }
        return this.requestGet(url_);
    }

    getAllBusinessCustomer(): Observable<IResponse<any>> {
        let url_ = "/api/business-customer/find?";
        url_ += this.convertParamUrl('pageSize', -1);
        return this.requestGet<IResponse<IBusinessCustomer>>(url_);
    }

    getAllRestaurant(businessCustomerId?: number): Observable<IResponse<any>> {
        let url_ = "/api/restaurant/find?";
        url_ += this.convertParamUrl('pageSize', -1);
        if(businessCustomerId) url_ += this.convertParamUrl('businessCustomerId', businessCustomerId);
        return this.requestGet<IResponse<IRestaurant>>(url_);
    }
}