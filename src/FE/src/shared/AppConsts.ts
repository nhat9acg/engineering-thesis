import { OBJECT_IMG_LOCALIZATIONS } from "./base-object";
import { ConfigLanguage } from "./consts/config-language";
import { SelectedLanguageService } from "./services/selected-language.service";

export class AppConsts {
    static remoteServiceBaseUrl: string;
    static appBaseCustomerUrl: string;
    static appBaseUrl: string;
    static appBaseHref: string; // returns angular's base-href parameter value if used during the publish
    static redicrectHrefOpenDocs = "https://docs.google.com/viewerng/viewer?url=";
    static baseUrlHome: string;
    static dictionary = {
        en: null,
        jp: null,
    };
    static localization = {
        code: '',
        name:'',
        img:'',
    }
    static readonly grantType = {
        password: 'password',
        refreshToken: 'refresh_token',
    };

    static localizations = [
        {
            name: "English",
            code: "en",
            img: OBJECT_IMG_LOCALIZATIONS.DEFAULT_IMAGE.EN,
        },
        {
            name: "Japan",
            code: "jp",
            img: OBJECT_IMG_LOCALIZATIONS.DEFAULT_IMAGE.JP,
        },
    ];

    static localStorageLangKey = 'language';
    static defaultLang = ConfigLanguage.ENGLISH;

    static readonly messageError = "Có lỗi xảy ra. Vui lòng thử lại sau ít phút!";

    static clientId: string;
    static scope: string;
    static clientSecret: string;

    static keyCrypt = 'idCrypt';

    static localeMappings: any = [];

    static readonly userManagement = {
        defaultAdminUserName: 'admin'
    };

    static readonly authorization = {
        accessToken: 'access_token',
        refreshToken: 'refresh_token',
        encryptedAuthTokenName: 'enc_auth_token'
    };

    static readonly localRefreshAction = {
        setToken: 'setToken',
        doNothing: 'doNothing',
    }
    static ApproveConst: any;

    static topHeightTabViewConst = 90 + 80 + 80;
    static delayTimeRedirect = 500;
    
    static readonly folder = 'core';
    static readonly endPointApiUpload = 'api/file/get?folder';
    static readonly localStorageUser = 'userInfo';
    
    static defaultAvatar = "assets/layout/images/topbar/anonymous-avatar.jpg";
}

export class localStorageKeyConst {
    public static topbarHeight = 'topbarHeight';
    public static breadcrumHeight = 'breadcrumHeight';
    public static layoutTopHeight = 'layoutTopHeight';
}

export class UserTypes {
    //
    public static list = [
       
    ];

    public static getUserTypeInfo(code, property) {
        let type = this.list.find(t => t.code == code);
        if (type) return type[property];
        return '';
    }

}

export class AppPermissionNames {
    // user
    public readonly Pages_Users = "Pages.Users";
    public readonly Actions_Users_Create = "Actions.Users.Create";
    public readonly Actions_Users_Update = "Actions.Users.Update";
    public readonly Actions_Users_Delete = "Actions.Users.Delete";
    public readonly Actions_Users_Activation = "Actions.Users.Activation";

    // role
    public readonly Pages_Roles = "Pages.Roles";
    public readonly Actions_Roles_Create = "Actions.Roles.Create";
    public readonly Actions_Roles_Update = "Actions.Roles.Update";
    public readonly Actions_Roles_Delete = "Actions.Roles.Delete";
}

export class PermissionConst {
    private static readonly Web: string = "web_";
    private static readonly Menu: string = "menu_";
    private static readonly Tab: string = "tab_";
    private static readonly Page: string = "page_";
    private static readonly Table: string = "table_";
    private static readonly Form: string = "form_";
    private static readonly ButtonTable: string = "btn_table_";
    private static readonly ButtonForm: string = "btn_form_";
    private static readonly ButtonAction: string = "btn_action_";

    public static readonly Dashboard: string = `${PermissionConst.Page}dashboard`;
    //
    // System Manager
    public static readonly SystemModule: string = "system_";
    //role
    public static readonly RoleMenu: string = `${PermissionConst.SystemModule}${PermissionConst.Menu}role_menu`;
    public static readonly RoleTable: string = `${PermissionConst.SystemModule}${PermissionConst.Table}role_menu_danh_sach`;
    public static readonly RoleCreate: string = `${PermissionConst.SystemModule}${PermissionConst.ButtonAction}role_menu_them_moi`;
    public static readonly RoleUpdate: string = `${PermissionConst.SystemModule}${PermissionConst.ButtonAction}role_menu_cap_nhat`;
    public static readonly RoleDelete: string = `${PermissionConst.SystemModule}${PermissionConst.ButtonAction}role_menu_xoa`;
    //account
    public static readonly UserMenu: string = `${PermissionConst.SystemModule}${PermissionConst.Menu}use_menu`;
    public static readonly UserTable: string = `${PermissionConst.SystemModule}${PermissionConst.Table}use_menu_danh_sach`;
    public static readonly UserCreate: string = `${PermissionConst.SystemModule}${PermissionConst.ButtonAction}use_menu_them_moi`;
    public static readonly UserUpdate: string = `${PermissionConst.SystemModule}${PermissionConst.ButtonAction}use_menu_cap_nhat`;
    public static readonly UserDelete: string = `${PermissionConst.SystemModule}${PermissionConst.ButtonAction}use_menu_xoa`;
    public static readonly UserChangeStatus: string = `${PermissionConst.SystemModule}${PermissionConst.ButtonAction}kich_hoat_or_huy`;
    // calendar
    public static readonly CalendarMenu: string = `${PermissionConst.SystemModule}${PermissionConst.Menu}calendar_menu`;
    public static readonly CalendarTable: string = `${PermissionConst.SystemModule}${PermissionConst.Table}calendar_menu_danh_sach`;
    public static readonly CalendarUpdate: string = `${PermissionConst.SystemModule}${PermissionConst.ButtonAction}calendar_menu_cap_nhat`;
    //
    // Business Customer Manager
    public static readonly BusinessCustomerModule: string = "business_customer_module";
    public static readonly BusinessCustomerMenu: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.Menu}business_customer_menu`;
    public static readonly BusinessCustomerTable: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.Table}business_customer_menu_danh_sach`;
    public static readonly BusinessCustomerCreate: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.ButtonAction}business_customer_menu_them_moi`;
    public static readonly BusinessCustomerUpdate: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.ButtonAction}business_customer_menu_cap_nhat`;
    public static readonly BusinessCustomerDelete: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.ButtonAction}business_customer_menu_xoa`;
    //price
    public static readonly ProductPriceMenu: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.Menu}product_price_menu`;
    public static readonly ProductPriceTable: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.Table}product_price_menu_danh_sach`;
    public static readonly ProductPriceCreate: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.ButtonAction}product_price_menu_them_moi`;
    public static readonly ProductPriceUpdate: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.ButtonAction}product_price_menu_cap_nhat`;
    public static readonly ProductPriceDelete: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.ButtonAction}product_price_menu_xoa`;
    //account BusinessCustomer
    public static readonly BusinessCustomeAccountMenu: string = "business_customer_module";
    public static readonly BusinessCustomeAccountTable: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.Menu}business_customer_menu`;
    public static readonly BusinessCustomeAccountCreate: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.Table}business_customer_menu_danh_sach`;
    public static readonly BusinessCustomeAccountUpdate: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.ButtonAction}business_customer_menu_them_moi`;
    public static readonly BusinessCustomeAccountDelete: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.ButtonAction}business_customer_menu_cap_nhat`;
    public static readonly BusinessCustomeAccountChangeStatus: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.ButtonAction}business_customer_menu_xoa`;

    //
    public static readonly RestaurantMenu: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.Menu}restaurant_menu`;
    public static readonly RestaurantTable: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.Table}restaurant_menu_menu_danh_sach`;
    public static readonly RestaurantCreate: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.ButtonAction}restaurant_menu_them_moi`;
    public static readonly RestaurantUpdate: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.ButtonAction}restaurant_menu_cap_nhat`;
    public static readonly RestaurantUpdateAddress: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.ButtonAction}restaurant_menu_cap_nhat_address`;
    public static readonly RestaurantDelete: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.ButtonAction}restaurant_menu_xoa`;
    //account BusinessCustomer
    public static readonly RestaurantAccountMenu: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.Menu}restaurant_account`;
    public static readonly RestaurantAccountTable: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.Table}restaurant_account_danh_sach`;
    public static readonly RestaurantAccountCreate: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.ButtonAction}restaurant_account_them_moi`;
    public static readonly RestaurantAccountUpdate: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.ButtonAction}restaurant_account_cap_nhat`;
    public static readonly RestaurantAccountDelete: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.ButtonAction}restaurant_account_xoa`;
    public static readonly RestaurantAccountChangeStatus: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.ButtonAction}kich_hoat_or_huy`;
    //RestaurantDeliveryAddressMenu
    public static readonly RestaurantDeliveryAddressMenu: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.Menu}restaurant_delivery_address`;
    public static readonly RestaurantDeliveryAddressTable: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.Table}restaurant_delivery_address_danh_sach`;
    public static readonly RestaurantDeliveryAddressCreate: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.ButtonAction}restaurant_delivery_address_them_moi`;
    public static readonly RestaurantDeliveryAddressUpdate: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.ButtonAction}restaurant_delivery_address_cap_nhat`;
    public static readonly RestaurantDeliveryAddressDelete: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.ButtonAction}restaurant_delivery_address_xoa`;
    // Group Customer
    public static readonly GroupCustomerMenu: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.Menu}group_customer_menu`;
    public static readonly GroupCustomerTable: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.Table}group_customer_menu_danh_sach`;
    public static readonly GroupCustomerCreate: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.ButtonAction}group_customer_menu_them_moi`;
    public static readonly GroupCustomerUpdate: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.ButtonAction}group_customer_menu_cap_nhat`;
    public static readonly GroupCustomerDelete: string = `${PermissionConst.BusinessCustomerModule}${PermissionConst.ButtonAction}group_customer_menu_xoa`;
    //
    // Product Management
    public static readonly ProductManagementModule: string = "product_management_module";
    //
    public static readonly ProductCategoryMenu: string = `${PermissionConst.ProductManagementModule}${PermissionConst.Menu}product_category_menu`;
    public static readonly ProductCategoryTable: string = `${PermissionConst.ProductManagementModule}${PermissionConst.Table}product_category_menu_danh_sach`;
    public static readonly ProductCategoryCreate: string = `${PermissionConst.ProductManagementModule}${PermissionConst.ButtonAction}product_category_menu_them_moi`;
    public static readonly ProductCategoryUpdate: string = `${PermissionConst.ProductManagementModule}${PermissionConst.ButtonAction}product_category_menu_cap_nhat`;
    public static readonly ProductCategoryChangeStatus: string = `${PermissionConst.ProductManagementModule}${PermissionConst.ButtonAction}product_category_menu_change_status`;
    public static readonly ProductCategoryDelete: string = `${PermissionConst.ProductManagementModule}${PermissionConst.ButtonAction}product_category_menu_xoa`;
    //
    public static readonly ProductMenu: string = `${PermissionConst.ProductManagementModule}${PermissionConst.Menu}product_menu`;
    public static readonly ProductTable: string = `${PermissionConst.ProductManagementModule}${PermissionConst.Table}product_menu_danh_sach`;
    public static readonly ProductCreate: string = `${PermissionConst.ProductManagementModule}${PermissionConst.ButtonAction}product_menu_them_moi`;
    public static readonly ProductUpdate: string = `${PermissionConst.ProductManagementModule}${PermissionConst.ButtonAction}product_menu_cap_nhat`;
    public static readonly ProductChangeStatus: string = `${PermissionConst.ProductManagementModule}${PermissionConst.ButtonAction}product_menu_change_status`;
    public static readonly ProductDelete: string = `${PermissionConst.ProductManagementModule}${PermissionConst.ButtonAction}product_menu_xoa`;
    // CalculationUnit
    public static readonly CalculationUnitMenu: string = `${PermissionConst.ProductManagementModule}${PermissionConst.Menu}calculation_unit_menu`;
    public static readonly CalculationUnitTable: string = `${PermissionConst.ProductManagementModule}${PermissionConst.Table}calculation_unit_menu_danh_sach`;
    public static readonly CalculationUnitCreate: string = `${PermissionConst.ProductManagementModule}${PermissionConst.ButtonAction}calculation_unit_menu_them_moi`;
    public static readonly CalculationUnitUpdate: string = `${PermissionConst.ProductManagementModule}${PermissionConst.ButtonAction}calculation_unit_menu_cap_nhat`;
    public static readonly CalculationUnitDelete: string = `${PermissionConst.ProductManagementModule}${PermissionConst.ButtonAction}calculation_unit_menu_xoa`;
    //
    // Order Management
    public static readonly OrderModule: string = "order_";
    //
    public static readonly OrderMenu: string = `${PermissionConst.SystemModule}${PermissionConst.Menu}order_menu`;
    public static readonly OrderTable: string = `${PermissionConst.SystemModule}${PermissionConst.Table}order_menu_danh_sach`;
    public static readonly OrderCreate: string = `${PermissionConst.SystemModule}${PermissionConst.ButtonAction}order_menu_them_moi`;
    public static readonly OrderExportExcel: string = `${PermissionConst.SystemModule}${PermissionConst.ButtonAction}order_menu_export_excel`;
    public static readonly OrderDetail: string = `${PermissionConst.SystemModule}${PermissionConst.ButtonAction}order_menu_detail`;
    public static readonly OrderUpdate: string = `${PermissionConst.SystemModule}${PermissionConst.ButtonAction}order_menu_cap_nhat`;
    public static readonly OrderShipping: string = `${PermissionConst.SystemModule}${PermissionConst.ButtonAction}order_menu_shipping`;
    public static readonly OrderProcessing: string = `${PermissionConst.SystemModule}${PermissionConst.ButtonAction}order_menu_processing`;
    public static readonly OrderCancel: string = `${PermissionConst.SystemModule}${PermissionConst.ButtonAction}order_menu_cancel`;
    public static readonly OrderFeedback: string = `${PermissionConst.SystemModule}${PermissionConst.ButtonAction}order_menu_feedback`;
    public static readonly OrderDelete: string = `${PermissionConst.SystemModule}${PermissionConst.ButtonAction}order_menu_xoa`;
}

export class DataTableConst {
    public static message = {
        emptyMessage: 'Không có dữ liệu',
        totalMessage: 'bản ghi',
        selectedMessage: 'chọn'
    }
}


export class ActiveDeactiveNumConst {
    public static ACTIVE = 1;
    public static DEACTIVE = 2;

    public static list = [
        {
            name: {
                en: 'Active',
                vi: 'Hoạt động',
                jp: '初期化'
            },
            code: this.ACTIVE,
            severity: 'success'
        },
        {
            name: {
                en: 'Deactivate',
                vi: 'Đóng',
                jp: '配達中'
            },
            code: this.DEACTIVE,
            severity: 'secondary'
        }
    ];


   
    public static getNameStatus(code, lang = 'en') {
        let status = this.list.find(status => status.code === code);
        const name = status ? status.name[lang] : '';
        const severity = status ? status.severity : '';
        return { name, severity };
    }

    public static getInfo(code, atribution = 'name') {
        let status = this.list.find(type => type.code == code);
        return status ? status[atribution] : null;
    }
   
}

export class StatusResponseConst {
    public static list = [
        {
            value: false,
            status: 0,
        },
        {
            value: true,
            status: 1,
        },
    ]

    public static RESPONSE_TRUE = 1;
    public static RESPONSE_FALSE = 0;

}

export class InvestorAccountConst {
    public static statusName = {
        A: { name: "Kích hoạt", color: "success" },
        D: { name: "Chưa kích hoạt", color: "warning" }
    }
}

export class YesNoConst {
    public static list = [
        {
            name: 'Có',
            code: 'Y',
        },
        {
            name: 'Không',
            code: 'N',
        },
    ]

    public static getName(code) {
        for (let item of this.list) {
            if (item.code == code) return item.name;
        }
        return '-';
    }

    public static YES = 'Y';
    public static NO = 'N';
}
//
export class TabViewConst {
    //
    public static FIRST = 0;
    public static SECOND = 1;
    public static THIRD = 2;
    public static FOURTH = 3;
    public static FIFTH = 4;
    public static SIXTH = 5;
    public static SEVENTH = 6;
    public static EIGHTH = 7;
    public static NINTH = 8;
    public static TENTH = 9;

    public static contentHeightInit = 42;
}

//
export class TypeFormatDatesConst {
    public static DMY = 'DD/MM/YYYY';
    public static DMYHm = 'DD/MM/YYYY HH:mm';
    public static DMYHms = 'DD/MM/YYYY HH:mm:ss';
    public static YMD = 'DD/MM/YYYY';
    public static YMDHm = 'DD/MM/YYYY HH:mm';
    public static YMDHms = 'DD/MM/YYYY HH:mm:ss';
    public static YMDHmsDB = 'YYYY-MM-DDTHH:mm:ss';
}

export class SearchConst {
    public static DEBOUNCE_TIME = 800;
}

export class MessageErrorConst {

    public static message = {
        Error: "ACTIVE",
        Validate: "Vui lòng nhập đủ thông tin cho các trường có dấu (*)",
        DataNotFound: "Không tìm thấy dữ liệu!"
    }
    
}

export class ExportExcelConst{
    public static TONG_HOP_TIEN_VE_SAN_PHAM= "synthetic-money-product";
}
export class FormNotificationConst {
    public static IMAGE_APPROVE = "IMAGE_APPROVE";
    public static IMAGE_CLOSE = "IMAGE_CLOSE";
}

export class PermissionTypes {
    // user
    public static readonly Web = 1;
    public static readonly Menu = 2;
    public static readonly Page = 3;
    public static readonly Table = 4;
    public static readonly Tab = 5;
    public static readonly Form = 6;
    public static readonly ButtonAction = 7;
    public static readonly ButtonTable = 8;
    public static readonly ButtonForm = 9;
}

export class UserConst {
    public static ACTIVE = 1;
    public static DEACTIVE = 2;
    public static listStatusActive = [
        {
            name: 'Active',
            code: this.ACTIVE,
            severity: 'success',
        },
        {
            name: 'Deactive',
            code: this.DEACTIVE,
            severity: 'danger',
        },
    ];

    public static getStatusActive(code: number, atribution = 'name') {
        let status = this.listStatusActive.find(status => status.code === code);
        return status ? status[atribution] : '';
    }

    public static ADMIN = 1;
    public static BUSINESS_CUSTOMER = 2;
    public static RESTAURANT = 3;
    public static CUSTOMER = 4;

    public static adminBusiness = [this.ADMIN,this.BUSINESS_CUSTOMER];
     public static businessRestaurant = [this.RESTAURANT,this.BUSINESS_CUSTOMER];

    public static types = [
        {
            name: 'Admin',
            code: this.ADMIN
        },
        {
            name: 'Business Customer',
            code: this.BUSINESS_CUSTOMER
        },
        {
            name: 'Restaurant',
            code: this.RESTAURANT
        },
        {
            name: 'Customer',
            code: this.CUSTOMER
        }
    ];

    public static getNameType(code: number) {
        let type = this.types.find(status => status.code === code);
        return type ? type.name : '';
    }
}

export class ProductCategoryConst {

}

interface FieldSearchItem {
    name: {
        en: string,
        vi: string,
        jp: string
    },
    code: number,
    field: string,
    placeholder: {
        en: string,
        vi: string,
        jp: string
    }
}
export class ProductConst {
    public static fieldSearch = [
        {
            name: {
                en: 'Product Code',
                vi: 'Mã sản phẩm',
                jp: '製品コード'
            },
            code: 1,
            field: 'code',
            placeholder: {
                en: 'Enter product code...',
                vi: 'Nhập mã sản phẩm...',
                jp: 'プロダクトコードを入力してください...'
            },
        },
        {
            name: {
                en: "Product's name",
                vi: 'Tên sản phẩm',
                jp: '製品名'
            },
            code: 2,
            field: 'name',
            placeholder: {
                en: 'Enter product name...',
                vi: 'Nhập tên sản phẩm...',
                jp: '製品名を入力してください...'
            },
        },
    ];

    public static getFieldSearchInfo(field: string, lang: string = 'en'): { name: string, value: string, placeholder: string } {
        const search = this.fieldSearch.find(fields => fields.field === field) as FieldSearchItem;
        const { name = '', placeholder = '', field: value = '' } = search || {};
        return { name: name[lang] || '', value, placeholder: placeholder[lang] || '' };
    }
}

export class OrderConst {

    public static NEW = 1;
    public static PROCESSING = 2;
    public static SUCCESS = 3;
    public static CANCEL = 4;

    public static listStatus = [
        {
            value: this.NEW,
            name: 'ORDER.STATUS_NEW',
            severity: 'help',
        },
        {
            value: this.PROCESSING,
            name: 'ORDER.STATUS_PROCESSING',
            severity: 'warning',
        },
        {
            value: this.SUCCESS,
            name: 'ORDER.STATUS_SUCCESS',
            severity: 'success',
        },
        {
            value: this.CANCEL,
            name: 'ORDER.STATUS_CANCEL',
            severity: 'danger',
        },
    ];

    public static getAttribution(value: number) {
        let status = this.listStatus.find(status => status.value === value);
        const name = status ? status.name : '';
        const severity = status ? status.severity : '';
        return { name, severity };
    }

    public static getNameStatus(value: number) {
        let status = this.listStatus.find(status => status.value === value);
        const name = status ? status.name: '';
        return { name };
    }
}

export class OrderCustomerConst {

    public static NEW = 1;
    public static PROCESSING = 2;
    public static SUCCESS = 3;
    public static CANCEL = 4;

    public static listStatus = [
        {
            name: {
                en: 'New',
                vi: 'Vận chuyển',
                jp: '新しい'
            },
            code: this.NEW,
            severity: 'help',
        },
        {
            name: {
                en: 'Processing',
                vi: 'Đang giao',
                jp: '処理'
            },
            code: this.PROCESSING,
            severity: 'warning',
        },
        {
            name: {
                en: 'Success',
                vi: 'Hoàn thành',
                jp: '成功'
            },
            code: this.SUCCESS,
            severity: 'success',
        },
        {
            name: {
                en: 'Cancel',
                vi: 'Đã hủy',
                jp: 'キャンセル'
            },
            code: this.CANCEL,
            severity: 'danger',
        },

       
    ];

    public static getNameStatus(code: number, lang = 'en') {
        let status = this.listStatus.find(status => status.code === code);
        const name = status ? status.name[lang] : '';
        const severity = status ? status.severity : '';
        return { name, severity };
    }
}


export class RestaurantConst {

    public static ACTIVE = 1;
    public static DEACTIVATE = 2;
    public static listStatus = [
        {
            name: {
                en: 'Active',
                vi: 'Hoạt động',
                jp: '初期化'
            },
            code: this.ACTIVE,
            severity: 'success',
        },
        {
            name: {
                en: 'Deactivate',
                vi: 'Đóng cửa',
                jp: '配達中'
            },
            code: this.DEACTIVATE,
            severity: 'danger',
        },   
    ];

    public static getNameStatus(code: number, lang = 'en') {
        let status = this.listStatus.find(status => status.code === code);
        const name = status ? status.name[lang] : '';
        const severity = status ? status.severity : '';
        return { name, severity };
    }
}

export class HistoryConst {
    public static CREATE = 1;
    public static UPDATE = 2;
    public static DELETE = 3;
    public static RESPONSE = 4;

    public static NEW = 1;
    public static PROCESSING = 2;
    public static SUCCESS = 3;
    public static CANCEL = 4;


    public static histories = [
        {
            name: 'HISTORY.NEW_ORDER',
            action: this.CREATE,
            newValue: null,
        },
        {
            name: 'HISTORY.PROCESSED_ORDER',
            action: this.UPDATE,
            newValue: this.PROCESSING.toString().trim(),
        },   
        {
            name: 'HISTORY.SENT_ORDER',
            action: this.CREATE,
            newValue: null,
        },  
        {
            name: 'HISTORY.SUCCESS_ORDER',
            action: this.UPDATE,
            newValue: this.SUCCESS.toString().trim(),
        },  
        {
            name: 'HISTORY.CANCEL_ORDER',
            action: this.UPDATE,
            newValue: this.CANCEL.toString().trim(),
        }, 
        {
            name: 'HISTORY.RESPONSE_ORDER',
            action: this.RESPONSE,
            newValue: null,
        }, 
    ];

    public static ADMIN = 1;
    public static BUSINESS_CUSTOMER = 2;
    public static RESTAURANT = 3;

    public static Roles = [
        {
            value: this.ADMIN,
            name: 'HISTORY.ADMIN_FEEDBACK',
        },
        {
            value: this.BUSINESS_CUSTOMER,
            name: 'HISTORY.BUSINESS_CUSTOMER_FEEDBACK',
        },
        {
            value: this.RESTAURANT,
            name: 'HISTORY.RESTAURANT_FEEDBACK',
        }
    ];

    public static getNameHistory(data: any, action: number, oldValue: any, newValue: any){
        if (action == this.RESPONSE){
            let role = this.Roles.find(item => item.value == data.type);
            return role ? role.name : this.histories[5].name;
        } else {
            let history = this.histories.find(item => item.action == action && item.newValue == newValue);
            return history ? history.name : '';
        }
    }
}

export class DocumentConst {
    public static ALL_CUSTOMER = 1;
    public static SOME_CUSTOMER = 2;
    public static types = [
        {
            value: this.ALL_CUSTOMER,
            name: 'DOCUMENT.VALUE_TYPE_1'
        },
        {
            value: this.SOME_CUSTOMER,
            name: 'DOCUMENT.VALUE_TYPE_2'
        },
    ]

    public static getAttribution(value: number) {
        let status = this.types.find(status => status.value === value);
        const name = status ? status.name: '';
        return { name };
    }
}

export const AtributionConfirmConst = {
    header: 'Thông báo!',
    icon: 'pi pi-exclamation-triangle',
    acceptLabel: 'Đồng ý',
    rejectLabel: 'Hủy bỏ',
}
