export const OBJECT_CONFIRMATION_DIALOG = {
	DEFAULT_IMAGE: {
		IMAGE_APPROVE: 'assets/layout/images/icon-dialog/icon-approve-modalDialog.svg',
		IMAGE_CLOSE: 'assets/layout/images/icon-dialog/icon-close-modalDialog.svg',
	},
};

export const OBJECT_IMG_LOCALIZATIONS = {
	DEFAULT_IMAGE: {
		EN: 'assets/layout/images/localization/en.svg',
		JP: 'assets/layout/images/localization/jp.svg',
	},
};

export const DEFAULT_MEDIA = {
	DEFAULT_IMAGE: {
		IMAGE_ADD: 'assets/layout/images/add-image-bg.png'
	},
	DEFAULT_ICON: {
		ICON_DEFAULT: 'assets/layout/images/default-icon.png'
	}

}