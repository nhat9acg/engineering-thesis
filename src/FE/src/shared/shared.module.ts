import { SignalrService } from './services/signalr.service';
import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppSessionService } from './session/app-session.service';
import { AppUrlService } from './nav/app-url.service';
import { AppAuthService } from './auth/app-auth.service';
import { AppRouteGuard } from './auth/auth-route-guard';
import { DateViewPipe } from './pipes/dateview.pipe';
import { DateTimeViewPipe } from './pipes/datetimeview.pipe';
import { TruncatePipe } from './pipes/truncate.pipe';

import { BusyDirective } from './directives/busy.directive';
import { EqualValidator } from './directives/equal-validator.directive';
import { AppUtilsService } from './services/utils.service';
import { MessageService } from 'primeng/api';
import { LoadingPageComponent } from './components/validation/loadingPage/loading-page.component';
import { FormatCurrencyPipe } from './pipes/formatCurrency.pipe';
import { BaseApiUrlPipe } from './pipes/baseApiUrl.pipe';
import { SelectedLanguageService } from './services/selected-language.service';
import { UserService } from './services/user.service';
import { BusinessCustomerService } from './services/business-customer.service';
import { RestaurantService } from './services/restaurant.service';
import { RoleService } from './services/role.service';
import { GroupCustomerService } from './services/group-customer.service';
import { ProductCategoryService } from './services/product-category.service';
import { FileService } from './services/file.service';
import { ProductService } from './services/product.service';
import { ProductPriceService } from './services/product-price.service';
import { OrderService } from './services/order.service';
import { CalculationUnitService } from './services/calculation-unit.service';
import { ShippingProviderService } from './services/shipping-provider.service';
import { TabViewModule } from 'primeng/tabview';
import { WrapperPageListComponent } from './components/wrapper-page-list/wrapper-page-list.component';
import { CalendarService } from './services/calendar.service';
import { DateConversionService } from './services/date-conversion.service';
import { LayoutService } from './auth/app.layout.service';
import { DocumentService } from './services/document.service';
import { CartService } from './services/cart.service';
import { CustomerService } from './services/customer.service';
import { UserAddressService } from './services/user-address.service';
import { OrderCustomerService } from './services/order-customer.service';
import { ShippingCompanyService } from './services/shipping-company.service';
import { NotificationService } from './services/notification.service';
import { ChatService } from './services/chat.service';
import { ExportReportService } from './services/export-report.service';
import { FormCalendarComponent } from './components/form-calendar/form-calendar.component';
import { FormsModule } from '@angular/forms';
import { CalendarModule } from 'primeng/calendar';
import { InputTextModule } from 'primeng/inputtext';
import { InputSwitchModule } from 'primeng/inputswitch';
import { CheckboxValueAccessorDirective } from './directives/checkbox-value-accessor.directive';
import { HelpersService } from './services/helpers.service';
import { FormUploadComponent } from './components/form-upload/form-upload.component';
import { HandleLinkYoutubePipe } from './pipes/handle-link-youtube.pipe';
import { DialogService } from 'primeng/dynamicdialog';
import { AcceptFilePipe } from './pipes/accept-file.pipe';
import { FileUploadModule } from 'primeng/fileupload';
import { FunctionPipe } from './pipes/function.pipe';
import { FormTableComponent } from './components/form-table/form-table.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        TabViewModule,
        FormsModule,
        CalendarModule,
        InputTextModule,
        InputSwitchModule,
        FileUploadModule,
    ],
    declarations: [
		DateViewPipe,
        DateTimeViewPipe,
        TruncatePipe,
		FormatCurrencyPipe,
		BaseApiUrlPipe,
        BusyDirective,
        EqualValidator,
        LoadingPageComponent,
        WrapperPageListComponent,
        FormCalendarComponent,
        CheckboxValueAccessorDirective,
        FormUploadComponent,
        HandleLinkYoutubePipe,
        AcceptFilePipe,
        FunctionPipe,
        FormTableComponent,
    ],
    exports: [
		DateViewPipe,
        DateTimeViewPipe,
        TruncatePipe,
		FormatCurrencyPipe,
		BaseApiUrlPipe,
        BusyDirective,
        EqualValidator,
        LoadingPageComponent,
        WrapperPageListComponent,
        FormCalendarComponent,
        HandleLinkYoutubePipe,
        AcceptFilePipe,
        FunctionPipe,
    ]
})
export class SharedModule {
    static forRoot(): ModuleWithProviders<SharedModule> {
        return {
            ngModule: SharedModule,
            providers: [
                AppSessionService,
                AppUrlService,
                AppAuthService,
                LayoutService,
                AppRouteGuard,
                AppUtilsService,
                SignalrService,
                MessageService,
                SelectedLanguageService,
                UserService,
                BusinessCustomerService,
                RestaurantService,
                RoleService,
                GroupCustomerService,
                ProductCategoryService,
                FileService,
                ProductService,
                ProductPriceService,
                OrderService,
                CalculationUnitService,
                ShippingProviderService,
                CalendarService,
                DateConversionService,
                DocumentService,
                CartService,
                CustomerService,
                UserAddressService,
                OrderCustomerService,
                ShippingCompanyService,
                NotificationService,
                ChatService,
                ExportReportService,
                HelpersService,
                DialogService,
            ]
        };
    }
}
