export enum StatusResonse {
    SUCCESS = 1,
    ERROR = 0,
}

export enum YesNo {
    YES = "Y",
    NO = "N",
}

export enum ActiveDeactive {
    ACTIVE = "A",
    DEACTIVE = "D",
}

export enum UserType {
    T = 'T',    // TradingProvider
    RT = 'RT',  // Root Trading
    P = 'P',    // Partner
    RP = 'RP',  // Root Partner
    E = 'E',    // Epic
    RE = 'RE',  // Root Epic
}

export enum UnitTime {
    DAY = 'D',  // NGÀY 
    MONTH = 'M', // THÁNG
    YEAR = 'Y', // NĂM
    QUARTER = 'Q', // QUÝ
}

export enum Action {
    ADD = 1,
    UPDATE = 2,
    DELETE = 3,
}

export enum EFormatDate {
    DATE_TIME_SECOND = 'DD-MM-YYYY HH:mm:ss', 
    DATE_TIME = 'DD-MM-YYYY HH:mm',
    DATE = 'DD-MM-YYYY',
    DATE_DMY_Hms = 'DD-MM-YYYY HH:mm:ss', 
    DATE_DMY_Hm = 'DD-MM-YYYY HH:mm',
    DATE_DMY = 'DD-MM-YYYY',
}

export enum IconConfirm {
    APPROVE = 'assets/layout/images/icon-dialog-confirm/approve.svg',
    DELETE = 'assets/layout/images/icon-dialog-confirm/delete.svg',
    WARNING = 'assets/layout/images/icon-dialog-confirm/warning.svg',
    QUESTION = 'assets/layout/images/icon-dialog-confirm/question.svg',
}

export enum ContentTypeEView {
    MARKDOWN = 'MARKDOWN',
    HTML = 'HTML',
    IMAGE = "IMAGE",
    FILE = "FILE",
}

export enum EAcceptFile {
    ALL = '',
    IMAGE = 'image',
    VIDEO = 'video',
    MEDIA = 'media',
}

export enum ETypeHandleLinkYoutube {
    CHECK_LINK = 'CHECK_LINK',
    GET_ID = 'GET_ID',
    GET_EMBED_LINK = 'GET_EMBED_LINK',
    GET_WATCH_LINK = 'GET_WATCH_LINK',
}

export enum ETypeUrlYoutube {
    WATCH = 'https://www.youtube.com/watch',
    LIVE = 'https://www.youtube.com/live',
    SHORT = 'https://youtu.be',
}

export class BaseConsts {
    public static imageExtensions = ['jpg', 'png', 'gif', 'webp', 'tiff', 'psd', 'raw', 'bmp', 'svg', 'indd', 'heif'];
    public static videoExtensions = ['webm', 'mkv', 'flv', 'vob', 'ogv', 'ogg', 'rrc', 'gifv', 'mng', 'mov', 'avi', 'qt', 'wmv', 'yuv', 'rm', 'asf', 'amv', 'mp4', 'm4p', 'm4v', 'mpg', 'mp2', 'mpeg', 'mpe', 'mpv', 'm4v', 'svi', '3gp', '3g2', 'mxf', 'roq', 'nsv', 'flv', 'f4v', 'f4p', 'f4a', 'f4b', 'mod'];
}