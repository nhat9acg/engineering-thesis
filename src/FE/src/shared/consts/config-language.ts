export class ConfigLanguage {
    public static VIETNAM = 'vi';
    public static ENGLISH = 'en';
    public static JAPAN = 'jp';

    public static LanguageList = [
        {
            name: "English",
            code: ConfigLanguage.ENGLISH,
            img: 'assets/layout/images/Flag_of_England.svg',
        },
        {
            name: "Japan",
            code: ConfigLanguage.JAPAN,
            img: "assets/layout/images/Flag_of_Japan.svg",
        },
        {
            name: "VietNam",
            code: ConfigLanguage.VIETNAM,
            img: "assets/layout/images/Flag_of_Vietnam.svg",
        },
    ]
}