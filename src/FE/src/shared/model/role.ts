
export class RoleDefault {
    id: number | undefined = undefined;
    name: string = "";
    userType: number = null;
    description?: string = "";
    permissionKeys?: string[] = [];
    permissionKeysRemove?: string[] = [];
}