export class UserDefault {
    id: number | undefined = undefined;
    username: string = "";
    password: string = "";
    repeatPassword: string = "";
    fullName: string = "";
    roleIds: number[] = [];
    userType: number = null;
    businessCustomerId?: number = null;
    restaurantId?: number = null;
    roleType?: number = null;
    email?: string = "";
    phone?: string = "";
}