export class Calendar {
    workingDate: string;
    workingYear: number;
    isDayOff?: string;
    note?: string;
  
    constructor(workingDate: string, workingYear: number, isDayOff?: string, note?: string) {
      this.workingDate = workingDate;
      this.workingYear = workingYear;
      this.isDayOff = isDayOff;
      this.note = note;
    }
}

export class Header {
    vi: string;
    order: number;
    
    constructor(vi: string,order: number) {
        this.vi = vi;
        this.order = order;
    }
}

export class Month {
    value: string;
    data: any[]; 
  
    constructor(value: string, data: any[]) {
      this.value = value;
      this.data = data;
    }
  }