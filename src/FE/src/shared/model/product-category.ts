export class ProductCategoryDefault {
    id: number | undefined = undefined;
    name: string = "";
    code: string = "";
    avatar: string = "";
    parentId?: number = null;
    status?: number = null;
}