export class ProductPriceModel{
    id: number | undefined = 0;
    businessCustomerId: number = 0;
    productId: number = 0;
    price: number = 0;
    isDeleted?: boolean = false;
}