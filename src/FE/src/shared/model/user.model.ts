export class User {
    user_type: number;
    user_id: number
    
    constructor(user_type: number, user_id: number) {
        this.user_type = user_type;
        this.user_id = user_id;
    }
}