export class DocumentManagement {
    id: number;
    title: string;
    businessCustomerIds: number[];
    fileUrl: string;
    sendType: number;
    numberCustomerSent: number;
    numberCustomerView: number;
    createdDate: Date;
    createdBy: {
      id: number;
      username: string;
      fullName: string;
      userType: number;
    };
  
    constructor(
      id: number,
      title: string,
      businessCustomerIds: number[],
      fileUrl: string,
      sendType: number,
      numberCustomerSent: number,
      numberCustomerView: number,
      createdDate: Date,
      createdBy: {
        id: number;
        username: string;
        fullName: string;
        userType: number;
      }
    ) {
      this.id = id;
      this.title = title;
      this.fileUrl = fileUrl;
      this.sendType = sendType;
      this.numberCustomerSent = numberCustomerSent;
      this.numberCustomerView = numberCustomerView;
      this.createdDate = createdDate;
      this.createdBy = createdBy;
      this.businessCustomerIds = businessCustomerIds;
    }
  }