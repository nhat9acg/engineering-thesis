export class ProductDefault {
    id: number | undefined = undefined;
    name: string = "";
    code: string = "";
    productCategoryId?: number = null;
    status?: number = null;
}