import { PermissionTypes } from "./AppConsts";

const PermissionConfig = {};

export class PermissionConst {
    private static readonly Web: string = "web_";
    private static readonly Menu: string = "menu_";
    private static readonly Tab: string = "tab_";
    private static readonly Page: string = "page_";
    private static readonly Table: string = "table_";
    private static readonly Form: string = "form_";
    private static readonly ButtonTable: string = "btn_table_";
    private static readonly ButtonForm: string = "btn_form_";
    private static readonly ButtonAction: string = "btn_action_";

    public static readonly PageDashboard: string = `${PermissionConst.Page}dashboard`;
    public static readonly MenuQLHeThong: string = `${PermissionConst.Menu}ql_he_thong`;
    // Module Cấu hình vai trò
    public static readonly MenuCauHinhVaiTro: string = `${PermissionConst.Menu}cau_hinh_vai_tro`;
    public static readonly CauHinhVaiTro_DanhSach: string = `${PermissionConst.Table}cau_hinh_vai_tro_ds`;
    public static readonly CauHinhVaiTro_ThemMoi: string = `${PermissionConst.ButtonAction}cau_hinh_vai_tro_them_moi`;
    public static readonly CauHinhVaiTro_ChinhSua: string = `${PermissionConst.ButtonAction}cau_hinh_vai_tro_chinh_sua`;
    public static readonly CauHinhVaiTro_Xoa: string = `${PermissionConst.ButtonAction}cau_hinh_vai_tro_xoa`;

    // Module Cấu hình user
    public static readonly MenuCauHinhUser: string = `${PermissionConst.Menu}cau_hinh_user`;
    public static readonly CauHinhUser_DanhSach: string = `${PermissionConst.Table}cau_hinh_user_ds`;
    public static readonly CauHinhUser_ThemMoi: string = `${PermissionConst.ButtonAction}cau_hinh_user_them_moi`;
    public static readonly CauHinhUser_ChinhSua: string = `${PermissionConst.ButtonAction}cau_hinh_user_chinh_sua`;
    public static readonly CauHinhUser_Xoa: string = `${PermissionConst.ButtonAction}cau_hinh_user_xoa`;

}
    // Dashboard
    PermissionConfig[PermissionConst.PageDashboard] = { type: PermissionTypes.Menu, name: 'Dashboard tổng quan', parentKey: null, icon: 'pi pi-fw pi-home' };
    
    PermissionConfig[PermissionConst.MenuQLHeThong] = { type: PermissionTypes.Menu, name: 'Quản lý hệ thống', parentKey: null, icon: 'pi pi-users' };
    
    // Module Cấu hình vai trò
    PermissionConfig[PermissionConst.MenuCauHinhVaiTro] = { type: PermissionTypes.Menu, name: 'Cấu hình vai trò', parentKey: PermissionConst.MenuQLHeThong };
    PermissionConfig[PermissionConst.CauHinhVaiTro_DanhSach] = { type: PermissionTypes.Table, name: 'Danh sách', parentKey: PermissionConst.MenuCauHinhVaiTro };
    PermissionConfig[PermissionConst.CauHinhVaiTro_ThemMoi] = { type: PermissionTypes.ButtonAction, name: 'Thêm mới', parentKey: PermissionConst.MenuCauHinhVaiTro };
    PermissionConfig[PermissionConst.CauHinhVaiTro_ChinhSua] = { type: PermissionTypes.ButtonAction, name: 'Chỉnh sửa', parentKey: PermissionConst.MenuCauHinhVaiTro };
    PermissionConfig[PermissionConst.CauHinhVaiTro_Xoa] = { type: PermissionTypes.ButtonAction, name: 'Xóa', parentKey: PermissionConst.MenuCauHinhVaiTro };
    
    // Module Cấu hình user
    PermissionConfig[PermissionConst.MenuCauHinhUser] = { type: PermissionTypes.Menu, name: 'Cấu hình tải khoản', parentKey: PermissionConst.MenuQLHeThong };
    PermissionConfig[PermissionConst.CauHinhUser_DanhSach] = { type: PermissionTypes.Table, name: 'Danh sách', parentKey: PermissionConst.MenuCauHinhUser };
    PermissionConfig[PermissionConst.CauHinhUser_ThemMoi] = { type: PermissionTypes.ButtonAction, name: 'Thêm mới', parentKey: PermissionConst.MenuCauHinhUser };
    PermissionConfig[PermissionConst.CauHinhUser_ChinhSua] = { type: PermissionTypes.ButtonAction, name: 'Chỉnh sửa', parentKey: PermissionConst.MenuCauHinhUser };
    PermissionConfig[PermissionConst.CauHinhUser_Xoa] = { type: PermissionTypes.ButtonAction, name: 'Xóa', parentKey: PermissionConst.MenuCauHinhUser };

export default PermissionConfig;
