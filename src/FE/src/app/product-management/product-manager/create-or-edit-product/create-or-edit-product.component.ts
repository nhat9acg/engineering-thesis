import { Component, EventEmitter, Injector, OnInit, Output } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { TranslateService } from "@ngx-translate/core";
import { AppConsts } from "@shared/AppConsts";
import { CrudComponentBase } from "@shared/crud-component-base";
import { IResponse } from "@shared/interfaces/base-interface";
import { ICalculationUnit } from "@shared/interfaces/calculation-unit.interface";
import { IProductCategory } from "@shared/interfaces/product-category.interface";
import { IProduct } from "@shared/interfaces/product.interface";
import { ProductDefault } from "@shared/model/product";
import { CalculationUnitService } from "@shared/services/calculation-unit.service";
import { FileService } from "@shared/services/file.service";
import { ProductCategoryService } from "@shared/services/product-category.service";
import { ProductService } from "@shared/services/product.service";
import { SelectedLanguageService } from "@shared/services/selected-language.service";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-create-or-edit-product",
  templateUrl: "./create-or-edit-product.component.html",
  styleUrls: ["./create-or-edit-product.component.scss"],
})
export class CreateOrEditProductComponent extends CrudComponentBase {
	constructor(
		injector: Injector,
		messageService: MessageService,
		private ref: DynamicDialogRef,
		private configDialog: DynamicDialogConfig,
		public _translateService: TranslateService,
		private fb: FormBuilder,
		private _fileService: FileService,
		private _productCategoryService: ProductCategoryService,
		private _productService: ProductService,
		private _selectedLangService: SelectedLanguageService,
		private _calculationUnitService: CalculationUnitService,
	) {
        super(injector, messageService);
	}

	parentLangKeys: string[] = ['RESPONSE_MSG'];
	labels: string[] = [];

	@Output() onCloseDialog = new EventEmitter<boolean>();
	AppConsts = AppConsts;
	product: IProduct;
    postForm: FormGroup;
	parentCategories: IProductCategory[];
	imageDefault = 'assets/layout/images/image-bg-default.jpg';
	calculationUnits: ICalculationUnit;

  	ngOnInit(): void {
		this._selectedLangService.getLanguage.subscribe((resLang) => {
			if(resLang) {
			  this.labels = this._selectedLangService.getKeyLangs(this.parentLangKeys, resLang.keys);
			  console.log('labels', this.labels);
			}
		})
		this.optionsUnit();
		this._productCategoryService.getAllNoPaging().subscribe(res => {
            if (this.handleResponseInterceptor(res)) {
				this.isLoading = false;
                this.parentCategories = res?.data;
            }
		});
        if( this.configDialog?.data?.product) {
			this.product = this.configDialog?.data?.product;
			console.log("this.product",this.product);
			
		}
		this.postForm = this.fb.group({
			id: [this.product?.id || null, [Validators.required]],
            name: [this.product?.name || null, [Validators.required]],
            code: [this.product?.code || null, [Validators.required]],
			image: [this.product?.image || null, []],
			productCategoryId: [this.product?.productCategoryId || null, []],
			defaultPrice: [this.product?.defaultPrice || null, []],
			allowOrder: [this.product?.allowOrder ?? true, []],
			calculationUnitId: [this.product?.calculationUnitId || null, []],
			description: [this.product?.description || null, []],
		})
		if(!this.product?.id) {
			this.postForm.removeControl('id');
		}
	}

	toggleCheckbox() {
		this.postForm.patchValue({
			allowOrder: !this.postForm.value.allowOrder
		  }); 
	  }

	myUploader(event){
		if (event?.files[0]) {
			this._fileService.uploadFileGetUrl(event?.files[0], "product").subscribe((response) => {
			  if (this.handleResponseInterceptor(response, "")) {
				this.postForm.patchValue({
                    image: response?.data
                });
			  }
			}, (err) => {
			  console.log('err-----', err);
			  this.messageError("Có sự cố khi upload!");
			});
		}
	}

	optionsUnit() {
		this.isLoading = true;
		this._calculationUnitService.getAllNoPaging().subscribe((res) => {
		  this.isLoading = false;
		  if (this.handleResponseInterceptor(res, '')) {
	
			this.calculationUnits = res.data?.items;
		  }
		}, (err) => {
		  this.isLoading = false;
		  console.log('Error-------', err);
	
		});
	  }

	hideDialog() {
		this.onCloseDialog.emit();
	}

	cancel() {
        this.ref.destroy();
    }

	onSubmit() {
		console.log('postForm ', this.postForm);
        if (!this.checkInValidForm(this.postForm)) {
			if (this.product?.id){
				this._productService.update(this.postForm.value).subscribe(
					(response) => {
					  if (this.handleResponseInterceptor(response, this.labels['RESPONSE_MSG']['UPDATE_SUCCESS'])) {
						this.ref.close(true);
					  }
					}, () => {
					this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER'], 3000);
				});
			} else {
				this._productService.create(this.postForm.value).subscribe(
					(response) => {
					  if (this.handleResponseInterceptor(response, this.labels['RESPONSE_MSG']['ADD_SUCCESS'])) {
						this.ref.close(true);
					  }
					}, () => {
					this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER'], 3000);
				});
			}
		} else {
			this.messageError(this.labels['RESPONSE_MSG']['PLEASE_ENTER_ENOUGH_INFOR'], 3000)
		}
	}


}
