import { Component, ElementRef, Injector, OnInit, ViewChild } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { ActiveDeactiveNumConst, AppConsts, FormNotificationConst, PermissionConst, ProductConst, SearchConst, UserConst, localStorageKeyConst } from "@shared/AppConsts";
import { CrudComponentBase } from "@shared/crud-component-base";
import { IBaseListAction, IPageInfo, IResponseLang } from "@shared/interfaces/base-interface";
import { IColumn } from "@shared/interfaces/business-customer.interface";
import { IProduct } from "@shared/interfaces/product.interface";
import { Page } from "@shared/model/page";
import { ProductService } from "@shared/services/product.service";
import { SelectedLanguageService } from "@shared/services/selected-language.service";
import { ConfirmationService, MessageService } from "primeng/api";
import { DialogService } from "primeng/dynamicdialog";
import { debounceTime } from "rxjs/operators";
import { BreadcrumbService } from "src/app/layout/breadcrumb/breadcrumb.service";
import { CreateOrEditProductComponent } from "./create-or-edit-product/create-or-edit-product.component";
import { FormNotificationComponent } from "src/app/form-general/form-notification/form-notification.component";

@Component({
  selector: "app-product-manager",
  templateUrl: "./product-manager.component.html",
  styleUrls: ["./product-manager.component.scss"],
})
export class ProductManagerComponent extends CrudComponentBase {
	constructor(
		injector: Injector,
		messageService: MessageService,
		private _dialogService: DialogService,
		private selectedLanguageService: SelectedLanguageService,
		private _translateService: TranslateService,
		private _breadcrumbService: BreadcrumbService,
		private _productService: ProductService,
		private _confirmationService: ConfirmationService,
	) {
		super(injector, messageService);
	}

	rows: IProduct[] = [];
	cols: IColumn[];
	page = new Page();
	listAction: IBaseListAction[] = [];
	UserConst = UserConst;
	ProductConst = ProductConst; 
	ActiveDeactiveNumConst = ActiveDeactiveNumConst;
	PermissionConst = PermissionConst;

	parentLangKeys: string[] = ['HOME', 'MENU', 'ROUTER', 'PRODUCT', 'CONFIRM', 'SHARE', 'SYSTEM_MANAGER', 'RESPONSE_MSG','CALCULATION_UNIT'];
	labels: string[] = [];
	dataFilter = {
		field: null,
		status: null,
	}
	language: string = 'en';
	baseUrl: string;
	imageDefault = 'assets/layout/images/image-bg-default.jpg'; 

	scrollHeight: number = 0;

  	ngOnInit(): void {		
		this.baseUrl = AppConsts.remoteServiceBaseUrl ?? this.baseUrl;
		this.selectedLanguageService.getLanguage.subscribe((resLang: IResponseLang) => {
			if(resLang) {
				this.language = resLang?.lang;
				this.labels = this.selectedLanguageService.getKeyLangs(this.parentLangKeys, resLang.keys);
				console.log('labels', this.labels);
				this.cols = [
					{ field: `code`, header: this.labels['PRODUCT']['CODE'], width: '6rem', isPin: true, type: '' },
					{ field: 'image', header: this.labels['PRODUCT']['COLUMN_IMAGE'], width: '6rem', isPin: true, type: '' },
					{ field: 'name', header: this.labels['PRODUCT']['NAME'], width: '12rem', isPin: true, type: '' },
					{ field: `productCategoryName`, header: this.labels['PRODUCT']['PRODUCT_CATEGORY'], width: '12rem', isPin: false, type: '' },
					{ field: `calculationUnitName`, header: this.labels['CALCULATION_UNIT']['TABLE_LINK'], width: '12rem', isPin: false, type: '' },
					{ field: `defaultPriceDisplay`, header: this.labels['PRODUCT']['COLUMN_DEFAULT_PRICE'], width: '12rem', isPin: false, type: '' },
					{ field: `allowOrder`, header: this.labels['PRODUCT']['COLUMN_ALLOW_ORDERING'], width: '12rem', isPin: false, type: '' },
					{ field: `columnResize`, header: '', width: '', isPin: false, type: 'hidden' },
				  ];
				  //
				this._breadcrumbService.setItems([
					{ label: this.labels['ROUTER']['HOME_PAGE'], routerLink: ['/home'] },
					{ label: this.labels['ROUTER']['PRODUCT'], routerLink: ['/product-management/product'] },
				]);
			}
			this.setPage();
		});

		this.subject.keyword.pipe(debounceTime(SearchConst.DEBOUNCE_TIME)).subscribe(() => {
			if (this.keyword === "") {
				this.setPage();
			} else {
				this.setPage();
			}
		});
	}

	changeFieldFilter() {
		if(this.keyword) {
		  this.setPage();
		}
	}

	genListAction(data = []) {
		this.listAction = data.map(item => {
			const actions = [];
			// actions.push({
			//   data: item,
			//   label: this.labels['SHARE.DETAILS'],
			//   icon: 'pi pi-info-circle',
			//   command: ($event) => {
			//     this.detail($event.item.data);
			//   }
			// })
		
			if (this.isGranted([PermissionConst.ProductUpdate])) { 
				actions.push({
					data: item,
					label: this.labels['SHARE']['UPDATE'],
					icon: 'pi pi-user-edit',
					command: ($event) => {
					this.edit($event.item.data);
					}
				})
			}

			if (this.isGranted([PermissionConst.ProductChangeStatus])) { 
				actions.push({ 
					data: item,
					label: item.status == UserConst.ACTIVE ? this.labels['SHARE']['DEACTIVE'] : this.labels['SHARE']['ACTIVE'],
					icon: item.status == UserConst.ACTIVE ? "pi pi-times-circle" : "pi pi-check-circle",
					command: ($event) => {
						this.changeStatus($event.item.data);
					}
				});
			}

			if (this.isGranted([PermissionConst.ProductDelete])) { 
				actions.push({
					data: item,
					label: this.labels['SHARE']['DELETE'],
					icon: 'pi pi-trash',
					command: ($event) => {
						this.delete($event.item.data);
					}
				});
			}
		  return actions;
		});
	}

	changeStatus(user){
		const ref = this._dialogService.open(
            FormNotificationComponent,
            {
                header: this.labels['SHARE']['TITLE_DIALOG_CHANGE_STATUS'],
                width: '400px',
                contentStyle: { "max-height": "600px", "overflow": "auto", "padding-bottom": "50px" },
                styleClass: 'p-dialog-custom',
                baseZIndex: 10000,
                data: {	
                    title: user.status == UserConst.ACTIVE ? this.labels['SYSTEM_MANAGER']['DEACTIVE'] : this.labels['SYSTEM_MANAGER']['ACTIVE'],
                    icon: user.status == UserConst.ACTIVE ? FormNotificationConst.IMAGE_CLOSE : FormNotificationConst.IMAGE_APPROVE,
                },
            }
        );
        ref.onClose.subscribe((dataCallBack) => {
            if (dataCallBack?.accept) {
                this._productService.changeStatus(user.id).subscribe((response) => {
                    if (this.handleResponseInterceptor(response, this.labels['RESPONSE_MSG.UPDATE_SUCCESS'])) {
                        this.setPage();
                    }
                }, (err) => {
                    console.log('err____', err);
					this.messageError(this.labels['RESPONSE_MSG.PLEASE_TRY_AGAIN_LATER']);
                });
            } 
        });
	}

	create(){
		const ref = this._dialogService.open(
			CreateOrEditProductComponent,
			{
				header: this.labels['PRODUCT']['ADD_PRODUCT'],
				contentStyle: {"max-height": "800px", "overflow": "auto", "margin-bottom": "60px" },
				width: '600px',
				baseZIndex: 100000,
			}
		);
		//
		ref.onClose.subscribe(res=> {
			if(res) {
				this.setPage();
			}
		});
	}

	edit(item){
		const ref = this._dialogService.open(
			CreateOrEditProductComponent,
			{
				header: this.labels['PRODUCT']['UPDATE_PRODUCT'],
				contentStyle: {"max-height": "800px", "overflow": "auto", "margin-bottom": "60px" },
				data: {
					product: item
				},
				width: '600px',
				baseZIndex: 100000,
			}
		);
		//
		ref.onClose.subscribe(res=> {
			if(res) {
				this.setPage();
			}
		});
	}

	delete(item) {
        this._confirmationService.confirm({
            header: this.labels['PRODUCT']['TITLE_DIALOG_DELETE'],
            message: this.labels['CONFIRM']['ARE_YOU_SURE_TO_DELETE_THIS_PRODUCT'],
			acceptLabel: this.labels['CONFIRM']['YES'],
			rejectLabel: this.labels['CONFIRM']['NO'],
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
				this._productService.delete(item.id).subscribe((response) => {
                    if (this.handleResponseInterceptor(response, this.labels['RESPONSE_MSG']['DELETE_SUCCESS'])) {
                        this.setPage();
                    }
                }, (err) => {
                    console.log('err____', err);
                    this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER'], 3000);
                });
            }
        });
    }

	showData(rows) {
		for (let row of rows) {      
		//   row.productCategoryName = row?.productCategory?.name;
			row.defaultPriceDisplay = this.formatCurrency(row?.defaultPrice);
		};
	}

	isRowOdds: boolean = false;
	setPage(event?: IPageInfo) {
		this.isRowOdds = false;
		this.isLoading = true;
		if(event) {
			this.page.pageNumber = event.page;
			this.page.pageSize = event.rows;
		}
		this.page.keyword = this.keyword;
		this._productService.getAll(this.page,this.dataFilter).subscribe((res) => {
			this.isLoading = false;
			if (this.handleResponseInterceptor(res, '')) {
				this.page.totalItems = res.data.totalItems;
				this.rows = res.data?.items;
		
				if (res.data?.items?.length) {
					this.isRowOdds = !(this.rows?.length%2 == 0);
					this.genListAction(this.rows);
					this.showData(this.rows);
				}
			}
		}, (err) => {
		  this.isLoading = false;
		  console.log('Error-------', err);
		});
	}
}
