import { Component, ElementRef, Injector, OnInit, ViewChild } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { ActiveDeactiveNumConst, AppConsts, FormNotificationConst, PermissionConst, SearchConst, UserConst, localStorageKeyConst } from "@shared/AppConsts";
import { CrudComponentBase } from "@shared/crud-component-base";
import { IBaseListAction, IPageInfo, IResponseLang } from "@shared/interfaces/base-interface";
import { IColumn } from "@shared/interfaces/business-customer.interface";
import { IProductCategory } from "@shared/interfaces/product-category.interface";
import { Page } from "@shared/model/page";
import { ProductCategoryService } from "@shared/services/product-category.service";
// import { SelectedCountryService } from "@shared/services/selected-country.service";
import { ConfirmationService, MessageService } from "primeng/api";
import { DialogService } from "primeng/dynamicdialog";
import { debounceTime } from "rxjs/operators";
import { BreadcrumbService } from "src/app/layout/breadcrumb/breadcrumb.service";
import { CreateOrUpdateProductCategoryComponent } from "./create-or-update-product-category/create-or-update-product-category.component";
import { SelectedLanguageService } from "@shared/services/selected-language.service";
import { FormNotificationComponent } from "src/app/form-general/form-notification/form-notification.component";

@Component({
  selector: "app-product-category",
  templateUrl: "./product-category.component.html",
  styleUrls: ["./product-category.component.scss"],
})
export class ProductCategoryComponent extends CrudComponentBase {
	constructor(
		injector: Injector,
		messageService: MessageService,
		private _dialogService: DialogService,
		private selectedLanguageService: SelectedLanguageService,
		private _translateService: TranslateService,
		private _breadcrumbService: BreadcrumbService,
		private _productCategoryService: ProductCategoryService,
		private _confirmationService: ConfirmationService
	) {
		super(injector, messageService);
	}

	rows: IProductCategory[] = [];
	cols: IColumn[];
	page = new Page();
	listAction: IBaseListAction[] = [];
	UserConst = UserConst;
	ActiveDeactiveNumConst = ActiveDeactiveNumConst;
	PermissionConst = PermissionConst; 
	parentLangKeys: string[] = ['HOME', 'MENU', 'ROUTER', 'PRODUCT_CATEGORY', 'SHARE', 'CONFIRM', 'RESPONSE_MSG', 'SYSTEM_MANAGER'];
	labels: string[] = [];
	baseUrl: string;
	imageDefault = 'assets/layout/images/image-bg-default.jpg';

	scrollHeight: number = 0;
    language: string = 'en';
	
	ngOnInit(): void {
		this.baseUrl = AppConsts.remoteServiceBaseUrl ?? this.baseUrl;
		this.selectedLanguageService.getLanguage.subscribe((resLang: IResponseLang) => {
			if(resLang) {
				this.language = resLang?.lang;
				this.labels = this.selectedLanguageService.getKeyLangs(this.parentLangKeys, resLang.keys);
				console.log('labels', this.labels);

				this.cols = [
					{ field: `code`, header: this.labels['PRODUCT_CATEGORY']['CODE'], width: '5rem', isPin: true, type: '' },
					{ field: 'avatar', header: this.labels['PRODUCT_CATEGORY']['COLUMN_IMAGE'], width: '5rem', isPin: true, type: '' }, 
					{ field: 'name', header: this.labels['PRODUCT_CATEGORY']['NAME'], width: '12rem', isPin: true, type: '' },
					{ field: `parentName`, header: this.labels['PRODUCT_CATEGORY']['PARENT_NAME'], width: '12rem', isPin: false, type: '' },
					{ field: `columnResize`, header: '', width: '', isPin: false, type: 'hidden' },
				];
				//
				this._breadcrumbService.setItems([
					{ label: this.labels['ROUTER']['HOME_PAGE'], routerLink: ['/home'] },
					{ label: this.labels['ROUTER']['PRODUCT_CATEGORY'], routerLink: ['/product-management/product-category'] },
				]);
			}
			this.setPage();
		});

		this.subject.keyword.pipe(debounceTime(SearchConst.DEBOUNCE_TIME)).subscribe(() => {
			if (this.keyword === "") {
				this.setPage();
			} else {
				this.setPage();
			}
		});
	}


	genListAction(data = []) {
		this.listAction = data.map(item => {
		  const actions = [];
		  // actions.push({
		  //   data: item,
		  //   label: this.labels['SHARE.DETAILS'],
		  //   icon: 'pi pi-info-circle',
		  //   command: ($event) => {
		  //     this.detail($event.item.data);
		  //   }
		  // })
			if (this.isGranted([PermissionConst.ProductCategoryUpdate])) {  
				actions.push({
					data: item,
					label: this.labels['SHARE']['UPDATE'],
					icon: 'pi pi-user-edit',
					command: ($event) => {
					this.edit($event.item.data);
					}
				})
			}
	
			if (this.isGranted([PermissionConst.ProductCategoryChangeStatus])) { 
				actions.push({ 
					data: item,
					label: item.status == UserConst.ACTIVE ? this.labels['SHARE']['DEACTIVE'] : this.labels['SHARE']['ACTIVE'],
					icon: item.status == UserConst.ACTIVE ? "pi pi-times-circle" : "pi pi-check-circle",
					command: ($event) => {
						this.changeStatus($event.item.data);
					}
				});
			}

			if (this.isGranted([PermissionConst.ProductCategoryDelete])) { 
				actions.push({
					data: item,
					label: this.labels['SHARE']['DELETE'],
					icon: 'pi pi-trash',
					command: ($event) => {
					this.delete($event.item.data);
					}
				})
			}
		  return actions;
		});
	}

	edit(item){
		const ref = this._dialogService.open(
			CreateOrUpdateProductCategoryComponent,
			{
				header: this.labels['PRODUCT_CATEGORY']['UPDATE_PRODUCT_CATEGORY'],
				contentStyle: {"max-height": "600px", "overflow": "auto", "margin-bottom": "60px" },
				data: {
					productCategory: item
				},
				width: '700px',
				baseZIndex: 100000,
			}
		);
		//
		ref.onClose.subscribe(res=> {
			if(res) {
				this.setPage();
			}
		});
	}

	delete(item) {
        this._confirmationService.confirm({
            header: this.labels['PRODUCT_CATEGORY']['TITLE_DIALOG_DELETE'],
            message: this.labels['CONFIRM']['ARE_YOU_SURE_TO_DELETE_THIS_PRODUCT_CATEGORY'],
			acceptLabel: this.labels['CONFIRM']['YES'],
			rejectLabel: this.labels['CONFIRM']['NO'],
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
				this._productCategoryService.delete(item.id).subscribe((response) => {
                    if (this.handleResponseInterceptor(response, this.labels['RESPONSE_MSG']['DELETE_SUCCESS'])) {
                        this.setPage();
                    }
                }, (err) => {
                    console.log('err____', err);
                    this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER'], 3000);
                });
            }
        });
    }

	create() {
		const ref = this._dialogService.open(
			CreateOrUpdateProductCategoryComponent,
			{
				header: this.labels['PRODUCT_CATEGORY']['ADD_PRODUCT_CATEGORY'],
				contentStyle: {"max-height": "600px", "overflow": "auto", "margin-bottom": "60px" },
				width: '700px',
				baseZIndex: 100000,
			}
		);
		//
		ref.onClose.subscribe(res=> {
		  if(res) {
			this.setPage();
		  }
		});
	}

	changeStatus(user){
		const ref = this._dialogService.open(
            FormNotificationComponent,
            {
                header: this.labels['SHARE']['TITLE_DIALOG_CHANGE_STATUS'],
                width: '400px',
                contentStyle: { "max-height": "600px", "overflow": "auto", "padding-bottom": "50px" },
                styleClass: 'p-dialog-custom',
                baseZIndex: 10000,
                data: {	
                    title: user.status == UserConst.ACTIVE ? this.labels['SYSTEM_MANAGER']['DEACTIVE'] : this.labels['SYSTEM_MANAGER']['ACTIVE'],
                    icon: user.status == UserConst.ACTIVE ? FormNotificationConst.IMAGE_CLOSE : FormNotificationConst.IMAGE_APPROVE,
                },
            }
        );
        ref.onClose.subscribe((dataCallBack) => {
            if (dataCallBack?.accept) {
                this._productCategoryService.changeStatus(user.id).subscribe((response) => {
                    if (this.handleResponseInterceptor(response, this.labels['RESPONSE_MSG']['UPDATE_SUCCESS'])) {
                        this.setPage();
                    }
                }, (err) => {
                    console.log('err____', err);
					this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER']);
                });
            } 
        });
	}

	showData(rows) {
		for (let row of rows) {      
		  row.parentName = row?.parentProductCategory?.name;
		};
	}

	isRowOdds: boolean = false;
	setPage(event?: IPageInfo) {
		this.isRowOdds = false;
		this.isLoading = true;
		if(event) {
			this.page.pageNumber = event.page;
			this.page.pageSize = event.rows;
		}

		this.page.keyword = this.keyword;
		this._productCategoryService.getAll(this.page).subscribe((res) => {
			this.isLoading = false;
			if (this.handleResponseInterceptor(res, '')) {
				this.page.totalItems = res.data.totalItems;
				this.rows = res.data?.items;
		
				if(res.data?.items?.length) {
					this.isRowOdds = !(this.rows?.length%2 == 0);
					this.genListAction(this.rows);
					this.showData(this.rows);
				}
			}
		}, (err) => {
		  this.isLoading = false;
		  console.log('Error-------', err);
		});
	}
}
