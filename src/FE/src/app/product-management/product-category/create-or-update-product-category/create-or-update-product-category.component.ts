import { Component, EventEmitter, Injector, Input, OnInit, Output } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { TranslateService } from "@ngx-translate/core";
import { AppConsts } from "@shared/AppConsts";
import { CrudComponentBase } from "@shared/crud-component-base";
import { IResponse } from "@shared/interfaces/base-interface";
import { IProductCategory } from "@shared/interfaces/product-category.interface";
import { ProductCategoryDefault } from "@shared/model/product-category";
import { FileService } from "@shared/services/file.service";
import { ProductCategoryService } from "@shared/services/product-category.service";
import { SelectedLanguageService } from "@shared/services/selected-language.service";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
  selector: "app-create-or-update-product-category",
  templateUrl: "./create-or-update-product-category.component.html",
  styleUrls: ["./create-or-update-product-category.component.scss"],
})
export class CreateOrUpdateProductCategoryComponent extends CrudComponentBase {
	constructor(
		injector: Injector,
		messageService: MessageService,
		private ref: DynamicDialogRef,
		private configDialog: DynamicDialogConfig,
		public _translateService: TranslateService,
        private fb: FormBuilder,
		private _fileService: FileService,
		private _productCategoryService: ProductCategoryService,
		private _selectedLangService: SelectedLanguageService,
	) {
        super(injector, messageService);
	}
	parentLangKeys: string[] = ['RESPONSE_MSG'];
	labels: string[] = [];
	@Output() onCloseDialog = new EventEmitter<boolean>();
	AppConsts = AppConsts;
	productCategory: IProductCategory = new ProductCategoryDefault;
    postForm: FormGroup;
	avatarImageUrl: IResponse<any>;
	imageDefault = 'assets/layout/images/image-bg-default.jpg';
	
	parentCategories: IProductCategory[];
  	ngOnInit(): void {
		this._selectedLangService.getLanguage.subscribe((resLang) => {
			if(resLang) {
			  this.labels = this._selectedLangService.getKeyLangs(this.parentLangKeys, resLang.keys);
			  console.log('labels', this.labels);
			}
		})
		this.isLoading = true;
		this._productCategoryService.getAllNoPaging().subscribe(res => {
            if (this.handleResponseInterceptor(res)) {
				this.isLoading = false;
                this.parentCategories = res?.data;
            }
		});
        if( this.configDialog?.data?.productCategory) {
			this.productCategory = this.configDialog?.data?.productCategory;
			this.avatarImageUrl = this.configDialog?.data?.productCategory?.avatar;
		}
		this.postForm = this.fb.group({
			id: [this.productCategory.id, [Validators.required]],
            name: [this.productCategory.name, [Validators.required]],
            code: [this.productCategory.code, [Validators.required]],
			avatar: [this.productCategory.avatar],
			parentId: [this.productCategory.parentId]
		})
		if(!this.productCategory?.id) {
			this.postForm.removeControl('id');
		}
	}

	myUploader(event){
		if (event?.files[0]) {
			this._fileService.uploadFileGetUrl(event?.files[0], "product_category").subscribe((response) => {
			  if (this.handleResponseInterceptor(response, "")) {
	  
				this.avatarImageUrl = response?.data;
				this.postForm.patchValue({
                    avatar: this.avatarImageUrl
                });
			  }
			}, (err) => {
			  console.log('err-----', err);
			  this.messageError("Có sự cố khi upload!");
			});
		}
	}

	hideDialog() {
		this.onCloseDialog.emit();
	}

	cancel() {
        this.ref.destroy();
    }

    onSubmit() {
		console.log('postForm ', this.postForm);
        if (!this.checkInValidForm(this.postForm)) {
			if (this.productCategory?.id){
				this._productCategoryService.update(this.postForm.value).subscribe(
					(response) => {
					  if (this.handleResponseInterceptor(response, this.labels['RESPONSE_MSG']['UPDATE_SUCCESS'])) {
						this.ref.close(true);
					  }
					}, () => {
					this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER'], 3000);
				});
			} else {
				this._productCategoryService.create(this.postForm.value).subscribe(
					(response) => {
					  if (this.handleResponseInterceptor(response, this.labels['RESPONSE_MSG']['ADD_SUCCESS'])) {
						this.ref.close(true);
					  }
					}, () => {
					this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER'], 3000);
				});
			}
		} else {
			this.messageError(this.labels['RESPONSE_MSG']['PLEASE_ENTER_ENOUGH_INFOR'], 3000)
		}
	}
}
