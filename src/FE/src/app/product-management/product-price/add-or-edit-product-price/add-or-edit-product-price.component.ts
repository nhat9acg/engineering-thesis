import { Component, EventEmitter, Injector, OnInit, Output } from "@angular/core";
import { FormArray, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { CrudComponentBase } from "@shared/crud-component-base";
import { IBusinessCustomer } from "@shared/interfaces/business-customer.interface";
import { IProductPrice } from "@shared/interfaces/product-price.interface";
import { IProduct } from "@shared/interfaces/product.interface";
import { BusinessCustomerService } from "@shared/services/business-customer.service";
import { ProductPriceService } from "@shared/services/product-price.service";
import { ProductService } from "@shared/services/product.service";
import { SelectedLanguageService } from "@shared/services/selected-language.service";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { forkJoin } from "rxjs";

@Component({
  selector: "app-add-or-edit-product-price",
  templateUrl: "./add-or-edit-product-price.component.html",
  styleUrls: ["./add-or-edit-product-price.component.scss"],
})
export class AddOrEditProductPriceComponent extends CrudComponentBase {
	constructor(
		injector: Injector, 
		messageService: MessageService,
		private ref: DynamicDialogRef,
		private configDialog: DynamicDialogConfig,
		private _selectedLangService: SelectedLanguageService,
		private fb: FormBuilder,
		private _productPriceService: ProductPriceService,
		private _businessCustomerService: BusinessCustomerService,
		private _productService: ProductService
	) {
		super(injector, messageService);
	}

	parentLangKeys: string[] = ['RESPONSE_MSG'];
	labels: string[] = [];
	@Output() onCloseDialog = new EventEmitter<boolean>();
    postForm: FormGroup;
	temp: number;
    businessCustomers: IBusinessCustomer[];
	products: IProduct[];
	productPrices: IProductPrice[];
	businessCustomerId: number;

  	ngOnInit(): void {
		this.businessCustomerId = this.configDialog?.data?.businessCustomerId;
		//
		this.init();
		this._selectedLangService.getLanguage.subscribe((resLang) => {
			if(resLang) {
			  this.labels = this._selectedLangService.getKeyLangs(this.parentLangKeys, resLang.keys);
			  console.log('labels', this.labels);
			}
		})
		this.postForm = this.fb.group({
			items: this.fb.array([])  // khởi tạo một form array để chứa các object
		});
		//
        if(this.configDialog?.data?.items) {
			this.productPrices = this.configDialog?.data?.items;
			this.productPrices.forEach(item => {
				let price = this.fb.group({
					id: [item.id, Validators.required],
					businessCustomerId: [ item.businessCustomerId, Validators.required],
					productId: [ item.productId, Validators.required],
					price: [item.price, Validators.required]
				});
				this.items.push(price);
			})
		} else {
			this.addItem();
		}
	}

	changeBusinessCustomerId(event) {
		console.log("event",event.value);
		this.temp = event.value;
	}

	init() {
		this.isLoading = true;
		forkJoin([this._businessCustomerService.getAllNoPaging(), this._productService.getAllNoPaging()]).subscribe(([res, resProduct]) => {
			this.isLoading = false;
            if (this.handleResponseInterceptor(res)) {
                this.businessCustomers = res?.data?.items;
            }
            if (this.handleResponseInterceptor(resProduct)) {
                this.products = resProduct?.data?.items;
            }
		})
	}

	get items(): FormArray {
		return this.postForm.get('items') as FormArray;
	}
	
	addItem() {
		const item = this.fb.group({
			businessCustomerId: [this.businessCustomerId, Validators.required],
			productId: [null, Validators.required],
			price: [null, Validators.required]
		});
		this.items.push(item);
	}
	
	hideDialog() {
		this.onCloseDialog.emit();
	}

	cancel() {
        this.ref.destroy();
    }

	onSubmit() {
		if (this.validateProductPriceForm(this.postForm)) {
			console.log(this.postForm?.value?.items);
			if (!this.postForm?.value?.items[0].id){
				this._productPriceService.create(this.postForm?.value?.items).subscribe( (res) => {
					if (this.handleResponseInterceptor(res, this.labels['RESPONSE_MSG']['ADD_SUCCESS'])) {
						this.ref.close(true);
					}
				}, () => {
					this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER'], 3000)
				})
			} else {
				this._productPriceService.update(this.postForm?.value?.items).subscribe( (res) => {
					if (this.handleResponseInterceptor(res, this.labels['RESPONSE_MSG']['UPDATE_SUCCESS'])) {
						this.ref.close(true);
					}
				}, () => {
					this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER'], 3000)
				})
			}

		} else {
			this.messageError(this.labels['RESPONSE_MSG']['PLEASE_ENTER_ENOUGH_INFOR'], 3000)
		}
	}

}
