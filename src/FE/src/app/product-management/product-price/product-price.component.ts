import { Component, ElementRef, Injector, Input, OnInit, ViewChild } from "@angular/core";
import { PermissionConst, SearchConst } from "@shared/AppConsts";
import { CrudComponentBase } from "@shared/crud-component-base";
import { IBaseListAction, IPageInfo, IResponseLang } from "@shared/interfaces/base-interface";
import { IColumn } from "@shared/interfaces/business-customer.interface";
import { IProductPrice } from "@shared/interfaces/product-price.interface";
import { SelectedLanguageService } from "@shared/services/selected-language.service";
import { ConfirmationService, MenuItem, MessageService } from "primeng/api";
import { DialogService } from "primeng/dynamicdialog";
import { debounceTime } from "rxjs/operators";
import { BreadcrumbService } from "src/app/layout/breadcrumb/breadcrumb.service";
import { AddOrEditProductPriceComponent } from "./add-or-edit-product-price/add-or-edit-product-price.component";
import { ProductPriceService } from "@shared/services/product-price.service";
import { IProduct } from "@shared/interfaces/product.interface";
import { ProductService } from "@shared/services/product.service";
import { forkJoin } from "rxjs";
import { Table } from "primeng/table";
import { ProductPriceModel } from "@shared/model/product-price.model";
import { Page } from "@shared/model/page";

@Component({
	selector: "app-product-price",
	templateUrl: "./product-price.component.html",
	styleUrls: ["./product-price.component.scss"],
})
export class ProductPriceComponent extends CrudComponentBase {
	constructor(
		injector: Injector,
		messageService: MessageService,
		private _dialogService: DialogService,
		private _languageService: SelectedLanguageService,
		private _breadcrumbService: BreadcrumbService,
		private _productPriceService: ProductPriceService,
		private _confirmationService: ConfirmationService,
		private _productService: ProductService,
	) {
		super(injector, messageService);
	}

	@Input() businessCustomerId: number = 0;

	listAction: IBaseListAction[] = [];
	productPrices: IProductPrice[] = [];
	productPriceClones: IProductPrice[] = [];
	cols: IColumn[];
	parentLangKeys: string[] = ['PRODUCT_PRICE', 'ROUTER', 'PRODUCT_PRICE', 'SHARE', 'CONFIRM', 'RESPONSE_MSG'];
	labels: string[] = [];
	selectedItems: IProductPrice[] = [];

	clonedProducts: { [productId: string]: IProductPrice } = {};
	products: IProduct[];

	fieldFilters = {
		businessCustomerId: null,
	}

	page = new Page();
	PermissionConst = PermissionConst;
	@Input() contentHeight: number = 0;
	@Input() breadcrumbItems: MenuItem[] = [];

	@ViewChild('table', { static: false }) table: Table;


	ngOnInit(): void {
		this.fieldFilters.businessCustomerId = this.businessCustomerId;
		this._languageService.getLanguage.subscribe((resLang: IResponseLang) => {
			if(resLang) {
				this.labels = this._languageService.getKeyLangs(this.parentLangKeys, resLang.keys);
			}
			this.setPage();
		});

		this.subject.keyword.pipe(debounceTime(SearchConst.DEBOUNCE_TIME)).subscribe(() => {
			if (this.keyword === "") {
				this.setPage();
			} else {
				this.setPage();
			}
		});
	}

	scrollHeight: number = 0;
  	@ViewChild('pageEl') pageEl: ElementRef<HTMLElement>;
	ngAfterViewInit() {
		setTimeout(() => {
			let pageInitHeight = this.pageEl.nativeElement.offsetHeight;
			this.scrollHeight = this.contentHeight - (pageInitHeight);
			// console.log('countTableHeight', this.contentHeight, tableInitHeight)
		}, 0);
	}

	showData(rows) {
		for (let row of rows) {      
			row.businessCustomerName = row?.businessCustomer?.fullName;
			row.productName = row?.product?.name;
			row.businessCustomerId = this.businessCustomerId;
			row.priceDisplay = this.formatCurrency(row?.price);
		};
	}

	setPage(event?: IPageInfo, isLoading = true) {
		if(event) {
			this.page.pageNumber = event.page;
			this.page.pageSize = event.rows;
		}
		this.page.keyword = this.keyword;
		this.isLoading = isLoading;

		forkJoin([this._productPriceService.getAll(this.page, this.fieldFilters), this._productService.getAllNoPaging()]).subscribe(([resProductPrice, resProduct]) => {
		  this.isLoading = false;
		  if (this.handleResponseInterceptor(resProductPrice, '')) {
			this.page.totalItems = resProductPrice.data.totalItems;
			this.productPrices = resProductPrice.data?.items;
			this.productPriceClones = JSON.parse(JSON.stringify(this.productPrices));
			this.selectedItems = [];			
			if (resProductPrice.data?.items?.length) {
			  this.showData(this.productPrices);
			}
		  }
		  //
		  this.products = resProduct?.data?.items;

		}, (err) => {
		  this.isLoading = false;
		  console.log('Error-------', err);
		});
	}

    onRowEditSave(item: IProductPrice, priceOld: number) {		
        if (item.id && item.price !== priceOld) {
            delete this.clonedProducts[item.id];
			let body = this.mapItemSendApis(new ProductPriceModel(), [item]);
            this._productPriceService.update(body).subscribe( (res) => {
				this.handleResponseInterceptor(res, this.labels['RESPONSE_MSG']['UPDATE_SUCCESS']);
				this.setPage(null, false);
			}, () => {
				this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER'], 3000)
			})
        }  else if(!	item.id) {
			delete this.clonedProducts[item.id];
			let body = this.mapItemSendApis(new ProductPriceModel(), [item]);
            this._productPriceService.create(body).subscribe( (res) => {
				this.handleResponseInterceptor(res, this.labels['RESPONSE_MSG']['ADD_SUCCESS']);
				this.setPage(null, false);
			}, () => {
				this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER'], 3000)
			})
        }
    }

	getProductProperty(productId: number, property: string): string {
		let product = this.products.find(product => product.id === productId);
		return product?.[property] || 'None';
	}

}
