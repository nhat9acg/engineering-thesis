import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ShippingCompanyAddComponent } from './shipping-company-add.component';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { MessageService } from 'primeng/api';
import { ShippingCompanyService } from '@shared/services/shipping-company.service';
import { FormBuilder } from '@angular/forms';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('ShippingCompanyAddComponent', () => {
  let component: ShippingCompanyAddComponent;
  let fixture: ComponentFixture<ShippingCompanyAddComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormsModule, ReactiveFormsModule],
      declarations: [ShippingCompanyAddComponent],
      providers: [
        DialogService,
        MessageService,
        ShippingCompanyService,
        FormBuilder,
        DynamicDialogRef,
        DynamicDialogConfig
      ],
      schemas: [NO_ERRORS_SCHEMA] // Use NO_ERRORS_SCHEMA to ignore template errors
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShippingCompanyAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize form with inputData if available', () => {
    const inputData = {
      id: 1,
      name: 'Test Shipping Company'
    };

    component.config = {
      data: {
        inputData: inputData,
        labels: [] // Add necessary labels for the test
      }
    };

    component.ngOnInit();

    expect(component.postForm.get('id')?.value).toBe(1);
    expect(component.postForm.get('name')?.value).toBe('Test Shipping Company');
  });

  it('should initialize form with empty values if inputData is not available', () => {
    component.config = {
      data: {
        inputData: null,
        labels: [] // Add necessary labels for the test
      }
    };

    component.ngOnInit();

    expect(component.postForm.get('id')?.value).toBe(null);
    expect(component.postForm.get('name')?.value).toBe('');
  });

  // Add more tests to cover other functionalities and interactions in the component
});
