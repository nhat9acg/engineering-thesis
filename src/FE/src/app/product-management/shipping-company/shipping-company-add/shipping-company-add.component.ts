import { Component, Injector, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CrudComponentBase } from '@shared/crud-component-base';
import { IBusinessCustomer } from '@shared/interfaces/business-customer.interface';
import { ICalculationUnit } from '@shared/interfaces/calculation-unit.interface';
import { IGroupCustomer } from '@shared/interfaces/group-customer.interface';
import { BusinessCustomerService } from '@shared/services/business-customer.service';
import { CalculationUnitService } from '@shared/services/calculation-unit.service';
import { GroupCustomerService } from '@shared/services/group-customer.service';
import { RestaurantService } from '@shared/services/restaurant.service';
import { ShippingCompanyService } from '@shared/services/shipping-company.service';
import { MessageService } from 'primeng/api';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-shipping-company-add',
  templateUrl: './shipping-company-add.component.html',
  styleUrls: ['./shipping-company-add.component.scss']
})
export class ShippingCompanyAddComponent extends CrudComponentBase {

  constructor(
    injector: Injector,
    messageService: MessageService,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private _shippingCompanyService: ShippingCompanyService,
    private dialogService: DialogService,
    private router: Router,
    private fb: FormBuilder,
    private _restaurantService: RestaurantService,
  ) {
    super(injector, messageService);
  }
  inputData: ICalculationUnit;
  postForm: FormGroup;
  labels: string[] = [];
  
  ngOnInit(): void {
    this.labels = this.config?.data?.labels
    this.inputData = this.config?.data?.inputData;
    this.postForm = this.fb.group({
      id: [this.inputData?.id || null, []],
      name: [this.inputData?.name || '', Validators.required],
    });
  }

  get postFormControl() {
    return this.postForm.controls;
  }

  onSubmit() {
    if (!this.checkInValidForm(this.postForm)) {
      if (this.inputData) {
        this._shippingCompanyService.update(this.postForm.value).subscribe((response) => {
          if (this.handleResponseInterceptor(response, this.labels['RESPONSE_MSG']['UPDATE_SUCCESS'])) {
            this.ref.close(true);
          }
        }, () => {
          this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER']);
        }
        );
      }
      else {
        this._shippingCompanyService.create(this.postForm.value).subscribe(
          (response) => {
            if (this.handleResponseInterceptor(response, this.labels['RESPONSE_MSG']['ADD_SUCCESS'])) {
              this.ref.close(true);
            }
          }, () => {
            this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER']);
          }
        );
      }
    } else {
      this.messageError(this.labels['RESPONSE_MSG']['PLEASE_ENTER_ENOUGH_INFOR']);
    }

  }

}




