import { Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SearchConst, FormNotificationConst, PermissionConst } from '@shared/AppConsts';
import { CrudComponentBase } from '@shared/crud-component-base';
import { Page } from '@shared/model/page';
import { BusinessCustomerService } from '@shared/services/business-customer.service';

import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { debounceTime } from 'rxjs/operators';
import { FormNotificationComponent } from 'src/app/form-general/form-notification/form-notification.component';
import { FormSetDisplayColumnComponent } from 'src/app/form-general/form-set-display-column/form-set-display-column.component';
import { BreadcrumbService } from 'src/app/layout/breadcrumb/breadcrumb.service';
import { IColumn, IBusinessCustomer } from '@shared/interfaces/business-customer.interface';
import { IBaseListAction, IPageInfo } from '@shared/interfaces/base-interface';
import { SelectedLanguageService } from '@shared/services/selected-language.service';
import { TranslateService } from '@ngx-translate/core';
import { GroupCustomerService } from '@shared/services/group-customer.service';
import { CalculationUnitService } from '@shared/services/calculation-unit.service';
import { ICalculationUnit } from '@shared/interfaces/calculation-unit.interface';
import { CalculationUnitAddComponent } from './calculation-unit-add/calculation-unit-add.component';

@Component({
  selector: 'app-calculation-unit',
  templateUrl: './calculation-unit.component.html',
  styleUrls: ['./calculation-unit.component.scss'],
  providers: [DialogService, ConfirmationService, MessageService]
})
export class CalculationUnitComponent extends CrudComponentBase {

    constructor(
        injector: Injector,
        private dialogService: DialogService,
        private _dialogService: DialogService,
        private confirmationService: ConfirmationService,
        private router: Router,
        private routeActive: ActivatedRoute,
        messageService: MessageService,
        private breadcrumbService: BreadcrumbService,
        private _calculationUnitService: CalculationUnitService,
        private selectedLanguageService: SelectedLanguageService,
        public _translateService: TranslateService,
    ) {
        super(injector, messageService);
    }

    ref: DynamicDialogRef;
    rows: ICalculationUnit[] = [];
    cols: IColumn[];
    listAction: IBaseListAction[] = [];
    page = new Page();
    parentLangKeys: string[] = ['HOME', 'MENU', 'RESTAURANT', 'SHARE', 
                'ROUTER', 'CONFIRM', 'RESPONSE_MSG', 'BUSINESS_CUSTOMER',
                'GROUP_CUSTOMER','CALCULATION_UNIT'];
    labels: string[] = [];

    scrollHeight: number = 0;

    PermissionConst = PermissionConst;

    ngOnInit(): void {
        this.selectedLanguageService.getLanguage.subscribe((resLang) => {
            if(resLang) {
                this.labels = this.selectedLanguageService.getKeyLangs(this.parentLangKeys, resLang.keys);
                this.cols = [
                    { field: 'name', header: this.labels['GROUP_CUSTOMER']['NAME'], width: '12rem', isPin: true, type: '' },
                    { field: `columnResize`, header: '', width: '', isPin: false, type: 'hidden' },
                ];
                //
                this.breadcrumbService.setItems([
                    { label: this.labels['ROUTER']['HOME_PAGE'], routerLink: ['/home'] },
                    { label: this.labels['CALCULATION_UNIT']['TABLE_LINK'], routerLink: ['/product-management/calculation-unit'] },
                ]);
            }
            this.setPage();
        });

        this.subject.keyword.pipe(debounceTime(SearchConst.DEBOUNCE_TIME)).subscribe(() => {
            if (this.keyword === "") {
                this.setPage();
            } else {
                this.setPage();
            }
        });
    }

    genListAction(data = []) {
        this.listAction = data.map(item => {
            const actions = [];
            if (this.isGranted([PermissionConst.CalculationUnitUpdate])) {  
                actions.push({
                    data: item,
                    label: this.labels['SHARE']['UPDATE'],
                    icon: 'pi pi-user-edit',
                    command: ($event) => {
                        this.edit($event.item.data);
                    }
                })
            }

            if (this.isGranted([PermissionConst.CalculationUnitDelete])) {  
                actions.push({
                    data: item,
                    label: this.labels['SHARE']['DELETE'],
                    icon: 'pi pi-trash',
                    command: ($event) => {
                        this.delete($event.item.data);
                    }
                })
            }
            return actions;
        });
    }


    create() {
        this.dialogService.open(CalculationUnitAddComponent, {
            header: this.labels['CALCULATION_UNIT']['ADD_TITLE'],
            width: '300px',
            contentStyle: {"max-height": "600px", "overflow": "auto", "margin-bottom": "60px" },
            data: {
                labels: this.labels
            },
        }).onClose.subscribe(result => {
            if (result) {
                this.setPage();
            } 
        })
    }

    edit(item) {
        this._calculationUnitService.get(item.id).subscribe((response) => {
        if (this.handleResponseInterceptor(response, "")) {
            this.dialogService.open(CalculationUnitAddComponent, {
                data: {
                    inputData: response.data,
                    labels: this.labels
                },
                contentStyle: {"max-height": "600px", "overflow": "auto", "margin-bottom": "60px" },
                header:  this.labels['CALCULATION_UNIT']['EDIT_TITLE'],
                width: '300px',
            }).onClose.subscribe(result => {
                if (result) this.setPage();
            })
        }
        });

    }

    delete(item) {
        this.confirmationService.confirm({
            message: this.labels['CALCULATION_UNIT']['DELETE_TITLE'],
            acceptLabel: this.labels['CONFIRM']['YES'],
            rejectLabel: this.labels['CONFIRM']['NO'],
            accept: () => {
                this._calculationUnitService.delete(item.id).subscribe((response) => {
                if (this.handleResponseInterceptor(response, this.labels['CONFIRM']['DELETE_SUCCESS'])) {
                    this.setPage();
                }
                });
            }
        });
    }

    isRowOdds: boolean = false;
    setPage(event?: IPageInfo) {
        this.isRowOdds = false;
        this.isLoading = true;
        if(event) {
            this.page.pageNumber = event.page;
            this.page.pageSize = event.rows;
        }
        this.page.keyword = this.keyword;
        this._calculationUnitService.getAll(this.page).subscribe((res) => {
        this.isLoading = false;
        if (this.handleResponseInterceptor(res, '')) {
            this.page.totalItems = res.data.totalItems;
            this.rows = res.data?.items;
            if (res.data?.items?.length) {
                this.isRowOdds = !(this.rows?.length%2 == 0);
                this.genListAction(this.rows);
            }
        }
        }, (err) => {
            this.isLoading = false;
            console.log('Error-------', err);
        });
    }
}



