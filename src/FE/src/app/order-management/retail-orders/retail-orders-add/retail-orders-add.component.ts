import { Component, Injector } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConsts, HistoryConst, OrderConst, TypeFormatDatesConst, UserConst } from '@shared/AppConsts';
import { CrudComponentBase } from '@shared/crud-component-base';
import { IDropdown } from '@shared/interfaces/base-interface';
import { IBusinessCustomer, IColumn } from '@shared/interfaces/business-customer.interface';
import { IHistoryView, IOrder, IOrderDetail, IShippingInfo } from '@shared/interfaces/order.interface';
import { IProductPrice } from '@shared/interfaces/product-price.interface';
import { IProduct } from '@shared/interfaces/product.interface';
import { IAddress, IBusinessCustomerDropdown, IRestaurant } from '@shared/interfaces/restaurant.interface';
import { IUser } from '@shared/interfaces/user-manager.interface';
import { Page } from '@shared/model/page';
import { UserDefault } from '@shared/model/user';
import { User } from '@shared/model/user.model';
import { OrderCustomerService } from '@shared/services/order-customer.service';
import { SelectedLanguageService } from '@shared/services/selected-language.service';
import { AppUtilsService } from '@shared/services/utils.service';
import * as moment from 'moment';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { AddressSelectComponent } from 'src/app/customer-management/profile/address/address-select/address-select.component';
import { enc, HmacSHA256, HmacSHA512 } from 'crypto-js';

@Component({
  selector: 'app-retail-orders-add',
  templateUrl: './retail-orders-add.component.html',
  styleUrls: ['./retail-orders-add.component.scss'],
  providers: [DialogService, ConfirmationService, MessageService],
})
export class RetailOrdersAddComponent extends CrudComponentBase {

  page = new Page();
  postForm?: FormGroup;
  businessCustomers: IBusinessCustomerDropdown[] = [];
  restaurants: IRestaurant[] = [];
  products: IProductPrice[] = [];
  addresses: IAddress[] = [];
  parentLangKeys: string[] = ['RESPONSE_MSG', 'ORDER', 'CONFIRM'];
  labels: string[] = [];
  orderDetails: IOrderDetail[] = [];
  orderId?: number;
  order: IOrder;
  fieldDates = ['orderDate'];
  isEdit: boolean = false;
  isDetail: boolean = false;
  header: string;
  labelProduct: IProductPrice[] = [];
  imageDefault = 'assets/layout/images/image-bg-default.jpg';
  AppConsts = AppConsts;
  UserConst = UserConst;
  OrderConst = OrderConst;
  language: string = 'en';
  statuses: IDropdown[] = [];
  cols: IColumn[];
  selectedProduct: number;
  totalPrice: number = 0;
  histories: IHistoryView[] = [];
  shippingInfo: IShippingInfo;
  description: string;
  deliveryAddressValue: string = "";
  userInfo: User;

  infoAccount: any;
  disabledDates: any[] = [];
  qrCode: string;
  data: any[];

  constructor(
    injector: Injector,
    messageService: MessageService,
    public ref: DynamicDialogRef,
    private dialogService: DialogService,
    private router: Router,
    private routeActive: ActivatedRoute,
    public confirmationService: ConfirmationService,
    private _orderCustomerService: OrderCustomerService,
    private _selectedLangService: SelectedLanguageService,
    public config: DynamicDialogConfig,
    private _utilsService: AppUtilsService,
  ) {
    super(injector, messageService);
    if (this.routeActive.snapshot.paramMap.get('id')) {
      this.orderId = +this.cryptDecode(this.routeActive.snapshot.paramMap.get('id'));
      this.isEdit = false;
      this.isDetail = true;
    }
    this.userInfo = this.getUser();
    this.data = this._utilsService.getData();
  }

  ngOnInit() {
    if (this.data.length) {
      this.orderDetails = this.data.map(item => {
        return {
          cartId: item?.id,
          id: item.productId,
          name: item.productName,
          category: item.productCategoryName,
          calculationUnitName: item.calculationUnitName,
          unitPrice: item.price,
          quantity: item.quantity,
          price: item.price * item.quantity
        }
      })
      this.setTotalPrice();
      this.optionsAddress();
    }
    this._selectedLangService.getLanguage.subscribe((resLang) => {
      if (resLang) {
        this.language = resLang?.lang;
        this.labels = this._selectedLangService.getKeyLangs(this.parentLangKeys, resLang.keys);
        this.header = this.orderId ? this.labels['ORDER']['ORDER_DETAILS'] : this.labels['ORDER']['ADD_NEW_ORDER'];
      }
    })
    this.statuses = OrderConst.listStatus.map(item => {
      return {
        labelName: item.name[this.language],
        code: item.value
      }
    })
    this.setPostForm();
    if (this.orderId) {
      this.init();
    }
  }
  bank = {
    bankId : "TCB",
    bankNo : "1339017658",
    note : "Thanh toan san pham",
    type : "compact2"
  }
  setTotalPrice() {
    this.totalPrice = this.orderDetails.reduce((accumulator, currentValue) => accumulator + currentValue.price, 0);
    this.qrCode = `https://img.vietqr.io/image/${this.bank.bankId}-${this.bank.bankNo}-${this.bank.type}.jpg?addInfo=${encodeURI(this.bank.note)}&amount=${this.totalPrice}`;
  }

  changeProduct(event, index) {
    this.labelProduct[index] = this.products.find(i => i.id === event?.value);
  }

  changePrice(row, value, argument) {
    if (value && argument) {
      row.price = value * argument;
      this.setTotalPrice();
    }
  }

  setPostForm() {
    this.postForm = new FormGroup({
      id: new FormControl(this.order?.id || null, null),
      orderDate: new FormControl(new Date(), Validators.required),
      deliveryAddressId: new FormControl(this.order?.deliveryAddressId || null, Validators.required),
      invoiceAddress: new FormControl(this.order?.invoiceAddress || null, null),
    });
  }

  setIsEdit() {
    this.isEdit = !this.isEdit;
  }

  init() {
    this.isLoading = true;
    this._orderCustomerService.get(this.orderId).subscribe((res) => {
      this.isLoading = false;
      if (this.handleResponseInterceptor(res, '')) {
        this.order = res.data;
        this.description = this.order?.description;
        this.deliveryAddressValue = this.order.deliveryAddress;
        this.orderDetails = this.order.orderDetails.map(item => {
          return {
            id: item.productId,
            name: item.productName,
            category: item.productCategoryName,
            calculationUnitName: item.calculationUnit,
            unitPrice: item.unitPrice,
            quantity: item.quantity,
            price: item.unitPrice * item.quantity
          }
        })

        this.shippingInfo = {
          trackingCode: this.order.trackingCode,
          shippingProviderName: this.order.shippingProviderName,
          estimatedShippingDate: this.order.estimatedShippingDate ? this.formatDate(this.order.estimatedShippingDate, TypeFormatDatesConst.DMYHm) : ''
        }

        this.setTotalPrice();
        this.setPostForm();
      }
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);

    });
  }

  selectDeliveryAddress() {
    const ref = this.dialogService.open(AddressSelectComponent, {
      header: this.labels['ORDER']['SELECT_ADDRESS'],
      width: '650px',
      contentStyle: { "max-height": "400px", "overflow": "auto" },
      data: {
        language: this.language,
        inputData: this.addresses,
      }
    }).onClose.subscribe(result => {
      if (result) {
        this.postForm.patchValue({
          deliveryAddressId: result.id,
        });
        console.log(' this.postForm ', this.postForm.value);

        this.deliveryAddressValue = result.labelName

      }
    })
  }

  optionsAddress() {
    this.isLoading = true;
    this._orderCustomerService.getAllAddress().subscribe((res) => {
      this.isLoading = false;
      if (this.handleResponseInterceptor(res, '')) {

        this.addresses = res.data.map((item) => {
          return {
            id: item.id,
            labelName: `${item.address} - ${item.phone}`,
            isDefault: item.isDefault
          };
        });
        if (this.addresses) {
          const address = this.addresses.filter(item => item.isDefault == true)[0];
          if (address) {
            this.deliveryAddressValue = address.labelName ?? '';
            this.postForm.patchValue({
              deliveryAddressId: address.id
            });
          }
        }
      }
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);

    });
  }

  deleteItem(index) {
    this.confirmationService.confirm({
      message: this.labels['CONFIRM']['ARE_YOU_SURE_TO_DELETE_THIS_PRODUCT'],
      acceptLabel: this.labels['CONFIRM']['YES'],
      rejectLabel: this.labels['CONFIRM']['NO'],
      accept: () => {
        this.orderDetails.splice(index, 1);
        this.setTotalPrice();
        this.messageSuccess(this.labels['RESPONSE_MSG']['DELETE_SUCCESS']);
      }
    });
  }

  onSubmit() {
    let body = {
      id: null,
      description: this.description,
      deliveryAddressId: this.postForm.value.deliveryAddressId,
      invoiceAddress: this.postForm.value.invoiceAddress,
      orderDate: this.formatCalendarItemSendApi(this.postForm.value.orderDate),
      orderDetails: this.orderDetails.map(item => {
        return {
          productId: item.id,
          cartId : item?.cartId,
          quantity: item.quantity,
          unitPrice: item.unitPrice
        }
      })
    }

    if (this.validateOrderForm(this.postForm) && this.orderDetails.length > 0) {
      if (this.orderId) {
        body.id = this.orderId;
        this._orderCustomerService.update(body).subscribe((res) => {
          if (this.handleResponseInterceptor(res, this.labels['RESPONSE_MSG']['UPDATE_SUCCESS'])) {
            this.router.navigate(['/order-management/retail-orders/view-detail/' + this.cryptEncode(this.orderId)]);
          }
        }, () => {
          this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER'], 3000)
        })
      }
      else {
        this._orderCustomerService.create(body).subscribe((res) => {
          if (this.handleResponseInterceptor(res, this.labels['RESPONSE_MSG']['ADD_SUCCESS'])) {
            if(this.userInfo.user_type == 4 &&  this.selectedPaymentMethod === 2) {
              setTimeout(() => {
                this.vnPay.returnUrl = `http://localhost:4200/account/successful-transaction/${res?.data}`,
                this.createChecksum()
                
              }, 900);
            } else if(this.userInfo.user_type == 4 ) {
              setTimeout(() => {
                this.router.navigate(['/purchase']);
              }, 900);
            }
            
            else {
              setTimeout(() => {
                this.router.navigate(['/order-management/retail-orders']);
              }, 900);
            }
          }
        }, () => {
          this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER'], 3000)
        })
      }

    } else {
      this.messageError(this.labels['RESPONSE_MSG']['PLEASE_ENTER_ENOUGH_INFOR'], 3000)
    }
  }

  vnPay = {
    paymentAmount: 100000,
    endTime:  moment().clone().add(100, 'minutes').format('YYYYMMDDHHmmss'),
    currCode: 'VND',
    ipAddr: '127.0.0.1',
    locale: 'vn',
    command: 'pay',
    orderInfo: 'thanh toan don hang', // Sử dụng encodeURIComponent để mã hóa
    orderType: 'other',
    returnUrl: 'http://localhost:4200/account/successful-transaction/108',
    tmnCode: 'EOFNDP4B',
    txnRef: parseInt(moment().clone().add(1, 'minutes').format('YYYYMMDDHHmmss')),
    version: '2.1.0',
    secretKey: 'HPYMUFVLEOKBKVTLVNZKCCTHBRDQSBAI',
    dataToHash: '',
    apiUrl: 'https://sandbox.vnpayment.vn/paymentv2/vpcpay.html',
    account: '9704198526191432198',
    owner: 'NGUYEN VAN A',
    date: '07/15',
  }

  createChecksum() {
    const secretKey = this.vnPay.secretKey;
    const dataToHash = `vnp_Amount=${this.vnPay.paymentAmount * 100}&vnp_Command=${this.vnPay.command}&vnp_CreateDate=${this.vnPay.endTime}&vnp_CurrCode=${this.vnPay.currCode}&vnp_IpAddr=${this.vnPay.ipAddr}&vnp_Locale=${this.vnPay.locale}&vnp_OrderInfo=Thanh+toan+don+hang+%3A5&vnp_OrderType=${this.vnPay.orderType}&vnp_ReturnUrl=${encodeURIComponent(this.vnPay.returnUrl)}&vnp_TmnCode=${this.vnPay.tmnCode}&vnp_TxnRef=${this.vnPay.txnRef}&vnp_Version=${this.vnPay.version}`;
    const hash = HmacSHA512(dataToHash, secretKey).toString(enc.Hex);
    const vnpUrl = `${this.vnPay.apiUrl}?${dataToHash}&vnp_SecureHash=${hash}`;
    this.openVnpPaymentTab(vnpUrl);
  }

  openVnpPaymentTab(url: string) {
    window.location.href = url;
    // window.open(url, '_blank');
  }

  
// Ngân hàng	NCB
// Số thẻ	9704198526191432198
// Tên chủ thẻ	NGUYEN VAN A
// Ngày phát hành	07/15
// Mật khẩu OTP	123456

  paymentMethods: any[] = [
    // { id: 1, name: 'Thẻ tín dụng' },
    { id:2, name: 'VNPay' },
    { id: 3, name: 'Tiền mặt' }
  ];
  selectedPaymentMethod: number = 3;

  submitPayment(id) {
    this.selectedPaymentMethod = id;
    // if(id !== 3) {
    //   this.messageError('Hiện tại chỉ áp dụng được phương thức thanh toán tiền mặt', 300)
    // }
  }
}

