import { Component, ElementRef, Injector, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { HistoryConst, OrderConst, TypeFormatDatesConst } from "@shared/AppConsts";
import { CrudComponentBase } from "@shared/crud-component-base";
import { IDropdown } from "@shared/interfaces/base-interface";
import { IColumn } from "@shared/interfaces/business-customer.interface";
import { IHistoryOrder, IHistoryView, IOrder, IOrderDetail, IShippingInfo } from "@shared/interfaces/order.interface";
import { OrderCustomerService } from "@shared/services/order-customer.service";
import { OrderService } from "@shared/services/order.service";
import { SelectedLanguageService } from "@shared/services/selected-language.service";
import { MessageService } from "primeng/api";

@Component({
  selector: 'app-view-retail-orders',
  templateUrl: './view-retail-orders.component.html',
  styleUrls: ['./view-retail-orders.component.scss']
})

export class ViewRetailOrdersComponent extends CrudComponentBase {
	order: IOrder;
	language: string = 'en';
	parentLangKeys: string[] = ['RESPONSE_MSG', 'ORDER','CONFIRM'];
	labels: string[] = [];
	statuses: IDropdown[] = [];
	orderId?: number;
	histories: IHistoryView[] = [];
	orderDetails: IOrderDetail[] = [];
	shippingInfo: IShippingInfo;
	cols: IColumn[];
	totalPrice: number = 0;
	qrCode: string;
	TypeFormatDatesConst = TypeFormatDatesConst;
	OrderConst = OrderConst;
	HistoryConst = HistoryConst;

	constructor(
		injector: Injector, 
		messageService: MessageService,
		private _selectedLangService: SelectedLanguageService,
		private routeActive: ActivatedRoute,
    private _orderCustomerService: OrderCustomerService,
	) {
		super(injector, messageService);
		if (this.routeActive.snapshot.paramMap.get('id')) {
			this.orderId = +this.cryptDecode(this.routeActive.snapshot.paramMap.get('id'));
		}
	}

	ngOnInit(): void {
		this._selectedLangService.getLanguage.subscribe((resLang) => {
			if (resLang) {
			  this.language = resLang?.lang;
			  this.labels = this._selectedLangService.getKeyLangs(this.parentLangKeys, resLang.keys);
			}
		  })
		  this.statuses = OrderConst.listStatus.map(item => {
			return {
			  labelName: item.name[this.language],
			  code: item.value
			}
		});
		this.init();
	}

	ngAfterViewChecked() {
		const deliveryAddressElement = document.getElementById("deliveryAddress");
		const invoiceAddressElement = document.getElementById("invoiceAddress");
	  
		if (deliveryAddressElement && invoiceAddressElement) {
			const deliveryAddressHeight = deliveryAddressElement.clientHeight;
			const invoiceAddressHeight = invoiceAddressElement.clientHeight;
			if (deliveryAddressHeight != invoiceAddressHeight) {
				if (deliveryAddressHeight > invoiceAddressHeight) {
					invoiceAddressElement.style.height = deliveryAddressHeight + "px";
				} else {
					deliveryAddressElement.style.height = invoiceAddressHeight + "px";
				}
			} 
		}
		const restaurantNameElement = document.getElementById("restaurantName");
		const orderDateElement = document.getElementById("orderDate");
	  
		if (restaurantNameElement && orderDateElement) {
			const restaurantNameHeight = restaurantNameElement.clientHeight;
			const orderDateHeight = orderDateElement.clientHeight;
			if (restaurantNameHeight != orderDateHeight) {
				if (restaurantNameHeight > orderDateHeight) {
					orderDateElement.style.height = restaurantNameHeight + "px";
				} 
			} 
		}

		const businessCustomerNameElement = document.getElementById("businessCustomerName");
		const statusElement = document.getElementById("status");

		if (businessCustomerNameElement && statusElement) {
			const businessCustomerNameHeight = businessCustomerNameElement.clientHeight;
			const statusHeight = statusElement.clientHeight;
			if (businessCustomerNameHeight > statusHeight) {
				statusElement.style.height = businessCustomerNameHeight + "px";
			} 
		}
	}

	bank = {
		bankId : "TCB",
		bankNo : "1339017658",
		note : "Thanh toan san pham",
		type : "compact2"
	  }
	setTotalPrice() {
		this.totalPrice = this.orderDetails.reduce((accumulator, currentValue) => accumulator + currentValue.price, 0);
		this.qrCode = `https://img.vietqr.io/image/${this.bank.bankId}-${this.bank.bankNo}-${this.bank.type}.jpg?addInfo=${encodeURI(this.bank.note)}&amount=${this.totalPrice}`;
	}
	  
	init() {
		this.isLoading = true;
		this._orderCustomerService.get(this.orderId).subscribe((res) => {
			this.isLoading = false;
			if (this.handleResponseInterceptor(res, '')) {
				this.order = res.data;
				this.histories = this.order?.historyUpdates.map(item => {
					return {
						date: this.formatDate(item.createdDate, TypeFormatDatesConst.DMYHm),
						note: item.action == HistoryConst.CREATE ? this.order.description : item.note,
						action: HistoryConst.getNameHistory(item, item.action , item.oldValue, item.newValue),
						actionTemp: item.action,
						createdUserBy: item?.createdUserBy,
					}
				});
		
				this.orderDetails = this.order.orderDetails.map( item => {
				return {
					id: item.productId,
					name: item.productName,
					category: item.productCategoryName,
					calculationUnitName: item.calculationUnit,
					unitPrice: item.unitPrice,
					quantity: item.quantity,
					price: item.unitPrice * item.quantity
				}
				})
		
				this.shippingInfo = {
				trackingCode: this.order.trackingCode,
				shippingProviderName: this.order.shippingProviderName,
				estimatedShippingDate: this.order.estimatedShippingDate ? this.formatDate(this.order.estimatedShippingDate , TypeFormatDatesConst.DMYHm) : ''
				}
				this.setTotalPrice();
			}
		}, (err) => {
		  this.isLoading = false;
		  console.log('Error-------', err);
		});
	}
}

