import { Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SearchConst, FormNotificationConst, UserConst, OrderConst, TypeFormatDatesConst, localStorageKeyConst, PermissionConst } from '@shared/AppConsts';
import { CrudComponentBase } from '@shared/crud-component-base';
import { Page } from '@shared/model/page';
import { BusinessCustomerService } from '@shared/services/business-customer.service';

import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { debounceTime } from 'rxjs/operators';
import { BreadcrumbService } from 'src/app/layout/breadcrumb/breadcrumb.service';
import { IColumn } from '@shared/interfaces/business-customer.interface';
import { IBaseListAction, IDropdown, IPageInfo } from '@shared/interfaces/base-interface';
import { SelectedLanguageService } from '@shared/services/selected-language.service';
import { TranslateService } from '@ngx-translate/core';
import { IBusinessCustomerDropdown, IRestaurant } from '@shared/interfaces/restaurant.interface';
import { forkJoin } from 'rxjs';
import { OrderService } from '@shared/services/order.service';
import { IOrder } from '@shared/interfaces/order.interface';
import { FormNotificationWithContentComponent } from 'src/app/form-general/form-notification-with-content/form-notification-with-content.component';
import { FormConfirmOrderComponent } from 'src/app/form-general/form-confirm-order/form-confirm-order.component';
import { User } from '@shared/model/user.model';
import { OrderCustomerService } from '@shared/services/order-customer.service';

@Component({
  selector: 'app-retail-orders',
  templateUrl: './retail-orders.component.html',
  styleUrls: ['./retail-orders.component.scss'],
  providers: [DialogService, ConfirmationService, MessageService]
})
export class RetailOrdersComponent extends CrudComponentBase {

    constructor(
        injector: Injector,
        private dialogService: DialogService,
        private confirmationService: ConfirmationService,
        private router: Router,
        messageService: MessageService,
        private breadcrumbService: BreadcrumbService,
        private _orderCustomerService: OrderCustomerService,
        private selectedLanguageService: SelectedLanguageService,
        public _translateService: TranslateService,
    ) {
        super(injector, messageService);
        this.userInfo = this.getUser();
    }
    
    parentLangKeys: string[] = ['HOME', 'MENU', 'RESTAURANT', 'SHARE', 'ROUTER', 'CONFIRM', 'RESPONSE_MSG', 'BUSINESS_CUSTOMER','ORDER'];
    UserConst = UserConst;
    labels: string[] = [];
    ref: DynamicDialogRef;
    rows: IOrder[] = [];
    cols: IColumn[];
    listAction: IBaseListAction[] = [];
    page = new Page();
    offset = 0;
    businessCustomers: IBusinessCustomerDropdown[] = [];
    restaurants: IDropdown[] = [];
    statuses: IDropdown[] = [];
    OrderConst = OrderConst;
    PermissionConst= PermissionConst;
    language: string = 'en';
    userInfo: User;
    filter = {
        status: null,
        businessCustomerId: null,
        dates: null
    }

    scrollHeight: number = 0;

    ngOnInit(): void {
        this.selectedLanguageService.getLanguage.subscribe((resLang) => {
            this.language = resLang?.lang;
            if(resLang) {
                this.labels = this.selectedLanguageService.getKeyLangs(this.parentLangKeys, resLang.keys);
                this.cols = [
                    { field: 'fullName', header: "Khách hàng", width: '20rem', isPin: true, type: '' },
                    { field: `totalMoneyDisplay`, header: this.labels['ORDER']['TOTAL_MONEY'], width: '12rem', isPin: false, type: '', class:'justify-content-end' },
                    { field: `orderDateDisplay`, header: this.labels['ORDER']['ORDER_DATE'], width: '12rem', isPin: false, type: '' },
                    { field: `columnResize`, header: '', width: '', isPin: false, type: 'hidden' },
                ];
                //
                this.breadcrumbService.setItems([
                    { label: this.labels['ROUTER']['HOME_PAGE'], routerLink: ['/home'] },
                    { label: this.labels['ROUTER']['ORDER'], routerLink: ['/order-management/order'] },
                ]);
            }
         
            this.setPage();
        });
        //
        this.subject.keyword.pipe(debounceTime(SearchConst.DEBOUNCE_TIME)).subscribe(() => {
            if (this.keyword === "") {
                this.setPage();
            } else {
                this.setPage();
            }
        });
    }

    showData(rows) {
        for (let row of rows) {      
            row.businessCustomer = this.businessCustomers.find(obj => obj.id === row.businessCustomerId)?.labelName;
            row.restaurant = this.restaurants.find(obj => obj.id === row.restaurantId)?.labelName;
            row.orderDateDisplay = row.orderDate ? this.formatDate(row.orderDate, TypeFormatDatesConst.DMYHm) : '';
            row.totalMoneyDisplay = this.formatCurrency(row?.totalMoney);
        };
    }

    public selectDays(event: any) {  
        if (event && this.filter.dates && this.filter.dates[1]) {
            this.setPage();
        }
    }

    genListAction(data = []) {
        this.listAction = data.map(item => {
            const actions = [];
            if(this.isGranted([PermissionConst.OrderDetail]) ) {
                actions.push({
                    data: item,
                    label: this.labels['ORDER']['ORDER_INFO'],
                    icon: 'pi pi-info-circle',
                    command: ($event) => {
                    this.detail($event.item.data);
                    }
                })
            }

            if (item.status === OrderConst.NEW && (this.isGranted([PermissionConst.OrderUpdate]) || (this.userInfo.user_type === UserConst.ADMIN))){
                actions.push({
                    data: item,
                    label: this.labels['ORDER']['ORDER_EDITING'],
                    icon: 'pi pi-pencil',
                    command: ($event) => {
                    this.edit($event.item.data);
                    }
                })
            }
            if (item.status === OrderConst.PROCESSING && (this.isGranted([PermissionConst.OrderShipping]) || (this.userInfo.user_type === UserConst.ADMIN))){
                actions.push({
                    data: item,
                    label: this.labels['ORDER']['SENT_CONFIRM'],
                    icon: 'pi pi-car',
                    command: ($event) => {
                    this.confirmShip($event.item.data);
                    }
                })
    
            }
            if (item.status === OrderConst.NEW && (this.isGranted([PermissionConst.OrderProcessing]) || (this.userInfo.user_type === UserConst.ADMIN))){
                actions.push({
                    data: item,
                    label: this.labels['ORDER']['PROCESSING'],
                    icon: 'pi pi-check-circle',
                    command: ($event) => {
                    this.processing($event.item.data);
                    }
                })
            }
            if (item.status !== OrderConst.SUCCESS && (this.isGranted([PermissionConst.OrderCancel]) || (this.userInfo.user_type === UserConst.ADMIN))){
                actions.push({
                    data: item,
                    label: this.labels['ORDER']['CANCEL'],
                    icon: 'pi pi-times-circle',
                    command: ($event) => {
                    this.cancel($event.item.data);
                    }
                })
            }
            if (item.status !== OrderConst.SUCCESS && (this.isGranted([PermissionConst.OrderFeedback]) || (this.userInfo.user_type === UserConst.ADMIN))){
                actions.push({
                    data: item,
                    label: this.labels['ORDER']['RESPONSE'],
                    icon: 'pi pi-comment',
                    command: ($event) => {
                    this.response($event.item.data);
                    }
                })
            }
            
            if (item.status !== OrderConst.SUCCESS && (this.isGranted([PermissionConst.OrderDelete]) || (this.userInfo.user_type === UserConst.ADMIN))){
                actions.push({
                    data: item,
                    label: this.labels['SHARE']['DELETE'],
                    icon: 'pi pi-trash',
                    command: ($event) => {
                    this.delete($event.item.data);
                    }
                })
            }

            return actions;
        });
    }

    cancel(item){
        this.dialogService.open(
        FormNotificationWithContentComponent,
            {
                header: this.labels['CONFIRM']['CANCEL_TITLE'],
                width: '400px',
                contentStyle: { "max-height": "600px", "overflow": "auto", "padding-bottom": "50px" },
                styleClass: 'p-dialog-custom',
                baseZIndex: 10000,
                data: {
                    title: this.labels['CONFIRM']['CANCEL_ORDER']
                }
            }
        ).onClose.subscribe((dataCallBack) => {
            if (dataCallBack?.accept) {
                let body = {
                    orderId: item.id,
                    note: dataCallBack.data?.note
                }
                this._orderCustomerService.cancel(body).subscribe(res => {
                    if (this.handleResponseInterceptor(res, this.labels['RESPONSE_MSG']['CANCEL_SUCCESS'])) {
                        this.setPage();
                    }
                }, (err) => {
                    console.log('err____', err);
                    this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER']);
                });
            } 
        });
    }

    response(item){
        const ref = this.dialogService.open(
        FormNotificationWithContentComponent,
            {
                header: this.labels['CONFIRM']['FEEDBACK_TITLE'],
                width: '400px',
                contentStyle: { "max-height": "600px", "overflow": "auto", "padding-bottom": "50px" },
                styleClass: 'p-dialog-custom',
                baseZIndex: 10000,
                data: {
                    title: this.labels['CONFIRM']['FEEDBACK_ORDER']
                }
            }
        );
        ref.onClose.subscribe((dataCallBack) => {
            if (dataCallBack?.accept) {
                let body = {
                    orderId: item.id,
                    note: dataCallBack.data?.note
                }
                this._orderCustomerService.response(body).subscribe(res => {
                    if (this.handleResponseInterceptor(res, this.labels['RESPONSE_MSG']['RESPONSE_SUCCESS'])) {
                        this.setPage();
                    }
                }, (err) => {
                    console.log('err____', err);
                    this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER']);
                });
            } 
        });
    }

    confirmShip(item){
        const ref = this.dialogService.open(
        FormConfirmOrderComponent,
            {
                header: this.labels['CONFIRM']['SHIPPING_CONFIRMATION'],
                width: '600px',
                contentStyle: { "max-height": "600px", "overflow": "auto", "padding-bottom": "50px" },
                styleClass: 'p-dialog-custom',
                baseZIndex: 10000,
            }
        );
        ref.onClose.subscribe((dataCallBack) => {
        if (dataCallBack?.accept) {
                let body = {
                    id: item.id,
                    trackingCode: dataCallBack.data?.trackingCode,
                    shippingProviderId: dataCallBack.data?.shippingProviderId,
                    estimatedShippingDate: this.formatCalendarItemSendApi(dataCallBack.data?.estimatedShippingDate)
                }
                this._orderCustomerService.confirmOrder(body).subscribe(res => {
                    if (this.handleResponseInterceptor(res, this.labels['RESPONSE_MSG']['CONFIRM_SHIPPING_ORDER_SUCCESS'])) {
                        this.setPage();
                    }
                }, (err) => {
                    console.log('err____', err);
                    this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER']);
                });
            } 
        });
    }

    detail(item) {
        this.router.navigate(['/order-management/retail-orders/view-detail/' + this.cryptEncode(item?.id)]);
    }

    create() {
        this.router.navigate(['/order-management/retail-orders/create']);
    }

    edit(item) {
        this.router.navigate(['/order-management/retail-orders/update/' + this.cryptEncode(item?.id)]);
    }

    processing(item){
        this.confirmationService.confirm({
            message: this.labels['CONFIRM']['ARE_YOU_SURE_TO_PROCESSING_THIS_ORDER'],
            icon: "pi pi-check-circle",
            acceptLabel: this.labels['CONFIRM']['YES'],
            rejectLabel: this.labels['CONFIRM']['NO'],
            accept: () => {
                this._orderCustomerService.processing(item.id).subscribe((response) => {
                    if (this.handleResponseInterceptor(response, this.labels['CONFIRM']['DELETE_SUCCESS'])) {
                        this.setPage();
                    }
                });
            }
        });
    }

    delete(item) {
        this.confirmationService.confirm({
            message: this.labels['CONFIRM']['ARE_YOU_SURE_TO_DELETE_THIS_ORDER'],
            acceptLabel: this.labels['CONFIRM']['YES'],
            rejectLabel: this.labels['CONFIRM']['NO'],
            accept: () => {
                this._orderCustomerService.delete(item.id).subscribe((response) => {
                    if (this.handleResponseInterceptor(response, this.labels['CONFIRM']['DELETE_SUCCESS'])) {
                        this.setPage();
                    }
                });
            }
        });
    }

    isRowOdds: boolean = false;
    setPage(event?: IPageInfo) {
        this.isRowOdds = false;
        this.isLoading = true;
        if(event) {
            this.page.pageNumber = event.page;
            this.page.pageSize = event.rows;
        }
        this.page.keyword = this.keyword;
        forkJoin([
            this._orderCustomerService.getAll(this.page, this.filter),
        ]).subscribe(([res, 
        ]) => {
            this.isLoading = false;
            if (this.handleResponseInterceptor(res, '')) {
                this.page.totalItems = res.data.totalItems;
                this.rows = res.data?.items.map( item => {
                    return { ...item, labelName: item.fullName };
                });

                if (res.data?.items?.length) {
                    this.isRowOdds = !(this.rows?.length%2 == 0);
                    this.genListAction(this.rows);
                    this.showData(this.rows);
                }
            }
        }, (err) => {
            this.isLoading = false;
            console.log('Error-------', err);
        });
    }
}




