import { Component, ElementRef, Injector, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { HistoryConst, OrderConst, TypeFormatDatesConst } from "@shared/AppConsts";
import { CrudComponentBase } from "@shared/crud-component-base";
import { IDropdown } from "@shared/interfaces/base-interface";
import { IColumn } from "@shared/interfaces/business-customer.interface";
import { IHistoryOrder, IHistoryView, IOrder, IOrderDetail, IShippingInfo } from "@shared/interfaces/order.interface";
import { OrderService } from "@shared/services/order.service";
import { SelectedLanguageService } from "@shared/services/selected-language.service";
import { MessageService } from "primeng/api";

@Component({
	selector: "app-view-order",
	templateUrl: "./view-order.component.html",
	styleUrls: ["./view-order.component.scss"],
})

export class ViewOrderComponent extends CrudComponentBase {
	order: IOrder;
	language: string = 'en';
	parentLangKeys: string[] = ['RESPONSE_MSG', 'ORDER','CONFIRM', 'HISTORY'];
	labels: string[] = [];
	statuses: IDropdown[] = [];
	orderId?: number;
	histories: IHistoryView[] = [];
	orderDetails: IOrderDetail[] = [];
	shippingInfo: IShippingInfo;
	cols: IColumn[];
	totalPrice: number = 0;
	
	TypeFormatDatesConst = TypeFormatDatesConst;
	OrderConst = OrderConst;
	HistoryConst = HistoryConst;

	constructor(
		injector: Injector, 
		messageService: MessageService,
		private _selectedLangService: SelectedLanguageService,
		private routeActive: ActivatedRoute,
		private _orderService: OrderService,
	) {
		super(injector, messageService);
		if (this.routeActive.snapshot.paramMap.get('id')) {
			this.orderId = +this.cryptDecode(this.routeActive.snapshot.paramMap.get('id'));
		}
	}

	ngOnInit(): void {
		this._selectedLangService.getLanguage.subscribe((resLang) => {
			if (resLang) {
			  this.language = resLang?.lang;
			  this.labels = this._selectedLangService.getKeyLangs(this.parentLangKeys, resLang.keys);
			}
		})

		this.init();
	}

	ngAfterViewChecked() {
		const deliveryAddressElement = document.getElementById("deliveryAddress");
		const invoiceAddressElement = document.getElementById("invoiceAddress");
	  
		if (deliveryAddressElement && invoiceAddressElement) {
			const deliveryAddressHeight = deliveryAddressElement.clientHeight;
			const invoiceAddressHeight = invoiceAddressElement.clientHeight;
			if (deliveryAddressHeight != invoiceAddressHeight) {
				if (deliveryAddressHeight > invoiceAddressHeight) {
					invoiceAddressElement.style.height = deliveryAddressHeight + "px";
				} else {
					deliveryAddressElement.style.height = invoiceAddressHeight + "px";
				}
			} 
		}
		const restaurantNameElement = document.getElementById("restaurantName");
		const orderDateElement = document.getElementById("orderDate");
	  
		if (restaurantNameElement && orderDateElement) {
			const restaurantNameHeight = restaurantNameElement.clientHeight;
			const orderDateHeight = orderDateElement.clientHeight;
			if (restaurantNameHeight != orderDateHeight) {
				if (restaurantNameHeight > orderDateHeight) {
					orderDateElement.style.height = restaurantNameHeight + "px";
				} 
			} 
		}

		const businessCustomerNameElement = document.getElementById("businessCustomerName");
		const statusElement = document.getElementById("status");

		if (businessCustomerNameElement && statusElement) {
			const businessCustomerNameHeight = businessCustomerNameElement.clientHeight;
			const statusHeight = statusElement.clientHeight;
			if (businessCustomerNameHeight > statusHeight) {
				statusElement.style.height = businessCustomerNameHeight + "px";
			} 
		}
	}

	setTotalPrice() {
		this.totalPrice = this.orderDetails.reduce((accumulator, currentValue) => accumulator + currentValue.price, 0);
	}
	  
	init() {
		this.isLoading = true;
		this._orderService.get(this.orderId).subscribe((res) => {
			this.isLoading = false;
			if (this.handleResponseInterceptor(res, '')) {
				this.order = res.data;
				this.histories = this.order?.historyUpdates.map(item => {
					return {
						date: this.formatDate(item.createdDate, TypeFormatDatesConst.DMYHm),
						note: item.action == HistoryConst.CREATE ? this.order.description : item.note,
						actionName: HistoryConst.getNameHistory(item, item.action , item.oldValue, item.newValue),
						action: item.action,
						createdUserBy: item?.createdUserBy,
					}
				});
		
				this.orderDetails = this.order.orderDetails.map( item => {
				return {
					id: item.productId,
					name: item.productName,
					category: item.productCategoryName,
					calculationUnitName: item.calculationUnit,
					unitPrice: item.unitPrice,
					quantity: item.quantity,
					price: item.unitPrice * item.quantity
				}
				})
		
				this.shippingInfo = {
				trackingCode: this.order.trackingCode,
				shippingProviderName: this.order.shippingProviderName,
				estimatedShippingDate: this.order.estimatedShippingDate ? this.formatDate(this.order.estimatedShippingDate , TypeFormatDatesConst.DMYHm) : ''
				}
				this.setTotalPrice();
			}
		}, (err) => {
		  this.isLoading = false;
		  console.log('Error-------', err);
		});
	}
}
