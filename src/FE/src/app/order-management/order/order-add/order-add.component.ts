import { Component, Injector } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConsts, HistoryConst, OrderConst, TypeFormatDatesConst, UserConst } from '@shared/AppConsts';
import { CrudComponentBase } from '@shared/crud-component-base';
import { IDropdown } from '@shared/interfaces/base-interface';
import { IBusinessCustomer, IColumn } from '@shared/interfaces/business-customer.interface';
import { IHistoryView, IOrder, IOrderDetail, IShippingInfo } from '@shared/interfaces/order.interface';
import { IProductPrice } from '@shared/interfaces/product-price.interface';
import { IProduct } from '@shared/interfaces/product.interface';
import { IAddress, IBusinessCustomerDropdown, IRestaurant } from '@shared/interfaces/restaurant.interface';
import { IUser } from '@shared/interfaces/user-manager.interface';
import { Page } from '@shared/model/page';
import { UserDefault } from '@shared/model/user';
import { User } from '@shared/model/user.model';
import { OrderService } from '@shared/services/order.service';
import { SelectedLanguageService } from '@shared/services/selected-language.service';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { DeliveryAddressSelectComponent } from 'src/app/business-customers-management/restaurant/restaurant-detail/delivery-address/delivery-address-select/delivery-address-select.component';

@Component({
  selector: 'app-order-add',
  templateUrl: './order-add.component.html',
  styleUrls: ['./order-add.component.scss']
})
export class OrderAddComponent extends CrudComponentBase {

  page = new Page();
  postForm?: FormGroup;
  businessCustomers: IBusinessCustomerDropdown[] = [];
  restaurants: IRestaurant[] = [];
  products: IProductPrice[] = [];
  addresses: IAddress[] = [];
  parentLangKeys: string[] = ['RESPONSE_MSG', 'ORDER', 'CONFIRM'];
  labels: string[] = [];
  orderDetails: IOrderDetail[] = [];
  orderId?: number;
  order: IOrder;
  fieldDates = ['orderDate'];
  isEdit: boolean = false;
  isDetail: boolean = false;
  header: string;
  labelProduct: IProductPrice[] = [];
  imageDefault = 'assets/layout/images/image-bg-default.jpg';
  AppConsts = AppConsts;
  UserConst = UserConst;
  OrderConst = OrderConst;
  language: string = 'en';
  statuses: IDropdown[] = [];
  cols: IColumn[];
  selectedProduct: number;
  totalPrice: number = 0;
  histories: IHistoryView[] = [];
  shippingInfo: IShippingInfo;
  description: string;
  deliveryAddressValue: string = "";
  userInfo: User;

  infoAccount: any;
  disabledDates: any[] =[];

  constructor(
    injector: Injector,
    messageService: MessageService,
    public ref: DynamicDialogRef,
    private dialogService: DialogService,
    private router: Router,
    private routeActive: ActivatedRoute,
    public confirmationService: ConfirmationService,
    private _orderService: OrderService,
    private _selectedLangService: SelectedLanguageService,
  ) {
    super(injector, messageService);
    if (this.routeActive.snapshot.paramMap.get('id')) {
      this.orderId = +this.cryptDecode(this.routeActive.snapshot.paramMap.get('id'));
      this.isEdit = false;
      this.isDetail = true;
    }
    this.userInfo = this.getUser();
  }

  ngOnInit() {
    this._selectedLangService.getLanguage.subscribe((resLang) => {
      if (resLang) {
        this.language = resLang?.lang;
        this.labels = this._selectedLangService.getKeyLangs(this.parentLangKeys, resLang.keys);
        this.header = this.orderId ? this.labels['ORDER']['ORDER_DETAILS'] : this.labels['ORDER']['ADD_NEW_ORDER'];
      }
    })
    this.setPostForm();
    if (this.orderId) {
      this.init();
    }


    this.optionsBusiness();
    this.optionsRestaurant();
    this.optionsCalendar(new Date().getFullYear());
    if(this.userInfo.user_type !== UserConst.ADMIN) {
      this.getInfoAccount();
    }
  }

  setTotalPrice() {
    this.totalPrice = this.orderDetails.reduce((accumulator, currentValue) => accumulator + currentValue.price, 0);
  }

  changeProduct(event, index) {
    console.log("event___", event);
    console.log("this.products", this.products);

    this.labelProduct[index] = this.products.find(i => i.id === event?.value);
    console.log('!!! labelProduct ', this.labelProduct);

  }

  setStatusEdit() {
    this.isEdit = !this.isEdit;
  }

  addProduct() {
    const result = this.products.filter(item => item.id == this.selectedProduct);
    if (result) {
      this.orderDetails.push({
        id: result[0].id,
        name: result[0].product.name,
        category: result[0].product.productCategoryName,
        calculationUnitName: result[0].product.calculationUnitName,
        unitPrice: result[0].price,
        quantity: 1,
        price: result[0].price
      });
      console.log('selectedProduct ', this.selectedProduct);

    }

    this.setTotalPrice();
  }

  changePrice(row, value, argument) {
    if (value && argument) {
      row.price = value * argument;
      this.setTotalPrice();
    }
  }

  setPostForm() {
    console.log('!!! this.order ', this.order);

    this.postForm = new FormGroup({
      id: new FormControl(this.order?.id || null, null),
      restaurantId: new FormControl(this.order?.restaurantId || null, Validators.required),
      orderDate: new FormControl(this.order?.orderDate ? new Date(this.order?.orderDate) : new Date(), Validators.required),
      businessCustomerId: new FormControl(this.order?.businessCustomerId || null, Validators.required),
      deliveryAddressId: new FormControl(this.order?.deliveryAddressId || null, Validators.required),
      invoiceAddress: new FormControl(this.order?.invoiceAddress || null, Validators.required),
    });
  }

  setIsEdit() {
    this.isEdit = !this.isEdit;
  }

  init() {
    this.isLoading = true;
    this._orderService.get(this.orderId).subscribe((res) => {
      this.isLoading = false;
      if (this.handleResponseInterceptor(res, '')) {
        this.order = res.data;
        this.description = this.order?.description;

        this.orderDetails = this.order.orderDetails.map(item => {
          return {
            id: item.productId,
            name: item.productName,
            category: item.productCategoryName,
            calculationUnitName: item.calculationUnit,
            unitPrice: item.unitPrice,
            quantity: item.quantity,
            price: item.unitPrice * item.quantity
          }
        })

        this.shippingInfo = {
          trackingCode: this.order.trackingCode,
          shippingProviderName: this.order.shippingProviderName,
          estimatedShippingDate: this.order.estimatedShippingDate ? this.formatDate(this.order.estimatedShippingDate, TypeFormatDatesConst.DMYHm) : ''
        }

        this.setTotalPrice();

        this.optionsAddress(this.order.restaurantId)
        this.optionsProduct(this.order.businessCustomerId);

        this.setPostForm();
      }
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);

    });
  }

  selectDeliveryAddress() {
    if (this.postForm.value.restaurantId) {
      const ref = this.dialogService.open(DeliveryAddressSelectComponent, {
        header: this.labels['ORDER']['SELECT_ADDRESS'],
        width: '650px',
        contentStyle: { "max-height": "400px", "overflow": "auto" },
        data: {
          restaurantId: this.postForm.value.restaurantId,
          language: this.language,
          inputData: this.addresses,
        }
      }).onClose.subscribe(result => {
        if (result) {
          this.postForm.patchValue({
            deliveryAddressId: result.id,
          });
          console.log(' this.postForm ', this.postForm.value);

          this.deliveryAddressValue = result.labelName

        }
      })
    } else {
      this.messageError(this.labels['ORDER']['NO_RESTAURANT']);
    }

  }

  resetDeliverryAddress() {
    this.postForm.patchValue({
      deliveryAddressId: null,
    });
    this.deliveryAddressValue = ''
  }

  optionsAddress(id) {
    this.isLoading = true;
    this._orderService.getAllAddress(id).subscribe((res) => {
      this.isLoading = false;
      if (this.handleResponseInterceptor(res, '')) {

        this.addresses = res.data.map((item) => {
          return {
            id: item.id,
            labelName: `${item.address} - ${item.phone}`,
            isDefault: item.isDefault
          };
        });
        if (this.addresses) {
          const address = this.addresses.filter(item => item.isDefault == true)[0];
          if (address) {
            this.deliveryAddressValue = address.labelName ?? '';
            this.postForm.patchValue({
              deliveryAddressId: address.id
            });
          }
        }
      }
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);

    });
  }

  setInvoiceAddress(businessCustomerId) {
    const address = this.businessCustomers.filter(item => item.id == businessCustomerId)[0]?.address;

    if (address) {
      this.postForm.patchValue({
        invoiceAddress: address
      });
    }
    console.log('!!! postForm ', this.postForm.value);

  }

  optionsProduct(businessCustomerId?) {
    this.isLoading = true;
    this._orderService.getAllProduct(businessCustomerId).subscribe((res) => {
      this.isLoading = false;
      if (this.handleResponseInterceptor(res, '')) {

        this.products = res.data?.map((item) => {
          return {
            ...item,
            id: item.productId,
            labelName: item.product.code + ' - ' + item.product.name + ' - ' + item.price,
            image: item?.product?.image,
            unitPrice: item?.price
          };
        });
      }
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);

    });
  }

  optionsRestaurant(id?: number) {
    this.isLoading = true;
    this._orderService.getAllRestaurant(id).subscribe((res) => {
      this.isLoading = false;
      if (this.handleResponseInterceptor(res, '')) {

        this.restaurants = res.data?.items.map((item) => {
          return {
            id: item.id,
            labelName: item.name
          };
        });
      }
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);

    });
  }

  getInfoAccount() {
    this.isLoading = true;
    this._orderService.getInfoAccount().subscribe((res) => {
      this.isLoading = false;
      if (this.handleResponseInterceptor(res, '')) {
        console.log("res",res);
        const { id, fullName }: IBusinessCustomer = res.data;
        this.infoAccount = { id, fullName };
       
        if(this.userInfo.user_type === UserConst.BUSINESS_CUSTOMER) {
          this.postForm.patchValue({
            businessCustomerId: res.data.id,
            invoiceAddress: res.data.address
          });
          this.optionsRestaurant(res.data.id);
          this.optionsProduct(res.data.id);
        } else if(this.userInfo.user_type === UserConst.RESTAURANT) {
          this.postForm.patchValue({
            businessCustomerId: res.data.businessCustomerId,
            restaurantId: res.data.id,
            invoiceAddress: res.data.businessCustomer.address,
          });
          this.optionsRestaurant(res.data.businessCustomerId);
          this.optionsProduct(res.data.businessCustomerId);
          this.optionsAddress(res.data.id);
        }

      }
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);

    });
  }
  handleYearChange(event) {
    this.optionsCalendar(event.year)
    
  }

  optionsCalendar(year) {
    this.isLoading = true;
    this._orderService.getCalendar(year).subscribe((res) => {
      this.isLoading = false;
      if (this.handleResponseInterceptor(res, '')) {
        this.disabledDates = res.data.map(item => new Date(item.date));
      }
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);

    });
  }

  optionsBusiness() {
    this.isLoading = true;
    this._orderService.getAllBusinessCustomer().subscribe((res) => {
      this.isLoading = false;
      if (this.handleResponseInterceptor(res, '')) {

        this.businessCustomers = res.data?.items.map((item) => {
          return {
            id: item.id,
            labelName: item.fullName,
            address: item.address
          };
        });
      }
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);

    });
  }

  deleteItem(index) {
    this.confirmationService.confirm({
      message: this.labels['CONFIRM']['ARE_YOU_SURE_TO_DELETE_THIS_PRODUCT'],
      acceptLabel: this.labels['CONFIRM']['YES'],
      rejectLabel: this.labels['CONFIRM']['NO'],
      accept: () => {
        this.orderDetails.splice(index, 1);
        this.setTotalPrice();
        this.messageSuccess(this.labels['RESPONSE_MSG']['DELETE_SUCCESS']);
      }
    });
  }




  onSubmit() {
    let body = {
      id: null,
      restaurantId: this.postForm.value.restaurantId,
      description: this.description,
      deliveryAddressId: this.postForm.value.deliveryAddressId,
      invoiceAddress: this.postForm.value.invoiceAddress,
      orderDate: this.formatCalendarItemSendApi(this.postForm.value.orderDate),
      orderDetails: this.orderDetails.map(item => {
        return {
          productId: item.id,
          quantity: item.quantity,
          unitPrice: item.unitPrice
        }
      })
    }
    console.log('body ', body);

    if (this.validateOrderForm(this.postForm) && this.orderDetails.length > 0) {
      if (this.orderId) {
        body.id = this.orderId;
        this._orderService.update(body).subscribe((res) => {
          if (this.handleResponseInterceptor(res, this.labels['RESPONSE_MSG']['UPDATE_SUCCESS'])) {
            this.router.navigate(['/order-management/order/view-detail/' + this.cryptEncode(res?.data?.id)]);
          }
        }, () => {
          this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER'], 3000)
        })
      }
      else {
        this._orderService.create(body).subscribe((res) => {
          if (this.handleResponseInterceptor(res, this.labels['RESPONSE_MSG']['ADD_SUCCESS'])) {
            this.router.navigate(['/order-management/order/view-detail/' + this.cryptEncode(res?.data?.id)]);
          }
        }, () => {
          this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER'], 3000)
        })
      }

    } else {
      this.messageError(this.labels['RESPONSE_MSG']['PLEASE_ENTER_ENOUGH_INFOR'], 3000)
    }
  }

}
