import { Component, Injector, Input, OnInit, SimpleChanges } from '@angular/core';
import { AppConsts, PermissionConst, UserConst } from '@shared/AppConsts';
import { MessageService } from 'primeng/api';
import { AppMainComponent } from '../main/app.main.component';
import { AppComponentBase } from '@shared/app-component-base';
import { CookieService } from 'ngx-cookie-service';
import { TranslateService } from '@ngx-translate/core';
import { SelectedLanguageService } from '@shared/services/selected-language.service';
import { IResponseLang } from '@shared/interfaces/base-interface';
import { User } from '@shared/model/user.model';
import { SignalrService } from '@shared/services/signalr.service';
import { CrudComponentBase } from '@shared/crud-component-base';
import { NotificationService } from '@shared/services/notification.service';
@Component({
    selector: 'app-menu',
    templateUrl: './app.menu.component.html',
    styleUrls: ['./app.menu.component.scss'],
})
export class AppMenuComponent extends CrudComponentBase {
    protected _cookieService: CookieService;
    model: any[];
    checkPartner: any;
    AppConsts = AppConsts;
    labelCheck: string;
    routerLinkCheck: string;
    constructor(
        public appMain: AppMainComponent,
        injector: Injector,
        messageService: MessageService,
        cookieService: CookieService,
        public _translateService: TranslateService,
        private notificationService: NotificationService,
        private selectedLanguageService: SelectedLanguageService,
        private _signalrService: SignalrService,
    ) {
        super(injector, messageService);
        this._cookieService = cookieService;
        this.userInfo = this.getUser();
        this.selectedLanguageService.getLanguage.subscribe((res: IResponseLang) => {
            if (res) {
                this.labels = this.selectedLanguageService.getKeyLangs(this.parentLangKeys, res.keys);
                this.getKeys()
            }
            //xu ly goi lại cac api tai 1 vi tri thay cho ngOnInit
        });
    }

    callAPI() {
        this.getKeys();
    }

    @Input() permissionsMenu: any[] = [];
    labels: string[] = [];

    userInfo: User;
    parentLangKeys: string[] = ['MENU'];
    keys = ['MENU.DASHBOARD', 'MENU.BASIC_FORM', 'MENU.SYSTEM_MANAGEMENT', 'MENU.ROLE_CONFIGURATION',
        'MENU.ACCOUNT_CONFIGURATION', 'MENU.BUSINESS_CUSTOMER_MANAGEMENT', 'MENU.BUSINESS_CUSTOMER', 'MENU.RESTAURANT',
        'MENU.PRODUCT_MANAGER', 'MENU.PRODUCT_CATEGORY', 'MENU.PRODUCT', 'MENU.GROUP_CUSTOMER', 'MENU.ORDER', 'MENU.CALENDAR_CONFIGURATION'];

    infoNotSeen: any = {
        notificationNumber: 0,
        messageNumber: 0,
    };
    getKeys() {
        this._translateService.get(this.keys).subscribe(res => {
            this.model = [
                { label: `${this.labels['MENU']['DASHBOARD']}`, icon: 'pi pi-home', routerLink: ['/home'], isShow: this.userInfo.user_type !== 4 },
                { label: this.userInfo.user_type == UserConst.BUSINESS_CUSTOMER ? `${this.labels['MENU']['BUSINESS_INFORMATION']}` : `${this.labels['MENU']['RESTAURANT_INFORMATION']}`, 
                    icon: 'pi pi-user', routerLink: ['/business-detail'], isShow: UserConst.businessRestaurant.includes(this.userInfo.user_type) },
                {
                    label: `${this.labels['MENU']['SYSTEM_MANAGEMENT']}`, icon: 'pi pi-cog', routerLink: ['/system-manager'], isShow: this.isPermission(PermissionConst.SystemModule) || this.userInfo.user_type === 1,
                    items: [
                        { label: `${this.labels['MENU']['ROLE_CONFIGURATION']} `, icon: '', routerLink: ['/system-manager/role-manager'], isShow: this.isPermission(PermissionConst.RoleMenu) || this.userInfo.user_type === 1 },
                        { label: `${this.labels['MENU']['ACCOUNT_CONFIGURATION']}`, icon: '', routerLink: ['/system-manager/user-manager'], isShow: this.isPermission(PermissionConst.UserMenu) || this.userInfo.user_type === 1 },
                        { label: `${this.labels['MENU']['CALENDAR_CONFIGURATION']}`, icon: '', routerLink: ['/system-manager/calendar'], isShow: this.isPermission(PermissionConst.CalendarMenu) || this.userInfo.user_type === 1 },
                        { label: `${this.labels['MENU']['DOCUMENT']}`, icon: '', routerLink: ['/system-manager/document'], isShow: UserConst.adminBusiness.includes(this.userInfo.user_type) },
                    ]
                },
                {
                    label: `${this.labels['MENU']['PRODUCT_MANAGER']}`, icon: 'pi pi-map', routerLink: ['/product-management'], isShow: this.isPermission(PermissionConst.ProductManagementModule) || this.userInfo.user_type === 1,
                    items: [
                        { label: `${this.labels['MENU']['CALCULATION_UNIT']}`, icon: '', routerLink: ['/product-management/calculation-unit'], isShow: this.isPermission(PermissionConst.CalculationUnitMenu) || this.userInfo.user_type === 1 },
                        { label: `Hãng vận chuyển`, icon: '', routerLink: ['/product-management/shipping-company'], isShow: this.isPermission(PermissionConst.CalculationUnitMenu) },
                        { label: `${this.labels['MENU']['PRODUCT_CATEGORY']}`, icon: '', routerLink: ['/product-management/product-category'], isShow: this.isPermission(PermissionConst.ProductCategoryMenu) || this.userInfo.user_type === 1 },
                        { label: `${this.labels['MENU']['PRODUCT']}`, icon: '', routerLink: ['/product-management/product-manager'], isShow: this.isPermission(PermissionConst.ProductMenu) || this.userInfo.user_type === 1 },
                    ]
                },
                {
                    label: `Quản lý thông báo`, icon: 'pi pi-map', routerLink: ['/notification-management'], isShow: this.userInfo.user_type === 1,
                    items: [
                        { label: `Thông báo hệ thống`, icon: '', routerLink: ['/notification-management/system-notification'], isShow: this.userInfo.user_type === 1 },
                    ]
                },
                 {
                    label: `Truyền thông`, icon: 'pi pi-send', routerLink: ['/media'], isShow: this.userInfo.user_type === 1,
                },
                {
                    label: `${this.labels['MENU']['BUSINESS_CUSTOMER_MANAGEMENT']}`, icon: 'pi pi-users', routerLink: ['/business-customer-management'], isShow: this.isPermission(PermissionConst.BusinessCustomerModule) || this.userInfo.user_type === 1,
                    items: [
                        { label: `${this.labels['MENU']['BUSINESS_CUSTOMER']}`, icon: '', routerLink: ['/business-customer-management/business-customer'], isShow: this.isPermission(PermissionConst.BusinessCustomerMenu) || this.userInfo.user_type === 1 },
                        { label: `${this.labels['MENU']['RESTAURANT']}`, icon: '', routerLink: ['/business-customer-management/restaurant'], isShow: this.isPermission(PermissionConst.RestaurantMenu) || this.userInfo.user_type === 1 },
                        { label: `${this.labels['MENU']['GROUP_CUSTOMER']}`, routerLink: ['/business-customer-management/group-customer'], isShow: this.isPermission(PermissionConst.GroupCustomerMenu || this.userInfo.user_type === 1) },
                    ]
                },
                {
                    label: `${this.labels['MENU']['ORDER_MANAGEMENT']}`, icon: 'pi pi-book', routerLink: ['/order-management'], isShow: this.isPermission(PermissionConst.OrderModule) || this.userInfo.user_type === 1,
                    items: [
                        // Wholesale orders
                        { label: `${this.labels['MENU']['ORDER']}`, icon: '', routerLink: ['/order-management/order'], isShow: this.isPermission(PermissionConst.OrderMenu) || this.userInfo.user_type === 1 },
                        { label: `Đơn hàng lẻ`, icon: '', routerLink: ['/order-management/retail-orders'], isShow: this.userInfo.user_type === 1 },
                    ]
                },
                {
                    label: 'Báo cáo thống kê', icon: 'pi pi-file', routerLink: ['/export-report'], isShow: this.userInfo.user_type === 1,
                    items: [
                        { label: 'Báo cáo quản trị', icon: '', routerLink: ['/export-report/management-report'], isShow: this.userInfo.user_type === 1 },
                        // { label: 'Báo cáo vận hành', icon: '', routerLink: ['/export-report/operational-report'], isShow: this.userInfo.user_type === 1 },
                        // { label: 'Báo cáo kinh doanh', icon: '', routerLink: ['/export-report/business-report'], isShow: this.userInfo.user_type === 1 }
                    ]
                },
                // {
                //     label: `Sản phẩm`, icon: 'pi pi-database', routerLink: ['/product'], isShow: this.userInfo.user_type === 4,
                // },

                {
                    label: `Giỏ hàng`, icon: 'pi pi-shopping-cart', routerLink: ['/cart'], isShow: this.userInfo.user_type === 4,
                },
                {
                    label: `Đơn mua`, icon: 'pi pi-list', routerLink: ['/purchase'], isShow: this.userInfo.user_type === 4,
                },
                {
                    label: `Thông báo`, icon: 'pi pi-bell', routerLink: ['/notification'], isShow: this.userInfo.user_type === 4 || this.userInfo.user_type === 1,
                },
                {
                    label: `Hỗ trợ`, icon: 'pi pi-comments', routerLink: ['/chat'], isShow: this.userInfo.user_type === 4 || this.userInfo.user_type === 1,
                },
                {
                    label: `Tài khoản của tôi`, icon: 'pi pi-user', routerLink: ['/user-account'], isShow: this.userInfo.user_type === 4,
                    items: [
                        { label: `Hồ sơ`, icon: '', routerLink: ['/user-account/profile'], isShow: this.userInfo.user_type === 4 },
                        // { label: `Ngân hàng`, icon: '', routerLink: ['/user-account/bank'], isShow: this.userInfo.user_type === 4 },
                        { label: `Địa chỉ`, routerLink: ['/user-account/address'], isShow: this.userInfo.user_type === 4 },
                    ]
                },
            ];
        })
    }


    ngOnInit() {
        this.setPage();
        this.startSignalR();
    }

    setPage(isLoading: boolean = true) {
        this.getKeys();
        this.isLoading = isLoading;
        this.notificationService.GetInfoNotSeenNotification().subscribe((res) => {
            this.isLoading = false;
            if (this.handleResponseInterceptor(res, '')) {
                this.infoNotSeen = res.data;
                this.model.filter(e => {
                    if(e?.icon === 'pi pi-bell' && this.infoNotSeen.notificationNumber > 0) {
                        e.label = `Thông báo (${this.infoNotSeen.notificationNumber})`;
                    } 
                    // else {
                    //     e.label = `Thông báo`;
                    // }
                })
            }
        }, (err) => {
            this.isLoading = false;
            console.log('Error-------', err);
        });
    }

    // ngAfterViewInit(): void {
    //     this.setPage();
    // }

    getChangeMenu() {

    }

    private startSignalR() {
        this._signalrService.startConnection()
            .then(() => {
                this._signalrService.listen('ReadNotification', (data) => {
                    console.log("data", data);
                    setTimeout(() => {
                        this.setPage(false);
                    }, 2000);
                });
            })
            .catch((error) => {
                console.log("Error while connecting to SignalR:", error);
            });
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes?.permissionsMenu) {
            this.getKeys();
        }
        this.setPage();
    }

    onMenuClick() {
        this.appMain.menuClick = true;
    }

    isPermission(keyName) {

        // return true;
        return this.permissionsMenu.includes(keyName);
    }
}
