import { MessageService } from 'primeng/api';
import { UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { AppSessionService } from '@shared/session/app-session.service';
import {Component, ElementRef, ViewChild} from '@angular/core';
import { AppAuthService } from '@shared/auth/app-auth.service';
import { AppMainComponent } from '../main/app.main.component';
import { SelectedLanguageService } from '@shared/services/selected-language.service';
import { TranslateService } from '@ngx-translate/core';
import { AppConsts, localStorageKeyConst } from '@shared/AppConsts';
import { ConfigLanguage } from '@shared/consts/config-language';
import { Router } from '@angular/router';

@Component({
    selector: 'app-topbar',
	templateUrl: './app.topbar.component.html',
	styleUrls: ['./app.topbar.component.scss'],
})
export class AppTopBarComponent {

	userDialog: boolean;
    activeItem: number;
	userInfo: any = {};
	user = {};
	selectLanguage: boolean = false;
	countries: any = [
		{
			name: "English",
			code: ConfigLanguage.ENGLISH,
			img: 'assets/layout/images/Flag_of_England.svg',
		},
		{
			name: "Japan",
			code: ConfigLanguage.JAPAN,
			img: "assets/layout/images/Flag_of_Japan.svg",
		},
		{
			name: "VietNam",
			code: ConfigLanguage.VIETNAM,
			img: "assets/layout/images/Flag_of_Vietnam.svg",
		},
	];
	selectedCountry = {
		code: '',
        name:'',
        img:'',
	};

	model: any[];

    constructor(
		public appMain: AppMainComponent,
		private authService: AppAuthService,
		private _appSessionService: AppSessionService,
		private userService: UserServiceProxy,
		private messageService: MessageService,
		private selectedLanguageService: SelectedLanguageService,
		private translate: TranslateService,
		private router: Router,
		) {}

    mobileMegaMenuItemClick(index) {
        this.appMain.megaMenuMobileClick = true;
        this.activeItem = this.activeItem === index ? null : index;
    }

	@ViewChild('topbarEl') topbarEl: ElementRef<HTMLElement>;
	ngAfterViewInit() {
		setTimeout(() => {
			let topbarHeight = this.topbarEl?.nativeElement?.offsetHeight;
                localStorage.setItem(localStorageKeyConst.topbarHeight, topbarHeight.toString());
                this.countHeightLayoutTop(topbarHeight);
		}, 0);
	}

	countHeightLayoutTop(topbarHeight) {
        let layoutTopHeight = (+localStorage?.getItem(localStorageKeyConst.breadcrumHeight) || 0) + topbarHeight;
        localStorage.setItem(localStorageKeyConst.layoutTopHeight, layoutTopHeight.toString());
    }

	switchLanguage(language) {		
		AppConsts.localization = this.selectedCountry;
		localStorage.setItem('selectedCountry', JSON.stringify(this.selectedCountry));
		//
		localStorage.setItem(AppConsts.localStorageLangKey, language.code);
		this.translate.setDefaultLang(language.code);
		this.translate.use(language.code);
		this.setLang(language.code);
	}

	ngOnInit() {
		this._appSessionService.getUserObs.subscribe((user) => {
			if(user) this.userInfo = user;
		}) 

		this.selectedCountry = AppConsts.localization;
		this.setLang();
		 this.model = [
                {
                    label: `Sản phẩm`, icon: 'pi pi-database', routerLink: ['/product'], isShow: this.userInfo.user_type === 4,
                },

                {
                    label: `Giỏ hàng`, icon: 'pi pi-shopping-cart', routerLink: ['/cart'], isShow: this.userInfo.user_type === 4,
                },
                {
                    label: `Đơn mua`, icon: 'pi pi-list', routerLink: ['/purchase'], isShow: this.userInfo.user_type === 4,
                },
                {
                    label: `Thông báo`, icon: 'pi pi-bell', routerLink: ['/notification'], isShow: this.userInfo.user_type === 4 || this.userInfo.user_type === 1,
                },
                {
                    label: `Hỗ trợ`, icon: 'pi pi-comments', routerLink: ['/chat'], isShow: this.userInfo.user_type === 4 || this.userInfo.user_type === 1,
                },
                {
                    label: `Tài khoản của tôi`, icon: 'pi pi-user', routerLink: ['/user-account'], isShow: this.userInfo.user_type === 4,
                    items: [
                        { label: `Hồ sơ`, icon: '', routerLink: ['/user-account/profile'], isShow: this.userInfo.user_type === 4 },
                        { label: `Ngân hàng`, icon: '', routerLink: ['/user-account/bank'], isShow: this.userInfo.user_type === 4 },
                        { label: `Địa chỉ`, routerLink: ['/user-account/address'], isShow: this.userInfo.user_type === 4 },
                    ]
                },
            ];
	}

	setLang(lang?: string) {
		this.selectedLanguageService.setLanguage(lang || localStorage.getItem(AppConsts.localStorageLangKey) || AppConsts.defaultLang);
	}

	editUser() {
        // this.user = {...this.userInfo};
        // this.userDialog = true;
		this.router.navigate([`/user-account/profile`]);
    }

	hideDialog() {
        this.userDialog = false;
    }

	saveUser() {
		this.userService.update(this.user).subscribe((res) => {
			this.userInfo = {...this.user};
			this.userDialog = false;
			this.messageService.add({severity: 'success', summary: '', detail: 'Cập nhật thành công!', life: 1500});
			this.userService.postRefreshToken().subscribe();
		});
	  }

	logout() {
		this.authService.logout();
	}
}
