import { Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { BreadcrumbService } from './breadcrumb.service';
import { Subscription } from 'rxjs';
import { MenuItem } from 'primeng/api';
import { localStorageKeyConst } from '@shared/AppConsts';

@Component({
    selector: 'app-breadcrumb',
    templateUrl: './app.breadcrumb.component.html'
})
export class AppBreadcrumbComponent implements OnDestroy {

    subscription: Subscription;

    items: MenuItem[];

    slogan: string;

    @ViewChild('breadcrumbEl') breadcrumbEl: ElementRef<HTMLElement>;

    constructor(public breadcrumbService: BreadcrumbService) {
        this.subscription = breadcrumbService.itemsHandler.subscribe(response => {
            this.items = response;
            setTimeout(() => {
                let breadcrumbHeight = this.breadcrumbEl?.nativeElement?.offsetHeight;
                localStorage.setItem(localStorageKeyConst.breadcrumHeight, breadcrumbHeight.toString());
                this.countHeightLayoutTop(breadcrumbHeight);
            }, 0);
        });
    }

    countHeightLayoutTop(breadcrumbHeight) {
        let layoutTopHeight = (+localStorage?.getItem(localStorageKeyConst.topbarHeight) || 0) + breadcrumbHeight;
        localStorage.setItem(localStorageKeyConst.layoutTopHeight, layoutTopHeight.toString());
    }
        
    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
}
