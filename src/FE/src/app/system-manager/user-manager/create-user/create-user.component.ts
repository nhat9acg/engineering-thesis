import { Component, Injector, OnInit } from "@angular/core";
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { MessageErrorConst, UserConst } from "@shared/AppConsts";
import { CrudComponentBase } from "@shared/crud-component-base";
import { IBusinessCustomer } from "@shared/interfaces/business-customer.interface";
import { IRestaurant } from "@shared/interfaces/restaurant.interface";
import { IRole } from "@shared/interfaces/role-manager.interface";
import { IFilterCustomer, IUser } from "@shared/interfaces/user-manager.interface";
import { UserDefault } from "@shared/model/user";
import { BusinessCustomerService } from "@shared/services/business-customer.service";
import { RestaurantService } from "@shared/services/restaurant.service";
import { RoleService } from "@shared/services/role.service";
import { UserService } from "@shared/services/user.service";
import { MessageService } from "primeng/api";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { forkJoin } from "rxjs";

@Component({
    selector: "app-create-user",
    templateUrl: "./create-user.component.html",
    styleUrls: ["./create-user.component.scss"],
})
export class CreateUserComponent extends CrudComponentBase {
    constructor(
        injector: Injector, 
        messageService: MessageService,
        private ref: DynamicDialogRef, 
        private configDialog: DynamicDialogConfig,
        private _userService: UserService,
        private _businessCustomerService: BusinessCustomerService,
        private _restaurantService: RestaurantService,
        private _roleService: RoleService,
        private fb: FormBuilder,
    ) {
        super(injector, messageService);
    }

    isView = false;
    user: IUser = new UserDefault;

    UserConst = UserConst;
    postForm: FormGroup;
    isHidePassword: boolean = false;
    businessCustomers: IBusinessCustomer[];
    restaurants: IRestaurant[];
    roles: IRole[];
    filterCustomer: IFilterCustomer = {};

    get f() {
      return this.postForm.controls;
    }
    
    ngOnInit(): void {
      this.filterCustomer = this.configDialog?.data?.filterCustomer;
        if( this.configDialog?.data?.user) {
            this.user = this.configDialog?.data?.user;
            this.isHidePassword = this.configDialog?.data?.isHidePassword;
            this.isView = this.configDialog?.data?.isView;

            this.postForm = this.fb.group({
              id: [this.user.id, [Validators.required]],
              username: [this.user.username, [Validators.required]],
              fullName: [this.user.fullName, [Validators.required]],
              userType: [this.filterCustomer.businessCustomerId ? UserConst.BUSINESS_CUSTOMER : 
                (this.filterCustomer.restaurantId ? UserConst.RESTAURANT : this.user.userType), 
                [Validators.required]],
              email: [this.user.email],
              phone: [this.user.phone],
              businessCustomerId: [this.filterCustomer.businessCustomerId ? this.filterCustomer.businessCustomerId : this.user.businessCustomerId],
              restaurantId: [this.filterCustomer.restaurantId ? this.filterCustomer.restaurantId : this.user.restaurantId],
              roleIds: [this.user.roleIds]
            });
        } else {
          this.postForm = this.fb.group({
            username: [this.user.username, [Validators.required]],
            fullName: [this.user.fullName, [Validators.required]],
            userType: [this.filterCustomer.businessCustomerId ? UserConst.BUSINESS_CUSTOMER : 
              (this.filterCustomer.restaurantId ? UserConst.RESTAURANT : this.user.userType), 
              [Validators.required]],
            password: [this.user.password, [Validators.required]],
            repeatPassword: [this.user.repeatPassword, [Validators.required]],
            email: [this.user.email],
            phone: [this.user.phone],
            businessCustomerId: [this.filterCustomer.businessCustomerId ? this.filterCustomer.businessCustomerId : this.user.businessCustomerId],
            restaurantId: [this.filterCustomer.restaurantId ? this.filterCustomer.restaurantId : this.user.restaurantId],
            roleIds: [this.user.roleIds]
          }, {
            // validators: MatchPassword('password', 'repeatPassword')
          });
        }
      this.init();
    }

    get postFormControl() {
        return this.postForm.controls;
    }

    matchPasswordValidator() {
      return (control: AbstractControl): { [key: string]: boolean } | null => {
        const password = control.get('password');
        const repeatPassword = control.get('repeatPassword');
  
        if (password?.value !== repeatPassword?.value) {
          return { matchPassword: true };
        }
  
        return null;
      };
    }
    
    init(){
        forkJoin([this._businessCustomerService.getAllNoPaging(), this._restaurantService.getAllNoPaging(), this._roleService.getAllRole()]).subscribe(([resBusinessCustomer, resRestaurant, resRole]) => {
            this.isLoading = false;
            if (this.handleResponseInterceptor(resBusinessCustomer)) {
                this.businessCustomers = resBusinessCustomer?.data?.items;
            }
            if (this.handleResponseInterceptor(resRestaurant)) {
                this.restaurants = resRestaurant?.data?.items;
            }
            if (this.handleResponseInterceptor(resRole)) {
              this.roles = resRole?.data?.items;
            }
        })
    }

    changeUserType(){
      this._roleService.getAllRole(this.postForm.value.userType).subscribe((res) => {
        if (this.handleResponseInterceptor(res)) {
          this.roles = res?.data?.items;
        }
      })
    }

    addValidator(field: string){
      this.postForm.controls[field].addValidators([Validators.required]);
    }

    clearValidator(field: string){
      this.postForm.controls[field].clearValidators()
    }

    configRequired(){
      if (this.postForm.value.userType == UserConst.BUSINESS_CUSTOMER){
        this.addValidator("businessCustomerId");
        this.clearValidator("restaurantId");
      } else if (this.postForm.value.userType == UserConst.RESTAURANT){
        this.addValidator("restaurantId");
        this.clearValidator("businessCustomerId");
      } else {
        this.clearValidator("restaurantId");
        this.clearValidator("businessCustomerId");
      }
    }
    
    cancel() {
        this.ref.destroy();
    }

    onSubmit() {
        if (!this.checkInValidForm(this.postForm)) {
          if (this.user.id) {
            this._userService.update(this.postForm.value).subscribe((response) => {
              if (this.handleResponseInterceptor(response, 'Success')) {
                this.ref.close(true);
              }
            }, () => {
              this.messageError("Can't update account", 3000);
            }
            );
          }
          else {
            this._userService.create(this.postForm.value,this.filterCustomer).subscribe(
              (response) => {
                if (this.handleResponseInterceptor(response, 'Successs')) {
                  this.ref.close(true);
                }
              }, () => {
                this.messageError("Can't create account", 3000);
              }
            );
          }
        }
    }
}
