import { Component, ElementRef, Injector, Input, OnInit, SimpleChanges, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { ActiveDeactiveNumConst, FormNotificationConst, PermissionConst, SearchConst, UserConst, localStorageKeyConst } from "@shared/AppConsts";
import { CrudComponentBase } from "@shared/crud-component-base";
import { ConfirmationService, MenuItem, MessageService } from "primeng/api";
import { DialogService } from "primeng/dynamicdialog";
import { debounceTime } from "rxjs/operators";
import { BreadcrumbService } from "src/app/layout/breadcrumb/breadcrumb.service";
import { CreateUserComponent } from "./create-user/create-user.component";
import { IBaseListAction, IPageInfo } from "@shared/interfaces/base-interface";
import { FormNotificationComponent } from "src/app/form-general/form-notification/form-notification.component";
import { UserService } from "@shared/services/user.service";
import { IFilterCustomer, IUser } from "@shared/interfaces/user-manager.interface";
import { TranslateService } from "@ngx-translate/core";
import { SelectedLanguageService } from "@shared/services/selected-language.service";
import { Table } from "primeng/table";

@Component({
	selector: "app-user-manager",
	templateUrl: "./user-manager.component.html",
	styleUrls: ["./user-manager.component.scss"],
})
export class UserManagerComponent extends CrudComponentBase {
	constructor(
		injector: Injector,
		messageService: MessageService,
		private router: Router,
		private dialogService: DialogService,
		private breadcrumbService: BreadcrumbService,
		private _userService: UserService,
		public _translateService: TranslateService,
		private _selectedLanguageService: SelectedLanguageService,
		private _confirmationService: ConfirmationService
	) {
		super(injector, messageService)
	}
	parentLangKeys: string[] = ['ROUTER', 'SHARE', 'SYSTEM_MANAGER', 'CONFIRM', 'RESPONSE_MSG'];
	labels: string[] = [];
	isLoadingPage: boolean;
	rows: any[] = [];
	listAction: IBaseListAction[] = [];
	UserConst = UserConst;
	ActiveDeactiveNumConst = ActiveDeactiveNumConst;
	PermissionConst = PermissionConst;
	@Input() businessCustomerId: number;
	@Input() restaurantId: number;

	filterCustomer: IFilterCustomer = {};

	@Input() scrollHeight: number = 0;
	@Input() contentHeight: number = 0;

	@Input() isComponentChild: boolean = false;
    language: string = 'en';
	ngOnInit(): void {
		if(this.businessCustomerId) {
			this.filterCustomer.businessCustomerId = this.businessCustomerId;
		} else if(this.restaurantId) {
			this.filterCustomer.restaurantId = this.restaurantId;
		}
		
		this._selectedLanguageService.getLanguage.subscribe((resLang) => {
			if (resLang) {
				this.language = resLang?.lang;
				this.labels = this._selectedLanguageService.getKeyLangs(this.parentLangKeys, resLang.keys);
				if(!this.isComponentChild) {
					this.breadcrumbService.setItems([
						{ label: this.labels['ROUTER']['HOME_PAGE'], routerLink: ['/home'] },
						{ label: this.labels['ROUTER']['ACCOUNT'], routerLink: ['/system-manager/user-manager']},
					]);
				}
			}

	  	});

		this.setPage();

		this.subject.keyword.pipe(debounceTime(SearchConst.DEBOUNCE_TIME)).subscribe(() => {
		  if (this.keyword === "") {
			this.setPage();
		  } else {
			this.setPage();
		  }
		});
	}

	@ViewChild('pageEl') pageEl: ElementRef<HTMLElement>;
	ngAfterViewInit() { 
		setTimeout(() => {
			if(this.isComponentChild) {
				this.scrollHeight = this.contentHeight - this.pageEl.nativeElement.offsetHeight;
			} 
		}, 0);
	}

	changeStatusPage() {
		this.setPage();
	}

	genListAction(data = []) {
		this.listAction = data.map((item) => {
		  const actions = [];
		//   if () {
			//   actions.push({
			// 	data: item,
			// 	label: "Thông tin chi tiết",
			// 	icon: "pi pi-info-circle",
			// 	command: ($event) => {
			// 	  this.detail($event.item.data);
			// 	}
			//   });

		if (this.isGranted([PermissionConst.UserUpdate]) || this.isGranted([PermissionConst.BusinessCustomerUpdate]) || this.isGranted([PermissionConst.RestaurantAccountUpdate])) {  
			actions.push({ 
				data: item,
				label: this.labels['SHARE']['UPDATE'],
				icon: "pi pi-pencil",
				command: ($event) => {
					this.update($event.item.data);
				}
				});
		}
		
		if (this.isGranted([PermissionConst.UserDelete]) || this.isGranted([PermissionConst.BusinessCustomerDelete]) || this.isGranted([PermissionConst.RestaurantAccountDelete])) {  
			actions.push({ 
			data: item,
			label: this.labels['SHARE']['DELETE'],
			icon: "pi pi-trash",
			command: ($event) => {
				this.delete($event.item.data);
			}
			});
		}

		if (this.isGranted([PermissionConst.UserUpdate]) || this.isGranted([PermissionConst.BusinessCustomerUpdate]) || this.isGranted([PermissionConst.RestaurantAccountUpdate])) { 
			actions.push({ 
				data: item,
				label: item.status == UserConst.ACTIVE ? this.labels['SHARE']['DEACTIVE'] : this.labels['SHARE']['ACTIVE'],
				icon: item.status == UserConst.ACTIVE ? "pi pi-times-circle" : "pi pi-check-circle",
				command: ($event) => {
					this.changeStatus($event.item.data);
				}
			});
		}
		
		  return actions;
		});
		console.log(this.listAction);
	}

	configUserDialog(header: string, isView: boolean, user?: IUser ){
		const ref = this.dialogService.open(CreateUserComponent, {
			header: header,
			width: '700px',
			contentStyle: {"max-height": "600px", overflow: "auto", "margin-bottom": "60px", },
			baseZIndex: 10000,
			data: {
			  user : user ?? null,
			  isHidePassword: user ? true : false,
			  isView: isView,
			  filterCustomer: this.filterCustomer,
			},
			});
			//
			ref.onClose.subscribe((res) => {
				if (res) {
					this.setPage();
				}
		});
	}

	detail(user){
		this.configUserDialog(this.labels['SYSTEM_MANAGER']['TITLE_DIALOG_VIEW'], true, user);
	}

	changeStatus(user){
		const ref = this.dialogService.open(
            FormNotificationComponent,
            {
                header: this.labels['SHARE']['TITLE_DIALOG_CHANGE_STATUS'],
                width: '400px',
                contentStyle: { "max-height": "600px", "overflow": "auto", "padding-bottom": "50px" },
                styleClass: 'p-dialog-custom',
                baseZIndex: 10000,
                data: {	
                    title: user.status == UserConst.ACTIVE ? this.labels['SYSTEM_MANAGER']['DEACTIVE'] : this.labels['SYSTEM_MANAGER']['ACTIVE'],
                    icon: user.status == UserConst.ACTIVE ? FormNotificationConst.IMAGE_CLOSE : FormNotificationConst.IMAGE_APPROVE,
                },
            }
        );
        ref.onClose.subscribe((dataCallBack) => {
            if (dataCallBack?.accept) {
                this._userService.changeStatus(user.id).subscribe((response) => {
                    if (this.handleResponseInterceptor(response,"Cập nhật thành công")) {
                        this.setPage();
                    }
                }, (err) => {
                    console.log('err____', err);
                });
            } 
        });
	}

	update(user){
		this.configUserDialog(this.labels['SYSTEM_MANAGER']['TITLE_DIALOG_UPDATE'], false , user);
	}

	delete(data) {
        this._confirmationService.confirm({
            header: this.labels['ROUTER']['TITLE_DIALOG_DELETE'],
            message: this.labels['CONFIRM']['ARE_YOU_SURE_TO_DELETE_THIS_ACCOUNT'],
			acceptLabel: this.labels['CONFIRM']['YES'],
			rejectLabel: this.labels['CONFIRM']['NO'],
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
				this._userService.delete(data.id).subscribe((response) => {
					if (this.handleResponseInterceptor(response, this.labels['RESPONSE_MSG']['DELETE_SUCCESS'])) {
						this.setPage();
					}
				}, (err) => {
					console.log('err____', err);
					this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER'], 3000);
				});
            }
        });
    }

	createUser() {
		const ref = this.dialogService.open(
		  CreateUserComponent,
		  {
			header: this.labels['SYSTEM_MANAGER']['TITLE_DIALOG_ADD'],
			contentStyle: {"overflow": "auto", "margin-bottom": "60px" },
			width: '700px',
			height: '500px',
			baseZIndex: 100000,
			data: {
				filterCustomer: this.filterCustomer,
			  },
		  }
		);
		//
		ref.onClose.subscribe(res=> {
		  if(res) {
			this.setPage();
		  }
		});
	}

	isRowOdds: boolean = true;
	setPage(event?: IPageInfo) {
		this.isRowOdds = true;
        this.isLoading = true;
		if(event) {
			this.page.pageNumber = event.page;
			this.page.pageSize = event.rows;
		}
		this.page.keyword = this.keyword;
		this._userService.getAll(this.page,this.filterCustomer).subscribe((res) => {
			this.isLoading = false;
            if (this.handleResponseInterceptor(res, '')) {
                this.page.totalItems = res.data.totalItems;
                this.rows = res?.data?.items;
                //
                if (this.rows?.length) {
					this.isRowOdds = !(this.rows?.length%2 == 0);
                    this.genListAction(this.rows);
                }
            }
		}, (err) => {
            this.isLoading = false;
            console.log('Error-------', err);
            
        });
	}
}
