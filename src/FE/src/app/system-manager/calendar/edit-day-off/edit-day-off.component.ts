import { Component, Injector, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { YesNoConst } from '@shared/AppConsts';
import { CrudComponentBase } from '@shared/crud-component-base';
import { Calendar } from '@shared/model/calendar.model';
import { CalendarService } from '@shared/services/calendar.service';
import * as moment from 'moment';
import { MessageService } from 'primeng/api';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';
import { DateConversionService } from '@shared/services/date-conversion.service';

@Component({
  selector: 'app-edit-day-off',
  templateUrl: './edit-day-off.component.html',
  styleUrls: ['./edit-day-off.component.scss']
})
export class EditDayOffComponent extends CrudComponentBase {

  constructor(
    injector: Injector,
    messageService: MessageService,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private dialogService: DialogService,
    private router: Router,
    private fb: FormBuilder,
    private _calendarService: CalendarService,
    private _dateConversionService: DateConversionService
  ) {
    super(injector, messageService);
  }
  inputData: Calendar;
  postForm: FormGroup;
  labels: string[] = [];
  YesNoConst = YesNoConst;
  rangeDates: Date[];

  ngOnInit(): void {
    this.labels = this.config?.data?.labels
    this.inputData = this.config?.data?.inputData;
    this.rangeDates = [new Date(this.inputData.workingDate)];

    this.postForm = this.fb.group({
      note: [this.inputData?.note || '', Validators.required],
      rangeDates: [[new Date(this.inputData.workingDate)], Validators.required],
      startDate: [null, null],
      endDate: [null, null],
      isDayOff: ['N', null],
    });
  }

  get postFormControl() {
    return this.postForm.controls;
  }

  onSubmit() {
    if (!this.checkInValidForm(this.postForm)) {
      if (this.postForm.value.rangeDates[1] != null) {
        this.postForm.patchValue({
          startDate: this._dateConversionService.convertDate(this.postForm.value.rangeDates[0]),
          endDate: this._dateConversionService.convertDate(this.postForm.value.rangeDates[1]),
        });
      } else {
        this.postForm.patchValue({
          startDate: this._dateConversionService.convertDate(this.postForm.value.rangeDates[0]),
          endDate: this._dateConversionService.convertDate(this.postForm.value.rangeDates[0]),
        });
      }
      if(this.postForm.value.isDayOff == YesNoConst.NO) {
        this._calendarService.update(this.postForm.value).subscribe((response) => {
          if (this.handleResponseInterceptor(response, this.labels['RESPONSE_MSG']['UPDATE_SUCCESS'])) {
            this.ref.close(true);
          }
        }, () => {
          this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER']);
        }
        );
      } else {
        this._calendarService.delete(this.postForm.value).subscribe((response) => {
          if (this.handleResponseInterceptor(response, this.labels['RESPONSE_MSG']['UPDATE_SUCCESS'])) {
            this.ref.close(true);
          }
        }, () => {
          this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER']);
        }
        );
      }

    } else {
      this.messageError(this.labels['RESPONSE_MSG']['PLEASE_ENTER_ENOUGH_INFOR']);
    }

  }

}



