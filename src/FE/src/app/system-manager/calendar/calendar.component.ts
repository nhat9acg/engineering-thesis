import { Component, Injector, OnInit } from '@angular/core';
import { PermissionConst, YesNoConst } from '@shared/AppConsts';
import { CrudComponentBase } from '@shared/crud-component-base';
import { Calendar, Header, Month } from '@shared/model/calendar.model';
import { CalendarService } from '@shared/services/calendar.service';
import * as moment from 'moment';
import { ConfirmationService, MessageService } from 'primeng/api';
import { BreadcrumbService } from 'src/app/layout/breadcrumb/breadcrumb.service';
import { EditDayOffComponent } from './edit-day-off/edit-day-off.component';
import { DialogService } from 'primeng/dynamicdialog';
import { SelectedLanguageService } from '@shared/services/selected-language.service';
import { User } from '@shared/model/user.model';

@Component({
    selector: 'app-calendar',
    templateUrl: './calendar.component.html',
    styleUrls: ['./calendar.component.scss'],
    providers: [DialogService, ConfirmationService, MessageService]
})
export class CalendarComponent extends CrudComponentBase {
    constructor(
        injector: Injector,
        messageService: MessageService,
        private breadcrumbService: BreadcrumbService,
        private _calendarService: CalendarService,
        private _dialogService: DialogService,
        private confirmationService: ConfirmationService,
        private selectedLanguageService: SelectedLanguageService,
    ) {
        super(injector, messageService);
        this.userInfo = this.getUser();
    }
    userInfo: User;
    YesNoConst = YesNoConst;
    calendar: Calendar[] = [];
    headers: Header[] = [
    ];
    months: Month[] = [];
    years: number[] = [];
    year: number = new Date().getFullYear();
    parentLangKeys: string[] = ['HOME', 'MENU', 'SHARE',
        'ROUTER', 'CONFIRM', 'RESPONSE_MSG','CALENDAR_CONFIGURATION','WEEK','MONTH'];
    labels: string[] = [];

    ngOnInit() {
        this.selectedLanguageService.getLanguage.subscribe((resLang) => {
            if (resLang) {
                this.labels = this.selectedLanguageService.getKeyLangs(this.parentLangKeys, resLang.keys);
                this.breadcrumbService.setItems([
                    { label: this.labels['ROUTER']['CALENDAR_CONFIGURATION'] },
                    
                ]);
                this.generateHeade();
            }
            this.init();
        });
        this.getListYear();

    }

    generateHeade(){
        this.headers = []
        const initialHeaders: Header[] = [
            { vi: this.labels['WEEK']['SUN'], order: 0 },
            { vi: this.labels['WEEK']['MON'], order: 1 },
            { vi: this.labels['WEEK']['TUE'], order: 2 },
            { vi: this.labels['WEEK']['WED'], order: 3 },
            { vi: this.labels['WEEK']['THU'], order: 4 },
            { vi: this.labels['WEEK']['FRI'], order: 5 },
            { vi: this.labels['WEEK']['SAT'], order: 6 },
          ];
          const totalDays = 37; 
      
          for (let i = 0; i < totalDays; i++) {
            const header: Header = {
              vi: initialHeaders[i % 7].vi,
              order: i < 7 ? initialHeaders[i].order : null,
            };
            this.headers.push(header);
          }
    }

    edit(item) {
        if(this.userInfo.user_type === 1 || this.isGranted([PermissionConst.CalculationUnitUpdate])) {
            this._dialogService.open(EditDayOffComponent, {
                data: {
                    inputData: item,
                    labels: this.labels,
                },
                contentStyle: { "max-height": "600px", "overflow": "auto", "margin-bottom": "60px" },
                header: this.labels['CALENDAR_CONFIGURATION']['TITLE_DIALOG_UPDATE'],
                width: '500px',
            }).onClose.subscribe(result => {
                if (result) {
                    this.init();
                }
            })
        }
    }

    async init() {
        this.calendar = await this.generateDatesList();
        await this.getFullDay();
    }

    async getFullDay() {
        this.months = Array.from({ length: 12 }, (_, i) => ({
            value: 'MONTH.' + (i + 1),
            data: [],
        }));

        for (let item of this.calendar) {
            let month = new Date(item.workingDate).getMonth() + 1;
            this.months[month - 1].data.push({
                day: moment(item.workingDate).format('DD'),
                dateTime: moment(item.workingDate).format('DD-MM-YYYY'),
                dayDate: new Date(item.workingDate).getDay(),
                workingDate: item.workingDate,
                isDayOff: item?.isDayOff,
                note: item?.note,
                workingYear: item.workingYear,
            });
        }
    }

    async generateDatesList(): Promise<Calendar[]> {
        let startDate = moment(`${this.year}-01-01`);
        let endDate = moment(`${this.year}-12-31`);
        let list: Calendar[] = [];

        while (startDate.isSameOrBefore(endDate)) {
            let dateObj = {
                workingDate: startDate.format('YYYY-MM-DDTHH:mm:ss'),
                workingYear: this.year,
                isDayOff: 'N',
                note: ''
            };

            list.push(dateObj);
            startDate.add(1, 'day');
        }

        let res = await this._calendarService.getAll(this.year).toPromise();
        this.isLoading = false;

        let dateMap: Map<string, { isDayOff: string, note: string }> = new Map();
        for (let item of res.data) {
            dateMap.set(item.date, { isDayOff: 'Y', note: item.note });
        }

        for (let i = 0; i < list.length; i++) {
            let item = list[i];
            let dateData = dateMap.get(item.workingDate);
            if (dateData) {
                item.isDayOff = dateData.isDayOff;
                item.note = dateData.note;
            }
        }
        return list;
    }

    getListYear() {
        this.years = [];
        const year = new Date().getFullYear();
        for (let i = (year - 3); i <= (year + 3); i++) {
            this.years.push(i);
        }
    }

}
