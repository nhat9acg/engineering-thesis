import { Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SearchConst, FormNotificationConst, localStorageKeyConst, PermissionConst, DocumentConst, UserConst, AppConsts } from '@shared/AppConsts';
import { CrudComponentBase } from '@shared/crud-component-base';
import { Page } from '@shared/model/page';
import { BusinessCustomerService } from '@shared/services/business-customer.service';

import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { debounceTime } from 'rxjs/operators';
import { FormNotificationComponent } from 'src/app/form-general/form-notification/form-notification.component';
import { FormSetDisplayColumnComponent } from 'src/app/form-general/form-set-display-column/form-set-display-column.component';
import { BreadcrumbService } from 'src/app/layout/breadcrumb/breadcrumb.service';
import { IColumn, IBusinessCustomer } from '@shared/interfaces/business-customer.interface';
import { IBaseListAction, IPageInfo } from '@shared/interfaces/base-interface';
import { SelectedLanguageService } from '@shared/services/selected-language.service';
import { TranslateService } from '@ngx-translate/core';
import { GroupCustomerService } from '@shared/services/group-customer.service';
import { DocumentAddComponent } from './document-add/document-add.component';
import { DocumentService } from '@shared/services/document.service';
import { User } from '@shared/model/user.model';
import { AppUtilsService } from '@shared/services/utils.service';

@Component({
  selector: 'app-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.scss'],
  providers: [DialogService, ConfirmationService, MessageService]
})
export class DocumentComponent extends CrudComponentBase {

    constructor(
        injector: Injector,
        private dialogService: DialogService,
        private _dialogService: DialogService,
        private confirmationService: ConfirmationService,
        private router: Router,
        private routeActive: ActivatedRoute,
        messageService: MessageService,
        private breadcrumbService: BreadcrumbService,
        private _documentService: DocumentService,
        private selectedLanguageService: SelectedLanguageService,
        public _translateService: TranslateService,
        private _utilsService: AppUtilsService,
    ) {
        super(injector, messageService);
        this.userInfo = this.getUser();
    }
    userInfo: User;
    ref: DynamicDialogRef;
    rows: IBusinessCustomer[] = [];
    cols: IColumn[];
    listAction: IBaseListAction[] = [];
    page = new Page();
    offset = 0;
    parentLangKeys: string[] = ['HOME', 'MENU', 'SHARE', 'ROUTER', 'CONFIRM', 'RESPONSE_MSG','DOCUMENT'];
    labels: string[] = [];
    PermissionConst = PermissionConst;
    UserConst = UserConst;
    scrollHeight: number = 0;
    urlfilePDF: string = '';
    modalDialogPDF: boolean;

    ngOnInit(): void {
        this.selectedLanguageService.getLanguage.subscribe((resLang) => {
            if(resLang) {
                this.labels = this.selectedLanguageService.getKeyLangs(this.parentLangKeys, resLang.keys);
                if(this.userInfo.user_type !== UserConst.ADMIN)
                    this.cols = [
                        { field: 'title', header: this.labels['DOCUMENT']['COLUMN_TITLE'], width: '12rem', isPin: true, type: '' },
                        { field: 'sendTypeDisplay', header: this.labels['DOCUMENT']['COLUMN_SEND_TYPE'], width: '12rem', isPin: false, type: '' },
                        { field: 'numberCustomerSent', header: this.labels['DOCUMENT']['COLUMN_CUSTOMER_SENT'], width: '12rem', isPin: false, type: '' },
                        { field: 'numberCustomerView', header: this.labels['DOCUMENT']['COLUMN_CUSTOMER_VIEW'], width: '12rem', isPin: false, type: '' },
                        { field: 'isView', header: this.labels['DOCUMENT']['COLUMN_VIEWED'], width: '12rem', isPin: false, type: '', class:'justify-content-center' },
                        // { field: 'createdDate', header: this.labels['DOCUMENT'][''], width: '12rem', isPin: false, type: '' },
                        { field: `columnResize`, header: '', width: '', isPin: false, type: 'hidden' },
                    ];
                else {
                    this.cols = [
                        { field: 'title', header: this.labels['DOCUMENT']['COLUMN_TITLE'], width: '12rem', isPin: true, type: '' },
                        { field: 'sendTypeDisplay', header: this.labels['DOCUMENT']['COLUMN_SEND_TYPE'], width: '12rem', isPin: false, type: '' },
                        { field: 'numberCustomerSent', header: this.labels['DOCUMENT']['COLUMN_CUSTOMER_SENT'], width: '12rem', isPin: false, type: '' },
                        { field: 'numberCustomerView', header: this.labels['DOCUMENT']['COLUMN_CUSTOMER_VIEW'], width: '12rem', isPin: false, type: '' },
                        // { field: 'isView', header: this.labels['DOCUMENT']['COLUMN_VIEWED'], width: '12rem', isPin: false, type: '', class:'justify-content-center' },
                        { field: `columnResize`, header: '', width: '', isPin: false, type: 'hidden' },
                    ];
                }
                //
                this.breadcrumbService.setItems([
                    { label: this.labels['ROUTER']['HOME_PAGE'], routerLink: ['/home'] },
                    { label: this.labels['ROUTER']['DOCUMENT'], routerLink: ['/system-manager/document'] },
                ]);
            }
            this.setPage();
        });

        this.subject.keyword.pipe(debounceTime(SearchConst.DEBOUNCE_TIME)).subscribe(() => {
            if (this.keyword === "") {
                this.setPage();
            } else {
                this.setPage();
            }
        });
    }

    hideDialog() {
        this.submitted = false;
        this.modalDialogPDF = false;
    }

    downloadFile(item) {
        const url = AppConsts.remoteServiceBaseUrl + "/" + item.fileUrl;
        this._utilsService.makeDownload("", url);
        this._documentService.get(item.id).subscribe((response) => {

        });
    }

    viewFile(item) {
        this.urlfilePDF = AppConsts.remoteServiceBaseUrl + '/' + item.fileUrl;
        if(!item.fileUrl){
            this.messageError("Không có file hồ sơ")
        }else{
            if(this.utils.isPdfFile(item.fileUrl)){
                this.modalDialogPDF = true;
                this._documentService.get(item.id).subscribe((response) => {

                });
            } else {
                this.messageError("Hệ thống hiện tại chỉ hỗ trợ xem file PDF")
            }
        }
      }

    genListAction(data = []) {
        this.listAction = data.map(item => {
            const actions = [];
           
                actions.push({
                    data: item,
                    label: this.labels['SHARE']['UPDATE'],
                    icon: 'pi pi-user-edit',
                    command: ($event) => {
                    this.edit($event.item.data);
                    }
                })
 
                actions.push({
                    data: item,
                    label: this.labels['SHARE']['DELETE'],
                    icon: 'pi pi-trash',
                    command: ($event) => {
                    this.delete($event.item.data);
                    }
                })
            
            return actions;
        });
    }

    create() {
        this.dialogService.open(DocumentAddComponent, {
            header: `${this.labels['DOCUMENT']['TITLE_DIALOG_ADD']}`,
            width: '600px',
            contentStyle: {"max-height": "600px", "overflow": "auto", "margin-bottom": "70px" },
            data: {
                labels: this.labels
            },
        }).onClose.subscribe(result => {
            if (result) {
                this.setPage();
            } 
        })
    }

    edit(item) {
        this._documentService.get(item.id).subscribe((response) => {
            if (this.handleResponseInterceptor(response, "")) {
                this.dialogService.open(DocumentAddComponent, {
                    contentStyle: {"max-height": "600px", "overflow": "auto", "margin-bottom": "70px" },
                    header:  `${this.labels['DOCUMENT']['TITLE_DIALOG_UPDATE']}`,
                    width: '600px',
                    data: {
                        inputData: response.data,
                        labels: this.labels
                    },
                }).onClose.subscribe(result => {
                    if (result) this.setPage();
                })
            }
        });
    }

    delete(item) {
        this.confirmationService.confirm({
            message: this.labels['DOCUMENT']['DELETE_TITLE'],
            acceptLabel: this.labels['CONFIRM']['YES'],
            rejectLabel: this.labels['CONFIRM']['NO'],
            accept: () => {
                this._documentService.delete(item.id).subscribe((response) => {
                    if (this.handleResponseInterceptor(response, this.labels['CONFIRM']['DELETE_SUCCESS'])) {
                        this.setPage();
                    }
                });
            }
        });
    }

    showData(rows) {
        for (let row of rows) {
            let parts = DocumentConst.getAttribution(row?.sendType).name.split('.');
            row.sendTypeDisplay = this.labels['DOCUMENT'][parts[1]];
        };
    }

    isRowOdds: boolean = false;
    setPage(event?: IPageInfo) {
        this.isRowOdds = false;
        this.isLoading = true;
        if(event) {
            this.page.pageNumber = event.page;
            this.page.pageSize = event.rows;
        }
        this.page.keyword = this.keyword;
        this._documentService.getAll(this.page).subscribe((res) => {
        this.isLoading = false;
        if (this.handleResponseInterceptor(res, '')) {
            this.page.totalItems = res.data.totalItems;
            this.rows = res.data?.items;
            this.showData(this.rows)
            if (res.data?.items?.length && this.userInfo.user_type === UserConst.ADMIN) {
                this.isRowOdds = !(this.rows?.length%2 == 0);
                this.genListAction(this.rows);
            }
        }
        }, (err) => {
            this.isLoading = false;
            console.log('Error-------', err);
        });
    }
}



