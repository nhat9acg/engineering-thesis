import { Component, Injector, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DocumentConst } from '@shared/AppConsts';
import { CrudComponentBase } from '@shared/crud-component-base';
import { IBusinessCustomer } from '@shared/interfaces/business-customer.interface';
import { DocumentManagement } from '@shared/model/document.model';
import { DocumentService } from '@shared/services/document.service';
import { FileService } from '@shared/services/file.service';
import { RestaurantService } from '@shared/services/restaurant.service';
import { MessageService } from 'primeng/api';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-document-add',
  templateUrl: './document-add.component.html',
  styleUrls: ['./document-add.component.scss']
})
export class DocumentAddComponent extends CrudComponentBase {

  constructor(
    injector: Injector,
    messageService: MessageService,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private _documentService: DocumentService,
    private dialogService: DialogService,
    private router: Router,
    private fb: FormBuilder,
    private _fileService: FileService,
    private _restaurantService: RestaurantService,
  ) {
    super(injector, messageService);
  }
  inputData: DocumentManagement;
  postForm: FormGroup;
  labels: string[] = [];
  businessCustomers: IBusinessCustomer[] = [];
  DocumentConst = DocumentConst;
 
  ngOnInit(): void {
    this.labels = this.config?.data?.labels    
    this.inputData = this.config?.data?.inputData;
    this.postForm = this.fb.group({
      id: [this.inputData?.id || null, []],
      title: [this.inputData?.title || '', Validators.required],
      sendType: [this.inputData?.sendType || DocumentConst.SOME_CUSTOMER, Validators.required],
      businessCustomerIds: [this.inputData?.businessCustomerIds || [], (this.inputData) ? [] :  Validators.required],
      fileUrl: [this.inputData?.fileUrl || '', Validators.required],
     
    });
    this.optionsBusiness();
  }

  onChangeType() {
    if (this.postForm.value.sendType === DocumentConst.SOME_CUSTOMER) {
      this.postForm.get('businessCustomerIds')?.setValidators(Validators.required);
    } else {
      this.postForm.get('businessCustomerIds')?.clearValidators();
    }
    
    this.postForm.get('businessCustomerIds')?.updateValueAndValidity();
    this.postForm.get('businessCustomerIds')?.markAsTouched();
  }

  myUploader(event){
		if (event?.files[0]) {
			this._fileService.uploadFileGetUrl(event?.files[0], "system_document").subscribe((response) => {
			  if (this.handleResponseInterceptor(response, "")) {
          this.postForm.patchValue({
              fileUrl: response?.data
            });
			  }
			}, (err) => {
			  console.log('err-----', err);
			  this.messageError("Có sự cố khi upload!");
			});
		}
	}

  optionsBusiness() {
    this.isLoading = true;
    this._restaurantService.getAllBusinessCustomer().subscribe((res) => {
      this.isLoading = false;
      if (this.handleResponseInterceptor(res, '')) {

        this.businessCustomers = res.data?.items.map((item) => {
          return {
            id: item.id,
            labelName: item.fullName
          };
        });
      }
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);

    });
  }

  get postFormControl() {
    return this.postForm.controls;
  }

  onSubmit() {
    if (!this.checkInValidForm(this.postForm)) {
      if (this.inputData) {
        this._documentService.update(this.postForm.value).subscribe((response) => {
          if (this.handleResponseInterceptor(response, this.labels['RESPONSE_MSG']['UPDATE_SUCCESS'])) {
            this.ref.close(true);
          }
        }, () => {
          this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER']);
        }
        );
      }
      else {
        this._documentService.create(this.postForm.value).subscribe(
          (response) => {
            if (this.handleResponseInterceptor(response, this.labels['RESPONSE_MSG']['ADD_SUCCESS'])) {
              this.ref.close(true);
            }
          }, () => {
            this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER']);
          }
        );
      }
    } else {
      this.messageError(this.labels['RESPONSE_MSG']['PLEASE_ENTER_ENOUGH_INFOR']);
    }

  }

}


