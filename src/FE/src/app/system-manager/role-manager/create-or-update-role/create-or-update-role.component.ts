import { Component, EventEmitter, Injector, Input, OnInit, Output } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { TranslateService } from "@ngx-translate/core";
import { MessageErrorConst, UserConst } from "@shared/AppConsts";
import { CrudComponentBase } from "@shared/crud-component-base";
import { IKeyPermission, INodePermission, IRole } from "@shared/interfaces/role-manager.interface";
import { RoleDefault } from "@shared/model/role";
import { RoleService } from "@shared/services/role.service";
import { MessageService, TreeNode } from "primeng/api";
import { Observable } from "rxjs";

@Component({
  selector: "app-create-or-update-role",
  templateUrl: "./create-or-update-role.component.html",
  styleUrls: ["./create-or-update-role.component.scss"],
})
export class CreateOrUpdateRoleComponent extends CrudComponentBase {
	constructor(
		injector: Injector,
		messageService: MessageService,
        private _fb: FormBuilder,
		private _roleService: RoleService,
		public _translateService: TranslateService,
	
	) {
		super(injector, messageService);
	}
	keys = ['RESPONSE_MSG.UPDATE_SUCCESS','RESPONSE_MSG.ADD_SUCCESS', 'ROLE_MANAGER.TITLE_ROLE_UPDATE', 'ROLE_MANAGER.TITLE_ROLE_ADD'];
	labels: string[] = [];
	UserConst = UserConst;
	
	@Output() onCloseDialog = new EventEmitter<boolean>();

	@Input() modalDialog: boolean;
	@Input() roleInfo: any = {};
	
	permissionFull: IKeyPermission[] = [];
	permissionsTree: INodePermission[] = [];
	permissionRole: IKeyPermission[] = [];
	parentNode: INodePermission[] =[];
	selectedOlds: string[] = [];

	selecteds: IKeyPermission[] = [];
    postForm: FormGroup;

	role: IRole = new RoleDefault;
  	ngOnInit(): void {
		this.getKeys(); 

		this._roleService.findAllPermission().subscribe((res) => {
			this.permissionFull = res?.data;
			this.permissionRole = [...this.permissionFull];
			// Xử lý quyền theo cấu trúc dữ liệu dạng cây
			if(this.permissionRole) this.handleDataPermision();
			console.log('permissions____', this.permissionFull);
		});

        // Get Permission Role
        if(this.roleInfo?.id) {
			this.role = this.roleInfo;
			console.log('!!! roleInfo ', this.roleInfo);
			this.getPermissionOfRole();
		} 
		this.postForm = this._fb.group({
			id: [this.role.id, [Validators.required]],
			name: [this.role.name, [Validators.required]],
			userType: [this.role.userType, [Validators.required]],
			description: [this.role.description],
			permissionKeys: [this.role.permissionKeys],
		})
        if(!this.roleInfo?.id) {
			this.postForm.removeControl('id');
		}
	}

	getKeys() {
		this._translateService.get(this.keys).subscribe(res => {
		  this.labels = res;
		});
	}

	hideDialog() {
		this.onCloseDialog.emit();
	}

	getPermissionOfRole() {
		this.isLoading = true;
		this._roleService.getRoleById(this.roleInfo.id).subscribe((res) => {
			if(this.handleResponseInterceptor(res, '')) {
				// Đổ dữ liệu quyền của Role
				let permissionKeyIsset = res?.data?.permissionDetails.map(d => d.permissionKey);
				this.selecteds = this.permissionRole.filter(p => permissionKeyIsset.includes(p.key));
				this.selectedOlds = [...this.selecteds.map(s => s.key)];
				this.role = res.data;
				this.isLoading = false;
			}
		}, (err) => {
			this.isLoading = false;
		});
	}


	// XỬ LÝ DỮ LIỆU THÀNH CẤU TRÚC CÂY THEO KEY VÀ PARENT_KEY (CÂY VÔ HẠN CẤP)
	handleDataPermision() {
		this.permissionsTree = [...this.permissionRole];
		for( let i=1; i <= this.permissionRole.length; i++) {
		  this.permissionsTree = this.permissionsTree.map(item => {
			item.children = this.permissionRole.filter(c => c.parentKey == item.key);
			item.styleClass = ((item.children?.length || !item.parentKey)  ? 'permission-parents ' : '') + (item.parentKey == null ? 'node-0 ' : '');
			return item;
		  });
		}
		//
		this.permissionsTree = this.permissionsTree.filter(item => item.parentKey == null);
	}

	// SHOW TẤT CẢ CÁC NODE CÓ TRONG CÂY (TREE)
	showNodes(node:TreeNode, isExpand:boolean) {
		node.expanded = isExpand;
		if (node.children) {
			node.children.forEach( childNode => {
				this.showNodes(childNode, isExpand);
			} );
		}
	}

	//  SELECT
	selectedNote(node) {
		this.customDataSelected(node);
	}
	
	// UNSELECT
	unSelectedNote(node) {
		this.customDataSelected(node, true);
	}

	// CUSTOM BASE XỬ LÝ UNSELECT NODE CON KO UNSELECT NODE CHA
	customDataSelected(node, unSelected: boolean = false) {
		// Xử lý bước 1 (Dùng cho cả Select và unSelect)
		this.parentNode = [];
		this.getParentNode(node);
		this.filterItemNodeDuplidate();
		//
		if(this.parentNode.length) {
			this.parentNode.forEach(node => {
			let issetNode = this.selecteds.find(s => s.key == node.key);
			if(!issetNode) this.selecteds.push(node);
			});			
		}

		// Xử lý bước 2 (Chỉ dùng cho unSelect)
		if(unSelected) this.unSelectParentNode(node?.parent);
	}

	// UNSELECT NODE CHA KHI CÁC NODE CON BỊ UNSELECT HẾT
	unSelectParentNode(parentNode) {
		if(parentNode) {
			let unSelect = true;
			//
			parentNode.children.forEach(childNode => {
				let childNodeIssetSelected = this.selecteds.find(s => s.key == childNode.key);
				if(childNodeIssetSelected) unSelect = false;
			});
			//
			if(unSelect) {
				let indexNode = this.selecteds.findIndex(s => s.key == parentNode.key);
				if(indexNode >= 0) this.selecteds.splice(indexNode, 1);
				if(parentNode.parent) this.unSelectParentNode(parentNode.parent);
			}
			//
		}
	}

	// LẤY DANH SÁCH TỔ TIÊN CHA, ÔNG, CỤ, KỴ... CỦA NODE
	getParentNode(node) {
		if(node?.parent) {
			this.parentNode.push(node.parent);
			this.getParentNode(node.parent);
		}                                
	}
	
	// LỌC PHẦN TỬ DUPLICATE TRONG DANH SÁCH NODE SELECTED
	filterItemNodeDuplidate() {
		let keyIsset = []
		this.selecteds.forEach((item, index) => {
		if(!keyIsset.includes(item.key)) {
			keyIsset.push(item.key);
		} else {
			this.selecteds.splice(index, 1);
		}
		});
	}

	onSubmit(){
        if (!this.checkInValidForm(this.postForm)) {
			this.postForm.value.permissionKeys = this.selecteds.map(s => s.key);

			if (this.roleInfo.id){
				this._roleService.update(this.postForm.value).subscribe((res) => {
					if(this.handleResponseInterceptor(res, this.labels['RESPONSE_MSG.UPDATE_SUCCESS'])) {
						this.onCloseDialog.emit(true);
					}
				})
			} else {
				this._roleService.create(this.postForm.value).subscribe((res) => {
					if(this.handleResponseInterceptor(res, this.labels['RESPONSE_MSG.ADD_SUCCESS'])) {
						this.onCloseDialog.emit(true);
					}
				})
			}
		}
	}

}
