import { Component, Injector, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { FormNotificationConst, PermissionConst, UserConst } from "@shared/AppConsts";
import PermissionConfig from "@shared/PermissionConfig";
import { CrudComponentBase } from "@shared/crud-component-base";
import { IBaseAction, IBaseListAction, IPageInfo, IResponseLang } from "@shared/interfaces/base-interface";
import { IKeyPermission } from "@shared/interfaces/role-manager.interface";
import { RoleService } from "@shared/services/role.service";
import { SelectedLanguageService } from "@shared/services/selected-language.service";
import { ConfirmationService, MessageService } from "primeng/api";
import { DialogService } from "primeng/dynamicdialog";
import { FormNotificationComponent } from "src/app/form-general/form-notification/form-notification.component";
import { BreadcrumbService } from "src/app/layout/breadcrumb/breadcrumb.service";

@Component({
	selector: "app-role-manager",
	templateUrl: "./role-manager.component.html",
	styleUrls: ["./role-manager.component.scss"],
})
export class RoleManagerComponent extends CrudComponentBase {
	constructor(
		injector: Injector,
		messageService: MessageService,
		private breadcrumbService: BreadcrumbService,
		private dialogService: DialogService,
		private _roleService: RoleService,
		public _translateService: TranslateService,
		private _confirmationService: ConfirmationService,
		private _selectedLangService: SelectedLanguageService,

	) {
		super(injector, messageService);
		this.breadcrumbService.setItems([
		  { label: "Trang chủ", routerLink: ["/home"] },
		  { label: "Cấu hình vai trò" },
		]);
	}
	parentLangKeys: string[] = ['HOME', 'MENU', 'ROUTER', 'SHARE', 'CONFIRM', 'RESPONSE_MSG'];
	labels: string[] = [];

	roleInfo: any = {};
	modalDialog: boolean = false;
	listAction: IBaseListAction[] = [];
	UserConst = UserConst;
	PermissionConst = PermissionConst;
	rows = [];

	permissionFull: IKeyPermission[] = [];

	ngOnInit(): void {
		this._selectedLangService.getLanguage.subscribe((resLang: IResponseLang) => {
			if(resLang) {
				this.labels = this._selectedLangService.getKeyLangs(this.parentLangKeys, resLang.keys);
				console.log('labels', this.labels);
			}
			this.setPage({ page: this.offset });
	 	 });
		this.getPermissionWeb();
	}


	genListAction(data = []) {
		this.listAction = data.map((item) => {
		  const actions = [];
		  if (this.isGranted([PermissionConst.RoleUpdate])) {  
			actions.push({ 
				data: item,
				label: this.labels['SHARE']['UPDATE'],
				icon: "pi pi-pencil",
				command: ($event) => {
					this.updateRole($event.item.data);
				}
				});
		  }
		
		if (this.isGranted([PermissionConst.RoleDelete])) {   
			actions.push({ 
			data: item,
			label: this.labels['SHARE']['DELETE'],
			icon: "pi pi-trash",
			command: ($event) => {
				this.deleteRole($event.item.data);
			}
			});
		}
	
		  return actions;
		});
		console.log(this.listAction);
	}

	// CREATE OR UPDATE ROLE PARTNER
	createRole() {
	this.roleInfo = {};
	this.modalDialog = true;
	}

	updateRole(role) {
		this.roleInfo = role;
		this.modalDialog = true;
	}

	deleteRole(role) {
        this._confirmationService.confirm({
            message: this.labels['CONFIRM']['ARE_YOU_SURE_TO_DELETE_THIS_ROLE'],
            header: this.labels['SHARE']['DELETE'],
			acceptLabel: this.labels['CONFIRM']['YES'],
			rejectLabel: this.labels['CONFIRM']['NO'],
            icon: 'pi pi-exclamation-triangle',
            accept: () => {
				this._roleService.delete(role.id).subscribe((response) => {
					if ( this.handleResponseInterceptor(response, this.labels['RESPONSE_MSG']['DELETE_SUCCESS'])) {
					  this.setPage(this.getPageCurrentInfo());
					}
				  }, (err) => {
					console.log('err____', err);
					this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER']);
				  });
            }
        });
    }

	getPermissionWeb() {
		this.permissionFull = [];
		this.permissionFull = Object.keys(PermissionConfig).map((key) => {
		  return { ...PermissionConfig[key], key: key, label: PermissionConfig[key].name };
		});
	}

	hideModal(reload: boolean = false) {
		this.modalDialog = false;
		if (reload) this.setPage();
	}

	setPage(pageInfo?: IPageInfo) {
		this.page.pageNumber = pageInfo?.page ?? this.offset;
		if (pageInfo?.rows) this.page.pageSize = pageInfo?.rows;
		this.page.keyword = this.keyword;
		this.isLoading = true;
		this._roleService.getAllRole().subscribe((res) => {
			this.rows = res.data.items;
			this.genListAction(this.rows);
			this.isLoading = false;
		})

	}
}
