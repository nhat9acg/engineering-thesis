import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CrudComponentBase } from '@shared/crud-component-base';
import { NotificationService } from '@shared/services/notification.service';
import { AppUtilsService } from '@shared/services/utils.service';
import { marked } from 'marked';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-notification-detail',
  templateUrl: './notification-detail.component.html',
  styleUrls: ['./notification-detail.component.scss']
})
export class NotificationDetailComponent extends CrudComponentBase {

  selectedNotification: any;
  constructor(
    injector: Injector,
    messageService: MessageService,
    private route: ActivatedRoute,
    private notificationService: NotificationService,
    private _utilsService: AppUtilsService,
  ) { 
    super(injector, messageService);
    this.selectedNotification = this._utilsService.getItem();
  }

  ngOnInit(): void {
  }

  transformContentToHTML(content: string): string {
    if (content.includes('<') && content.includes('>')) {
      return content;
    } else {
      return marked(content);
    }
  }

}
