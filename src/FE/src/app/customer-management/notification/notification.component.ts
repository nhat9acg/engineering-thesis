import { Component, Injector, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CrudComponentBase } from '@shared/crud-component-base';
import { IPageInfo } from '@shared/interfaces/base-interface';
import { NotificationService } from '@shared/services/notification.service';
import { AppUtilsService } from '@shared/services/utils.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent extends CrudComponentBase {
  notifications: any[] = [];

  constructor(
    injector: Injector,
    messageService: MessageService,
    private notificationService: NotificationService,
    private _utilsService: AppUtilsService, 
    private router: Router,) 
    {
      super(injector, messageService);
    }

  ngOnInit(): void {
    this.setPage();
  }

  setPage(event?: IPageInfo) {
    this.isLoading = true;
    if (event) {
      this.page.pageNumber = event.page;
      this.page.pageSize = event.rows;
    }
    this.page.keyword = this.keyword;
    this.notificationService.getNotification().subscribe((res) => {
      this.isLoading = false;
      if (this.handleResponseInterceptor(res, '')) {
        this.notifications = res.data?.items;
      }
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);
    });
  }

  viewNotificationDetail(notification): void {
    this.notificationService.readNotification(notification.id).subscribe((res) => {
      this.isLoading = false;
      if (this.handleResponseInterceptor(res, '')) {
        this._utilsService.setItem(notification);
        this.router.navigate(['notification', notification.id]);
      }
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);
    });
  }

}

