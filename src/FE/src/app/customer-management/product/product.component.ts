import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConsts } from '@shared/AppConsts';
import { CrudComponentBase } from '@shared/crud-component-base';
import { IPageInfo } from '@shared/interfaces/base-interface';
import { CartService } from '@shared/services/cart.service';
import { ProductService } from '@shared/services/product.service';
import { AppUtilsService } from '@shared/services/utils.service';
import { ConfirmationService, MessageService, SelectItem } from 'primeng/api';
import { DataView } from 'primeng/dataview';
import { DialogService } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent extends CrudComponentBase {

    products: any[] = [];

    sortOptions: SelectItem[] = [];
    AppConsts = AppConsts;
    sortOrder: number = 0;

    sortField: string = '';

    constructor(
      injector: Injector,
      private _dialogService: DialogService,
      private confirmationService: ConfirmationService,
      private router: Router,
      private routeActive: ActivatedRoute,
      messageService: MessageService,
      private _productService: ProductService,
      private _cartService: CartService,
      private _utilsService: AppUtilsService,
      ) { 
        super(injector, messageService);
      }
      dataFilter = {
        field: null,
        status: null,
      }
    ngOnInit() {
       
      this.setPage();
        this.sortOptions = [
            { label: 'Price High to Low', value: '!defaultPrice' },
            { label: 'Price Low to High', value: 'defaultPrice' }
        ];
    }

    detail(item) {
      this._utilsService.setItem(item);
      this.router.navigate([`/product/detail/` + this.cryptEncode(item?.id)]);
    }

    addCart(product) {
      let body = {
        productId: product.id,
        quantity: 1,
      }
      this._cartService.create(body).subscribe((res) => {
        this.isLoading = false;
        if (this.handleResponseInterceptor(res, 'Sản phẩm đã được thêm vào giỏ hàng')) {
        }
      }, (err) => {
        this.isLoading = false;
        console.log('Error-------', err);
      });
    }

    setPage(event?: IPageInfo) {
      this._productService.getAllNoPaging().subscribe((res) => {
        this.isLoading = false;
        if (this.handleResponseInterceptor(res, '')) {
          this.products = res.data?.items.map(item => {
            if(item.allowOrder) {
              item.inventoryStatus = 'INSTOCK';
            } else {
              item.inventoryStatus = 'OUTOFSTOCK'
            }
            return { ...item, rating: 5 };
          });
      
        }
      }, (err) => {
        this.isLoading = false;
        console.log('Error-------', err);
      });
    }

    onSortChange(event: any) {
        const value = event.value;

        if (value.indexOf('!') === 0) {
            this.sortOrder = -1;
            this.sortField = value.substring(1, value.length);
        } else {
            this.sortOrder = 1;
            this.sortField = value;
        }
    }

    onFilter(dv: DataView, event: Event) {
        dv.filter((event.target as HTMLInputElement).value);
    }
    
}