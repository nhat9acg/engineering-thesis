import { Component, Injector, OnInit } from '@angular/core';
import { AppConsts } from '@shared/AppConsts';
import { CrudComponentBase } from '@shared/crud-component-base';
import { AppUtilsService } from '@shared/services/utils.service';
import { MessageService } from 'primeng/api';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent extends CrudComponentBase {

  constructor(
    injector: Injector,
    messageService: MessageService,
    private _utilsService: AppUtilsService,
  ) { 
    super(injector, messageService);
    this.product = this._utilsService.getItem();
  }

  product: any;
  AppConsts = AppConsts;

  ngOnInit(): void {
  }

  addToCart(): void {
  }

  submitReview(): void {
  
  }
}
