import { Component, Injector, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConsts } from '@shared/AppConsts';
import { CrudComponentBase } from '@shared/crud-component-base';
import { IPageInfo } from '@shared/interfaces/base-interface';
import { CartService } from '@shared/services/cart.service';
import { AppUtilsService } from '@shared/services/utils.service';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { RetailOrdersAddComponent } from 'src/app/order-management/retail-orders/retail-orders-add/retail-orders-add.component';

interface CartItem {
  id?: number;
  productName: string;
  price: number;
  quantity: number;
  total: number;
  image?: string;
}

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})

export class CartComponent extends CrudComponentBase {

  constructor(
    injector: Injector,
    private _dialogService: DialogService,
    private confirmationService: ConfirmationService,
    private router: Router,
    private routeActive: ActivatedRoute,
    messageService: MessageService,
    private _cartService: CartService,
    private _utilsService: AppUtilsService,
  ) {
    super(injector, messageService);
  }
  AppConsts = AppConsts;

  cartItems: any[] = [

  ];
  selectedItems: CartItem[] = [];
  selectAll: boolean = false;

  ngOnInit(): void {
    this.setPage();
  }

  setPage(event?: IPageInfo) {
    this.isLoading = true;
    if (event) {
      this.page.pageNumber = event.page;
      this.page.pageSize = event.rows;
    }
    this.page.keyword = this.keyword;
    this._cartService.getAll(this.page).subscribe((res) => {
      this.isLoading = false;
      if (this.handleResponseInterceptor(res, '')) {
        this.cartItems = res.data?.items;

      }
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);
    });
  }


  decreaseQuantity(item: CartItem) {
    if (item.quantity > 1) {
      item.quantity--;
      item.total = item.price * item.quantity;
    }
    this._cartService.update(item).subscribe((res) => {
      this.isLoading = false;
      if (this.handleResponseInterceptor(res, '')) {
        // this.setPage();
      }
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);
    });
  }

  increaseQuantity(item: CartItem) {
    item.quantity++;
    item.total = item.price * item.quantity;
    this._cartService.update(item).subscribe((res) => {
      this.isLoading = false;
      if (this.handleResponseInterceptor(res, '')) {
        // this.setPage();
      }
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);
    });
  }

  removeItem(item: CartItem) {
    this._cartService.delete(item).subscribe((res) => {
      this.isLoading = false;
      if (this.handleResponseInterceptor(res, 'Xóa thành công')) {
        this.setPage();
      }
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);
    });

    const selectedIndex = this.selectedItems.indexOf(item);
    if (selectedIndex !== -1) {
      this.selectedItems.splice(selectedIndex, 1);
    }
  }

  toggleSelectAll() {
    this.selectAll = !this.selectAll;
    if (this.selectAll) {
      this.selectedItems = [...this.cartItems];
    } else {
      this.selectedItems = [];
    }
  }

  calculateTotalSelected() {
    let total = 0;
    for (const item of this.selectedItems) {
      total += item.total;
    }
    return this.formatCurrency(total);
  }

  buyItems() {
    // Logic for buying the selected items
    console.log('Buying selected items:', this.selectedItems);
    if(this.selectedItems.length) {
      this._utilsService.setData(this.selectedItems);
      this.router.navigate(['/order-management/retail-orders/create']);
    } else {
      this.messageError("Mời chọn sản phẩm để thanh toán");
    }
  }
}
