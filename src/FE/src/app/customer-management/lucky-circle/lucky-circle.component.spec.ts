import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LuckyCircleComponent } from './lucky-circle.component';

describe('LuckyCircleComponent', () => {
  let component: LuckyCircleComponent;
  let fixture: ComponentFixture<LuckyCircleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LuckyCircleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LuckyCircleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
