import { Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudComponentBase } from '@shared/crud-component-base';
import { IPageInfo } from '@shared/interfaces/base-interface';
import { ChatService } from '@shared/services/chat.service';
import { SignalrService } from '@shared/services/signalr.service';
import { AppUtilsService } from '@shared/services/utils.service';
import { ConfirmationService, MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
interface Message {
  message: string;
  sentByMe: boolean;
  createdDate: Date;
}

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
  export class ChatComponent extends CrudComponentBase {
    constructor(
      injector: Injector,
      private _dialogService: DialogService,
      private confirmationService: ConfirmationService,
      private router: Router,
      private routeActive: ActivatedRoute,
      messageService: MessageService,
      private _chatService: ChatService,
      private _utilsService: AppUtilsService,
      private _signalrService: SignalrService,
    ) {
      super(injector, messageService);
    }

  ngOnInit(): void {
    this.setPage();
    this.startSignalR();
  }

  private startSignalR() {
    this._signalrService.startConnection()
      .then(() => {
        this._signalrService.listen('ReceiveMessage', (data) => {
          console.log("data",data);
          this.setPage(false);
          // const index = this.dataSource.findIndex(e => e.id === data?.id);
          // if (index !== -1) {
           
          // }
        });
      })
      .catch((error) => {
        console.log("Error while connecting to SignalR:", error);
      });
  }

  setPage(isLoading: boolean = true, event?: IPageInfo) {
    this.isLoading = isLoading;
    if (event) {
      this.page.pageNumber = event.page;
      this.page.pageSize = event.rows;
    }
    this.page.keyword = this.keyword;
    this._chatService.getAll(this.page).subscribe((res) => {
      this.isLoading = false;
      if (this.handleResponseInterceptor(res, '')) {
        this.messageList = res.data?.items;
        this.itemSelected = this.messageList[0];
        this.messages = this.itemSelected.chatDetail;
        // if(this.itemSelected) {
        //   this.itemSelected = this.messageList[0];
        //   this.messages = this.itemSelected.chatDetail;
        // } else {
        //   this.itemSelected = this.messageList[0];
        //   this.messages = this.itemSelected.chatDetail;
        // }
      } 
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);
    });
  }

  callMenuItems: MenuItem[] = [
    { label: 'Gọi điện thoại', icon: 'pi pi-phone', command: () => this.makeCall() },
    { label: 'Gọi video', icon: 'pi pi-video', command: () => this.makeVideoCall() }
  ];
  messageList: any[] = [];
  messages: Message[] = [
  ];

  newMessage: string = '';

  sendMessage(itemSelected) {
    if (this.newMessage.trim() !== '') {
      let body = {
        receiverUserId: itemSelected?.receiverUserId,
        message: this.newMessage,
      }
      this._chatService.create(body).subscribe((res) => {
        this.isLoading = false;
        if (this.handleResponseInterceptor(res, '')) {
          this.setPage();
          console.log("itemSelected",this.itemSelected);
          
          this.newMessage = '';
        }
      }, (err) => {
        this.isLoading = false;
        console.log('Error-------', err);
      });
    }
  }

  makeCall() {
    this.messages.push({
      message: 'Đã gọi điện thoại...',
      sentByMe: true,
      createdDate: new Date()
    });
  }

  makeVideoCall() {
    this.messages.push({
      message: 'Đã gọi video...',
      sentByMe: true,
      createdDate: new Date()
    });
  }
  // @ViewChild('chatListContainer') list?: ElementRef<HTMLDivElement>;
 
  // ngAfterViewChecked() {
  //   this.scrollToBottom()
  // }
 
  // send() {
  //   this.chatMessages.push({
  //     message: this.chatInputMessage,
  //     user: this.human
  //   });
 
  //   this.chatInputMessage = ""
  //   this.scrollToBottom()
  // }
 
  // scrollToBottom() {
  //   const maxScroll = this.list?.nativeElement.scrollHeight;
  //   this.list?.nativeElement.scrollTo({top: maxScroll, behavior: 'smooth'});
  // }
 
  itemSelected: any;

  chatChange(item, isAdmin) {
    console.log("item", item);
    this.messages = item.chatDetail;
  }
}
