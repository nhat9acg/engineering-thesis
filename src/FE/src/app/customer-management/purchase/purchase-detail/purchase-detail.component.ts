import { Component, Injector, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AppConsts, OrderCustomerConst } from '@shared/AppConsts';
import { CrudComponentBase } from '@shared/crud-component-base';
import { IPageInfo } from '@shared/interfaces/base-interface';
import { CartService } from '@shared/services/cart.service';
import { OrderCustomerService } from '@shared/services/order-customer.service';
import { ProductService } from '@shared/services/product.service';
import { SelectedLanguageService } from '@shared/services/selected-language.service';
import { ConfirmationService, MessageService, SelectItem } from 'primeng/api';
import { DataView } from 'primeng/dataview';
import { DialogService } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-purchase-detail',
  templateUrl: './purchase-detail.component.html',
  styleUrls: ['./purchase-detail.component.scss']
})
export class PurchaseDetailComponent extends CrudComponentBase {

  products: any[] = [];

  sortOptions: SelectItem[] = [];
  AppConsts = AppConsts;
  sortOrder: number = 0;
  OrderCustomerConst = OrderCustomerConst;
  sortField: string = '';
  filter = {
    status: null,
    dates: null
  }
  labels: string[] = [];
  language: string = 'en';
  @Input() type: number;
  parentLangKeys: string[] = ['HOME', 'MENU', 'RESTAURANT', 'SHARE', 'ROUTER', 'CONFIRM', 'RESPONSE_MSG', 'BUSINESS_CUSTOMER','ORDER'];

  constructor(
    injector: Injector,
    private _dialogService: DialogService,
    private confirmationService: ConfirmationService,
    private router: Router,
    private routeActive: ActivatedRoute,
    messageService: MessageService,
    private _orderCustomerService: OrderCustomerService,
    private _cartService: CartService,
    private selectedLanguageService: SelectedLanguageService,
    public _translateService: TranslateService,

  ) {
    super(injector, messageService);
  }

  ngOnInit() {
    this.selectedLanguageService.getLanguage.subscribe((resLang) => {
      if (resLang) {
        this.labels = this.selectedLanguageService.getKeyLangs(this.parentLangKeys, resLang.keys);
        this.language = resLang?.lang;
      }
    });
    if (this.type) {
      this.filter.status = this.type;
    }
    this.setPage();
    this.sortOptions = [
      { label: 'Price High to Low', value: '!defaultPrice' },
      { label: 'Price Low to High', value: 'defaultPrice' }
    ];
  }

  addCart(product) {
    let body = {
      productId: product.id,
      quantity: 1,
    }
    this._cartService.create(body).subscribe((res) => {
      this.isLoading = false;
      if (this.handleResponseInterceptor(res, 'Sản phẩm đã được thêm vào giỏ hàng')) {
      }
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);
    });
  }

  setPage(event?: IPageInfo) {
    if (event) {
      this.page.pageNumber = event.page;
      this.page.pageSize = event.rows;
    }
    this.page.keyword = this.keyword;
    this._orderCustomerService.getAll(this.page, this.filter).subscribe((res) => {
      this.isLoading = false;
      if (this.handleResponseInterceptor(res, '')) {
        this.purchaseOrders = res.data?.items;
      }
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);
    });
  }

  onSortChange(event: any) {
    const value = event.value;

    if (value.indexOf('!') === 0) {
      this.sortOrder = -1;
      this.sortField = value.substring(1, value.length);
    } else {
      this.sortOrder = 1;
      this.sortField = value;
    }
  }

  onFilter(dv: DataView, event: Event) {
    dv.filter((event.target as HTMLInputElement).value);
  }

  ////
  purchaseOrders: any[] = [
    {
      orderNumber: 'PO001',
      products: [
        { productName: 'Product 1', quantity: 10 },
        { productName: 'Product 2', quantity: 5 }
      ],
      status: 'Pending'
    },
    {
      orderNumber: 'PO002',
      products: [
        { productName: 'Product 3', quantity: 8 },
        { productName: 'Product 4', quantity: 3 }
      ],
      status: 'Completed'
    }
  ];

  newOrder: any = {
    orderNumber: '',
    products: [],
    status: ''
  };

  newProduct: any = {
    productName: '',
    quantity: 0
  };

  addProduct() {
    this.newOrder.products.push({ ...this.newProduct });
    this.newProduct = {
      productName: '',
      quantity: 0
    };
  }

  addOrder() {
    this.purchaseOrders.push({ ...this.newOrder });
    this.newOrder = {
      orderNumber: '',
      products: [],
      status: ''
    };
  }

  removeOrder(purchaseOrder: any) {
    const index = this.purchaseOrders.indexOf(purchaseOrder);
    if (index !== -1) {
      this.purchaseOrders.splice(index, 1);
    }
  }  
  
  navigate(item) {
    this.router.navigate(['/order-management/retail-orders/view-detail/'+this.cryptEncode(item?.orderId)]);
  }
}