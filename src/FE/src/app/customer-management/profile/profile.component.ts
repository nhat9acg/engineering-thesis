import { Component, ElementRef, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PermissionConst } from '@shared/AppConsts';
import { CrudComponentBase } from '@shared/crud-component-base';
import { CustomerService } from '@shared/services/customer.service';
import { SelectedLanguageService } from '@shared/services/selected-language.service';
import { MessageService } from 'primeng/api';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent extends CrudComponentBase {

  constructor(
    injector: Injector,
    messageService: MessageService,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private _customerService: CustomerService,
    private dialogService: DialogService,
    private router: Router,
    private fb: FormBuilder,
    private selectedLanguageService: SelectedLanguageService,
  ) {
    super(injector, messageService);
  }

  itemDetail: any;
  @Input() contentHeight: number = 0;

  postForm: FormGroup;
  labels: string[] = [];
  isEdit: boolean = false;

  parentLangKeys: string[] = ['RESPONSE_MSG'];
  currentLang: string;
  PermissionConst = PermissionConst;
  cols = [
    { labelKeyLang: 'Tên đăng nhập', controlName: 'username', type: ''},
    { labelKeyLang: 'Họ và Tên', controlName: 'fullName', type: 'input' },
    { labelKeyLang: 'Số điện thoại', controlName: 'phone', type: 'input' },
    { labelKeyLang: 'Email', controlName: 'email', type: 'input' },
  ]

  ngOnInit(): void {
    this.selectedLanguageService.getLanguage.subscribe((resLang) => {
      if(resLang) {
        this.currentLang = resLang?.lang;
        this.labels = this.selectedLanguageService.getKeyLangs(this.parentLangKeys, resLang.keys);
      }
    });
    this.init(); 
   
  }

  init() {
    this._customerService.get().subscribe((response) => {
      if (this.handleResponseInterceptor(response, '')) {
        this.itemDetail = response.data;
        this.postForm = this.fb.group({
          id: [this.itemDetail?.id || null, []],
          fullName: [this.itemDetail?.fullName || '', Validators.required],
          username: [this.itemDetail?.username || '', [Validators.required]],
          phone: [this.itemDetail?.phone || '', [Validators.required]],
          email: [this.itemDetail?.email || '', [Validators.required]],
        });
      }
    }, () => {
      this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER']);
    }
    );
  }

  get postFormControl() {
    return this.postForm.controls;
  }

  changeEdit() {
    if(!this.isEdit) {
      this.onSubmit();
    }
  }

  onSubmit() {
    if (!this.checkInValidForm(this.postForm)) {
 
        this._customerService.update(this.postForm.value).subscribe((response) => {
          if (this.handleResponseInterceptor(response, this.labels['RESPONSE_MSG']['UPDATE_SUCCESS'])) {
            this.ref.close(true);
          }
        }, () => {
          this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER']);
        }
        );
    }
  }

}


