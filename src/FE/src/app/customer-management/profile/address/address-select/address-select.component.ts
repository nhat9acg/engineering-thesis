import { Component, Injector, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { RestaurantConst, SearchConst } from "@shared/AppConsts";
import { CrudComponentBase } from "@shared/crud-component-base";
import { IBaseListAction } from "@shared/interfaces/base-interface";
import { IColumn } from "@shared/interfaces/business-customer.interface";
import { IAddress, IBusinessCustomerDropdown, IRestaurant } from "@shared/interfaces/restaurant.interface";
import { Page } from "@shared/model/page";
import { RestaurantService } from "@shared/services/restaurant.service";
import { SelectedLanguageService } from "@shared/services/selected-language.service";
import { ConfirmationService, MessageService } from "primeng/api";
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { forkJoin } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { BreadcrumbService } from "src/app/layout/breadcrumb/breadcrumb.service";
import { AddressAddComponent } from "../address-add/address-add.component";
import { OrderCustomerService } from "@shared/services/order-customer.service";
import { User } from "@shared/model/user.model";

@Component({
  selector: 'app-address-select',
  templateUrl: './address-select.component.html',
  styleUrls: ['./address-select.component.scss'],
	providers: [DialogService, ConfirmationService, MessageService],
})
export class AddressSelectComponent extends CrudComponentBase {
	constructor(
		injector: Injector,
		private dialogService: DialogService,
		private confirmationService: ConfirmationService,
		messageService: MessageService,
		private breadcrumbService: BreadcrumbService,
		private selectedLanguageService: SelectedLanguageService,
		public _translateService: TranslateService,
		public config: DynamicDialogConfig,
		public ref: DynamicDialogRef,
    private _orderCustomerService: OrderCustomerService,
	) {
		super(injector, messageService);
    this.userInfo = this.getUser();
	}
  userInfo: User;
	parentLangKeys: string[] = ['HOME', 'MENU', 'RESTAURANT', 'SHARE', 'ROUTER', 'CONFIRM', 'RESPONSE_MSG', 'BUSINESS_CUSTOMER', 'DELIVERY_ADDRESS'];
	labels: string[] = [];
	rows: IRestaurant[] = [];
	listAction: IBaseListAction[] = [];
	page = new Page();
	offset = 0;
	language: string = 'en';

	ngOnInit(): void {
		this.selectedLanguageService.getLanguage.subscribe((resLang) => {
		  if(resLang) {
			this.language = resLang?.lang;
			this.labels = this.selectedLanguageService.getKeyLangs(this.parentLangKeys, resLang.keys);
		  }
		  this.setPage();
		});
		//
	}

	chooseAddress(row){
		let result = {
			id: row.id,
			labelName: `${row.address} - ${row.phone}` 
		}

		this.ref.close(result);
	}

	create() {
		const ref = this.dialogService.open(AddressAddComponent, {
		  data: {
      userId : this.userInfo.user_id,
			labels: this.labels,
			language: this.language,
		  },
		  contentStyle: {"max-height": "400px", "overflow": "auto", "margin-bottom": "10px" },
		  header: this.labels['DELIVERY_ADDRESS']['ADD_TITLE'],
		  width: '600px',
		}).onClose.subscribe(result => {
		  if (result) {
			this.setPage();
		  } 
		})
	}

	setPage() {
		this.isLoading = true;
		forkJoin([this._orderCustomerService.getAllAddress()]).subscribe(([res]) => {
		  this.isLoading = false;
		  if (this.handleResponseInterceptor(res, '')) {
			  this.rows = res.data;
		  }
		}, (err) => {
		  this.isLoading = false;
		  console.log('Error-------', err);
	
		});
	  }
}