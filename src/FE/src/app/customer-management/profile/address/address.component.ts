import { Component, Injector, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SearchConst, RestaurantConst, PermissionConst } from '@shared/AppConsts';
import { CrudComponentBase } from '@shared/crud-component-base';
import { Page } from '@shared/model/page';

import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { debounceTime } from 'rxjs/operators';
import { BreadcrumbService } from 'src/app/layout/breadcrumb/breadcrumb.service';
import { RestaurantService } from '@shared/services/restaurant.service';
import { IColumn } from '@shared/interfaces/business-customer.interface';
import { IBaseListAction } from '@shared/interfaces/base-interface';
import { SelectedLanguageService } from '@shared/services/selected-language.service';
import { TranslateService } from '@ngx-translate/core';
import { IAddress, IBusinessCustomerDropdown, IRestaurant } from '@shared/interfaces/restaurant.interface';
import { forkJoin } from 'rxjs';
import { UserAddressService } from '@shared/services/user-address.service';
import { User } from '@shared/model/user.model';
import { AddressAddComponent } from './address-add/address-add.component';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss'],
  providers: [DialogService, ConfirmationService, MessageService]
})
export class AddressComponent extends CrudComponentBase {

  constructor(
    injector: Injector,
    private dialogService: DialogService,
    private confirmationService: ConfirmationService,
    private router: Router,
    private routeActive: ActivatedRoute,
    messageService: MessageService,
    private breadcrumbService: BreadcrumbService,
    private _userAddressService: UserAddressService,
    private selectedLanguageService: SelectedLanguageService,
    public _translateService: TranslateService,
  ) {
    
    super(injector, messageService);
    this.userInfo = this.getUser();
  }

 
  @Input() contentHeight: number;
  userInfo: User;
  parentLangKeys: string[] = ['HOME', 'MENU', 'RESTAURANT', 'SHARE', 'ROUTER', 'CONFIRM', 'RESPONSE_MSG', 'BUSINESS_CUSTOMER','DELIVERY_ADDRESS'];
  labels: string[] = [];
  ref: DynamicDialogRef;
  rows: IRestaurant[] = [];
  listAction: IBaseListAction[] = [];
  page = new Page();
  offset = 0;
  language: string = 'en';
  RestaurantConst = RestaurantConst;
  PermissionConst = PermissionConst;
  
  ngOnInit(): void {
    this.selectedLanguageService.getLanguage.subscribe((resLang) => {
      if(resLang) {
        this.language = resLang?.lang;
        this.labels = this.selectedLanguageService.getKeyLangs(this.parentLangKeys, resLang.keys);
      
        // this.breadcrumbService.setItems([
        //   { label: this.labels['ROUTER']['HOME_PAGE'], routerLink: ['/home'] },
        //   { label: this.labels['ROUTER']['RESTAURANT'], routerLink: ['/business-customer-management/restaurant'] },
        // ]);
      }
      this.setPage();
    });
    //
    this.subject.keyword.pipe(debounceTime(SearchConst.DEBOUNCE_TIME)).subscribe(() => {
      if (this.keyword === "") {
        this.setPage();
      } else {
        this.setPage();
      }
    });
  }

  showData(rows) {
    for (let row of rows) {      
    };
  }

  create() {
    const ref = this.dialogService.open(AddressAddComponent, {
      data: {
        labels: this.labels,
        userId: this.userInfo.user_id,
        language: this.language,
      },
      contentStyle: {"max-height": "400px", "overflow": "auto", "margin-bottom": "10px" },
      header: this.labels['DELIVERY_ADDRESS']['ADD_TITLE'],
      width: '600px',
    }).onClose.subscribe(result => {
      if (result) {
        this.setPage();
      } 
    })
  }

  edit(item: IAddress, isDefault) {
    console.log("+__+",item);
    if(!isDefault) {
      const ref = this.dialogService.open(AddressAddComponent, {
        data: {
          labels: this.labels,
          userId: this.userInfo.user_id,
          language: this.language,
          inputData: item,
        },
        contentStyle: {"max-height": "400px", "overflow": "auto", "margin-bottom": "10px" },
        header: this.labels['DELIVERY_ADDRESS']['EDIT_TITLE'],
        width: '600px',
      }).onClose.subscribe(result => {
        if (result) {
          this.setPage();
        } 
      })

    } else {
      item.isDefault = true;
      this._userAddressService.updateAddress(item).subscribe((response) => {
        if (this.handleResponseInterceptor(response, this.labels['RESPONSE_MSG']['UPDATE_SUCCESS'])) {
          this.setPage();
        }
      }, () => {
        this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER']);
      }
      );
    }
  }

  delete(item) {
    this.confirmationService.confirm({
      message: this.labels['DELIVERY_ADDRESS']['DELETE_TITLE'],
      acceptLabel: this.labels['CONFIRM']['YES'],
      rejectLabel: this.labels['CONFIRM']['NO'],
      accept: () => {
        this._userAddressService.deleteAddress(item.id).subscribe((response) => {
          if (this.handleResponseInterceptor(response, this.labels['CONFIRM']['DELETE_SUCCESS'])) {
            this.setPage();
          }
        });
      }
    });
  }

  setPage() {
    this.isLoading = true;
    forkJoin([this._userAddressService.getAllAddresses(this.userInfo.user_id,)]).subscribe(([res]) => {
      this.isLoading = false;
      if (this.handleResponseInterceptor(res, '')) {
        this.rows = res.data;
      }
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);

    });
  }
}

