import { RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { AppMainComponent } from "./layout/main/app.main.component";
import { HomeComponent } from "./home/home.component";
import { AppRouteGuard } from "@shared/auth/auth-route-guard";
import { FormBaseComponent } from "./form-general/form-base/form-base.component";
import { RoleManagerComponent } from "./system-manager/role-manager/role-manager.component";
import { UserManagerComponent } from "./system-manager/user-manager/user-manager.component";
import { RestaurantComponent } from "./business-customers-management/restaurant/restaurant.component";
import { BusinessCustomerComponent } from "./business-customers-management/business-customer/business-customer.component";
import { BusinessCustomerDetailComponent } from "./business-customers-management/business-customer/business-customer-detail/business-customer-detail.component";
import { RestaurantDetailComponent } from "./business-customers-management/restaurant/restaurant-detail/restaurant-detail.component";
import { GroupCustomerComponent } from "./business-customers-management/group-customer/group-customer.component";
import { ProductManagerComponent } from "./product-management/product-manager/product-manager.component";
import { ProductCategoryComponent } from "./product-management/product-category/product-category.component";
import { ProductPriceComponent } from "./product-management/product-price/product-price.component";
import { OrderComponent } from "./order-management/order/order.component";
import { OrderAddComponent } from "./order-management/order/order-add/order-add.component";
import { CalculationUnitComponent } from "./product-management/calculation-unit/calculation-unit.component";
import { ViewOrderComponent } from "./order-management/order/view-order/view-order.component";
import { CalendarComponent } from "./system-manager/calendar/calendar.component";
import { PermissionConst } from "@shared/AppConsts";
import { ProductComponent } from "./customer-management/product/product.component";
import { DocumentComponent } from "./system-manager/document/document.component";
import { CartComponent } from "./customer-management/cart/cart.component";
import { ProfileComponent } from "./customer-management/profile/profile.component";
import { AddressComponent } from "./customer-management/profile/address/address.component";
import { RetailOrdersComponent } from "./order-management/retail-orders/retail-orders.component";
import { RetailOrdersAddComponent } from "./order-management/retail-orders/retail-orders-add/retail-orders-add.component";
import { ViewRetailOrdersComponent } from "./order-management/retail-orders/view-retail-orders/view-retail-orders.component";
import { ShippingCompanyComponent } from "./product-management/shipping-company/shipping-company.component";
import { PurchaseComponent } from "./customer-management/purchase/purchase.component";
import { ProductDetailComponent } from "./customer-management/product/product-detail/product-detail.component";
import { SystemNotificationComponent } from "./notification-management/system-notification/system-notification.component";
import { NotificationComponent } from "./customer-management/notification/notification.component";
import { NotificationDetailComponent } from "./customer-management/notification/notification-detail/notification-detail.component";
import { ChatComponent } from "./customer-management/chat/chat.component";
import { ManagementReportComponent } from "./export-report/management-report/management-report.component";
import { MediaComponent } from "./media/media.component";
import { DetailBusinessComponent } from "./business-customers-management/detail-business/detail-business.component";

@NgModule({
	imports: [
		RouterModule.forChild([
			{
				path: "",
				component: AppMainComponent,
				children: [
					{ path: "home", component: HomeComponent, canActivate: [AppRouteGuard] },
					{ path: "form-base", component: FormBaseComponent},
					{ path: "business-detail", component: DetailBusinessComponent, canActivate: [AppRouteGuard] },
					{ 
						path: "system-manager",
						children: [
							{ path: 'role-manager', component: RoleManagerComponent, canActivate: [AppRouteGuard]},
							{ path: 'user-manager', component: UserManagerComponent, canActivate: [AppRouteGuard]},
							{ path: 'calendar', component: CalendarComponent, canActivate: [AppRouteGuard]},
							{ path: "document", component: DocumentComponent, canActivate: [AppRouteGuard]},
						] 
					},
					{ 
						path: "notification-management",
						children: [
							{ path: 'system-notification', component: SystemNotificationComponent, canActivate: [AppRouteGuard]},
						] 
					},
					{ 
						path: "business-customer-management",
						children: [
							{ path: 'business-customer', component: BusinessCustomerComponent, canActivate: [AppRouteGuard],data: {permissions: [PermissionConst.BusinessCustomerMenu]},},
							{ path: 'business-customer/detail/:id', component: BusinessCustomerDetailComponent, canActivate: [AppRouteGuard] ,data: {permissions: [PermissionConst.BusinessCustomerUpdate]},},
							{ path: 'restaurant', component: RestaurantComponent, canActivate: [AppRouteGuard],data: {permissions: [PermissionConst.RestaurantMenu]},},
							{ path: 'restaurant/detail/:id', component: RestaurantDetailComponent, canActivate: [AppRouteGuard],data: {permissions: [PermissionConst.RestaurantUpdate]},},
							{ path: 'group-customer', component: GroupCustomerComponent, canActivate: [AppRouteGuard],data: {permissions: [PermissionConst.GroupCustomerTable]},},
						] 
					},
					{
						path: "product-management",
						children: [
							{ path: 'calculation-unit', component: CalculationUnitComponent, canActivate: [AppRouteGuard]},
							{ path: 'shipping-company', component: ShippingCompanyComponent, canActivate: [AppRouteGuard]},
							{ path: 'product-manager', component: ProductManagerComponent, canActivate: [AppRouteGuard]},
							{ path: 'product-category', component: ProductCategoryComponent, canActivate: [AppRouteGuard]},
							{ path: 'product-price', component: ProductPriceComponent, canActivate: [AppRouteGuard]},
						] 
					},
					{
						path: "order-management",
						children: [
							{ path: 'order', component: OrderComponent, canActivate: [AppRouteGuard],data: {permissions: [PermissionConst.OrderMenu]},},
							{ path: 'order/create', component: OrderAddComponent, canActivate: [AppRouteGuard],data: {permissions: [PermissionConst.OrderCreate]},},
							{ path: 'order/update/:id', component: OrderAddComponent, canActivate: [AppRouteGuard],data: {permissions: [PermissionConst.OrderUpdate]},},
							{ path: 'order/view-detail/:id', component: ViewOrderComponent, canActivate: [AppRouteGuard],data: {permissions: [PermissionConst.OrderMenu]},},

							{ path: 'retail-orders', component: RetailOrdersComponent, canActivate: [AppRouteGuard]},
							{ path: 'retail-orders/create', component: RetailOrdersAddComponent, canActivate: [AppRouteGuard]},
							{ path: 'retail-orders/update/:id', component: RetailOrdersAddComponent, canActivate: [AppRouteGuard]},
							{ path: 'retail-orders/view-detail/:id', component: ViewRetailOrdersComponent, canActivate: [AppRouteGuard]},
						] 
					},
					{
						path: "export-report",
						children: [
							{ path: "management-report", component: ManagementReportComponent, canActivate: [AppRouteGuard]},
							{ path: "operational-report", component: ManagementReportComponent, canActivate: [AppRouteGuard]},
							{ path: "business-report", component: ManagementReportComponent, canActivate: [AppRouteGuard]},
						],
					},
					{ path: "media", component: MediaComponent, canActivate: [AppRouteGuard]},
					{ path: "product", component: ProductComponent, canActivate: [AppRouteGuard]},
					{ path: 'product/detail/:id', component: ProductDetailComponent, canActivate: [AppRouteGuard]},
					{ path: "cart", component: CartComponent, canActivate: [AppRouteGuard] },
					{ path: "purchase", component: PurchaseComponent, canActivate: [AppRouteGuard] },
					{ path: "notification", component: NotificationComponent, canActivate: [AppRouteGuard] },
					{ path: 'notification/:id', component: NotificationDetailComponent, canActivate: [AppRouteGuard]  },
					{ path: "chat", component: ChatComponent, canActivate: [AppRouteGuard] },
					{
						path: "user-account",
						children: [
							{ path: 'profile', component: ProfileComponent, canActivate: [AppRouteGuard]},
							{ path: 'address', component: AddressComponent, canActivate: [AppRouteGuard]},
						] 
					},
				],
			},
		]),
	],
	exports: [RouterModule],
})
export class AppRoutingModule { }
