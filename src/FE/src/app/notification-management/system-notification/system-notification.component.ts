import { Component, Injector, OnInit } from "@angular/core";
import { AppConsts, AtributionConfirmConst } from "@shared/AppConsts";
import { ConfirmationService, MessageService } from "primeng/api";
import { DialogService } from "primeng/dynamicdialog";
import { BreadcrumbService } from "src/app/layout/breadcrumb/breadcrumb.service";
import { decode } from "html-entities";
import { CrudComponentBase } from "@shared/crud-component-base";
import * as moment from "moment";
import { Observable, forkJoin } from "rxjs";
import { NotificationService } from "@shared/services/notification.service";
import { UploadImageComponent } from "src/app/components/upload-image/upload-image.component";

@Component({
  selector: 'app-system-notification',
  templateUrl: './system-notification.component.html',
  styleUrls: ['./system-notification.component.scss'],
	providers: [DialogService, ConfirmationService, MessageService],
})
export class SystemNotificationComponent
	extends CrudComponentBase
	implements OnInit
{
	baseUrl: string;
	systemNotificationTemplate: any;
	items: any[];
	itemsAdmin: any[];
	itemSelected: any;
	itemSelectedAdmin: any;
	caretPos: number = 0;
	actionsList: any = [
		{ value: "PUSH_WEB", name: "Đẩy thông báo trên web" },
		// { value: "SEND_SMS", name: "Gửi SMS" },
		{ value: "SEND_EMAIL", name: "Gửi email" },
	];
	currentKey: any;
	htmlMarkdownOptions: any = [
		{
			value: "MARKDOWN",
			name: "MARKDOWN",
		},
		{
			value: "HTML",
			name: "HTML",
		},
	];
	configKeys: any = {};
	settingTimeNotifySend: any = [];

  // keyNotifyDateConst = ConfigureSystemConst.keyNotifyDates;

	constructor(
		private notificationService: NotificationService,
		private dialogService: DialogService,
		injector: Injector,
		protected messageService: MessageService,
		private breadcrumbService: BreadcrumbService,
		public confirmationService: ConfirmationService,
	) {
		super(injector, messageService);

		this.breadcrumbService.setItems([
			{ label: "Trang chủ", routerLink: ["/home"] },
			{ label: "Cài đặt" },
			{ label: "Cấu hình thông báo từ hệ thống" },
		]);
	}

	ngOnInit(): void {
		this.baseUrl = AppConsts.remoteServiceBaseUrl ?? this.baseUrl;
		this.systemNotificationTemplate = {
			key: "",
			notificationTemplateName: "",
			description: "",
			actions: [],
			emailContentType: "MARKDOWN",
			pushAppContentType: "MARKDOWN",
			smsContentType: "MARKDOWN",
			emailContent: "",
			pushAppContent: "",
			smsContent: "",
			titleAppContent: "",
		};
		
		forkJoin([this.notificationService.getSystemNotification()]).subscribe(
			([res]) => {
				if (res.data && res.data.configKeys) {
          console.log("-__-",res.data);
          
					var normalizedSettings = new Object();
					if (res.data.value.settings) {
						res.data.value.settings.forEach((setting) => {
							normalizedSettings[setting.key] = { ...setting };
						});
						//Nếu FE chưa có thì bôt sung thêm config vào.
						res.data.configKeys.forEach((config) => {
							if (
								normalizedSettings[config.key]?.hasOwnProperty("isAdmin") &&
								config?.isAdmin
							) {
								normalizedSettings[config.key].isAdmin = true;
								normalizedSettings[config.key].adminEmailDisplay =
									normalizedSettings[config.key].adminEmail.map((email) => ({
										email,
									}));
							}
							if (!normalizedSettings[config.key]) {
								normalizedSettings[config.key] = config?.isAdmin
									? { ...config, adminEmailDisplay: [] }
									: { ...config };
							}
						});
					} else {
						res.data.configKeys.forEach((config) => {
							normalizedSettings[config.key] = config?.isAdmin
								? { ...config, adminEmailDisplay: [] }
								: { ...config };
						});
					}
					this.configKeys = normalizedSettings;
					this.itemsAdmin = res?.data?.configKeys
						.filter((item) => item.isAdmin)
						.map((item) => ({
							...item,
							notificationTemplateName:
								item.notificationTemplateName + " [Admin]",
						}));
					this.items = res?.data?.configKeys?.filter((item) => !item.isAdmin);
				}
				// if(dataConfig?.data?.length) {
				// 	this.setDataConfigSystemSendNotify(dataConfig.data);
				// }
			},
			(error) => {
				return this.messageError('Lỗi khi tải cấu hình thông báo');
			}
		);
	}

	// setDataConfigSystemSendNotify(dataConfig) {
	// 	this.settingTimeNotifySend = this.keyNotifyDateConst.reduce((result, info) => {
	// 	  let configSystem = dataConfig.find(c => c.key === info.code);
	// 	  let obj;
	// 	  if (configSystem && configSystem.key === "EventTimeSendNotiEventUpComingForCustomer") {
	// 		obj = {
	// 		  key: info.code,
	// 		  value: new Date(moment().format("YYYY-MM-DD ") + configSystem.value),
	// 		  isUpdate: !!configSystem,
	// 		};
	// 	  } else {
	// 		obj = {
	// 		  key: info.code,
	// 		  value: configSystem ? configSystem.value : 0,
	// 		  isUpdate: !!configSystem,
	// 		};
	// 	  }
	// 	  result[info.keyNotify] = result[info.keyNotify] || [];
	// 	  result[info.keyNotify].push(obj);
	// 	  return result;
	// 	}, {});
	// }
	  
	addvalue(item) {
		this.configKeys[this.currentKey].adminEmailDisplay.push({ email: null });
	}

	removeElement(index) {
		this.confirmationService.confirm({
			message: "Xóa email này?",
			...AtributionConfirmConst,
			accept: () => {
				this.configKeys[this.currentKey].adminEmailDisplay.splice(index, 1);
			},
		});
	}

	notificationChange(item, isAdmin) {
		isAdmin ? (this.itemSelected = null) : (this.itemSelectedAdmin = null);
		this.currentKey = item.key;
		this.configKeys[this.currentKey].pushAppContent = decode(
			this.configKeys[this.currentKey].pushAppContent
		);
		this.configKeys[this.currentKey].emailContent = decode(
			this.configKeys[this.currentKey].emailContent
		);
	}

	saveSetting() {
		if (this.configKeys[this.currentKey]?.adminEmailDisplay?.length) {
			this.configKeys[this.currentKey].adminEmail = this.configKeys[
				this.currentKey
			].adminEmailDisplay.map((obj) => obj.email);
		}
		var settings = Object.keys(this.configKeys).map((key) => {
			return {
				key,
				...this.configKeys[key],
			};
		});

    /**
     * thêm thông báo mới
     */
    
    // let systemNotification = {
    //  "key":"TIN_NHAN_MOI",
    //  "description":"Có tin nhắn mới",
    //  "emailContent":"   \n    \n      \n        <title>\n          \n        </title>\n        \n        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n        \n        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n        <style type=\"text/css\">\n          #outlook a { padding:0; }\n          body { margin:0;padding:0;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%; }\n          table, td { border-collapse:collapse;mso-table-lspace:0pt;mso-table-rspace:0pt; }\n          img { border:0;height:auto;line-height:100%; outline:none;text-decoration:none;-ms-interpolation-mode:bicubic; }\n          p { display:block;margin:13px 0; }\n        </style>\n       \n        \n        \n    <style type=\"text/css\">\n      @media only screen and (min-width:480px) {\n        .mj-column-per-100 { width:100% !important; max-width: 100%; }\n.mj-column-per-30 { width:30% !important; max-width: 30%; }\n.mj-column-per-50 { width:50% !important; max-width: 50%; }\n.mj-column-per-20 { width:20% !important; max-width: 20%; }\n      }\n    </style>\n    <style media=\"screen and (min-width:480px)\">\n      .moz-text-html .mj-column-per-100 { width:100% !important; max-width: 100%; }\n.moz-text-html .mj-column-per-30 { width:30% !important; max-width: 30%; }\n.moz-text-html .mj-column-per-50 { width:50% !important; max-width: 50%; }\n.moz-text-html .mj-column-per-20 { width:20% !important; max-width: 20%; }\n    </style>\n    \n  \n        <style type=\"text/css\">\n        \n        \n\n    @media only screen and (max-width:480px) {\n      table.mj-full-width-mobile { width: 100% !important; }\n      td.mj-full-width-mobile { width: auto !important; }\n    }\n  \n        </style>\n        <style type=\"text/css\">@media (max-width: 639px) {\n      .height-60-mobile {\n      height: 60px !important;\n      }\n\n      .mobile-text-center {\n      text-align: center;\n      }\n\n      .mobile-mr-20px {\n      margin-right: 20px;\n      }\n\n      .mobile-mb-20px {\n      margin-bottom: 20px;\n      }\n      }</style>\n        \n      \n      \n        \n        \n      <div style=\"\">\n        \n      <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"width:100%;\">\n        <tbody>\n          <tr>\n            <td>\n              \n        \n        \n      <div style=\"margin:0px auto;max-width:600px\">\n        \n        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"width:100%;\">\n          <tbody>\n            <tr>\n              <td style=\"direction:ltr;font-size:0px;padding:0px;text-align:center;\">\n\n            \n      <div class=\"mj-column-per-100 mj-outlook-group-fix\" style=\"font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">\n        \n      <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" width=\"100%\">\n        <tbody>\n          <tr>\n            <td style=\"vertical-align:top;padding:0px;\">\n              \n      <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"\" width=\"100%\">\n        <tbody>\n          \n              <tr>\n                <td align=\"center\" style=\"font-size:0px;padding:0px;word-break:break-word;\">\n                  \n      <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"border-collapse:collapse;border-spacing:0px;\">\n        <tbody>\n          <tr>\n            <td style=\"width:600px;\">\n              \n      \n    \n            </td>\n          </tr>\n        </tbody>\n      </table>\n    \n                </td>\n              </tr>\n            \n        </tbody>\n      </table>\n    \n            </td>\n          </tr>\n        </tbody>\n      </table>\n    \n      </div>\n    \n        \n              </td>\n            </tr>\n          </tbody>\n        </table>\n        \n      </div>\n    \n        \n    \n      \n            </td>\n          </tr>\n        </tbody>\n      </table>\n    \n      \n\n    \n      \n      <div style=\"margin:0px auto;max-width:600px\">\n        \n        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"width:100%;\">\n          <tbody>\n            <tr>\n              <td style=\"direction:ltr;font-size:0px;padding:30px 25px 20px 25px;text-align:center;\">\n               \n    \n      \n      <div style=\"margin:0px auto;max-width:550px\">\n        \n        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"width:100%;\">\n          <tbody>\n            <tr>\n              <td style=\"direction:ltr;font-size:0px;padding:0px;text-align:center;\">\n\n            \n      <div class=\"mj-column-per-100 mj-outlook-group-fix\" style=\"font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">\n        \n      <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" width=\"100%\">\n        <tbody>\n          <tr>\n            <td style=\"vertical-align:top;padding:0px;\">\n              \n      <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"\" width=\"100%\">\n        <tbody>\n          \n              <tr>\n                <td align=\"left\" style=\"font-size:0px;padding:0px;padding-right:0;padding-left:0;word-break:break-word;\">\n                  \n      <div style=\"font-family:SFProText, Raleway, Arial;font-size:13px;line-height:150%;text-align:left;color:#333333;\"><p class=\"u-fz-16px u-color-blue ta-justify\" style=\"font-size: 16px; color: #353282; text-align: left;\"> <strong> Kính gửi Quý khách [CustomerName], </strong> </p>\n             \n                        <div>\n              <p class=\"u-fz-16px ta-justify\" style=\"font-size: 16px; text-align: justify;\"> Emir xin trân trọng thông báo, Quý khách đã đăng ký vé tham gia thành công sự kiện qua hệ thống của Epic Center như sau:\n              </p>\n            \n                              \n                        </div></div>\n    \n                </td>\n              </tr>\n            \n        </tbody>\n      </table>\n    \n            </td>\n          </tr>\n        </tbody>\n      </table>\n    \n      </div>\n    \n          <!--[if mso | IE]></td></tr></table><![endif]-->\n              </td>\n            </tr>\n          </tbody>\n        </table>\n        \n      </div>\n    \n      \n     \n          \n      <div style=\"background:url('') center center / auto no-repeat;background-position:center center;background-repeat:no-repeat;background-size:auto;margin:0px auto;border-radius:4px;max-width:550px;\">\n        <div style=\"line-height:0;font-size:0;\">\n        <table align=\"center\" background=\"\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"background:url('') center center / auto no-repeat;background-position:center center;background-repeat:no-repeat;background-size:auto;width:100%;border-radius:4px;\">\n        \n                 <tbody>\n            <tr>\n              <td style=\"direction:ltr;font-size:0px;text-align:center;\">\n                \n            \n      <div class=\"mj-column-per-100 mj-outlook-group-fix\" style=\"font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">\n        \n      <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" width=\"100%\">\n        <tbody>\n          <tr>\n            <td style=\"vertical-align:top;padding:0px;\">\n              \n      <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"\" width=\"100%\">\n        <tbody>\n          \n              <tr>\n                <td align=\"left\" style=\"font-size:0px;padding:0px;padding-right:0;padding-left:0;word-break:break-word;\">\n                  \n      <div style=\"font-family:SFProText, Raleway, Arial;font-size:13px;line-height:150%;text-align:left;color:#333333;\"><table width=\"100%\" class=\"u-fz-16px table-detail\" style=\"font-size: 16px;\">\n                        <tbody><tr style=\"border-bottom: 2px solid rgb(51 51 51 / 15%);\">\n                <td style=\"padding-bottom: 12px; padding-top: 12px\"><span style=\"opacity: 0.8; display: inline-block; \">Tên sự kiện</span> </td>\n                <td style=\"padding-bottom: 12px; padding-top: 12px; text-align: right;\" align=\"right\"><strong>[EventName]</strong></td>\n            </tr>\n                        \n            \n                        <tr style=\"border-bottom: 2px solid rgb(51 51 51 / 15%);\">\n                <td style=\"padding-bottom: 12px; padding-top: 12px\"><span style=\"opacity: 0.8; display: inline-block; \">Số lượng vé</span>  </td>\n                <td style=\"padding-bottom: 12px; padding-top: 12px; text-align: right;\" align=\"right\"><strong>[TicketQuantity]</strong></td>\n            </tr>\n                       \n                        <tr style=\"border-bottom: 2px solid rgb(51 51 51 / 15%);\">\n                <td style=\"padding-bottom: 12px; padding-top: 12px\"><span style=\"opacity: 0.8; display: inline-block; \">Thời gian bắt đầu</span> </td>\n                <td style=\"padding-bottom: 12px; padding-top: 12px;  text-align: right;\" align=\"right\"><strong>[StartDate] </strong></td>\n            </tr>\n            <tr style=\"border-bottom: 2px solid rgb(51 51 51 / 15%);\">\n                <td style=\"padding-bottom: 12px; padding-top: 12px\"><span style=\"opacity: 0.8; display: inline-block; \">Thời gian kết thúc</span> </td>\n                <td style=\"padding-bottom: 12px; padding-top: 12px;  text-align: right;\" align=\"right\"><strong>[EndDate] </strong></td>\n            </tr>\n                       \n                       \n            </tbody></table>        \n                </div>\n               </td>\n              </tr>\n            \n        </tbody>\n      </table>\n    \n            </td>\n          </tr>\n        </tbody>\n      </table>\n    \n      </div>\n    \n         \n              </td>\n            </tr>\n          </tbody>\n        </table>\n        </div>\n      </div>\n    \n       \n    \n      \n      <div style=\"margin:0px auto;max-width:550px;\">\n        \n        <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"width:100%;\">\n          <tbody>\n            <tr>\n              <td style=\"direction:ltr;font-size:0px;padding:10px 0px 0px 0px;text-align:center;\">\n                \n            \n      <div class=\"mj-column-per-100 mj-outlook-group-fix\" style=\"font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\">\n        \n      <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" width=\"100%\">\n        <tbody>\n          <tr>\n            <td style=\"vertical-align:top;padding:0px;\">\n              \n      <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"\" width=\"100%\">\n        <tbody>\n          \n              <tr>\n                <td align=\"left\" style=\"font-size:0px;padding:0px;padding-right:0;padding-left:0;word-break:break-word;\">\n                  \n      <div style=\"font-family:SFProText, Raleway, Arial;font-size:13px;line-height:150%;text-align:left;color:#333333;\"><div>\n              <p class=\"u-fz-16px ta-justify\" style=\"font-size: 16px; text-align: justify;\"> Đây là email được gửi tự động từ hệ thống của EMIR. Quý khách vui lòng không trả lời email này.</p>\n                        <p class=\"u-fz-16px ta-justify\" style=\"font-size: 16px; text-align: justify;\">Mọi thông tin cần hỗ trợ, Quý khách vui lòng liên hệ email info@emir.vn\n                hoặc số điện thoại hotline 0247 303 799. </p>\n             \n            </div>\n            <div class=\"u-color-blue u-fw-600\" style=\"font-weight: 600; color: #353282;\">\n             \n              </div></div></td>\n            </tr>\n          </tbody>\n        </table>\n        \n      \n    \n      \n      \n              </td>\n            </tr>\n          </tbody>\n        </table>\n        \n      </div>\n    \n      \n     \n      \n      <div class=\"bg-gradient-blue footer-section\" style=\"margin: 0px auto; max-width: 600px\">\n        <div style=\"position:relative;margin-top:20px\">\n   <table align=\"center\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"width:100%\">\n          <tbody style=\"position:absolute;margin-top:-35%;margin-left:10%\">\n            <tr>\n              <td style=\"direction:ltr;font-size:0px;padding:5px 0px 0px 25px;text-align:center;\">\n           \n                                <div class=\"mj-column-per-50 mj-outlook-group-fix\" style=\"font-size:0px;text-align:left;direction:ltr;display:contents;vertical-align:top;width:100%;\">\n        \n                                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" width=\"100%\">\n                                                <tbody>\n                                                        <tr>\n                                                                <td style=\"vertical-align:top;padding:0px;\">\n              \n                                                                        <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"\" width=\"100%\">\n                                                                                <tbody>\n          \n\n        </tbody>\n      </table>\n    \n      \n    \n\n            \n        <div class=\"mj-column-per-20 mj-outlook-group-fix\" style=\"font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;\"> \n        \n      <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" width=\"100%\">\n        <tbody>\n          <tr>\n            <td style=\"vertical-align:top;padding:0px;\">\n              \n      <table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" role=\"presentation\" style=\"\" width=\"100%\">\n        <tbody>\n          \n              <tr>\n                <td align=\"right\" style=\"font-size:0px;padding:0px;padding-right:0;padding-left:0;word-break:break-word;\">\n    \n                </td>\n              </tr>\n\n        </tbody>\n      </table>\n    \n            </td>\n          </tr>\n        </tbody>\n      </table>\n    \n      </div>\n    \n\n              </td>\n            </tr>\n\n          </tbody>\n        </table>\n                        </div>\n      \n    \n    \n      \n    \n      \n    \n  \n\n\n</td></tr></tbody></table></div></div></td></tr></tbody></table></div></td></tr></tbody></table></div></div>\n\n",
    //  "emailContentType":"HTML",
    //  "notificationTemplateName":"Có tin nhắn mới",
    //  "pushAppContent":"Có tin nhắn mới",
    //  "pushAppContentType":"MARKDOWN",
    //  "titleAppContent":"Có tin nhắn mới",
    //  "actions":["SEND_EMAIL","PUSH_WEB"],
	//  "id": 7, // khi update thi truyen them id
    // }
    // settings.push(systemNotification)
    
    //settings[1] = systemNotification // khi update thi truyen them vi tri
		this.notificationService
			.createOrUpdateSystemNotification(settings)
			.subscribe(
				(data) => {
					return this.messageSuccess("Cập nhật thành công");
				},
				(error) => {
					return this.messageError("Lỗi khi cập nhật cấu hình thông báo"); 
				}
			);
	}

	insertImage(contentName) {
		const ref = this.dialogService.open(UploadImageComponent, {
			data: {
				inputData: [],
				showOrder: false,
			},
			header: "Chèn hình ảnh",
			width: "600px",
		});
		ref.onClose.subscribe((images) => {
			let imagesUrl = "";
			images.forEach((image) => {
				imagesUrl += `![](${this.baseUrl}/${image.data}) \n`;
			});

			let oldEmailContentValue = this.configKeys[this.currentKey][contentName];
			let a =
				oldEmailContentValue.slice(0, this.caretPos) +
				imagesUrl +
				oldEmailContentValue.slice(this.caretPos);
			this.configKeys[this.currentKey][contentName] = a;
		});
	}

	getCaretPos(oField) {
		if (oField.selectionStart || oField.selectionStart == "0") {
			this.caretPos = oField.selectionStart;
		}
	}
}


