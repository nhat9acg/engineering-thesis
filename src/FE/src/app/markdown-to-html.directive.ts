import { Directive, ElementRef, Input, OnChanges, SimpleChanges } from '@angular/core';
import { marked } from 'marked';

@Directive({
  selector: '[appMarkdownToHtml]'
})
export class MarkdownToHtmlDirective implements OnChanges {
  @Input('appMarkdownToHtml') content: string;

  constructor(private elementRef: ElementRef) {}

  ngOnChanges(changes: SimpleChanges) {
    if (changes.content) {
      this.renderContent();
    }
  }

  private renderContent() {
    if (this.content) {
      const htmlContent = this.isHtmlContent(this.content)
        ? this.content
        : marked(this.content);

      this.elementRef.nativeElement.innerHTML = htmlContent;
    }
  }

  private isHtmlContent(content: string): boolean {
    // Kiểm tra nếu nội dung đã chứa thẻ HTML bằng cách kiểm tra các ký tự "<" và ">"
    return content.includes('<') && content.includes('>');
  }
}
