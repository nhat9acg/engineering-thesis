import { Component, Injector, OnInit } from "@angular/core";
import { UserConst } from "@shared/AppConsts";
import { CrudComponentBase } from "@shared/crud-component-base";
import { User } from "@shared/model/user.model";
import { MessageService } from "primeng/api";

@Component({
	selector: "app-detail-business",
	templateUrl: "./detail-business.component.html",
	styleUrls: ["./detail-business.component.scss"],
})
export class DetailBusinessComponent extends CrudComponentBase {
	constructor(
		injector: Injector, 
		messageService: MessageService
		) {
		super(injector, messageService);
		this.userLogin = this.getUser();
	}
	userLogin: User;

	UserConst = UserConst;

	ngOnInit(): void {
		console.log('userLogin ', this.userLogin);
		
	}
}