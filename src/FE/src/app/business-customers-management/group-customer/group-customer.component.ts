import { Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SearchConst, FormNotificationConst, localStorageKeyConst, PermissionConst } from '@shared/AppConsts';
import { CrudComponentBase } from '@shared/crud-component-base';
import { Page } from '@shared/model/page';
import { BusinessCustomerService } from '@shared/services/business-customer.service';

import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { debounceTime } from 'rxjs/operators';
import { FormNotificationComponent } from 'src/app/form-general/form-notification/form-notification.component';
import { FormSetDisplayColumnComponent } from 'src/app/form-general/form-set-display-column/form-set-display-column.component';
import { BreadcrumbService } from 'src/app/layout/breadcrumb/breadcrumb.service';
import { IColumn, IBusinessCustomer } from '@shared/interfaces/business-customer.interface';
import { IBaseListAction, IPageInfo } from '@shared/interfaces/base-interface';
import { SelectedLanguageService } from '@shared/services/selected-language.service';
import { TranslateService } from '@ngx-translate/core';
import { GroupCustomerService } from '@shared/services/group-customer.service';
import { GroupCustomerAddComponent } from './group-customer-add/group-customer-add.component';

@Component({
  selector: 'app-group-customer',
  templateUrl: './group-customer.component.html',
  styleUrls: ['./group-customer.component.scss'],
  providers: [DialogService, ConfirmationService, MessageService]
})
export class GroupCustomerComponent extends CrudComponentBase {

    constructor(
        injector: Injector,
        private dialogService: DialogService,
        private _dialogService: DialogService,
        private confirmationService: ConfirmationService,
        private router: Router,
        private routeActive: ActivatedRoute,
        messageService: MessageService,
        private breadcrumbService: BreadcrumbService,
        private _groupCustomerService: GroupCustomerService,
        private selectedLanguageService: SelectedLanguageService,
        public _translateService: TranslateService,
    ) {
        super(injector, messageService);
    }

    ref: DynamicDialogRef;
    rows: IBusinessCustomer[] = [];
    cols: IColumn[];
    listAction: IBaseListAction[] = [];
    page = new Page();
    offset = 0;
    parentLangKeys: string[] = ['HOME', 'MENU', 'RESTAURANT', 'SHARE', 'ROUTER', 'CONFIRM', 'RESPONSE_MSG', 'BUSINESS_CUSTOMER','GROUP_CUSTOMER'];
    labels: string[] = [];
    PermissionConst = PermissionConst;
    scrollHeight: number = 0;

    ngOnInit(): void {
        this.selectedLanguageService.getLanguage.subscribe((resLang) => {
            if(resLang) {
                this.labels = this.selectedLanguageService.getKeyLangs(this.parentLangKeys, resLang.keys);
                this.cols = [
                    { field: 'name', header: this.labels['GROUP_CUSTOMER']['NAME'], width: '12rem', isPin: true, type: '' },
                    { field: `columnResize`, header: '', width: '', isPin: false, type: 'hidden' },
                ];
                //
                this.breadcrumbService.setItems([
                    { label: this.labels['ROUTER']['HOME_PAGE'], routerLink: ['/home'] },
                    { label: this.labels['ROUTER']['GROUP_CUSTOMER'], routerLink: ['/business-customer-management/group-customer'] },
                ]);
            }
            this.setPage();
        });

        this.subject.keyword.pipe(debounceTime(SearchConst.DEBOUNCE_TIME)).subscribe(() => {
            if (this.keyword === "") {
                this.setPage();
            } else {
                this.setPage();
            }
        });
    }

    genListAction(data = []) {
        this.listAction = data.map(item => {
            const actions = [];
            if(this.isGranted([PermissionConst.GroupCustomerUpdate])){
                actions.push({
                    data: item,
                    label: this.labels['SHARE']['UPDATE'],
                    icon: 'pi pi-user-edit',
                    command: ($event) => {
                    this.edit($event.item.data);
                    }
                })
            }
            if(this.isGranted([PermissionConst.GroupCustomerDelete])){
                actions.push({
                    data: item,
                    label: this.labels['SHARE']['DELETE'],
                    icon: 'pi pi-trash',
                    command: ($event) => {
                    this.delete($event.item.data);
                    }
                })
            }
            return actions;
        });
    }

    detail(item) {
        this.router.navigate(['/business-customer-management/business-customer/detail/' + this.cryptEncode(item?.id)]);
    }

    create() {
        this.dialogService.open(GroupCustomerAddComponent, {
            header: `${this.labels['GROUP_CUSTOMER']['TITLE_DIALOG_ADD']}`,
            width: '400px',
            contentStyle: {"max-height": "600px", "overflow": "auto", "margin-bottom": "70px" },
            data: {
                labels: this.labels
            },
        }).onClose.subscribe(result => {
            if (result) {
                this.setPage();
            } 
        })
    }

    edit(item) {
        this._groupCustomerService.get(item.id).subscribe((response) => {
            if (this.handleResponseInterceptor(response, "")) {
                this.dialogService.open(GroupCustomerAddComponent, {
                    contentStyle: {"max-height": "400px", "overflow": "auto", "margin-bottom": "40px" },
                    header:  `${this.labels['GROUP_CUSTOMER']['TITLE_DIALOG_UPDATE']}`,
                    width: '500px',
                    data: {
                        inputData: response.data,
                        labels: this.labels
                    },
                }).onClose.subscribe(result => {
                    if (result) this.setPage();
                })
            }
        });
    }

    delete(item) {
        this.confirmationService.confirm({
            message: this.labels['GROUP_CUSTOMER']['DELETE_TITLE'],
            acceptLabel: this.labels['CONFIRM']['YES'],
            rejectLabel: this.labels['CONFIRM']['NO'],
            accept: () => {
                this._groupCustomerService.delete(item.id).subscribe((response) => {
                    if (this.handleResponseInterceptor(response, this.labels['CONFIRM']['DELETE_SUCCESS'])) {
                        this.setPage();
                    }
                });
            }
        });
    }

    isRowOdds: boolean = false;
    setPage(event?: IPageInfo) {
        this.isRowOdds = false;
        this.isLoading = true;
        if(event) {
            this.page.pageNumber = event.page;
            this.page.pageSize = event.rows;
        }
        this.page.keyword = this.keyword;
        this._groupCustomerService.getAll(this.page).subscribe((res) => {
        this.isLoading = false;
        if (this.handleResponseInterceptor(res, '')) {
            this.page.totalItems = res.data.totalItems;
            this.rows = res.data?.items;

            if (res.data?.items?.length) {
                this.isRowOdds = !(this.rows?.length%2 == 0);
                this.genListAction(this.rows);
            }
        }
        }, (err) => {
            this.isLoading = false;
            console.log('Error-------', err);
        });
    }
}


