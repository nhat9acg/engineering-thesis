import { Component, Injector, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CrudComponentBase } from '@shared/crud-component-base';
import { IBusinessCustomer } from '@shared/interfaces/business-customer.interface';
import { IGroupCustomer } from '@shared/interfaces/group-customer.interface';
import { BusinessCustomerService } from '@shared/services/business-customer.service';
import { GroupCustomerService } from '@shared/services/group-customer.service';
import { RestaurantService } from '@shared/services/restaurant.service';
import { MessageService } from 'primeng/api';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-group-customer-add',
  templateUrl: './group-customer-add.component.html',
  styleUrls: ['./group-customer-add.component.scss']
})
export class GroupCustomerAddComponent extends CrudComponentBase {

  constructor(
    injector: Injector,
    messageService: MessageService,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private _groupCustomerService: GroupCustomerService,
    private dialogService: DialogService,
    private router: Router,
    private fb: FormBuilder,
    private _restaurantService: RestaurantService,
  ) {
    super(injector, messageService);
  }
  inputData: IGroupCustomer;
  postForm: FormGroup;
  labels: string[] = [];
  businessCustomers: IBusinessCustomer[] = [];
  
  ngOnInit(): void {
    this.labels = this.config?.data?.labels
    this.inputData = this.config?.data?.inputData;
    this.postForm = this.fb.group({
      id: [this.inputData?.id || null, []],
      name: [this.inputData?.name || '', Validators.required],
      businessCustomerIds: [this.inputData?.businessCustomerIds || [], Validators.required]
    });
    this.optionsBusiness();
  }

  optionsBusiness() {
    this.isLoading = true;
    this._restaurantService.getAllBusinessCustomer().subscribe((res) => {
      this.isLoading = false;
      if (this.handleResponseInterceptor(res, '')) {

        this.businessCustomers = res.data?.items.map((item) => {
          return {
            id: item.id,
            labelName: item.fullName
          };
        });
      }
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);

    });
  }

  get postFormControl() {
    return this.postForm.controls;
  }

  onSubmit() {
    if (!this.checkInValidForm(this.postForm)) {
      if (this.inputData) {
        this._groupCustomerService.update(this.postForm.value).subscribe((response) => {
          if (this.handleResponseInterceptor(response, this.labels['RESPONSE_MSG']['UPDATE_SUCCESS'])) {
            this.ref.close(true);
          }
        }, () => {
          this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER']);
        }
        );
      }
      else {
        this._groupCustomerService.create(this.postForm.value).subscribe(
          (response) => {
            if (this.handleResponseInterceptor(response, this.labels['RESPONSE_MSG']['ADD_SUCCESS'])) {
              this.ref.close(true);
            }
          }, () => {
            this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER']);
          }
        );
      }
    } else {
      this.messageError(this.labels['RESPONSE_MSG']['PLEASE_ENTER_ENOUGH_INFOR']);
    }

  }

}


