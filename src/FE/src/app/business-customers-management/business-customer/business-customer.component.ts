import { Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PermissionConst, SearchConst, localStorageKeyConst } from '@shared/AppConsts';
import { CrudComponentBase } from '@shared/crud-component-base';
import { Page } from '@shared/model/page';
import { BusinessCustomerService } from '@shared/services/business-customer.service';

import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { debounceTime } from 'rxjs/operators';
import { BreadcrumbService } from 'src/app/layout/breadcrumb/breadcrumb.service';
import { BusinessCustomerAddComponent } from './business-customer-add/business-customer-add.component';
import { IColumn, IBusinessCustomer } from '@shared/interfaces/business-customer.interface';
import { IBaseListAction, IPageInfo, IResponse } from '@shared/interfaces/base-interface';
import { SelectedLanguageService } from '@shared/services/selected-language.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-business-customer',
  templateUrl: './business-customer.component.html',
  styleUrls: ['./business-customer.component.scss'],
  providers: [DialogService, ConfirmationService, MessageService]
})
export class BusinessCustomerComponent extends CrudComponentBase {

        constructor(
                injector: Injector,
                private dialogService: DialogService,
                private confirmationService: ConfirmationService,
                private router: Router,
                messageService: MessageService,
                private breadcrumbService: BreadcrumbService,
                private _businessCustomerService: BusinessCustomerService,
                private selectedLanguageService: SelectedLanguageService,
                public _translateService: TranslateService,
            ) {
                super(injector, messageService);
        }

        ref: DynamicDialogRef;
        rows: IBusinessCustomer[] = [];
        cols: IColumn[];
        listAction: IBaseListAction[] = [];
        page = new Page();
        offset = 0;
        PermissionConst = PermissionConst;
        scrollHeight: number = 0;

        parentLangKeys: string[] = ['HOME', 'MENU', 'BUSINESS_CUSTOMER', 'RESTAURANT', 'SHARE', 'CONFIRM', 'RESPONSE_MSG', 'ROUTER'];
        labels: string[] = [];

        ngOnInit(): void {
            this.selectedLanguageService.getLanguage.subscribe((resLang) => {
                if(resLang) {
                    this.labels = this.selectedLanguageService.getKeyLangs(this.parentLangKeys, resLang.keys);
                    this.cols = [
                        { field: `fullName`, header: this.labels['BUSINESS_CUSTOMER']['FULL_NAME'], width: '25rem', isPin: true, isResize: true },
                        { field: `email`, header: this.labels['BUSINESS_CUSTOMER']['EMAIL'], width: '18rem', isPin: false, type: '' },
                        { field: `phone`, header: this.labels['BUSINESS_CUSTOMER']['PHONE'], width: '10rem', isPin: false, type: '' },
                        { field: 'taxCode', header: this.labels['BUSINESS_CUSTOMER']['TAX_CODE'], width: '10rem', isPin: false, type: '' },
                        { field: `address`, header: this.labels['RESTAURANT']['ADDRESS'], width: '30rem', isPin: false, isResize: true },
                        { field: `language`, header: this.labels['SHARE']['LANGUAGE'], width: '12rem', isPin: false, type: '' },
                    ];
                    //
                    this.breadcrumbService.setItems([
                        { label: this.labels['ROUTER']['HOME_PAGE'], routerLink: ['/home'] },
                        { label: this.labels['ROUTER']['BUSINESS_CUSTOMER'], routerLink: ['/business-customer-management/business-customer'] },
                    ]);
                }
                this.setPage();
            });

            this.subject.keyword.pipe(debounceTime(SearchConst.DEBOUNCE_TIME)).subscribe(() => {
                if (this.keyword === "") {
                    this.setPage();
                } else {
                    this.setPage();
                }
            });
        }

        showData(rows) {
            for (let row of rows) {
            };
        }

    genListAction(data = []) {
        this.listAction = data.map(item => {
            const actions = [];
            if(this.isGranted([PermissionConst.BusinessCustomerUpdate])) {
                actions.push({
                    data: item,
                    label: this.labels['SHARE']['BUTTON_DETAIL'],
                    icon: 'pi pi-info-circle',
                    command: ($event) => {
                        this.detail($event.item.data);
                    }
                })

            }
            if(this.isGranted([PermissionConst.BusinessCustomerDelete])) {
            
                actions.push({
                    data: item,
                    label: this.labels['SHARE']['DELETE'],
                    icon: 'pi pi-trash',
                    command: ($event) => {
                        this.delete($event.item.data);
                    }
                })
            }
            return actions;
        });
    }

        detail(item) {
            this.router.navigate(['/business-customer-management/business-customer/detail/' + this.cryptEncode(item?.id)]);
        }

        create() {
            this.dialogService.open(BusinessCustomerAddComponent, {
                    header: `${this.labels['BUSINESS_CUSTOMER']['TITLE_DIALOG_ADD']}`,
                    width: '700px',
                    contentStyle: { "max-height": "600px", overflow: "auto", "margin-bottom": "60px", },
                    baseZIndex: 10000,
                    data: {
                        labels: this.labels
                    },
                }).onClose.subscribe(result => {
                if (result) this.setPage();
            })
        }

    edit(item) {
        this._businessCustomerService.get(item.id).subscribe((response) => {
            if (this.handleResponseInterceptor(response, "")) {
                this.dialogService.open(BusinessCustomerAddComponent, {
                    header: `${this.labels['BUSINESS_CUSTOMER']['TITLE_DIALOG_UPDATE']}`,
                    width: '700px',
                    data: {
                        itemDetail: response.data,
                        labels: this.labels
                    },
                }).onClose.subscribe(result => {
                    if (result) this.setPage();
                })
            }
        });

    }

    delete(item) {
        this.confirmationService.confirm({
            message: this.labels['CONFIRM']['ARE_YOU_SURE_TO_DELETE_THIS_BUSINESS'],
            acceptLabel: this.labels['CONFIRM']['YES'],
            rejectLabel: this.labels['CONFIRM']['NO'],
            accept: () => {
                this._businessCustomerService.delete(item.id).subscribe((response) => {
                    if (this.handleResponseInterceptor(response, this.labels['CONFIRM']['DELETE_SUCCESS'])) {
                        this.setPage();
                    }
                });
            }
        });
    }

    isRowOdds: boolean = true;
    setPage(event?: IPageInfo) {
        this.isRowOdds = true;
        this.isLoading = true;
        if(event) {
            this.page.pageNumber = event.page;
            this.page.pageSize = event.rows;
        }
        this.page.keyword = this.keyword;
        this._businessCustomerService.getAll(this.page).subscribe((res: IResponse<any>) => {
            this.isLoading = false;
            if (this.handleResponseInterceptor(res, '')) {
                this.page.totalItems = res.data.totalItems;
                this.rows = res.data?.items;

                if (res.data?.items?.length) {
                    this.isRowOdds = !(this.rows?.length%2 == 0);
                    this.genListAction(this.rows);
                    this.showData(this.rows);
                }
            }
        }, (err) => {
            this.isLoading = false;
            console.log('Error-------', err);
        });
    }
}

