import { Component, ElementRef, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PermissionConst } from '@shared/AppConsts';
import { CrudComponentBase } from '@shared/crud-component-base';
import { IBusinessCustomer } from '@shared/interfaces/business-customer.interface';
import { BusinessCustomerService } from '@shared/services/business-customer.service';
import { SelectedLanguageService } from '@shared/services/selected-language.service';
import { MessageService } from 'primeng/api';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-business-customer-add',
  templateUrl: './business-customer-add.component.html',
  styleUrls: ['./business-customer-add.component.scss']
})
export class BusinessCustomerAddComponent extends CrudComponentBase {

  constructor(
    injector: Injector,
    messageService: MessageService,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private _businessCustomerService: BusinessCustomerService,
    private dialogService: DialogService,
    private router: Router,
    private fb: FormBuilder,
    private selectedLanguageService: SelectedLanguageService,
  ) {
    super(injector, messageService);
  }

  @Input() itemDetail: IBusinessCustomer;
  @Input() contentHeight: number = 0;

  postForm: FormGroup;
  labels: string[] = [];
  isEdit: boolean = false;

  parentLangKeys: string[] = ['RESPONSE_MSG'];
  currentLang: string;
  PermissionConst = PermissionConst;
  businessCustomer = [
    { labelKeyLang: 'BUSINESS_CUSTOMER.FULL_NAME', controlName: 'fullName', type: 'input' },
    { labelKeyLang: 'BUSINESS_CUSTOMER.PHONE', controlName: 'phone', type: 'input' },
    { labelKeyLang: 'BUSINESS_CUSTOMER.TAX_CODE', controlName: 'taxCode', type: 'input' },
    { labelKeyLang: 'BUSINESS_CUSTOMER.ADDRESS', controlName: 'address', type: 'input' },
    { labelKeyLang: 'BUSINESS_CUSTOMER.EMAIL', controlName: 'email', type: 'input' },
    { labelKeyLang: 'SHARE.LANGUAGE', controlName: 'language', type: 'input' },
  ]

  ngOnInit(): void {
    this.selectedLanguageService.getLanguage.subscribe((resLang) => {
      if(resLang) {
        this.currentLang = resLang?.lang;
        this.labels = this.selectedLanguageService.getKeyLangs(this.parentLangKeys, resLang.keys);
      }
    });
    //
    this.postForm = this.fb.group({
      id: [this.itemDetail?.id || null, []],
      isRestaurant: [false, false],
      fullName: [this.itemDetail?.fullName || '', Validators.required],
      address: [this.itemDetail?.address || '', [Validators.required]],
      taxCode: [this.itemDetail?.taxCode || '', [Validators.required]],
      phone: [this.itemDetail?.phone || '', [Validators.required]],
      email: [this.itemDetail?.email || '', [Validators.required]],
      language: [this.itemDetail?.language || '', [Validators.required]],
    });
  }

  toggleCheckbox() {
		this.postForm.patchValue({
			isRestaurant: !this.postForm.value.isRestaurant
		  }); 
	  }

  get postFormControl() {
    return this.postForm.controls;
  }

  changeEdit() {
    if(!this.isEdit) {
      this.onSubmit();
    }
  }

  onSubmit() {
    if (!this.checkInValidForm(this.postForm)) {
      if (this.itemDetail) {
        this._businessCustomerService.update(this.postForm.value).subscribe((response) => {
          if (this.handleResponseInterceptor(response, this.labels['RESPONSE_MSG']['UPDATE_SUCCESS'])) {
            this.ref.close(true);
          }
        }, () => {
          this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER']);
        }
        );
      }
      else {
        this._businessCustomerService.create(this.postForm.value).subscribe(
          (response) => {
            if (this.handleResponseInterceptor(response, this.labels['RESPONSE_MSG']['ADD_SUCCESS'])) {
              this.ref.close(true);
              // this.router.navigate(['/partner-manager/partner/detail', this.cryptEncode(response.data.partnerId)]);
            }
          }, () => {
            this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER']);
          }
        );
      }
    } else {
      this.messageError(this.labels['RESPONSE_MSG']['PLEASE_ENTER_ENOUGH_INFOR']);
    }
  }

}

