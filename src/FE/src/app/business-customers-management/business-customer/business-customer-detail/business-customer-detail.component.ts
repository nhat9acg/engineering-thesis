import { Component, ElementRef, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudComponentBase } from '@shared/crud-component-base';
import { BusinessCustomerService } from '@shared/services/business-customer.service';

import { ConfirmationService, MenuItem, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { BreadcrumbService } from 'src/app/layout/breadcrumb/breadcrumb.service';
import { SelectedLanguageService } from '@shared/services/selected-language.service';
import { TranslateService } from '@ngx-translate/core';
import { TabView } from 'primeng/tabview';
import { IBusinessCustomer } from '@shared/interfaces/business-customer.interface';
import { AppConsts, PermissionConst, TabViewConst, localStorageKeyConst } from '@shared/AppConsts';

@Component({
  selector: 'app-business-customer-detail',
  templateUrl: './business-customer-detail.component.html',
  styleUrls: ['./business-customer-detail.component.scss'],
  providers:[TabView],
})
export class BusinessCustomerDetailComponent extends CrudComponentBase {

    constructor(
        injector: Injector,
        private routeActive: ActivatedRoute,
        messageService: MessageService,
        private breadcrumbService: BreadcrumbService,
        private _businessCustomerService: BusinessCustomerService,
        private selectedLanguageService: SelectedLanguageService,
        public _translateService: TranslateService,
    ) {
        super(injector, messageService);
    }

    @ViewChild(TabView) tabView: TabView;
    tabViewActive = {
        'thongTinChung': true,
        'taiKhoan': false,
        'bangGia': false,
    };

    itemDetail: IBusinessCustomer;
    @Input() businessCustomerId: number;
    PermissionConst = PermissionConst;
    contentTabHeight: number = 0;

    labels: string[] = [];
    parentLangKeys: string[] = ['ROUTER', 'BUSINESS_CUSTOMER'];
    
    isShowTab: boolean = false;

    ngOnInit(): void {
        this.selectedLanguageService.getLanguage.subscribe((resLang) => {
            if(resLang) {
                this.labels = this.selectedLanguageService.getKeyLangs(this.parentLangKeys, resLang.keys);
                this.breadcrumbService.setItems([
                    { label: this.labels['ROUTER']['HOME_PAGE'], routerLink: ['/home'] },
                    { label: this.labels['BUSINESS_CUSTOMER']['PAGE_DETAIL_BREADCRUMB'], routerLink: ['/business-customer-management/business-customer'] },
                ]);
            }
        });
        if(!this.businessCustomerId){
            this.businessCustomerId = +this.cryptDecode(this.routeActive.snapshot.paramMap.get('id'));
        }
        
        this.isLoading = true;
        this._businessCustomerService.get(this.businessCustomerId).subscribe((response) => {
            if (this.handleResponseInterceptor(response, "")) {
                this.itemDetail = response?.data;
            }
            this.isLoading = false;
        }, (err) => {
            this.isLoading = false;
        });
    }
    
    changeTab(e) {
        let tabHeader = this.tabView.tabs[e.index].header;
        this.tabViewActive[tabHeader] = true;
    }

}

