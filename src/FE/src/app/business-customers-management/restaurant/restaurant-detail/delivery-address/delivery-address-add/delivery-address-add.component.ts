import { Component, Injector, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RestaurantConst } from '@shared/AppConsts';
import { CrudComponentBase } from '@shared/crud-component-base';
import { IAddress } from '@shared/interfaces/restaurant.interface';
import { BusinessCustomerService } from '@shared/services/business-customer.service';
import { RestaurantService } from '@shared/services/restaurant.service';
import { SelectedLanguageService } from '@shared/services/selected-language.service';
import { MessageService } from 'primeng/api';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-delivery-address-add',
  templateUrl: './delivery-address-add.component.html',
  styleUrls: ['./delivery-address-add.component.scss']
})
export class DeliveryAddressAddComponent extends CrudComponentBase {

  constructor(
    injector: Injector,
    messageService: MessageService,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private _restaurantService: RestaurantService,
    private _businessCustomerService: BusinessCustomerService,
    private dialogService: DialogService,
    private router: Router,
    private fb: FormBuilder,
    private selectedLanguageService: SelectedLanguageService,
  ) {
    super(injector, messageService);
  }
  inputData: IAddress;
  postForm: FormGroup;

  labels: string[] = [];
  RestaurantConst = RestaurantConst;
  language: string = 'en';
  restaurantId: number;
  parentLangKeys: string[] = ['HOME', 'MENU', 'RESTAURANT', 'SHARE', 'ROUTER', 'CONFIRM', 'RESPONSE_MSG', 'BUSINESS_CUSTOMER'];
  isEdit: boolean = false;
  
  ngOnInit(): void {
    this.restaurantId = this.config?.data?.restaurantId;
    this.inputData = this.config?.data?.inputData;
    this.selectedLanguageService.getLanguage.subscribe((resLang) => {
      if(resLang) {
        this.language = resLang?.lang;
        this.labels = this.selectedLanguageService.getKeyLangs(this.parentLangKeys, resLang.keys);
      }
    });

    this.postForm = this.fb.group({
      id: [this.inputData?.id || null, []],
      receiver: [this.inputData?.receiver || null, Validators.required],
      restaurantId: [this.config?.data?.restaurantId, Validators.required],
      address: [this.inputData?.address || '', Validators.required],
      isDefault: [this.inputData?.isDefault || false, []],
      phone: [this.inputData?.phone || '', [Validators.required]],
    });
  }

  get postFormControl() {
    return this.postForm.controls;
  }



  onSubmit() {
    console.log("+__+",this.postForm.value);
    
    if (!this.checkInValidForm(this.postForm)) {
      if (this.inputData) {
        this._restaurantService.updateAddress(this.postForm.value).subscribe((response) => {
          if (this.handleResponseInterceptor(response, this.labels['RESPONSE_MSG']['UPDATE_SUCCESS'])) {
            this.ref.close(true);
          }
        }, () => {
          this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER']);
        }
        );
      }
      else {
        this._restaurantService.createAddress(this.postForm.value).subscribe(
          (response) => {
            if (this.handleResponseInterceptor(response, this.labels['RESPONSE_MSG']['ADD_SUCCESS'])) {
              this.ref.close(true);
            }
          }, () => {
            this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER']);
          }
        );
      }
    } else {
      this.messageError(this.labels['RESPONSE_MSG']['PLEASE_ENTER_ENOUGH_INFOR']);
    }
  }
}



