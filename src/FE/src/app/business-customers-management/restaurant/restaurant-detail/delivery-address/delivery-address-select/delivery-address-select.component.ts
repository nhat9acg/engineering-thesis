import { Component, Injector, OnInit } from "@angular/core";
import { TranslateService } from "@ngx-translate/core";
import { RestaurantConst, SearchConst } from "@shared/AppConsts";
import { CrudComponentBase } from "@shared/crud-component-base";
import { IBaseListAction } from "@shared/interfaces/base-interface";
import { IColumn } from "@shared/interfaces/business-customer.interface";
import { IAddress, IBusinessCustomerDropdown, IRestaurant } from "@shared/interfaces/restaurant.interface";
import { Page } from "@shared/model/page";
import { RestaurantService } from "@shared/services/restaurant.service";
import { SelectedLanguageService } from "@shared/services/selected-language.service";
import { ConfirmationService, MessageService } from "primeng/api";
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";
import { forkJoin } from "rxjs";
import { debounceTime } from "rxjs/operators";
import { BreadcrumbService } from "src/app/layout/breadcrumb/breadcrumb.service";
import { DeliveryAddressAddComponent } from "../delivery-address-add/delivery-address-add.component";

@Component({
	selector: "app-delivery-address-select",
	templateUrl: "./delivery-address-select.component.html",
	styleUrls: ["./delivery-address-select.component.scss"],
	providers: [DialogService, ConfirmationService, MessageService],
})
export class DeliveryAddressSelectComponent extends CrudComponentBase {
	constructor(
		injector: Injector,
		private dialogService: DialogService,
		private confirmationService: ConfirmationService,
		messageService: MessageService,
		private breadcrumbService: BreadcrumbService,
		private _restaurantService: RestaurantService,
		private selectedLanguageService: SelectedLanguageService,
		public _translateService: TranslateService,
		public config: DynamicDialogConfig,
		public ref: DynamicDialogRef,

	) {
		super(injector, messageService);
	}

	restaurantId: number;
	// @Input() contentHeight: number;
	inputData: IAddress;
	
	parentLangKeys: string[] = ['HOME', 'MENU', 'RESTAURANT', 'SHARE', 'ROUTER', 'CONFIRM', 'RESPONSE_MSG', 'BUSINESS_CUSTOMER', 'DELIVERY_ADDRESS'];
	labels: string[] = [];
	rows: IRestaurant[] = [];
	listAction: IBaseListAction[] = [];
	page = new Page();
	offset = 0;
	businessCustomers: IBusinessCustomerDropdown[] = [];
	language: string = 'en';
	RestaurantConst = RestaurantConst;

	ngOnInit(): void {
		this.restaurantId = this.config?.data?.restaurantId;
		
		this.selectedLanguageService.getLanguage.subscribe((resLang) => {
		  if(resLang) {
			this.language = resLang?.lang;
			this.labels = this.selectedLanguageService.getKeyLangs(this.parentLangKeys, resLang.keys);
		  }
		  this.setPage();
		});
		//
	}

	chooseAddress(row){
		let result = {
			id: row.id,
			labelName: `${row.address} - ${row.phone}` 
		}

		this.ref.close(result);
	}

	showData(rows) {
		for (let row of rows) {      
		  row.businessCustomer = this.businessCustomers.find(obj => obj.id === row.businessCustomerId)?.labelName;
		};
	}

	create() {
		const ref = this.dialogService.open(DeliveryAddressAddComponent, {
		  data: {
			labels: this.labels,
			restaurantId: this.restaurantId,
			language: this.language,
		  },
		  contentStyle: {"max-height": "400px", "overflow": "auto", "margin-bottom": "10px" },
		  header: this.labels['DELIVERY_ADDRESS']['ADD_TITLE'],
		  width: '600px',
		}).onClose.subscribe(result => {
		  if (result) {
			this.setPage();
		  } 
		})
	  }

	setPage() {
		this.isLoading = true;
		forkJoin([this._restaurantService.getAllAddresses(this.restaurantId)]).subscribe(([res]) => {
		  this.isLoading = false;
		  if (this.handleResponseInterceptor(res, '')) {
			this.rows = res.data;
		  }
		}, (err) => {
		  this.isLoading = false;
		  console.log('Error-------', err);
	
		});
	  }
}