import { Component, ElementRef, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CrudComponentBase } from '@shared/crud-component-base';

import { MessageService } from 'primeng/api';
import { BreadcrumbService } from 'src/app/layout/breadcrumb/breadcrumb.service';
import { SelectedLanguageService } from '@shared/services/selected-language.service';
import { TranslateService } from '@ngx-translate/core';
import { TabView } from 'primeng/tabview';
import { IBusinessCustomer } from '@shared/interfaces/business-customer.interface';
import { IRestaurant } from '@shared/interfaces/restaurant.interface';
import { RestaurantService } from '@shared/services/restaurant.service';
import { AppConsts, PermissionConst, localStorageKeyConst } from '@shared/AppConsts';

@Component({
  selector: 'app-restaurant-detail',
  templateUrl: './restaurant-detail.component.html',
  styleUrls: ['./restaurant-detail.component.scss']
})
export class RestaurantDetailComponent extends CrudComponentBase {

    constructor(
        injector: Injector,
        private routeActive: ActivatedRoute,
        messageService: MessageService,
        private breadcrumbService: BreadcrumbService,
        private selectedLanguageService: SelectedLanguageService,
        public _translateService: TranslateService,
        private _restaurantService: RestaurantService,
    ) {
        super(injector, messageService);
    }

    @ViewChild(TabView) tabView: TabView;
    tabViewActive = {
        'detail': true,
        'account': false,
        'address': false,
    };

    itemDetail: IRestaurant;
    @Input() restaurantId: number;

    labels: string[] = [];
    parentLangKeys: string[] = ['HOME', 'MENU', 'RESTAURANT', 'SHARE', 'ROUTER', 'CONFIRM', 'RESPONSE_MSG', 'BUSINESS_CUSTOMER'];
    PermissionConst = PermissionConst;
    contentTabHeight: number = 0;
    
    ngOnInit(): void {
        this.selectedLanguageService.getLanguage.subscribe((resLang) => {
            if(resLang) {
                this.labels = this.selectedLanguageService.getKeyLangs(this.parentLangKeys, resLang.keys);
                this.breadcrumbService.setItems([
                    { label: this.labels['ROUTER']['HOME_PAGE'], routerLink: ['/home'] },
                    { label: this.labels['ROUTER']['RESTAURANT'], routerLink: ['/business-customer-management/restaurant'] },
                ]);
            }
        });
        if(!this.restaurantId){
            this.restaurantId = +this.cryptDecode(this.routeActive.snapshot.paramMap.get('id'));
        }
        
        this.isLoading = true;
        this._restaurantService.get(this.restaurantId).subscribe((response) => {
            if (this.handleResponseInterceptor(response, "")) {
                this.itemDetail = response?.data;
            }
            this.isLoading = false;
        }, (err) => {
            this.isLoading = false;
        });
    }

    changeTab(e) {
        let tabHeader = this.tabView.tabs[e.index].header;
        this.tabViewActive[tabHeader] = true;
    }

}

