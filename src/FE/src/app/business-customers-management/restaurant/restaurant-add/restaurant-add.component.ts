import { Component, Injector, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PermissionConst, RestaurantConst } from '@shared/AppConsts';
import { CrudComponentBase } from '@shared/crud-component-base';
import { IAddress, IBusinessCustomerDropdown, IRestaurant } from '@shared/interfaces/restaurant.interface';
import { BusinessCustomerService } from '@shared/services/business-customer.service';
import { RestaurantService } from '@shared/services/restaurant.service';
import { SelectedLanguageService } from '@shared/services/selected-language.service';
import { MessageService } from 'primeng/api';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-restaurant-add',
  templateUrl: './restaurant-add.component.html',
  styleUrls: ['./restaurant-add.component.scss']
})
export class RestaurantAddComponent extends CrudComponentBase {

  constructor(
    injector: Injector,
    messageService: MessageService,
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private _restaurantService: RestaurantService,
    private _businessCustomerService: BusinessCustomerService,
    private dialogService: DialogService,
    private router: Router,
    private fb: FormBuilder,
    private selectedLanguageService: SelectedLanguageService,
  ) {
    super(injector, messageService);
  }
  inputData: IRestaurant;
  postForm: FormGroup;
  activeIndex: number;
  postFormChild: FormGroup;
  businessCustomers: IBusinessCustomerDropdown[] = [];
  addresses: IAddress[] = [];
  labels: string[] = [];
  RestaurantConst = RestaurantConst;
  
  parentLangKeys: string[] = ['RESTAURANT', 'SHARE', 'RESPONSE_MSG','BUSINESS_CUSTOMER'];
  isEdit: boolean = false;

  restaurantDetail = [];
  PermissionConst = PermissionConst;
  @Input() itemDetail: IRestaurant;
  @Input() contentHeight: number = 0;

  currentLang: string;
  
  ngOnInit(): void {
    this.isEdit = this.config?.data?.isEdit;
    this.selectedLanguageService.getLanguage.subscribe((resLang) => {
      if(resLang) {
        this.currentLang = resLang?.lang;
        this.labels = this.selectedLanguageService.getKeyLangs(this.parentLangKeys, resLang.keys);
        this.restaurantDetail = [
          { label: this.labels['RESTAURANT']['COLUMN_NAME'], controlName: 'name', readonly: true },
          { label: this.labels['RESTAURANT']['PHONE'], controlName: 'phone', readonly: true },
          { label: this.labels['BUSINESS_CUSTOMER']['EMAIL'], controlName: 'email', readonly: true },
          { label: this.labels['RESTAURANT']['COLUMN_ADDRESS'], controlName: 'address', readonly: true },
        
        ];
      }
    });
    
    this.optionsBusiness();

    this.postForm = this.fb.group({
      id: [this.itemDetail?.id || null, []],
      status: [this.itemDetail?.status || RestaurantConst.ACTIVE, []],
      businessCustomerId: [this.itemDetail?.businessCustomerId || null, Validators.required],
      address: [this.itemDetail?.address || '', Validators.required],
      name: [this.itemDetail?.name || '', Validators.required],
      email: [this.itemDetail?.email || '', [Validators.required]],
      phone: [this.itemDetail?.phone || '', [Validators.required]],
    });
  }

  optionsBusiness() {
    this.isLoading = true;
    this._restaurantService.getAllBusinessCustomer().subscribe((res) => {
      this.isLoading = false;
      if (this.handleResponseInterceptor(res, '')) {

        this.businessCustomers = res.data?.items.map((item) => {
          return {
            id: item.id,
            labelName: item.fullName
          };
        });
      }
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);

    });
  }

  get postFormControl() {
    return this.postForm.controls;
  }

  changeEdit() {
    if(!this.isEdit) {
      this.onSubmit();
    }
  }

  onSubmit() {
    if (!this.checkInValidForm(this.postForm)) {
      if (this.itemDetail) {
        this._restaurantService.update(this.postForm.value).subscribe((response) => {
          if (this.handleResponseInterceptor(response, this.labels['RESPONSE_MSG']['UPDATE_SUCCESS'])) {
            this.ref.close(true);
          }
        }, () => {
          this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER']);
        }
        );
      }
      else {
        this._restaurantService.create(this.postForm.value).subscribe(
          (response) => {
            if (this.handleResponseInterceptor(response, this.labels['RESPONSE_MSG']['ADD_SUCCESS'])) {
              this.ref.close(true);
              // this.router.navigate(['/partner-manager/partner/detail', this.cryptEncode(response.data.partnerId)]);
            }
          }, () => {
            this.messageError(this.labels['RESPONSE_MSG']['PLEASE_TRY_AGAIN_LATER']);
          }
        );
      }
    } else {
      this.messageError(this.labels['RESPONSE_MSG']['PLEASE_ENTER_ENOUGH_INFOR']);
    }
  }
}


