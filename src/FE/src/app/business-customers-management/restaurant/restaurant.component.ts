import { Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SearchConst, FormNotificationConst, RestaurantConst, localStorageKeyConst, PermissionConst } from '@shared/AppConsts';
import { CrudComponentBase } from '@shared/crud-component-base';
import { Page } from '@shared/model/page';
import { BusinessCustomerService } from '@shared/services/business-customer.service';

import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService, DynamicDialogRef } from 'primeng/dynamicdialog';
import { debounceTime } from 'rxjs/operators';
import { FormNotificationComponent } from 'src/app/form-general/form-notification/form-notification.component';
import { FormSetDisplayColumnComponent } from 'src/app/form-general/form-set-display-column/form-set-display-column.component';
import { BreadcrumbService } from 'src/app/layout/breadcrumb/breadcrumb.service';
import { RestaurantAddComponent } from './restaurant-add/restaurant-add.component';
import { RestaurantService } from '@shared/services/restaurant.service';
import { IColumn } from '@shared/interfaces/business-customer.interface';
import { IBaseListAction } from '@shared/interfaces/base-interface';
import { SelectedLanguageService } from '@shared/services/selected-language.service';
import { TranslateService } from '@ngx-translate/core';
import { IBusinessCustomerDropdown, IRestaurant } from '@shared/interfaces/restaurant.interface';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'app-restaurant',
  templateUrl: './restaurant.component.html',
  styleUrls: ['./restaurant.component.scss'],
  providers: [DialogService, ConfirmationService, MessageService]
})
export class RestaurantComponent extends CrudComponentBase {

    constructor(
        injector: Injector,
        private dialogService: DialogService,
        private _dialogService: DialogService,
        private confirmationService: ConfirmationService,
        private router: Router,
        private routeActive: ActivatedRoute,
        messageService: MessageService,
        private breadcrumbService: BreadcrumbService,
        private _restaurantService: RestaurantService,
        private selectedLanguageService: SelectedLanguageService,
        public _translateService: TranslateService,
    ) {
        super(injector, messageService);
    }
    parentLangKeys: string[] = ['RESTAURANT', 'SHARE', 'ROUTER', 'CONFIRM', 'RESPONSE_MSG', 'BUSINESS_CUSTOMER'];

    labels: string[] = [];
    ref: DynamicDialogRef;
    rows: IRestaurant[] = [];
    cols: IColumn[];
    listAction: IBaseListAction[] = [];
    page = new Page();
    offset = 0;
    businessCustomers: IBusinessCustomerDropdown[] = [];
    language: string = 'en';
    RestaurantConst = RestaurantConst;
    PermissionConst = PermissionConst;
    scrollHeight: number = 0;
    
    ngOnInit(): void {
        this.selectedLanguageService.getLanguage.subscribe((resLang) => {
        if(resLang) {
            this.language = resLang?.lang;
            this.labels = this.selectedLanguageService.getKeyLangs(this.parentLangKeys, resLang.keys);
            this.cols = [
                { field: `name`, header: this.labels['RESTAURANT']['COLUMN_NAME'], width: '20rem', isPin: true, type: '' },
                { field: 'businessCustomer', header: this.labels['RESTAURANT']['BUSINESS_CUSTOMER'], width: '20rem', isPin: true, type: '' },
                { field: `address`, header: this.labels['RESTAURANT']['COLUMN_ADDRESS'], width: '24rem', isPin: false, type: '' },       
                { field: `columnResize`, header: '', width: '', isPin: false, type: 'hidden' },
            ];
            //
            this.breadcrumbService.setItems([
                { label: this.labels['ROUTER']['HOME_PAGE'], routerLink: ['/home'] },
                { label: this.labels['ROUTER']['RESTAURANT'], routerLink: ['/business-customer-management/restaurant'] },
            ]);
        }
        this.setPage();
        });
        //
        this.subject.keyword.pipe(debounceTime(SearchConst.DEBOUNCE_TIME)).subscribe(() => {
            if (this.keyword === "") {
                this.setPage();
            } else {
                this.setPage();
            }
        });
    }

    showData(rows) {
        for (let row of rows) {      
            row.businessCustomer = this.businessCustomers.find(obj => obj.id === row.businessCustomerId)?.labelName;
        };
    }

    genListAction(data = []) {
        this.listAction = data.map(item => {
            const actions = [];
            if (this.isGranted([PermissionConst.RestaurantTable]) ) { 
                actions.push({
                    data: item,
                    label: this.labels['SHARE']['BUTTON_DETAIL'],
                    icon: 'pi pi-info-circle',
                    command: ($event) => {
                    this.detail($event.item.data);
                    }
                })
            }
            if (this.isGranted([PermissionConst.RestaurantDelete]) ) { 
                actions.push({
                    data: item,
                    label: this.labels['SHARE']['DELETE'],
                    icon: 'pi pi-trash',
                    command: ($event) => {
                    this.delete($event.item.data);
                    }
                });
            }
            return actions;
        });
    }

    detail(item) {
        this.router.navigate(['/business-customer-management/restaurant/detail/' + this.cryptEncode(item?.id)]);
    }

    create() {
        this.dialogService.open(RestaurantAddComponent, {
            contentStyle: {"max-height": "600px", "overflow": "auto", "margin-bottom": "60px" },
            header: `${this.labels['RESTAURANT']['TITLE_DIALOG_ADD']}`,
            width: '800px',
            data: {
                labels: this.labels,
                language: this.language,
                isEdit: true,
            },
        }).onClose.subscribe(result => {
            if (result) {
                this.setPage();
            } 
        })
    }

    delete(item) {
        this.confirmationService.confirm({
            message: this.labels['CONFIRM']['ARE_YOU_SURE_TO_DELETE_THIS_RESTAURANT'],
            acceptLabel: this.labels['CONFIRM']['YES'],
            rejectLabel: this.labels['CONFIRM']['NO'],
            accept: () => {
                this._restaurantService.delete(item.id).subscribe((response) => {
                    if (this.handleResponseInterceptor(response, this.labels['CONFIRM']['DELETE_SUCCESS'])) {
                        this.setPage();
                    }
                });
            }
        });
    }

    isRowOdds: boolean = false;
    setPage(event?: any) {
        this.isRowOdds = false;
        this.isLoading = true;
        if(event) {
                this.page.pageNumber = event.page;
                this.page.pageSize = event.rows;
            }
        this.page.keyword = this.keyword;
        forkJoin([
            this._restaurantService.getAll(this.page),
            this._restaurantService.getAllBusinessCustomer()
        ]).subscribe(([res, resBusiness]) => {
            this.isLoading = false;
            if (this.handleResponseInterceptor(res, '')) {
                this.page.totalItems = res.data.totalItems;
                this.rows = res.data?.items.map( item => {
                    return { ...item, labelName: item.fullName};
                });
                this.businessCustomers = resBusiness.data?.items.map((item) => {
                    return { id: item.id, labelName: item.fullName };
                });
            
                if (res.data?.items?.length) {
                this.isRowOdds = !(this.rows?.length%2 == 0);
                this.genListAction(this.rows);
                this.showData(this.rows);
            }
        }
        }, (err) => {
            this.isLoading = false;
            console.log('Error-------', err);
        });
    }
}


