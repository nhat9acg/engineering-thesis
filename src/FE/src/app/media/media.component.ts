import { Component, Injector, OnInit } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { DEFAULT_MEDIA } from '@shared/base-object';
import { EAcceptFile } from '@shared/consts/base.consts';
import { CrudComponentBase } from '@shared/crud-component-base';
import { IResponseDialogUpload } from '@shared/interfaces/response.interface';
import { HelpersService } from '@shared/services/helpers.service';
import { ConfirmationService, MessageService } from 'primeng/api';
import { DialogService } from 'primeng/dynamicdialog';
import { FormNotificationComponent } from '../form-general/form-notification/form-notification.component';
import { FormNotificationConst } from '@shared/AppConsts';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { NotificationService } from '@shared/services/notification.service';

@Component({
  selector: 'app-media',
  templateUrl: './media.component.html',
  styleUrls: ['./media.component.scss']
})

export class MediaComponent extends CrudComponentBase {

	constructor(
		injector: Injector,
		messageService: MessageService,
		private confirmationService: ConfirmationService,
    private _notificationService: NotificationService,
		private router: Router,
		private _dialogService: DialogService,
    private _helpers: HelpersService,
		private sanitizer: DomSanitizer,
	) {
		super(injector, messageService);
	}
	imgBackground = DEFAULT_MEDIA.DEFAULT_IMAGE.IMAGE_ADD;
	public dataMediaTypes = [
		{
			name: 'Hình ảnh',
			typeFile: 'IMAGE',
			isMultiple: true,
		}
	];
  public inputData: string[] = [""];
	public baseUrl: string = "";

	ngOnInit(): void {
		this.baseUrl = this.AppConsts.remoteServiceBaseUrl ?? this.baseUrl;
		this.init();
	}

	private mapDto() {

	}

	private init() {
		this._notificationService.getSystemMedia().subscribe(
			(res) => {
				this.isLoading = false;
				if (this.handleResponseInterceptor(res, '')) {
          this.inputData = res.data?.images || [];
				}
			}, (err) => {
				this.isLoading = false;
				console.log('Error-------', err);
				this.messageError('Có lỗi xảy ra. Vui lòng thử lại sau!');
			});
	}

	public insertImages(media) {
		const ref = this._helpers.dialogUploadRef('media', { accept: EAcceptFile.IMAGE, uploadServer: true });
		ref.onClose.subscribe((response: IResponseDialogUpload) => {
			if (response && response?.fileUrls?.length > 0) {
        for (const item of response.fileUrls) {
          if (typeof item == 'string') {
            this.inputData.push(item);
          }
        }
        this.updateCommon();
			}
		}
		);
	}

	public getImage(image: File | Blob | string): SafeUrl | string {
		if (image instanceof File || image instanceof Blob) {
			const objectURL = URL.createObjectURL(image);
			return this.sanitizer.bypassSecurityTrustUrl(objectURL);
		}
		return `${this.baseUrl}/${image}`;
	}

  private updateCommon() {
    this._notificationService.createOrUpdateMedia(this.inputData).subscribe(
      (response) => {
        if (this.handleResponseInterceptor(response, "Cập nhật thành công")) {
          this.init();
        }
      },
      (err) => {
        this.submitted = false;
      }
    );
  }

	public deleteImage(index) {
		const ref = this._dialogService.open(FormNotificationComponent, {
			header: "Thông báo",
			width: "600px",
			contentStyle: { "max-height": "600px", "overflow": "auto", "padding-bottom": "50px" },
			styleClass: 'p-dialog-custom',
			baseZIndex: 10000,
			data: {
				title: `Bạn có chắc chắn muốn xóa hình ảnh?`,
				icon: FormNotificationConst.IMAGE_CLOSE,
			},
		});
		ref.onClose.subscribe((dataCallBack) => {
			if (dataCallBack?.accept) {
				if (index >= 0 && index < this.inputData?.length) {
					this.inputData.splice(index, 1);
          this.updateCommon();
				}
			} 
		});
	}

	public drop(event: CdkDragDrop<any>, index, media) {
		this.dataMediaTypes[index] = { ...media };
		moveItemInArray(
			this.inputData,
			event.previousContainer.data.index,
			event.container.data.index
		);
	}
}

