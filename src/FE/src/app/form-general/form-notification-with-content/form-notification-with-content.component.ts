import { Component, OnInit } from "@angular/core";
import { AppConsts } from "@shared/AppConsts";
import { DynamicDialogConfig, DynamicDialogRef } from "primeng/dynamicdialog";

@Component({
	selector: "app-form-notification-with-content",
	templateUrl: "./form-notification-with-content.component.html",
	styleUrls: ["./form-notification-with-content.component.scss"],
})
export class FormNotificationWithContentComponent implements OnInit {
	constructor(
		public ref: DynamicDialogRef, 
		public configDialog: DynamicDialogConfig
	) {}

	acceptStatus: boolean = true;
  
	data = {
	  title: null,
	  note: null,
	  trackingCode: null
	}

	ngOnInit(): void {
		this.data.title = this.configDialog.data.title;
	}

	hideDialog() {
	}
  
	accept() {
	  this.acceptStatus = true;
	  this.onAccept();
	}
  
	cancel() {
	  this.acceptStatus = false;
	  this.onAccept();
	}
  
	onAccept() {
	  this.ref.close({data: this.data,accept: this.acceptStatus});
	}
}
