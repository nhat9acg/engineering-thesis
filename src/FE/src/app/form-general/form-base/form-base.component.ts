import { Component, Injector, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { City, Country, DataSeedConst, FormBase, Option } from '../interface/form-base.interface';
import { CrudComponentBase } from '@shared/crud-component-base';
import { MessageService } from 'primeng/api';
import { MatchPassword } from '@shared/validators/match-password.validator';
import { TranslateService } from '@ngx-translate/core';
import { AppConsts } from '@shared/AppConsts';
import { HelperService } from '@shared/services/helper.service';
import { SelectedLanguageService } from '@shared/services/selected-language.service';

@Component({
  selector: 'app-form-base',
  templateUrl: './form-base.component.html',
  styleUrls: ['./form-base.component.scss']
})
export class FormBaseComponent extends CrudComponentBase {

  constructor(
    injector: Injector,
    messageService: MessageService,
    private _helperService: HelperService,
    private _selectedLangService: SelectedLanguageService,
  ) { 
    super(injector, messageService);
  }

  formBase: FormGroup;
  options: Option[];
  countries: Country[] = [...DataSeedConst.coutries];
  cities: Option[] = [...DataSeedConst.cities];

  get f() {
    return this.formBase;
  }

  parentLangKeys: string[] = ['HOME', 'MENU'];
  labels: string[];

  ngOnInit(): void {
    this._selectedLangService.getLanguage.subscribe((resLang) => {
      if(resLang) {
        this.labels = this._selectedLangService.getKeyLangs(this.parentLangKeys, resLang.keys);
        console.log('labels', this.labels);
      }
    })
    //
    this.formBase = new FormGroup({
      inputText: new FormControl(null, [Validators.required]),
      inputNumber: new FormControl(null, [Validators.required]),
      inputTextarea: new FormControl(null, [Validators.required]),
      select: new FormControl(null, [Validators.required]),
      multipleSelect: new FormControl(null, [Validators.required]),
      cascadeSelect: new FormControl(null, [Validators.required]),
      calendar: new FormControl(null, [Validators.required]),
      password: new FormControl(null, [Validators.required]),
      confirmPassword: new FormControl(null, [Validators.required]),
    }, {
      validators: MatchPassword
    })
  }

  getValue(field: string) {
    return this.formBase.value[field];
  }

  submitForm(): void {
    if(!this.checkInValidForm(this.formBase)) {
      let body: FormBase = this.formBase.value;
      console.log('body', body);
      
    } else {
      this.messageError('Vui lòng nhập đủ thông tin!');
    }
  }
}
