import { Component, Injector, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CrudComponentBase } from '@shared/crud-component-base';
import { IDropdown } from '@shared/interfaces/base-interface';
import { SelectedLanguageService } from '@shared/services/selected-language.service';
import { ShippingProviderService } from '@shared/services/shipping-provider.service';
import { MessageService } from 'primeng/api';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/dynamicdialog';

@Component({
  selector: 'app-form-confirm-order',
  templateUrl: './form-confirm-order.component.html',
  styleUrls: ['./form-confirm-order.component.scss']
})
export class FormConfirmOrderComponent extends CrudComponentBase{

  constructor(
    injector: Injector,
    messageService: MessageService,
    public ref: DynamicDialogRef, 
		public configDialog: DynamicDialogConfig,
    private _shippingProviderService: ShippingProviderService,
		private _selectedLangService: SelectedLanguageService,
  ) {
    super(injector, messageService);
  }

	acceptStatus: boolean = true;

  postForm?: FormGroup;
	parentLangKeys: string[] = ['RESPONSE_MSG'];
	labels: string[] = [];

  shippingProviders: IDropdown[] = [];

	ngOnInit(): void {
    this._selectedLangService.getLanguage.subscribe((resLang) => {
			if(resLang) {
			  this.labels = this._selectedLangService.getKeyLangs(this.parentLangKeys, resLang.keys);
			  console.log('labels', this.labels);
			}
		})
    this.setPostForm();
    this._shippingProviderService.getAllNoPaging().subscribe((res) => {
      if (this.handleResponseInterceptor(res, '')) {
        this.shippingProviders = res?.data?.items.map(item => {
          return {
            id: item.id,
            labelName: item.name,
          };
        });
      }
    }, (err) => {
      this.isLoading = false;
      console.log('Error-------', err);
    });

    
	}

  setPostForm() {
    this.postForm = new FormGroup({
      trackingCode: new FormControl( null, Validators.required),
      shippingProviderId: new FormControl(null, Validators.required),
      estimatedShippingDate: new FormControl(new Date(), Validators.required),
    });
  }

  onSubmit() {
		console.log('postForm ', this.postForm);
    if (!this.checkInValidForm(this.postForm)) {
          this.onAccept();
		} else {
			this.messageError(this.labels['RESPONSE_MSG']['PLEASE_ENTER_ENOUGH_INFOR'], 3000)
		}
	}
  
	cancel() {
    this.ref.close();
	}
  
	onAccept() {
	  this.ref.close({data: this.postForm.value, accept: true});
	}
}
