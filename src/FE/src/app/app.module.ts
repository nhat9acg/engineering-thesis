import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import {
  APP_BASE_HREF,
  CommonModule,
  HashLocationStrategy,
  LocationStrategy,
} from "@angular/common";

// PrimeNG Components for demos
import { AccordionModule } from "primeng/accordion";
import { AutoCompleteModule } from "primeng/autocomplete";
import { AvatarModule } from "primeng/avatar";
import { AvatarGroupModule } from "primeng/avatargroup";
import { BadgeModule } from "primeng/badge";
import { BreadcrumbModule } from "primeng/breadcrumb";
import { ButtonModule } from "primeng/button";
import { CalendarModule } from "primeng/calendar";
import { CardModule } from "primeng/card";
import { CarouselModule } from "primeng/carousel";
import { CascadeSelectModule } from "primeng/cascadeselect";
import { ChartModule } from "primeng/chart";
import { CheckboxModule } from "primeng/checkbox";
import { ChipModule } from "primeng/chip";
import { ChipsModule } from "primeng/chips";
import { CodeHighlighterModule } from "primeng/codehighlighter";
import { ConfirmDialogModule } from "primeng/confirmdialog";
import { ConfirmPopupModule } from "primeng/confirmpopup";
import { ColorPickerModule } from "primeng/colorpicker";
import { ContextMenuModule } from "primeng/contextmenu";
import { DataViewModule } from "primeng/dataview";
import { DialogModule } from "primeng/dialog";
import { DividerModule } from "primeng/divider";
import { DropdownModule } from "primeng/dropdown";
import { FieldsetModule } from "primeng/fieldset";
import { FileUploadModule } from "primeng/fileupload";
import { FullCalendarModule } from "@fullcalendar/angular";
import { GalleriaModule } from "primeng/galleria";
import { ImageModule } from "primeng/image";
import { InplaceModule } from "primeng/inplace";
import { InputNumberModule } from "primeng/inputnumber";
import { InputMaskModule } from "primeng/inputmask";
import { InputSwitchModule } from "primeng/inputswitch";
import { InputTextModule } from "primeng/inputtext";
import { InputTextareaModule } from "primeng/inputtextarea";
import { KnobModule } from "primeng/knob";
import { LightboxModule } from "primeng/lightbox";
import { ListboxModule } from "primeng/listbox";
import { MegaMenuModule } from "primeng/megamenu";
import { MenuModule } from "primeng/menu";
import { MenubarModule } from "primeng/menubar";
import { MessagesModule } from "primeng/messages";
import { MessageModule } from "primeng/message";
import { MultiSelectModule } from "primeng/multiselect";
import { OrderListModule } from "primeng/orderlist";
import { OrganizationChartModule } from "primeng/organizationchart";
import { OverlayPanelModule } from "primeng/overlaypanel";
import { PaginatorModule } from "primeng/paginator";
import { PanelModule } from "primeng/panel";
import { PanelMenuModule } from "primeng/panelmenu";
import { PasswordModule } from "primeng/password";
import { PickListModule } from "primeng/picklist";
import { ProgressBarModule } from "primeng/progressbar";
import { RadioButtonModule } from "primeng/radiobutton";
import { RatingModule } from "primeng/rating";
import { RippleModule } from "primeng/ripple";
import { ScrollPanelModule } from "primeng/scrollpanel";
import { ScrollTopModule } from "primeng/scrolltop";
import { SelectButtonModule } from "primeng/selectbutton";
import { SidebarModule } from "primeng/sidebar";
import { SkeletonModule } from "primeng/skeleton";
import { SlideMenuModule } from "primeng/slidemenu";
import { SliderModule } from "primeng/slider";
import { SplitButtonModule } from "primeng/splitbutton";
import { SplitterModule } from "primeng/splitter";
import { StepsModule } from "primeng/steps";
import { TabMenuModule } from "primeng/tabmenu";
import { TableModule } from "primeng/table";
import { TabViewModule } from "primeng/tabview";
import { TagModule } from "primeng/tag";
import { TerminalModule } from "primeng/terminal";
import { TieredMenuModule } from "primeng/tieredmenu";
import { TimelineModule } from "primeng/timeline";
import { ToastModule } from "primeng/toast";
import { ToggleButtonModule } from "primeng/togglebutton";
import { ToolbarModule } from "primeng/toolbar";
import { TooltipModule } from "primeng/tooltip";
import { TreeModule } from "primeng/tree";
import { TreeTableModule } from "primeng/treetable";
import { VirtualScrollerModule } from "primeng/virtualscroller";
import { MarkdownModule } from "ngx-markdown";

// Application Components
import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import { AppMainComponent } from "./layout/main/app.main.component";
import { AppMenuComponent } from "./layout/menu/app.menu.component";
import { AppMenuitemComponent } from "./layout/menu/app.menuitem.component";
import { AppBreadcrumbComponent } from "./layout/breadcrumb/app.breadcrumb.component";
import { AppTopBarComponent } from "./layout/top-bar/app.topbar.component";
import { AppFooterComponent } from "./layout/footer/app.footer.component";

import { ProgressSpinnerModule } from "primeng/progressspinner";
import { ReactiveFormsModule } from "@angular/forms";

// Application services
import { BreadcrumbService } from "./layout/breadcrumb/breadcrumb.service";
import { MenuService } from "./layout/menu/app.menu.service";

import { AngularEditorModule } from "@kolkov/angular-editor";

import dayGridPlugin from "@fullcalendar/daygrid";
import timeGridPlugin from "@fullcalendar/timegrid";
import interactionPlugin from "@fullcalendar/interaction";
import { HomeComponent } from "./home/home.component";
import { SharedModule } from "@shared/shared.module";

import { UploadImageComponent } from "./components/upload-image/upload-image.component";
import {
  DialogService,
  DynamicDialogConfig,
  DynamicDialogModule,
  DynamicDialogRef,
} from "primeng/dynamicdialog";

import { KeyFilterModule } from "primeng/keyfilter";
import { PdfViewerModule } from "ng2-pdf-viewer";
import { NgOtpInputModule } from "ng-otp-input";

import { FormViewPdfFileComponent } from "./form-general/form-view-pdf-file/form-view-pdf-file.component";
import { FormSetDisplayColumnComponent } from "./form-general/form-set-display-column/form-set-display-column.component";

import { CountdownModule } from '@ciri/ngx-countdown';
import { DragDropModule } from "@angular/cdk/drag-drop";
import { FormBaseComponent } from './form-general/form-base/form-base.component';
import { RoleManagerComponent } from './system-manager/role-manager/role-manager.component';
import { CreateOrUpdateRoleComponent } from './system-manager/role-manager/create-or-update-role/create-or-update-role.component';
import { UserManagerComponent } from './system-manager/user-manager/user-manager.component';
import { CreateUserComponent } from './system-manager/user-manager/create-user/create-user.component';
import { FormNotificationComponent } from './form-general/form-notification/form-notification.component';
import { TranslateModule } from '@ngx-translate/core';
import { BusinessCustomerComponent } from './business-customers-management/business-customer/business-customer.component';
import { RestaurantComponent } from './business-customers-management/restaurant/restaurant.component';
import { BusinessCustomerDetailComponent } from './business-customers-management/business-customer/business-customer-detail/business-customer-detail.component';
import { RestaurantDetailComponent } from './business-customers-management/restaurant/restaurant-detail/restaurant-detail.component';
import { BusinessCustomerAddComponent } from './business-customers-management/business-customer/business-customer-add/business-customer-add.component';
import { RestaurantAddComponent } from './business-customers-management/restaurant/restaurant-add/restaurant-add.component';
import { GroupCustomerComponent } from './business-customers-management/group-customer/group-customer.component';
import { GroupCustomerAddComponent } from './business-customers-management/group-customer/group-customer-add/group-customer-add.component';
import { ProductCategoryComponent } from './product-management/product-category/product-category.component';
import { ProductManagerComponent } from './product-management/product-manager/product-manager.component';
import { CreateOrUpdateProductCategoryComponent } from './product-management/product-category/create-or-update-product-category/create-or-update-product-category.component';
import { CreateOrEditProductComponent } from './product-management/product-manager/create-or-edit-product/create-or-edit-product.component';
import { ProductPriceComponent } from './product-management/product-price/product-price.component';
import { AddOrEditProductPriceComponent } from './product-management/product-price/add-or-edit-product-price/add-or-edit-product-price.component';
import { OrderComponent } from './order-management/order/order.component';
import { OrderAddComponent } from './order-management/order/order-add/order-add.component';
import { FormNotificationWithContentComponent } from './form-general/form-notification-with-content/form-notification-with-content.component';
import { FormConfirmOrderComponent } from './form-general/form-confirm-order/form-confirm-order.component';
import { DeliveryAddressComponent } from './business-customers-management/restaurant/restaurant-detail/delivery-address/delivery-address.component';
import { DeliveryAddressAddComponent } from './business-customers-management/restaurant/restaurant-detail/delivery-address/delivery-address-add/delivery-address-add.component';
import { CalculationUnitComponent } from './product-management/calculation-unit/calculation-unit.component';
import { CalculationUnitAddComponent } from './product-management/calculation-unit/calculation-unit-add/calculation-unit-add.component';
import { ViewOrderComponent } from './order-management/order/view-order/view-order.component';
import { DeliveryAddressSelectComponent } from './business-customers-management/restaurant/restaurant-detail/delivery-address/delivery-address-select/delivery-address-select.component';
import { CalendarComponent } from "./system-manager/calendar/calendar.component";
import { EditDayOffComponent } from './system-manager/calendar/edit-day-off/edit-day-off.component';
import { ProductComponent } from './customer-management/product/product.component';
import { DocumentComponent } from './system-manager/document/document.component';
import { DocumentAddComponent } from './system-manager/document/document-add/document-add.component';
import { CartComponent } from './customer-management/cart/cart.component';
import { ProfileComponent } from './customer-management/profile/profile.component';
import { AddressComponent } from './customer-management/profile/address/address.component';
import { AddressAddComponent } from './customer-management/profile/address/address-add/address-add.component';
import { RetailOrdersComponent } from './order-management/retail-orders/retail-orders.component';
import { RetailOrdersAddComponent } from './order-management/retail-orders/retail-orders-add/retail-orders-add.component';
import { AddressSelectComponent } from './customer-management/profile/address/address-select/address-select.component';
import { PurchaseComponent } from './customer-management/purchase/purchase.component';
import { ViewRetailOrdersComponent } from './order-management/retail-orders/view-retail-orders/view-retail-orders.component';
import { ShippingCompanyComponent } from './product-management/shipping-company/shipping-company.component';
import { ShippingCompanyAddComponent } from './product-management/shipping-company/shipping-company-add/shipping-company-add.component';
import { PurchaseDetailComponent } from "./customer-management/purchase/purchase-detail/purchase-detail.component";
import { ProductDetailComponent } from './customer-management/product/product-detail/product-detail.component';
import { SystemNotificationComponent } from './notification-management/system-notification/system-notification.component';
import { NotificationComponent } from './customer-management/notification/notification.component';
import { NotificationDetailComponent } from './customer-management/notification/notification-detail/notification-detail.component';
import { MarkdownToHtmlDirective } from './markdown-to-html.directive';
import { ChatComponent } from './customer-management/chat/chat.component';
import { LuckyCircleComponent } from './customer-management/lucky-circle/lucky-circle.component';
import { ManagementReportComponent } from './export-report/management-report/management-report.component';
import { FormCalendarComponent } from "@shared/components/form-calendar/form-calendar.component";
import { MediaComponent } from './media/media.component';
import { DetailBusinessComponent } from './business-customers-management/detail-business/detail-business.component';

FullCalendarModule.registerPlugins([
  dayGridPlugin,
  timeGridPlugin,
  interactionPlugin,
]);

@NgModule({
  imports: [
    SharedModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    AccordionModule,
    AutoCompleteModule,
    AvatarModule,
    AvatarGroupModule,
    BadgeModule,
    BreadcrumbModule,
    ButtonModule,
    CalendarModule,
    CardModule,
    CarouselModule,
    CascadeSelectModule,
    ChartModule,
    CheckboxModule,
    ChipModule,
    ChipsModule,
    CodeHighlighterModule,
    ConfirmDialogModule,
    ConfirmPopupModule,
    ColorPickerModule,
    ContextMenuModule,
    DataViewModule,
    DialogModule,
    DividerModule,
    DropdownModule,
    FieldsetModule,
    FileUploadModule,
    FullCalendarModule,
    GalleriaModule,
    ImageModule,
    InplaceModule,
    InputNumberModule,
    InputMaskModule,
    InputSwitchModule,
    InputTextModule,
    InputTextareaModule,
    KnobModule,
    LightboxModule,
    ListboxModule,
    MegaMenuModule,
    MenuModule,
    MenubarModule,
    MessageModule,
    MessagesModule,
    MultiSelectModule,
    OrderListModule,
    OrganizationChartModule,
    OverlayPanelModule,
    PaginatorModule,
    PanelModule,
    PanelMenuModule,
    PasswordModule,
    PickListModule,
    ProgressBarModule,
    RadioButtonModule,
    RatingModule,
    RippleModule,
    ScrollPanelModule,
    ScrollTopModule,
    SelectButtonModule,
    SidebarModule,
    SkeletonModule,
    SlideMenuModule,
    SliderModule,
    SplitButtonModule,
    SplitterModule,
    StepsModule,
    TableModule,
    TabMenuModule,
    TabViewModule,
    TagModule,
    TerminalModule,
    TimelineModule,
    TieredMenuModule,
    ToastModule,
    ToggleButtonModule,
    ToolbarModule,
    TooltipModule,
    TreeModule,
    TreeTableModule,
    VirtualScrollerModule,
    ProgressSpinnerModule,
    AngularEditorModule,
    DynamicDialogModule,
    KeyFilterModule,
    PdfViewerModule,
    MarkdownModule.forRoot(),
    NgOtpInputModule,
    CountdownModule,
    DragDropModule,
    TranslateModule,
  ],

  declarations: [
    AppComponent,
    AppMainComponent,
    AppMenuComponent,
    AppMenuitemComponent,
    AppTopBarComponent,
    AppFooterComponent,
    AppBreadcrumbComponent,
    HomeComponent,
    UploadImageComponent,
    FormSetDisplayColumnComponent,
    FormViewPdfFileComponent,
    HomeComponent,
    FormBaseComponent,
    RoleManagerComponent,
    CreateOrUpdateRoleComponent,
    UserManagerComponent,
    CreateUserComponent,
    FormNotificationComponent,
    BusinessCustomerComponent,
    RestaurantComponent,
    BusinessCustomerDetailComponent,
    RestaurantDetailComponent,
    BusinessCustomerAddComponent,
    RestaurantAddComponent,
    GroupCustomerComponent,
    GroupCustomerAddComponent,
    ProductCategoryComponent,
    ProductManagerComponent,
    CreateOrUpdateProductCategoryComponent,
    CreateOrEditProductComponent,
    ProductPriceComponent,
    AddOrEditProductPriceComponent,
    OrderComponent,
    OrderAddComponent,
    FormNotificationWithContentComponent,
    FormConfirmOrderComponent,
    DeliveryAddressComponent,
    DeliveryAddressAddComponent,
    CalculationUnitComponent,
    CalculationUnitAddComponent,
    ViewOrderComponent,
    DeliveryAddressSelectComponent,
    CalendarComponent,
    EditDayOffComponent,
    ProductComponent,
    DocumentComponent,
    DocumentAddComponent,
    CartComponent,
    ProfileComponent,
    AddressComponent,
    AddressAddComponent,
    RetailOrdersComponent,
    RetailOrdersAddComponent,
    AddressSelectComponent,
    PurchaseComponent,
    ViewRetailOrdersComponent,
    ShippingCompanyComponent,
    ShippingCompanyAddComponent,
    PurchaseDetailComponent,
    ProductDetailComponent,
    SystemNotificationComponent,
    NotificationComponent,
    NotificationDetailComponent,
    MarkdownToHtmlDirective,
    ChatComponent,
    LuckyCircleComponent,
    ManagementReportComponent,
    MediaComponent,
    DetailBusinessComponent,
  ],

  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    MenuService,
    BreadcrumbService,
    DialogService,
    DynamicDialogRef,
    DynamicDialogConfig,

  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
