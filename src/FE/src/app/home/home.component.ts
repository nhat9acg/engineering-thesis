import {Chart, ChartComponent} from 'chart.js';
import ChartDataLabels from 'chartjs-plugin-datalabels';
import { Component, ElementRef, Inject, Injector, OnInit, PLATFORM_ID, ViewChild } from '@angular/core';
import { MessageService, SelectItem } from 'primeng/api';
import { Subscription, forkJoin, fromEvent } from 'rxjs';
import { BreadcrumbService } from '../layout/breadcrumb/breadcrumb.service';
import { options } from 'preact';
import { CrudComponentBase } from '@shared/crud-component-base';
import { DashBoardServiceProxy } from '@shared/service-proxies/dashboard-service';
import * as moment from 'moment';
import { isPlatformBrowser } from '@angular/common';
import { SelectedLanguageService } from '@shared/services/selected-language.service';
import { AppConsts } from '@shared/AppConsts';
import { IResponseLang } from '@shared/interfaces/base-interface';
import { BusinessCustomerService } from '@shared/services/business-customer.service';
import { RestaurantService } from '@shared/services/restaurant.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  providers: []
})
export class HomeComponent extends CrudComponentBase {

    constructor(
        injector: Injector,
        messageService: MessageService,
        private breadcrumbService: BreadcrumbService,
        private _dashBoardService: DashBoardServiceProxy,

        @Inject(PLATFORM_ID) private platformId: Object
        ) {
        super(injector, messageService);
        this.breadcrumbService.setItems([]);
        
        Chart.register(ChartDataLabels);
    }

    refreshTemplate = true;
    subscription: Subscription;

    dataFilter = {
        firstDate: null,
        endDate: null,
        businessCustomerId: null,
        restaurantId: null
    }

    moneyChart: any;
    moneyChartOptions: any;
    widthMoney:any;

    overview = {
        moneyInDay: 0,
        totalMoney: 0,
        orderInDay: 0,
        orderQuantity: 0
    };

    //
    order: any;
    orderOptions: any;
    widthOrder:any;

    //
    businessCustomers: any = [];    
    restaurants: any = [];

    convertPriceDisplay(value: number, decimal?: number) {
        let negativeValue: boolean = false || value < 0, unit: number, unitSymbol: string;
        //
        if(value === 0 || (value > -1000 && value < 1000)) return 0;
        if(value < 0) value = -1*value;
        //
        if(value >= 1000 && value < 1000000) { unit = 1000; unitSymbol = 'K'; } // Đơn vị nghìn
        if(value >= 1000000 && value < 1000000000) { unit = 1000000; unitSymbol = 'M'; } // Đơn vị triệu
        if(value >= 1000000000 && value < 1000000000000) { unit = 1000000000; unitSymbol = 'G'; } // Đơn vị tỷ
        if(value >= 1000000000000) { unit = 1000000000000; unitSymbol = 'T'; } // Đơn vị nghìn tỷ
        //
        let priceConvert: number = (value/unit);
        let priceDisplay: string = ((decimal && !Number.isInteger(priceConvert)) ? (priceConvert.toFixed(decimal)) : Math.floor(priceConvert)) + unitSymbol;
        //
        if(negativeValue) priceDisplay = '-' + priceDisplay;
        //
        return priceDisplay;
    }

    ngOnInit() {
        // Báo cáo thực chi
        this.moneyChart = {
            labels: ['01/01','02/01','03/01','04/01','05/01','06/01','07/01','08/01','09/01','10/01','11/01','12/01'],
            datasets: [
                {
                    label: 'Giá trị',
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    fill: true,
                    backgroundColor: 'rgba(83,70,224,0.2)',
                    pointBackgroundColor: '#5346E0',
                    pointBorderColor: '#5346E0',
                    pointRadius: 3,
                    pointHoverRadius: 5, 
                    borderColor: '#ED589D',
                    borderWidth: 2,
                    tension: 0,
                    datalabels: {
                        display: false
                    }
                },
            ]
        };

        this.moneyChartOptions = {
            maintainAspectRatio: false,
            aspectRatio: 0.6,
            plugins: {
                legend: {
                    display: false
                },
            },
            scales: {
                x: {
                    ticks: {
                        color: '#232154',
                        font: {
                            size: 9,
                            style: 'italic'
                        }
                    },
                    grid: {
                        color: 'white'
                    }
                },
                y: {
                    ticks: {
                        color: '#99A2BC'
                    },
                    grid: {
                        color: '#ebedef'
                    }
                }
            }
        }

        // Danh sách theo kỳ hạn sản phẩm
        this.order = {
            labels: ['01/01','02/01','03/01','04/01','05/01','06/01','07/01','08/01','09/01','10/01','11/01','12/01'],
            datasets: [
                {
                    label: 'Số đơn hàng',
                    backgroundColor: '#6C63F0',
                    borderColor: '#FFFFFF',
                    borderRadius: 4,
                    barThickness: 12,   // Độ rộng của cột #ED589D
                    borderSkipped: false,
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    datalabels: {
                        display: false
                    },
                },
            ]
        };

        this.orderOptions = {
            responsive: true,
            plugins: {
                legend: {
                  display: false
                }
            },
            scales: {
                x: {
                    ticks: {
                        color: '#232154',
                        font: {
                            size: 9,
                            style: 'italic'
                        }
                    },
                    grid: {
                        color: 'white',
                        // padding: 50,
                        beginAtZero: true,
                        min: 0,
                        max: 200,
                    }
                },
                y: {
                    display: true,
                    ticks: {
                        color: '#99A2BC',
                        // padding: 50,
                        // callback: (value, index, ticks) => {
                        //     return this.convertPriceDisplay(value, 1);
                        // }
                    },
                    grid: {
                        color: '#ebedef',
                        // padding: 50,
                        borderDash: [8, 4],
                        drawBorder: false,
                    }
                },
            }
        };
        //
        this.checkLengthMoney();        
        this.checkLengthOrder();
        //
        this.genListBusinessCustomer();
        this.initDate();
    }

    changeBusinessCustomer(businessCustomerId) {
        if(businessCustomerId){
            this.genListRestaurant(businessCustomerId);
        } else {
            this.dataFilter.restaurantId = null;
        }
        this.setPage();
    }


    genListBusinessCustomer(){
        this._dashBoardService.getAllBusinessCustomer().subscribe(res => {
            if(this.handleResponseInterceptor(res)){
                this.businessCustomers = res?.data?.items;
            } 
        })
    }

    genListRestaurant(businessCustomerId?: number){
        this._dashBoardService.getAllRestaurant(businessCustomerId).subscribe(res => {
            if(this.handleResponseInterceptor(res)){
                this.restaurants = res?.data?.items;
            } 
        })
    }

    initDate(){
        const today = moment();
        const thirtyDaysAgo = moment().subtract(30, 'days');
        this.dataFilter.firstDate = thirtyDaysAgo.toDate();
        this.dataFilter.endDate = today.toDate();
        
        this.setPage();
    }

    checkLengthMoney() {
        let checkLengthMoney = this.moneyChart.labels.length;
        if(checkLengthMoney <= 30 ){
            this.widthMoney = "100%";
        }  else {
            this.widthMoney = `${checkLengthMoney*3}rem`;
        }
    }

    checkLengthOrder() {
        let checkLengthOrder = this.order.labels.length;
        if(checkLengthOrder <= 30 ){
            this.widthOrder = "100%";
        }  else {
            this.widthOrder = `${checkLengthOrder*3}rem`;
        }
    }

    updateMoneyChart(data){
        this.moneyChart.datasets[0].data = data.map(element => element.totalValue ?? 0);
        this.moneyChart.labels = data.map(element => this.formatDateMonth(element.date));
        this.checkLengthMoney();
    }

    updateOrderChart(data) {
        this.order.datasets[0].data = data.map(element => element.totalValue);
        this.order.labels = data.map(element => this.formatDateMonth(element.date));
        this.checkLengthOrder();
    };
    
    setPage() {
        this.isLoading = true; 
        this._dashBoardService.getInfoDashBoard(this.dataFilter).subscribe((res) => {
        this.isLoading = false;
            if (this.handleResponseInterceptor(res, '')) {
                this.overview.moneyInDay = res?.data?.moneyInDay;
                this.overview.totalMoney = res?.data?.totalMoney;
                this.overview.orderInDay = res?.data?.orderInDay;
                this.overview.orderQuantity = res?.data?.orderQuantity;
                this.updateMoneyChart(res?.data?.moneyChart);
                this.updateOrderChart(res?.data?.orderChart);
                this.refreshTemplate = false;
                setTimeout(() => this.refreshTemplate = true);
            }
        }, (err) => {
            this.isLoading = false;
            console.log('Error-------', err);
        });
    }
}