import { Component, Inject, Injector } from "@angular/core";
import { ExportExcelConst, MessageErrorConst } from "@shared/AppConsts";
import { AppComponentBase } from "@shared/app-component-base";
import { ReportModel } from "@shared/model/report.model";
import { API_BASE_URL } from "@shared/service-proxies/service-proxies-base";
import { ExportReportService } from "@shared/services/export-report.service";
import { MessageService } from "primeng/api";
import { BreadcrumbService } from "src/app/layout/breadcrumb/breadcrumb.service";

@Component({
	selector: "app-management-report",
	templateUrl: "./management-report.component.html",
	styleUrls: ["./management-report.component.scss"],
})
export class ManagementReportComponent extends AppComponentBase {
	constructor(
		injector: Injector,
		messageService: MessageService,
		private _exportReportService: ExportReportService,
		private breadcrumbService: BreadcrumbService,
		@Inject(API_BASE_URL) baseUrl?: string
	) {
		super(injector, messageService);
		this.breadcrumbService.setItems([
			{ label: "Trang chủ", routerLink: ["/home"] },
			{ label: "Báo cáo quản trị" },
		]);
	}
	
	startDate: any;
	endDate: any;
	type: any = null;
	
	isLoadingPage: boolean = false;
	isDownload: boolean = false;
	permission: any;

	reports: ReportModel[] = [];

	ngOnInit(): void {
		this.isLoadingPage = true;
    this.reports.push({
      name: "BÁO CÁO TỔNG HỢP KHÁCH HÀNG CÁ NHÂN ĐẶT SẢN PHẨM",
      type: ExportExcelConst.TONG_HOP_TIEN_VE_SAN_PHAM,
    });
		
	}

	onAccept() {
		if (this.validForm()) {
			this.isDownload = true;
			this._exportReportService
				.exportExcel(this.startDate, this.endDate, this.type)
				.subscribe((res) => {
					res?.code && this.handleResponseInterceptor(res);
					this.isDownload = false;
				});
		} else {
			this.messageError(MessageErrorConst.message.Validate);
		}
	}

	validForm(): boolean {
		let validRequired;
		validRequired = this.startDate && this.endDate && this.type;
		return validRequired;
	}
}
