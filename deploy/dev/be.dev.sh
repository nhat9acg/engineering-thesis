#!/bin/bash#
# variable BE
FOLDER_PUBLISH="/var/lib/jenkins/workspace/publish"
WEB_SELL_PUBLISH="$FOLDER_PUBLISH/websell/BE"
WEB_SELL_SRC="/var/lib/jenkins/workspace/web-sell-dev/src/BE/STE.WebSell"

# set permission
#cd /var/lib/jenkins/workspace/web-sell-dev.git
#sudo chown -R jenkins:jenkins *

cd /var/lib/jenkins/workspace/web-sell-dev
sudo chown -R jenkins:jenkins *

# kill all dotnet app
echo === Kill dotnet app ===
#killall -9 dotnet
ps -u jenkins -o user,pid,ppid,%cpu,%mem,cmd | grep 'dotnet.*BE/STE.WebSell.API.dll$'| awk '{print $2}' | xargs kill

test -d "$WEB_SELL_PUBLISH" || mkdir -p "$WEB_SELL_PUBLISH"

cd $FOLDER_PUBLISH
sudo chown -R jenkins:jenkins *

# set enviroment variable
export ASPNETCORE_ENVIRONMENT=Staging
export BUILD_ID=dontKillMe

dll="STE.WebSell.API.dll"
cd "$WEB_SELL_SRC"
echo === Publish $dll ===
dotnet publish -c Release -o "$WEB_SELL_PUBLISH"
cd $WEB_SELL_PUBLISH
nohup dotnet "$WEB_SELL_PUBLISH/$dll" > "$WEB_SELL_PUBLISH/dotnetcore.log" &