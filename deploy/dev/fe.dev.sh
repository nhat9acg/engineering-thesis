#!/bin/bash#
# variable BE
FOLDER_PUBLISH="/var/lib/jenkins/workspace/publish"
WEB_SELL_PUBLISH="/var/lib/jenkins/workspace/web-sell-dev/src/FE"
WEB_SELL_SRC="/var/lib/jenkins/workspace/web-sell-dev/src/FE"

# set permission
#cd /var/lib/jenkins/workspace/web-sell-dev.git
#sudo chown -R jenkins:jenkins *

cd /var/lib/jenkins/workspace/web-sell-dev
sudo chown -R jenkins:jenkins *

test -d "$WEB_SELL_PUBLISH" || mkdir -p $WEB_SELL_PUBLISH

# INSTALL
cd $WEB_SELL_SRC
yarn install

# REMOVE DIST
rm -rf "$WEB_SELL_PUBLISH/dist"

# BUILD
pwd
#sass src/assets && ng build --configuration=staging --output-path $WEB_SELL_PUBLISH
npm run build:staging